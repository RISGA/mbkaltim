package sgo.mobile.bankkaltim;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;
import com.securepreferences.SecurePreferences;

public class AppHelper {
	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	public static Point screenSize(Context ctx)
	{
		WindowManager wm = (WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		Point size = new Point();
        if (android.os.Build.VERSION.SDK_INT >= 13) {
        	display.getSize(size);
        } else {
        	size.x = display.getWidth();
        	size.y = display.getHeight();
        }
        return size;
	}

    public static String getUserId(Context context){
        SecurePreferences sp = new SecurePreferences(context);
        return sp.getString("user_id", "");
    }

    public static String getUserName(Context context){
        SecurePreferences sp = new SecurePreferences(context);
        return sp.getString("userFullname", "");
    }

    public static String getUserEmail(Context context){
        SecurePreferences sp = new SecurePreferences(context);
        return sp.getString("userEmail", "");
    }

    public static String getMobilePhone(Context context){
        SecurePreferences sp = new SecurePreferences(context);
        return sp.getString("userMobilePhone", "");
    }

    public static String getAccessFlag(Context context){
        SecurePreferences sp = new SecurePreferences(context);
        return sp.getString("accesflag", "");
    }

    public static String getUserFullName(Context context){
        SecurePreferences sp = new SecurePreferences(context);
        return sp.getString("userFullname", "");
    }

    public static String getAccessKey(Context context){
       // keys = AppHelper.getAccessKey(keys);
        SecurePreferences sp = new SecurePreferences(context);
        return sp.getString("access_key", "");
    }

    public static String getUserLang(Context context){
        // keys = AppHelper.getAccessKey(keys);
        SecurePreferences sp = new SecurePreferences(context);
        return sp.getString("userLang", "");
    }

    public static String getLastLogin(Context context){
        // keys = AppHelper.getAccessKey(keys);
        SecurePreferences sp = new SecurePreferences(context);
        return sp.getString("userLastLogin", "");
    }

    public static String getSourceAccount(Context context){
        // keys = AppHelper.getAccessKey(keys);
        SecurePreferences sp = new SecurePreferences(context);
        return sp.getString("sourceAccountData", "");
    }

    public static String getDestAccount(Context context){
        // keys = AppHelper.getAccessKey(keys);
        SecurePreferences sp = new SecurePreferences(context);
        return sp.getString("destAccountData", "");
    }

    public static String getuserChangePassword(Context context){
        // keys = AppHelper.getAccessKey(keys);
        SecurePreferences sp = new SecurePreferences(context);
        return sp.getString("userChangePassword", "");
    }





   /* public static String getRef(Context context){
        return context.getSharedPreferences("userinfo", Context.MODE_PRIVATE).getString("ref", "");
    }

    public static String getCustomerId(Context context){
        return context.getSharedPreferences("userinfo", Context.MODE_PRIVATE).getString("cust_id", "");
    }

    public static String getUrlSgoPlus(Context context){
        return context.getSharedPreferences("userinfo", Context.MODE_PRIVATE).getString("rel_sgo_plus", "");
    }*/
}


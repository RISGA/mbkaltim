package sgo.mobile.bankkaltim.conf;

public class AplConstants {
    public static final int HTTP_DEFAULT_TIMEOUT = 70 * 1000;

    public static final String API ="https://mb.bankaltim.co.id/bankkaltim";
    public static final String SignMobileAPI  = API+"/ServiceLogin/SignIn";
    public static final String SignOutMobileAPI  = API+"/ServiceLogout/SignOut";
    public static final String SourceAccountListAPI  = API+"/SourceAccountList/Retrieve";
    public static final String DestinationAccountListAPI  = API+"/DestAccountList/Retrieve";
    public static final String AccountListAPI  = API+"/BalanceAccountList/Retrieve";
    public static final String FundTransferAPI  = API+"/ServiceFundTransInHouse/Invoke";
    public static final String FundDomesticTransferAPI  =API+"/ServiceFundTransDomestic/Invoke";
    public static final String InquiryAccountAPI  = API+"/InquiryAccount/Invoke";
    public static final String AccountStatementAPI  = API+"/AccountStatement/Retrieve";
    public static final String RequestTokenAPI =API+"/RequestToken/Invoke";
    public static final String ConfirmTokenAPI  =API+"/ConfirmToken/Invoke";
    public static final String BankCodeAPI =API+"/BankList/Retrieve";
    public static final String AddBeneficiaryAPI =API+"/ServiceAcctDest/Invoke";
    public static final String FundTransferInquiryAPI =API+"/ServiceFundTransferInquiry/Invoke";
    public static final String InquiryBeneficiaryAPI =API+"/InquiryBenef/Invoke";
    public static final String ListMenuBiller = API+"/MenuBiller/Retrieve";
    public static final String InquiryBiller = API+"/InquiryBiller/Invoke";
    public static final String InquiryBillerCatalog = API+"/InquiryBillerCatalog/Invoke";
    public static final String InquiryBillerPayment = API+"/InquiryBillerPayment/Invoke";
    public static final String BillerPaymentNotif = API+"/BillerPaymentNotif/Invoke";
    public static final String ListMenuSettingMobileAPI = API+"/ServiceMenuSetting/GetAllMenuSetting";
    public static final String TrxInquiryIBMobileAPI = API+"/TrxInquiry/Retrieve";
    public static final String TrxSummaryIBMobileAPI = API+"/TrxInquiry/Summary";
    public static final String BillerPaymentType = API+"/BillerPaymentType/Retrieve";
    public static final String BillerPaymentReport = API+"/BillerPaymentReport/Retrieve";
    public static final String DeleteBeneficiary = API+"/DelAcctDest/Invoke";
    public static final String ChangePassword = API+"/ChangePass/Submit";
    public static final String ChangeLanguage = API+"/ServiceLang/Invoke";
    public static final String CheckVersion = API+"/ServiceApp/getAppVersion";

    public static final int HTTP_TIMEOUT = 20 * 1000;
    public static final String Bankcode ="124";
    public static final String pcd = "qwe123";
    public static final String Type = "1";
    public static final String TypeInhouse = "1";
    public static final String TypeBeneficiary = "8";
    public static final String CurrencyIDR = "IDR";
    public static final String executetransferProcess = "1";
    public static final String executetransferValidate = "2";
    public static final String billTypeBuy = "BUY";
    public static final String billTypeBil = "BIL";
    public static final String appName= "BANKKALTIM";
    /// List of Biller Code //
    public static final String billElectricity = "1";
    public static final String billwater = "2";
    public static final String billPhone = "3";
    public static final String billInternet = "4";
    public static final String billCableTV = "5";
    public static final String billTax = "6";
    public static final String billCreditCard = "7";
    public static final String billInstallment = "8";
    public static final String billIInsurance = "9";
    public static final String billIEducation = "10";
    public static final String billITicket = "11";
    public static final String billOthers = "12";

    /// List Purchase Code ///f
    public static final String purchaseElectricityVoucher = "13";
    public static final String purchasePhoneVoucher= "14";
    public static final String purchaseOthers= "15";
    public AplConstants(){}
}

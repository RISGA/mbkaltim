package sgo.mobile.bankkaltim.conf;

import android.app.Application;
import com.activeandroid.ActiveAndroid;
import com.activeandroid.Configuration;
import sgo.mobile.bankkaltim.app.models.*;
import sgo.mobile.bankkaltim.app.models.payment.*;
import sgo.mobile.bankkaltim.app.models.purchase.*;

/**
 * Created by MOBILEDEV on 3/29/2016.
 */
public class CoreApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Configuration.Builder configbuilder = new Configuration.Builder(getApplicationContext());
        configbuilder.addModelClasses(
                SourceAccountModel.class,
                DestinationAccountListModel.class,
                BankListModel.class,
                MenuBillerModelPayment.class,
                MenuBillerModelPurchase.class,
                PaymentElectricityModel.class,
                PaymentInstallmentModel.class,
                PaymentInsuranceModel.class,
                PaymentInternetModel.class,
                PaymentPamModel.class,
                PaymentPhoneModel.class,
                PaymentTaxModel.class,
                PaymentTicketModel.class,
                PaymentTvCableModel.class,
                PurchaseVoucherPhoneModel.class,
                PurchaseCatalogPhoneModel.class,
                PurchaseNominalPhoneModel.class,
                PurchaseCatalogElectricityModel.class,
                PurchaseNominalElectricityModel.class,
                PurchaseVoucherKartuAsModel.class,
                PurchaseVoucherBoltModel.class,
                PurchaseVoucherEsiaModel.class,
                PurchaseVoucherSimpatiModel.class,
                PurchaseVoucherIm3SmartModel.class,
                PurchaseVoucherXlModel.class,
                PurchaseVoucherThreeModel.class,
                PurchaseVoucherMentariModel.class,
                PurchaseVoucherSmartfrenModel.class,
                MenuSettingModel.class


        );

        ActiveAndroid.initialize(configbuilder.create());

    }


}

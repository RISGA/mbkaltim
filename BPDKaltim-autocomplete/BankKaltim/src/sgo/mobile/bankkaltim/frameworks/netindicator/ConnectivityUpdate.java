package sgo.mobile.bankkaltim.frameworks.netindicator;

/**
 * Created by SGO-Mobile-DEV on 3/19/2015.
 */
 /*public class ConnectivityUpdate extends BroadcastReceiver {
    private static final int MY_NOTIFICATION_ID = 100;

    private static final String LOG_TAG = "ZBroadcastReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {

        // Wifi = ConnectivityManager.TYPE_WIFI
        // Mobile =  ConnectivityManager.TYPE_MOBILE
        // For mobile: netSubtype == TelephonyManager.NETWORK_TYPE_UMTS --> 3G
        // For mobile: !mTelephony.isNetworkRoaming())

        boolean hasInternet = false;

        Log.d(LOG_TAG, "Received broadcast!");

        if(intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
            hasInternet = (new NDUtils()).hasInternet(context);
        }

        SharedPrefsManager prefs = new SharedPrefsManager();
        boolean prevValue = prefs.hasNetwork(context);

        if(prevValue != hasInternet){
            (new SharedPrefsManager()).saveNetworkState(context, hasInternet);
            sendNotification(hasInternet, context);
        }
    }

    private void sendNotification(boolean hasInternet, Context context) {
        NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);


        String notificationTitle = "Change in data connection!";
        String notificationText = "We have internet?: " + (hasInternet ? "YES!" : "NO :(");
        Notification myNotification = new Notification(R.drawable.ic_launcher, "Broadcast received!", System.currentTimeMillis());


        Intent myIntent = new Intent(context.getApplicationContext(), MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context.getApplicationContext(), 0, myIntent, FLAG_ACTIVITY_SINGLE_TOP);

        myNotification.setLatestEventInfo(context,
                notificationTitle,
                notificationText,
                pendingIntent);
        notificationManager.notify(MY_NOTIFICATION_ID, myNotification);

    }

}
*/
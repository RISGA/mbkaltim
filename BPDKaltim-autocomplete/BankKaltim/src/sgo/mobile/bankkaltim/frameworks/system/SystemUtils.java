package sgo.mobile.bankkaltim.frameworks.system;

import android.os.Build;

public class SystemUtils {

    /** recommend default thread pool size according to system available processors, {@link #getDefaultThreadPoolSize()} **/
    public static final int DEFAULT_THREAD_POOL_SIZE = getDefaultThreadPoolSize();

    /**
     * get recommend default thread pool size
     *
     * @return if 2 * availableProcessors + 1 less than 8, return it, else return 8;
     * @see {@link #getDefaultThreadPoolSize(int)} max is 8
     */
    public static int getDefaultThreadPoolSize() {
        return getDefaultThreadPoolSize(8);
    }

    /**
     * get recommend default thread pool size
     *
     * @param max
     * @return if 2 * availableProcessors + 1 less than max, return it, else return max;
     */
    public static int getDefaultThreadPoolSize(int max) {
        int availableProcessors = 2 * Runtime.getRuntime().availableProcessors() + 1;
        return availableProcessors > max ? max : availableProcessors;
    }

    /*
 * returns true if device is running Android 4.0 (ICS) or later
 */
    public static boolean isGteAndroid4() {
        return (Build.VERSION.SDK_INT >= 15);
    }

    /*
     *  returns true if device is running Android 4.1 (JellyBean) or later
     */
    public static boolean isGteAndroid41() {
        return (Build.VERSION.SDK_INT >= 16);
    }

    /*
     * returns true if device is running Android 4.2 or later
     */
    public static boolean isGteAndroid42() {
        return (Build.VERSION.SDK_INT >= 17);
    }

    /*
     * returns true on API 11 and above - called to determine whether
     * AsyncTask.executeOnExecutor() can be used
     */
    public static boolean canUseExecuteOnExecutor() {
        return (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB);
    }
}

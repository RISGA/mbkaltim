package sgo.mobile.bankkaltim.frameworks.time;


import sgo.mobile.bankkaltim.frameworks.math.Flags;
import sgo.mobile.bankkaltim.frameworks.math.Functions;
import sgo.mobile.bankkaltim.frameworks.text.StringUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.Vector;

/**
 * The <code>DateUtil</code> class contains methods to format date.
 *
 * @author Diki Irawan
 * @version 2013-10-31
 */

public class DateUtil {
    private static final String[] DAY_OF_WEEK = {
            "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
    private static final String[] MONTH = {
            "Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    public static final long SECOND = 1000;
    public static final long MINUTE = 60000;
    public static final long HOUR = 3600000;
    public static final long DAY = 86400000;
    public static final long WEEK = 6048000000L;

    public static final int FLAG_SECOND = 1;
    public static final int FLAG_MINUTE = 2;
    public static final int FLAG_HOUR = 4;
    public static final int FLAG_DAY = 8;
    public static final int FLAG_WEEK = 16;

    public static final long MICROSECONDS_IN_A_MILLISECOND = 1000L;
    public static final long MICROSECONDS_IN_A_SECOND = 1000000L;
    public static final long MILLISECONDS_IN_A_SECOND = 1000L;
    public static final int SECONDS_IN_A_MINUTE = 60;
    public static final int MINUTES_IN_AN_HOUR = 60;
    public static final long HOURS_IN_A_DAY = 24;
    public static final int MONTHS_IN_A_YEAR = 12;

    private static final String TEXT_SECOND = "sec";
    private static final String TEXT_MINUTE = "min";
    private static final String TEXT_HOUR = "hour";
    private static final String TEXT_DAY = "day";
    private static final String TEXT_WEEK = "week";

    private static final int INDEX_WEEK = 0;
    private static final int INDEX_DAY = 1;
    private static final int INDEX_HOUR = 2;
    private static final int INDEX_MINUTE = 3;
    private static final int INDEX_SECOND = 4;


    /**
     * Parse RSS date format to Date object.
     * Example of RSS date:
     * Sat, 23 Sep 2006 22:25:11 +0000
     * @param dateString the string to format.
     * @return the formatted <code>Date</code>.
     */
    public static Date parseDate(String dateString) {
        Date pubDate = null;
        try {
            // Split date string to values
            // 0 = week day
            // 1 = day of month
            // 2 = month
            // 3 = year (could be with either 4 or 2 digits)
            // 4 = time
            // 5 = GMT
            int dayOfMonthIndex = 2;
            int monthIndex = 1;
            int yearIndex = 5;
            int timeIndex = 3;

            String[] values = StringUtils.split(dateString, " ");
            int columnCount = values.length;
            // Wed Aug 29 20:14:27 +0000 2007

            if( columnCount==5 ) {
                // Expected format:
                // 09 Nov 2006 23:18:49 EST
                dayOfMonthIndex = 0;
                monthIndex = 1;
                yearIndex = 2;
                timeIndex = 3;
//                gmtIndex = 4;
            } else if( columnCount==7 ) {
                // Expected format:
                // Thu, 19 Jul  2007 00:00:00 N
                yearIndex = 4;
                timeIndex = 5;
            } else if( columnCount<5 || columnCount>6 ) {
                throw new Exception("Invalid date format: " + dateString);
            }

            // Day of month
            int dayOfMonth = Integer.parseInt( values[ dayOfMonthIndex ] );

            // Month
            String[] months =  {
                    "Jan", "Feb", "Mar", "Apr", "May", "Jun",
                    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
            String monthString = values[ monthIndex ];
            int month=0;
            for(int monthEnumIndex=0; monthEnumIndex<12; monthEnumIndex++) {
                if( monthString.equals( months[ monthEnumIndex ] )) {
                    month = monthEnumIndex;
                }
            }

            // Year
            int year = Integer.parseInt(values[ yearIndex ]);
            if(year<100) {
                year += 2000;
            }

            // Time
            String[] timeValues = StringUtils.split(values[timeIndex], ":");
            int hours = Integer.parseInt( timeValues[0] );
            int minutes = Integer.parseInt( timeValues[1] );
            int seconds = Integer.parseInt( timeValues[2] );

            pubDate = getCal(dayOfMonth, month, year, hours, minutes, seconds);

        } catch(Exception ex) {
            // TODO: Add exception handling code
            System.err.println("parseRssDate error while converting date string to object: " +
                    dateString + "," + ex.toString());
        } catch(Throwable t) {
            // TODO: Add exception handling code
            System.err.println("parseRssDate error while converting date string to object: " +
                    dateString + "," + t.toString());
        }
        return pubDate;
    } // end method parseDate

    /** Get calendar date.
     * @param dayOfMonth the day of month.
     * @param month the month.
     * @param year the year.
     * @param hours the hours.
     * @param minutes the minutes.
     * @param seconds the seconds.
     * @return <code>Calendar</code> object representing the date.
     * @throws Exception if the specified date is invalid.
     */
    public static Date getCal(int dayOfMonth, int month, int year, int hours,
                              int minutes, int seconds) throws Exception {
        // Create calendar object from date values
        Calendar cal = Calendar.getInstance();
        cal.setTimeZone( TimeZone.getTimeZone("GMT+0") );
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.HOUR_OF_DAY, hours);
        cal.set(Calendar.MINUTE, minutes);
        cal.set(Calendar.SECOND, seconds);

        return cal.getTime();
    } // end method getCal

    /**
     * Format a HTTP <code>Date</code> object.
     *
     * @param date the <code>Date</code> to format.
     * @return the formatted date.
     */
    public static String formatHTTPDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeZone( TimeZone.getTimeZone("GMT+0") );
        cal.setTime(date);
        return
                DAY_OF_WEEK[cal.get(Calendar.DAY_OF_WEEK)-1]+", "+
                        cal.get(Calendar.DAY_OF_MONTH)+" "+
                        MONTH[cal.get(Calendar.MONTH)]+" "+
                        cal.get(Calendar.YEAR)+" "+
                        cal.get(Calendar.HOUR_OF_DAY)+":"+
                        cal.get(Calendar.MINUTE)+":"+
                        cal.get(Calendar.SECOND)+" GMT";
        //Tue%2C+27+Mar+2007+22%3A55%3A48+GMT
    } // end method formatHTTPDate


    /**
     * Returns a human-readable String describing the totalAmount of
     * time represented by the given number of seconds.
     *
     * @param seconds  number of seconds.
     * @return  human-readable String describing the totalAmount of time represented by the given parameter (e.g., "2 hours 3mins 10secs".
     */
    public static String getTimeDuration( int seconds ) {

        long milliseconds = 1000 * seconds;
        return getTimeDuration( milliseconds );
    }

    /**
     * Returns a human-readable String describing the totalAmount of
     * time represented by the given number of milliseconds.
     *
     * @param milliseconds  number of milliseconds.
     * @return human-readable String describing the totalAmount of time represented by the given parameter (e.g., "2hours 3mins 10secs".
     */
    public static String getTimeDuration( long milliseconds ) {

        return getTimeDuration( milliseconds, FLAG_WEEK | FLAG_DAY | FLAG_HOUR | FLAG_MINUTE | FLAG_SECOND );
    }

    /**
     * Returns a human-readable String describing the totalAmount of time
     * represented by the given number of milliseconds.
     *
     * @param milliseconds Number of milliseconds
     * @param mask Mask
     * @return human-readable String describing the totalAmount of time
     */
    public static String getTimeDuration( long milliseconds, int mask ) {

        // display week, day, hour, minute and second
        long[] unitOrder = { WEEK, DAY, HOUR, MINUTE, SECOND };
        return getTimeDuration( milliseconds, mask, unitOrder );
    }

    /**
     * Returns a human-readable String describing the totalAmount of
     * time represented by the given number of milliseconds.
     *
     * @param milliseconds  number of milliseconds
     * @param mask
     * @param unitOrder order units should be printed eg. xhrs xmins xsecs would be { HOUR, MINUTE, SECOND }
     * @return human-readable String describing the totalAmount of time represented by the given parameter (e.g., "2hours 3mins 10secs".
     */
    public static String getTimeDuration( long milliseconds, int mask, long[] unitOrder ) {

        StringBuffer stringBuffer = new StringBuffer();

        boolean lessThan = true;
        lessThan = getLessThanText( stringBuffer, milliseconds, SECOND, mask, lessThan );
        lessThan = getLessThanText( stringBuffer, milliseconds, MINUTE, mask, lessThan );
        lessThan = getLessThanText( stringBuffer, milliseconds, HOUR, mask, lessThan );
        lessThan = getLessThanText( stringBuffer, milliseconds, DAY, mask, lessThan );
        lessThan = getLessThanText( stringBuffer, milliseconds, WEEK, mask, lessThan );

        if ( stringBuffer.length() > 0 ) {

            stringBuffer.insert( 0, "less than 1 " );
        }
        else {

            Vector<String> timeDurationStrings = new Vector<String>();

            milliseconds = getTimeDuration( timeDurationStrings, milliseconds, WEEK, mask );
            milliseconds = getTimeDuration( timeDurationStrings, milliseconds, DAY, mask );
            milliseconds = getTimeDuration( timeDurationStrings, milliseconds, HOUR, mask );
            milliseconds = getTimeDuration( timeDurationStrings, milliseconds, MINUTE, mask );
            milliseconds = getTimeDuration( timeDurationStrings, milliseconds, SECOND, mask );

            for (int i = 0; i < unitOrder.length; i++) {

                int index = getIndexForUnit( unitOrder[i] );

                if ( index != -1 ) {

                    String timeDuration = timeDurationStrings.elementAt( index );

                    if ( timeDuration != null ) {

                        if ( stringBuffer.length() > 0 ) {

                            stringBuffer.append( " " );
                        }

                        stringBuffer.append( timeDuration );
                    }
                }
            }
        }

        return stringBuffer.toString();
    }

    private static boolean getLessThanText( StringBuffer stringBuffer, long milliseconds, long unit, int mask, boolean lessThan ) {

        int flag = getFlagForUnit( unit );

        if ( lessThan && Flags.isSet(flag, mask) ) {

            if ( milliseconds < unit ) {

                if ( stringBuffer.length() == 0 && Flags.isSet( flag, mask ) ) {

                    stringBuffer.append( getTextForUnit( unit ) );
                }
            }
            else {

                lessThan = false;
            }
        }

        return lessThan;
    }

    private static long getTimeDuration( Vector<String> timeDurationStrings, long milliseconds, long unit, int mask ) {

        int flag = getFlagForUnit( unit );
        if ( milliseconds >= unit && Flags.isSet( flag, mask ) ) {

            StringBuffer stringBuffer = new StringBuffer();

            int amount = (int) Math.floor( Functions.doubleDivision(milliseconds, unit) );
            milliseconds -= ( unit * amount );

            stringBuffer.append( amount );
            stringBuffer.append( " " );
            stringBuffer.append( getTextForUnit( unit ) );
            stringBuffer.append( ( amount > 1 ) ? "s" : "" );

            timeDurationStrings.addElement( stringBuffer.toString() );
        }
        else {

            timeDurationStrings.addElement( null );
        }

        return milliseconds;
    }

    private static final int getFlagForUnit( long unit ) {

        if ( unit == SECOND ) {

            return FLAG_SECOND;
        }
        else if ( unit == MINUTE ) {

            return FLAG_MINUTE;
        }
        else if ( unit == HOUR ) {

            return FLAG_HOUR;
        }
        else if ( unit == DAY ) {

            return FLAG_DAY;
        }
        else if ( unit == WEEK ) {

            return FLAG_WEEK;
        }
        else {

            return 0;
        }
    }

    private static final String getTextForUnit( long unit ) {

        if ( unit == SECOND ) {

            return TEXT_SECOND;
        }
        else if ( unit == MINUTE ) {

            return TEXT_MINUTE;
        }
        else if ( unit == HOUR ) {

            return TEXT_HOUR;
        }
        else if ( unit == DAY ) {

            return TEXT_DAY;
        }
        else if ( unit == WEEK ) {

            return TEXT_WEEK;
        }
        else {

            return "";
        }
    }

    private static final int getIndexForUnit( long unit ) {

        if ( unit == SECOND ) {

            return INDEX_SECOND;
        }
        else if ( unit == MINUTE ) {

            return INDEX_MINUTE;
        }
        else if ( unit == HOUR ) {

            return INDEX_HOUR;
        }
        else if ( unit == DAY ) {

            return INDEX_DAY;
        }
        else if ( unit == WEEK ) {

            return INDEX_WEEK;
        }
        else {

            return -1;
        }
    }

    /**
     * To the second, not millisecond like reddit
     * @param timeSeconds
     * @return
     */
    public static String getTimeAgo(long utcTimeSeconds) {
        long systime = System.currentTimeMillis() / 1000;
        long diff = systime - utcTimeSeconds;
        if (diff <= 0)
            return "very recently";
        else if (diff < 60) {
            if (diff == 1)
                return "1 second ago";
            else
                return diff + " seconds ago";
        }
        else if (diff < 3600) {
            if ((diff / 60) == 1)
                return "1 minute ago";
            else
                return (diff / 60) + " minutes ago";
        }
        else if (diff < 86400) { // 86400 seconds in a day
            if ((diff / 3600) == 1)
                return "1 hour ago";
            else
                return (diff / 3600) + " hours ago";
        }
        else if (diff < 604800) { // 86400 * 7
            if ((diff / 86400) == 1)
                return "1 day ago";
            else
                return (diff / 86400) + " days ago";
        }
        else if (diff < 2592000) { // 86400 * 30
            if ((diff / 604800) == 1)
                return "1 week ago";
            else
                return (diff / 604800) + " weeks ago";
        }
        else if (diff < 31536000) { // 86400 * 365
            if ((diff / 2592000) == 1)
                return "1 month ago";
            else
                return (diff / 2592000) + " months ago";
        }
        else {
            if ((diff / 31536000) == 1)
                return "1 year ago";
            else
                return (diff / 31536000) + " years ago";
        }
    }

    public static String getTimeAgo(double utcTimeSeconds) {
        return getTimeAgo((long)utcTimeSeconds);
    }

    /**
     *
     * @param longDate
     * @return date string in format yyyy-MM-dd
     */
    public static String toDateSting_YYYY_MM_DD(int year, int month, int dayOfMonth) {
        StringBuffer result = new StringBuffer();
        // year
        result.append(year);
        // month
        result.append("-");
        month++;
        if(month < 10 ) {
            result.append("0");
        }
        result.append(month);
        // day of month
        result.append("-");
        if(dayOfMonth < 10) {
            result.append("0");
        }
        result.append(dayOfMonth);

        return result.toString();
    }

    /**
     *
     * @param year
     * @param month
     * @param dayOfMonth
     * @return Date string in format dd.MM.yyy
     */
    public static String toDateString(int year, int month, int dayOfMonth) {

        StringBuffer result = new StringBuffer();
        // day of month
        if(dayOfMonth < 10) {
            result.append("0");
        }
        result.append(dayOfMonth);
        // month
        result.append(".");
        month++;
        if(month < 10 ) {
            result.append("0");
        }
        result.append(month);
        // year
        result.append(".").append(year);

        return result.toString();
    }

    /**
     *
     * @param time date and time in format yyyyMMddThhmm
     * @return time string
     */
    public static String parseTime(String time) {
        int indexOfT = time.indexOf("T");
        if(indexOfT != -1) {
            StringBuffer result = new StringBuffer(time.substring(indexOfT + 1, indexOfT + 5));
            result.insert(2, ":");
            return result.toString();
        }
        return "";
    }

    /**
     *
     * @param time - string in format hhmm or hmm
     * @return time string in format hh:mm
     */
    public static String convertTimeToNormalForm(String time) {
        boolean hasDelitel = (time.indexOf(":") != -1);
        StringBuffer result = new StringBuffer(time);
        if(time.length() < 4 || (time.length() == 4 && hasDelitel)) {
            result.insert(0, "0");
        }
        if(result.length() == 4) {
            result.insert(2, ":");
        }
        return result.toString();
    }


    /*
    * returns the current UTC date
    */
    public static Date nowUTC() {
        Date dateTimeNow = new Date();
        return localDateToUTC(dateTimeNow);
    }

    public static Date localDateToUTC(Date dtLocal) {
        if (dtLocal==null)
            return null;
        TimeZone tz = TimeZone.getDefault();
        int currentOffsetFromUTC = tz.getRawOffset() + (tz.inDaylightTime(dtLocal) ? tz.getDSTSavings() : 0);
        return new Date(dtLocal.getTime() - currentOffsetFromUTC);
    }



} // end class DateUtil

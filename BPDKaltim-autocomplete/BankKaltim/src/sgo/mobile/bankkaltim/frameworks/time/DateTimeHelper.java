package sgo.mobile.bankkaltim.frameworks.time;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Utility class for parsing and formatting dates and times.
 *
 * @author Diki Irawan
 */

public final class DateTimeHelper {

    private static final SimpleDateFormat DATE_FORMATTER =
            new SimpleDateFormat("yyyy-MM-dd");

    private static final SimpleDateFormat TIME_FORMATTER =
            new SimpleDateFormat("hh:mm a");

    private static final SimpleDateFormat VERBOSE_LOCALE_DATE_FORMATTER =
            new SimpleDateFormat("MMM dd',' yyyy");

    private static final SimpleDateFormat VERBOSE_LOCALE_TIME_FORMATTER =
            new SimpleDateFormat("hh:mm a z");

    private static final SimpleDateFormat VERBOSE_LOCALE_DATE_TIME_FORMATTER =
            new SimpleDateFormat("MMM  dd',' yyyy hh:mm a z");

    private static final SimpleDateFormat ISO_8601_FORMATTER = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SS");
    private static final SimpleDateFormat ALMOST_ISO_8601_FORMATTER = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS");

    private static final long SECONDS_PER_MINUTE = 60;
    private static final long SECONDS_PER_HOUR = 3600;
    private static final long SECONDS_PER_DAY = 86400;

    static final SimpleDateFormat sDateFormat = new SimpleDateFormat("MMM d");

    public static String agoString(Date date) {
        long currentTime = System.currentTimeMillis();

        long diff = (currentTime - date.getTime()) / 1000;

        if(diff < SECONDS_PER_MINUTE) {
            return "less than 1 minute ago";
        } else if(diff < SECONDS_PER_HOUR) {
            long minutes = diff / SECONDS_PER_MINUTE;
            String plural = (minutes > 1) ? "s" : "";
            return minutes + " minute" + plural + " ago";
        } else if(diff < SECONDS_PER_DAY) {
            long hours = diff / SECONDS_PER_HOUR;
            String plural = (hours > 1) ? "s" : "";
            return hours + " hour" + plural + " ago";
        } else {
            return sDateFormat.format(date);
        }
    }

    /**
     * To the second, not millisecond like reddit
     * @param timeSeconds
     * @return
     */
    public static String getTimeAgo(long utcTimeSeconds) {
        long systime = System.currentTimeMillis() / 1000;
        long diff = systime - utcTimeSeconds;
        if (diff <= 0)
            return "very recently";
        else if (diff < 60) {
            if (diff == 1)
                return "1 second ago";
            else
                return diff + " seconds ago";
        }
        else if (diff < 3600) {
            if ((diff / 60) == 1)
                return "1 minute ago";
            else
                return (diff / 60) + " minutes ago";
        }
        else if (diff < 86400) { // 86400 seconds in a day
            if ((diff / 3600) == 1)
                return "1 hour ago";
            else
                return (diff / 3600) + " hours ago";
        }
        else if (diff < 604800) { // 86400 * 7
            if ((diff / 86400) == 1)
                return "1 day ago";
            else
                return (diff / 86400) + " days ago";
        }
        else if (diff < 2592000) { // 86400 * 30
            if ((diff / 604800) == 1)
                return "1 week ago";
            else
                return (diff / 604800) + " weeks ago";
        }
        else if (diff < 31536000) { // 86400 * 365
            if ((diff / 2592000) == 1)
                return "1 month ago";
            else
                return (diff / 2592000) + " months ago";
        }
        else {
            if ((diff / 31536000) == 1)
                return "1 year ago";
            else
                return (diff / 31536000) + " years ago";
        }
    }

    public static String getTimeAgo(double utcTimeSeconds) {
        return getTimeAgo((long)utcTimeSeconds);
    }

    public static Date parseAlmostISO8601DateTimeWithTSeparator(String datetime) {
        //Log.d(TAG, "datetime="+ datetime);
        try {
            return ALMOST_ISO_8601_FORMATTER.parse(datetime);
        } catch (ParseException e) {
            //Log.e(TAG, "caught ParseException", e);
            return null;
        }
    }

    /**
     * Parses an ISO8601 formatted datetime and returns a
     * java.util.Date object for it, or NULL if parsing
     * the date fails.
     *
     * @param String datetime
     * @return Date|null
     */
    public static Date parseFromISO8601(String datetime) {
        //Log.d(TAG, "datetime="+ datetime);
        try {
            return ISO_8601_FORMATTER.parse(datetime);
        } catch (ParseException e) {
            //Log.e(TAG, "caught ParseException", e);
            return null;
        }
    }

    public static String formatToIS08601(Date date) {
        return ISO_8601_FORMATTER.format(date);
    }

    public static Date parseOnlyDate(String date) {
        //Log.d(TAG, "date="+ date);
        try {
            return DATE_FORMATTER.parse(date);
        } catch (ParseException e) {
            //Log.e(TAG, "caught ParseException", e);
            return null;
        }
    }

    /**
     * Tries to parse most date times that it is passed, using
     * some heuristics.
     *
     * @param string
     * @return
     */
    public static Date parseDateTime(String datetime) {
        if (datetime.contains("T")) {
            //Log.d(TAG, "parseDateTime(): Trying ISO8601 with a T separator");
            return parseAlmostISO8601DateTimeWithTSeparator(datetime);
        } else if (datetime.length() == 10 && datetime.contains("-")) {
            //Log.d(TAG, "parseDateTime(): Trying just yyyy-MM-dd date");
            return parseOnlyDate(datetime);
        } else {
            //Log.d(TAG, "parseDateTime(): Trying regular ISO8601");
            return parseFromISO8601(datetime);
        }
    }

    public static Date parseTime(String time) {
        try {
            return TIME_FORMATTER.parse(time);
        } catch (ParseException e) {
            //Log.d(TAG, "parseTime() caught ParseException", e);
            e.printStackTrace();
            return null;
        }
    }

    public static Date parseDate(String date) {
        try {
            return DATE_FORMATTER.parse(date);
        } catch (ParseException e) {
            //Log.d(TAG, "parseDate() caught ParseException", e);
            e.printStackTrace();
            return null;
        }
    }

    public static Date parseDateAndTime(String strDate, String strTime) {
        Date date = parseDate(strDate);
        Date time = parseTime(strTime);
        long dateMillis = date.getTime();
        long timeMillis = time.getTime();
        return new Date(dateMillis + timeMillis);
    }

    public static String pad(int value) {
        if (value < 10) {
            return "0" + value;
        } else {
            return "" + value;
        }
    }

    public static String toLocaleDateTime(Date date) {
        return VERBOSE_LOCALE_DATE_TIME_FORMATTER.format(date);
    }

    public static String toLocaleDate(Date date) {
        return VERBOSE_LOCALE_DATE_FORMATTER.format(date);
    }

    public static String toLocaleTime(Date date) {
        return VERBOSE_LOCALE_TIME_FORMATTER.format(date);
    }

    public static Date toLocalTime(Date date) {
        long millisUTC = date.getTime();
        TimeZone tz = TimeZone.getDefault();
        int tzOffset = tz.getOffset(millisUTC);
        if (tz.inDaylightTime(new Date(millisUTC))) {
            millisUTC -= tz.getDSTSavings();
        }
        return new Date(millisUTC + tzOffset);
    }

    public static long toLocalTime(long millisUTC) {
        // TODO: Refactor this and save it. Perfect DST code.
        TimeZone tz = TimeZone.getDefault();
        int tzOffset = tz.getOffset(millisUTC);
        if (tz.inDaylightTime(new Date(millisUTC))) {
            millisUTC -= tz.getDSTSavings();
        }
        return millisUTC + tzOffset;
    }

    public static Date string2Date(String yyyyMMdd){
        if (null != yyyyMMdd && yyyyMMdd.matches("^\\d{8}$")){
            SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
            try {
                return format.parse(yyyyMMdd);
            } catch (ParseException e) {
                return new Date();
            }
        }

        return new Date();
    }

    public static String formatMonthDay(String yyyyMMdd){
        if (null != yyyyMMdd && yyyyMMdd.matches("^\\d{8}$")){
            return yyyyMMdd.substring(4);
        }
        return yyyyMMdd;
    }

    public static String formatDateStr(String yyyyMMdd){
        if (null != yyyyMMdd && yyyyMMdd.matches("^\\d{8}$")){
            StringBuffer sb = new StringBuffer(yyyyMMdd);
            sb.insert(4, "-");
            sb.insert(7, "-");
            return sb.toString();
        }

        return yyyyMMdd;
    }

    public static String formatDate(Date date){
        if (null != date){
            return new SimpleDateFormat("yyyy-MM-dd").format(date);
        }
        return "";
    }

    public static String formatDate2(Date date){
        if (null != date){
            return new SimpleDateFormat("yyyyMMdd").format(date);
        }

        return "";
    }

    public static String formatTime(Date date){
        if (null != date){
            return new SimpleDateFormat("HHmmss").format(date);
        }

        return "";
    }

    public static String getSystemDate(){
        return new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    }

    public static String getSystemMonthDay() {
        return new SimpleDateFormat("MMdd").format(new Date());
    }

    public static String getSystemTime(){
        return new SimpleDateFormat("HHmmss").format(new Date());
    }

    public static String getSystemDateTime(){
        return new SimpleDateFormat("MMddHHmmss").format(new Date());
    }

    public static String formathhmmss(String hhmmss){
        if (null != hhmmss && hhmmss.length() == 6){
            StringBuffer sb = new StringBuffer(hhmmss);
            sb.insert(2, ":");
            sb.insert(5, ":");
            return sb.toString();
        }
        return "";
    }

    public static String formatDateTime(String yyyyMMddhhmmss){
        try{
            SimpleDateFormat format1 = new SimpleDateFormat("yyyyMMddHHmmss");
            Date date = format1.parse(yyyyMMddhhmmss.replace(" ", ""));

            SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return format2.format(date);
        }catch(Exception e){
            e.printStackTrace();
            return yyyyMMddhhmmss;
        }
    }

    public static String formatMMddDash(String MMdd){
        if (null != MMdd && MMdd.length() == 4){
            StringBuffer sb = new StringBuffer(MMdd);
            sb.insert(2, "-");
            return sb.toString();

        }
        return MMdd;
    }

    public static int daysBetween(java.sql.Date date1,java.sql.Date date2)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date1);
        long time1 = cal.getTimeInMillis();
        cal.setTime(date2);
        long time2 = cal.getTimeInMillis();
        long between_days=(time2-time1)/(1000*3600*24);

        return Integer.parseInt(String.valueOf(between_days));
    }

    public static String getCurrentYear(){
        return new SimpleDateFormat("yyyy").format(new Date());
    }
    public static String getCurrentMonth(){
        return new SimpleDateFormat("MM").format(new Date());
    }
    public static String getCurrentDd(){
        return new SimpleDateFormat("dd").format(new Date());
    }

    public static String getCurrentYearMonthDay(){
        return new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    }

    public static String convertTime(String time) {
        Date date;
        if(time != null){
            long timeStamp = Long.parseLong(time) * 1000;
            date = new Date(timeStamp);
        }else{
            date = new Date();
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(date);
    }
}
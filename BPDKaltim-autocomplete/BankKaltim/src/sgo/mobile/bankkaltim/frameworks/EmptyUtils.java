package sgo.mobile.bankkaltim.frameworks;

import java.util.List;

/**
 *
 * @author Diki Irawan
 * $Rev$
 */
public class EmptyUtils extends Object{

    /**
     * Empty check.<br/>
     * If value is null or empty or "null" String, return true.
     *
     * @param value
     *            checkValue
     * @return If value is null or empty or "null" String, return true. Else
     *         true.
     */
    public static boolean isEmpty(String value) {
        return value == null || value.trim().length() == 0 ? true : false;
    }

    /**
     * @param value
     * @return
     */
    public static <E> boolean isEmpty(List<E> value) {
        return value == null || value.size() == 0 ? true : false;
    }

    /**
     * Not empty check.<br/>
     * If value is not null and empty and not "null" String, return true.
     *
     * @param value
     *            checkValue.
     * @return If value is not null and empty and not "null" String, return
     *         true.
     */
    public static boolean isNotEmpty(String value) {
        return !isEmpty(value);
    }

    /**
     *
     *
     * @param value List&lt;E&gt;
     * @return boolean (true|false)
     */
    public static <E> boolean isNotEmpty(List<E> value) {
        return !isEmpty(value);
    }
}
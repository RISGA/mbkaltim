package sgo.mobile.bankkaltim.frameworks.net;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;

import java.net.HttpURLConnection;
import java.net.URL;

public class Networks {


    public static final String NETWORK_MOBILE = "MOBILE";
    public static final String NETWORK_WIFI = "WIFI";

    public Networks() {
        // Do Nothing
    }

    public static boolean isCDMA( Context context ) {

        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService( Context.TELEPHONY_SERVICE );
        return telephonyManager.getPhoneType() != TelephonyManager.PHONE_TYPE_GSM;
    }

    public static boolean isNetworkAvailable( Context context ) {

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService( Context.CONNECTIVITY_SERVICE );
        return connectivityManager.getActiveNetworkInfo() != null;
    }

    public static boolean isNetworkConnected( Context context ) {

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService( Context.CONNECTIVITY_SERVICE );
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    public boolean isConnectingToInternet(Context context){
        ConnectivityManager connectivity = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                    {
                        return true;
                    }

        }
        return false;
    }

    /**
     * wheter is wifi or not interface.
     * @param context
     * @return bool (true: wifi connect, false : not wifi)
     */
    public static boolean isWifi(Context context) {
        if (context == null) {
            return false;
        }
        boolean flg = false;
        try {
            ConnectivityManager cm = (ConnectivityManager)
                    context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            return networkInfo.isConnected();
        } catch (Exception e) {
            return flg;
        }

    }

    /*
 * This function checks hosts availability.
 * @param host
 */
    public static  boolean isURLReachable(String host) {

        try {
            URL url = new URL(host);
            HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
            urlc.setConnectTimeout(10 * 1000);          // 10 s.
            urlc.connect();
            if (urlc.getResponseCode() == 200) {        // 200 = "OK" code (http connection is fine).
                System.out.println("Success !");
                return true;
            } else {
                System.out.println("STATUS::FALSE");
                return false;
            }
        } catch (Exception e1) {
            return false;
        }

    }

    public static String getActiveNetwork( Context context ) {

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService( Context.CONNECTIVITY_SERVICE );
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        if ( activeNetworkInfo != null ) {
            return activeNetworkInfo.getTypeName();
        }

        return null;
    }

    public static String getNetworkName( Context context ) {

        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService( Context.TELEPHONY_SERVICE );
        return telephonyManager.getNetworkOperatorName();
    }
}

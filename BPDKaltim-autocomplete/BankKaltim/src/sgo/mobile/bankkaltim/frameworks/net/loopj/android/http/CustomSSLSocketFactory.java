package sgo.mobile.bankkaltim.frameworks.net.loopj.android.http;

import android.content.Context;
import com.activeandroid.util.Log;
import org.apache.http.conn.ssl.SSLSocketFactory;
import sgo.mobile.bankkaltim.R;

import java.io.InputStream;
import java.security.KeyStore;

/**
 * Created by Lenovo Thinkpad on 4/12/2016.
 */
public class CustomSSLSocketFactory {

    protected static final String TAG = "CustomSSLSocketFactory";



    public static SSLSocketFactory getSSLSocketFactory(final Context context){
        try {
            // Get an instance of the Bouncy Castle KeyStore format
           // KeyStore trusted = KeyStore.getInstance("BKS");
            // Get the raw resource, which contains the keystore with
            // your trusted certificates (root and any intermediate certs_)
            KeyStore keyStore = KeyStore.getInstance("BKS");
            InputStream instream = context.getResources().openRawResource(R.raw.cert);
//            try {
//                // Initialize the keystore with the provided trusted certificates
//                // Also provide the password of the keystore
//                trusted.load(in, "mysecret".toCharArray());
//            } finally {
//                in.close();
//            }


            try {
                keyStore.load(instream, context.getString(R.string.store_pass).toCharArray());
            } catch (java.security.cert.CertificateException e) {
                e.printStackTrace();
                Log.d("HERE SSSSSLLLLLLL");
            }

            // Pass the keystore to the SSLSocketFactory. The factory is responsible
            // for the verification of the server certificate.
            SSLSocketFactory sf = new SSLSocketFactory(keyStore);
            // Hostname verification from certificate
            // http://hc.apache.org/httpcomponents-client-ga/tutorial/html/connmgmt.html#d4e506
            sf.setHostnameVerifier(SSLSocketFactory.STRICT_HOSTNAME_VERIFIER);
            return sf;
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("HERE SSSSSLLLLLLL");
            throw new AssertionError(e);

        }
    }
}

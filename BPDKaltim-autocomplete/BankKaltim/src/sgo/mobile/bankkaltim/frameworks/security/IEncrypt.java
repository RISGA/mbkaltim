package sgo.mobile.bankkaltim.frameworks.security;

public interface IEncrypt {
    public String encrypt(String str);
}

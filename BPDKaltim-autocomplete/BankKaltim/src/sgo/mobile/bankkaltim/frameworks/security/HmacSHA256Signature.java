package sgo.mobile.bankkaltim.frameworks.security;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class HmacSHA256Signature{

    public String generateSignature(String uniqueKey, String dateTime, String apiKey,String appName,
                                          String serviceName, String bankCode, String userName ){

        try {

            uniqueKey = UUID.randomUUID().toString();
           // System.out.println ("UUID : " + uniqueKey);
            appName = "BPDNET";
            serviceName = "InquiryAccount";
            bankCode = "111";
            userName = "risga01";
            SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");//dd/MM/yyyy
            Date now = new Date();
            dateTime = sdfDate.format(now);

            //  Long tsLong = System.currentTimeMillis();
            // String timestamp = tsLong.toString();
            // System.out.println("Date Time : " + dateTime);

            apiKey  = "aea09527a68100e995a2f2027fb94ed5";
            String signature = uniqueKey+dateTime + appName + serviceName + bankCode+userName;

            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);

            byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));

           //final String hash = new String(encodeUrlSafe(hmacData));

            return new String(encodeUrlSafe(hmacData));
        }catch (Exception e){
            System.out.println("Error");
        }return null;

    }


    public static byte[] encodeUrlSafe(byte[] data) {
        byte[] encode = Base64.encodeBase64(data);
        for (int i = 0; i < encode.length; i++) {
            if (encode[i] == '+') {
                encode[i] = '-';
            } else if (encode[i] == '=') {
                encode[i] = '_';
            } else if (encode[i] == '/') {
                encode[i] = '~';
            }
        }
        return encode;
    }
}

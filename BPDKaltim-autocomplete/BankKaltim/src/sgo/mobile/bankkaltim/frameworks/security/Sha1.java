package sgo.mobile.bankkaltim.frameworks.security;

import sgo.mobile.bankkaltim.frameworks.log.SgoLog;

import java.security.MessageDigest;

public class Sha1 {

    /**
     * Create a SHA-1 digest of the provided message.
     *
     * @param message The message to digest.
     *
     * @return The digest of the message using SHA-1.
     */
    public static String sha1(String message) {
        String hash = null;
        try {
            MessageDigest digest = MessageDigest.getInstance( "SHA-1" );
            byte[] bytes = message.getBytes("UTF-8");
            digest.update(bytes, 0, bytes.length);
            bytes = digest.digest();
            StringBuilder sb = new StringBuilder();
            for( byte b : bytes )
            {
                sb.append( String.format("%02X", b) );
            }
            hash = sb.toString();
        } catch(Exception e ) {
            SgoLog.e("Exception Error SHA-1 : ", e.getMessage(), e);
        }
        return hash;
    }
}

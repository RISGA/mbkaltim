package sgo.mobile.bankkaltim.frameworks.security;

public interface IDecrypt {
    public String decrypt(String str);
}

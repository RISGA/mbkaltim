package sgo.mobile.bankkaltim.frameworks.text;

import android.annotation.SuppressLint;
import android.text.TextUtils;
import sgo.mobile.bankkaltim.frameworks.array.ObjectUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The <code>StringUtils</code> class contains utility methods to parse string, format string,
 * decode string, encode string, split string, and so on.
 *
 * @author Diki Irawan
 */
public class StringUtils {
    private static final String BASE_DIGITS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    public static final String SINGLE_SPACE = " ";

    // Hex constants concatenated into a string, messy but efficient
    private final static String hex = "%00%01%02%03%04%05%06%07%08%09%0a%0b%0c%0d%0e%0f%10%11%12%13%14%15%16%17%18%19%1a%1b%1c%1d%1e%1f"
            + "%20%21%22%23%24%25%26%27%28%29%2a%2b%2c%2d%2e%2f%30%31%32%33%34%35%36%37%38%39%3a%3b%3c%3d%3e%3f"
            + "%40%41%42%43%44%45%46%47%48%49%4a%4b%4c%4d%4e%4f%50%51%52%53%54%55%56%57%58%59%5a%5b%5c%5d%5e%5f"
            + "%60%61%62%63%64%65%66%67%68%69%6a%6b%6c%6d%6e%6f%70%71%72%73%74%75%76%77%78%79%7a%7b%7c%7d%7e%7f"
            + "%80%81%82%83%84%85%86%87%88%89%8a%8b%8c%8d%8e%8f%90%91%92%93%94%95%96%97%98%99%9a%9b%9c%9d%9e%9f"
            + "%a0%a1%a2%a3%a4%a5%a6%a7%a8%a9%aa%ab%ac%ad%ae%af%b0%b1%b2%b3%b4%b5%b6%b7%b8%b9%ba%bb%bc%bd%be%bf"
            + "%c0%c1%c2%c3%c4%c5%c6%c7%c8%c9%ca%cb%cc%cd%ce%cf%d0%d1%d2%d3%d4%d5%d6%d7%d8%d9%da%db%dc%dd%de%df"
            + "%e0%e1%e2%e3%e4%e5%e6%e7%e8%e9%ea%eb%ec%ed%ee%ef%f0%f1%f2%f3%f4%f5%f6%f7%f8%f9%fa%fb%fc%fd%fe%ff";

    public static final String NULL = "NULL";

    private static String newLine = null;


    /**
     * Creates a new instance of StringUtils
     */
    private StringUtils() {
    }

    /**
     * Parse the phone string. Remove all characters except digit and '+'.
     *
     * @param phone the phone string to parse.
     * @return the parsed string.
     */
    public static String parsePhone(String phone) {
        StringBuffer buf = new StringBuffer();

        for (int i = 0; i < phone.length(); i++) {
            char ch = phone.charAt(i);
            if (ch == '+' || Character.isDigit(ch)) buf.append(ch);
        }

        return buf.toString();
    } // end method parsePhone

    /**
     * Split string into multiple strings
     *
     * @param original  Original string
     * @param separator Separator string in original string
     * @return Splitted string array
     */
    public static String[] split(String original, String separator) {
        // original code:
        // Vector nodes = new Vector();
        Vector<String> nodes = new Vector<String>();

        // Parse nodes into vector
        int index = original.indexOf(separator);
        while (index >= 0) {
            nodes.addElement(original.substring(0, index));
            original = original.substring(index + separator.length());
            index = original.indexOf(separator);
        }
        // Get the last node
        nodes.addElement(original);

        // modified: ignore empty string
        int realSize = nodes.size();
        for (int i = 0; i < nodes.size(); i++) {
            String elem = (String) nodes.elementAt(i);
            if (elem.length() == 0) realSize--;
        }

        // Create splitted string array
        String[] result = new String[realSize];
        if (nodes.size() > 0) {
            int idx = 0;
            for (int loop = 0; loop < nodes.size(); loop++) {
                String elem = (String) nodes.elementAt(loop);
                if (elem.length() > 0) {
                    result[idx] = elem;
                    idx++;
                }
            }
        }
        return result;
    } // end method split

    /**
     * Replace all instances of a String in a String.
     *
     * @param s String to alter.
     * @param f String to look for.
     * @param r String to replace it with, or null to just remove it.
     * @return the String after replace.
     */
    public static String replace(String s, String f, String r) {
        if (s == null) {
            return s;
        }
        if (f == null) {
            return s;
        }
        if (r == null) {
            r = "";
        }
        int index01 = s.indexOf(f);
        while (index01 != -1) {
            s = s.substring(0, index01) + r + s.substring(index01 + f.length());
            index01 += r.length();
            index01 = s.indexOf(f, index01);
        }
        return s;
    } // end method replace

    /**
     * Decode html entities in the specified string.
     *
     * @param html the string to decode.
     * @return the decoded string.
     */
    public static String decodeEntities(String html) {
        String result = StringUtils.replace(html, "&lt;", "<");
        result = StringUtils.replace(result, "&gt;", ">");
        result = StringUtils.replace(result, "&nbsp;", " ");
        result = StringUtils.replace(result, "&amp;", "&");
        result = StringUtils.replace(result, "&auml;", "|");
        result = StringUtils.replace(result, "&ouml;", "|");
        result = StringUtils.replace(result, "&quot;", "'");
        result = StringUtils.replace(result, "&#xd;", "\r");
        return result;
    } // end method decodeEntities

    /**
     * <p>
     * Takes an array of messages and a 'Screen-width' and returns the same
     * messages, but any string in that that is wider than 'width' will be
     * split up into 2 or more Strings that will fit on the screen
     * </p>
     *
     * 'Spliting' up a String is done on the basis of Words, so if a single
     * WORD is longer than 'width' it will be on a Line on it's own, but
     * that line WILL be WIDER than 'width'
     *
     * @param message the message to format
     * @param width
     *                the maximum width a string may be before being split
     *                up.
     * @param f the <code>Font</code> object.
     * @return the formatted array of <code>String</code>
     */


    /**
     * Apply url decode to the specified <code>String</code>.
     *
     * @param s the <code>String</code> to decode.
     * @return the decoded <code>String</code>
     */
    public static String urlDecode(String s) {
        if (s != null) {
            StringBuffer tmp = new StringBuffer();
            int i = 0;
            try {
                while (i < s.length()) {
                    char ch = s.charAt(i);
                    if (ch == '%') {
                        if (i + 2 < s.length()) {
                            String s2 = "" + s.charAt(i + 1) + s.charAt(i + 2);
                            tmp.append(((char) Integer.parseInt(s2, 16)));
                            i += 2;
                        } else {
                            tmp.append(ch);
                        }
                    } else {
                        tmp.append(ch);
                    }

                    i++;
                }
            } catch (Exception e) {

            }
            return tmp.toString();
        }
        return null;
    } // end method urlDecode

    /**
     * Escape the specified string. Any special characters other than '-', '.', '_',
     * and '~' will be converted to '%' and its hexadecimal string.
     *
     * @param input the string to escape.
     * @return the escaped string.
     */
    public static String escape(String input) {
        final char[] unreserved = {'-', '.', '_', '~'};

        StringBuffer o = new StringBuffer();
        int l = input.length();
        for (int i = 0; i < l; i++) {
            char ch = input.charAt(i);
            char ch2 = Character.toLowerCase(ch);
            boolean alphanum = false;
            alphanum = Character.isDigit(ch2);
            if (ch2 >= 'a' && ch2 <= 'z') alphanum = true;
            boolean escaped = false;
            boolean contains = false;
            for (int j = 0; j < unreserved.length; j++) {
                if (ch == unreserved[j]) {
                    contains = true;
                    break;
                }
            }
            if (!contains && !alphanum) {
                o.append("%");
                o.append(Integer.toHexString(ch).toUpperCase());
                escaped = true;
            }
            if (!escaped) {
                o.append("" + ch);
            }
        }
        return o.toString();
    } // end method escape

    /**
     * URL encode given string.
     *
     * @param s the string to encode.
     * @return the encoded string.
     */
    public static String urlEncode(String s) {
        // source:
        // http://wiki.forum.nokia.com/index.php/How_to_encode_URL_in_Java_ME_%3F,
        // with some modification (i.e. still encode space and some
        // characters)

        StringBuffer sbuf = new StringBuffer();
        for (int i = 0; i < s.length(); i++) {
            int ch = s.charAt(i);
            if ('A' <= ch && ch <= 'Z') { // 'A'..'Z'
                sbuf.append((char) ch);
            } else if ('a' <= ch && ch <= 'z') { // 'a'..'z'
                sbuf.append((char) ch);
            } else if ('0' <= ch && ch <= '9') { // '0'..'9'
                sbuf.append((char) ch);
            } else if (ch <= 0x007f) { // other ASCII
                sbuf.append(hex(ch));
            } else if (ch <= 0x07FF) { // non-ASCII <= 0x7FF
                sbuf.append(hex(0xc0 | (ch >> 6)));
                sbuf.append(hex(0x80 | (ch & 0x3F)));
            } else { // 0x7FF < ch <= 0xFFFF
                sbuf.append(hex(0xe0 | (ch >> 12)));
                sbuf.append(hex(0x80 | ((ch >> 6) & 0x3F)));
                sbuf.append(hex(0x80 | (ch & 0x3F)));
            }
        }
        return sbuf.toString();
    }

    // get the encoded value of a single symbol, each return value is 3
    // characters long
    private static String hex(int sym) {
        int start = sym * 3;
        return (hex.substring(start, start + 3));
    }

    /**
     * JSON encode given string.
     *
     * @param s the string to encode.
     * @return the encoded string.
     */
    public static String jsonEncode(String s) {
        return "\"" + replace(replace(s, "\\", "\\\\"), "\"", "\\\"") + "\"";
    } // end method jsonEncode

    /**
     * Insert string before a character.
     *
     * @param s        the string to be inserted.
     * @param toInsert the string to insert.
     * @param lastChar the character
     * @return the modified string.
     */
    public static String insertBeforeLastChar(String s, String toInsert, char lastChar) {
        if (s == null || s.trim().equals(""))
            return s;

        int charPos = s.lastIndexOf(lastChar);

        if (charPos <= 0)
            return s;

        String before = s.substring(0, charPos);
        String after = s.substring(charPos);
        String result = before + toInsert + after;
        return result;
    } // end method insertBeforeLastChar


    /**
     * Convert the given number to base 62.
     *
     * @param decimalNumber the number to convert.
     * @return <code>String</code> representation in base 62.
     */
    public static String toBase62(int decimalNumber) {
        return fromDecimalToOtherBase(62, decimalNumber);
    } // end method toBase62

    /**
     * Convert the given number to specified base.
     *
     * @param decimalNumber the number to convert.
     * @param base          the target base.
     * @return <code>String</code> representation in base 62.
     */
    private static String fromDecimalToOtherBase(int base, int decimalNumber) {
        String tempVal = decimalNumber == 0 ? "0" : "";
        int mod = 0;

        while (decimalNumber != 0) {
            mod = decimalNumber % base;
            tempVal = BASE_DIGITS.substring(mod, mod + 1) + tempVal;
            decimalNumber = decimalNumber / base;
        }

        return tempVal;
    } // end method fromDecimalToOtherBase

    /**
     * Checks whether the 'text' ends with 'end'.
     *
     * @param text the text to check.
     * @param end  the suffix.
     * @return true if the suffix of 'text' is 'end', false otherwise.
     */
    public static boolean endsWith(String text, String end) {
        if (text.length() < end.length()) return false;


        for (int i = 0; i < end.length(); i++) {
            if (end.charAt(end.length() - i - 1) != text.charAt(text.length() - i - 1)) {
                return false;
            }
        }

        return true;
    } // end method endsWith

    /**
     * Concatenates all the elements of a string array, using the specified
     * separator between each element.
     *
     * @param separator The string to use as a separator.
     * @param value     An array that contains the elements to concatenate.
     * @return A string that consists of the elements in value delimited by the
     * separator string.
     */
    public static String join(String separator, String[] value) {
        StringBuffer buffer = new StringBuffer();

        for (int i = 0; i < value.length; i++) {
            String word = value[i];

            if (i == 0) {
                // first word, just add
                buffer.append(word);
            } else {
                // next word, append the separator first
                buffer.append(separator);
                buffer.append(word);
            }
        }

        return buffer.toString();
    } // end method join

    /**
     * Creates a hexadecimal String from the given array of bytes.
     *
     * @param bytes an array of bytes.
     * @return hexadecimal String.
     */
    public static String bytesToHexString(byte[] bytes) {

        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {

            String hexString = Integer.toHexString(0xFF & bytes[i]);
            stringBuffer.append((hexString.length() == 1) ? "0" : "");
            stringBuffer.append(hexString);
        }

        return stringBuffer.toString();
    }

    /**
     * Creates a substring of the given String.
     *
     * @param input the original String.
     * @param limit the maximum length for the substring.
     * @return the substring (length &lt;= limit).
     */
    public static String limit(String input, int limit) {

        input = (input == null) ? "" : input;
        limit = Math.min(input.length(), limit);
        return input.substring(0, limit);
    }

    /**
     * The Levenshtein distance (or edit distance) is the minimum number of operations needed
     * to transform the source String into the target String.  This determines the
     * totalAmount of difference between the given Strings, which is useful in detecting simple
     * typos or performing loose word matching.
     *
     * @param source        the source String.
     * @param target        the target String.
     * @param caseSensitive if false, case sensitivity will be ignored in the distance calculation.
     * @return the calculated Levenshtein distance.
     */
    public static int levenshteinDistance(String source, String target, boolean caseSensitive) {

        try {
            if (caseSensitive) {
                String modSource = (source == null) ? null : source.toUpperCase();
                String modTarget = (target == null) ? null : target.toUpperCase();
                return levenshteinDistance(modSource, modTarget);
            } else {
                return levenshteinDistance(source, target);
            }
        } catch (Throwable t) {
            throw new RuntimeException("Error computing edit distance. Source: " + source + " Target: " + target);
        }
    }

    /**
     * Helper method used in calculating the Levenshtein distance of two Strings.
     *
     * @param source the source String.
     * @param target the target String.
     * @return the calculated Levenshtein distance.
     */
    private static int levenshteinDistance(String source, String target) {

        if (source == null && target == null) {
            return 0;
        }
        if (source == null) {
            return target.length();
        }
        if (target == null) {
            return source.length();
        }

        int n = source.length();
        int m = target.length();

        if (n == 0)
            return m;
        if (m == 0)
            return n;

        int[][] d = new int[n + 1][m + 1];

        for (int i = 0; i <= n; d[i][0] = i++)
            ;
        for (int j = 1; j <= m; d[0][j] = j++)
            ;

        for (int i = 1; i <= n; i++) {
            char sc = source.charAt(i - 1);
            for (int j = 1; j <= m; j++) {
                int v = d[i - 1][j - 1];
                if (target.charAt(j - 1) != sc)
                    v++;
                d[i][j] = Math.min(Math.min(d[i - 1][j] + 1, d[i][j - 1] + 1), v);
            }
        }
        return d[n][m];
    }

    /**
     * Gets the NewLine attribute of the Formatter class
     *
     * @return The NewLine value
     */
    public static String lineSeparator() {

        if (newLine == null) {

            newLine = System.getProperty("line.separator");
            if (newLine == null) {
                newLine = "\n";
            }
        }

        return newLine;
    }

    /**
     * Retrieves a substring of text from the specified input.  For example, the input "abcdefg"
     * with the delimiter "e" will return the substring "abcd".  If the specified delimiter can
     * not be found in the input string, a null string is returned.
     *
     * @param input     the string which should be searched for a given delimiter.
     * @param delimiter the string denoting the end of the desired substring.
     * @return string after delimiter, otherwise null.
     */
    public static String substringBefore(String input, String delimiter) {

        if (isNull(delimiter))
            throw new IllegalArgumentException("Delimiter cannot be null.");
        if (isNull(input)) {
            return null;
        } else {
            int splitPosition = input.indexOf(delimiter);
            if (splitPosition == -1) {
                return null;
            } else {
                //splitPosition = splitPosition;// - delimiter.length();
                return input.substring(0, splitPosition);
            }
        }
    }

    /**
     * Creates a substring of the given input from 0 to the
     * nth occurrence of the given delimiter.
     * <p/>
     * time - O(n)
     * object handles - 3
     *
     * @param input      String to search
     * @param delimiter  String to search for
     * @param occurrence A 1-indexed count of which occurrence to end before
     * @return input if occurrence < 1, empty string if there aren't enough
     * occurrences in input, substring from 0 to occurrence's index otherwise
     */
    public static String substringBefore(String input, String delimiter, int occurrence) {

        if (occurrence < 1) {
            return input;
        }
        int start = 0;
        char[] inputArray = input.toCharArray();
        char[] delimiterArray = delimiter.toCharArray();
        for (int i = 0; i < occurrence; i++) {
            start = indexOf(inputArray, start, inputArray.length, delimiterArray, 0, delimiterArray.length, 0);
            if (start == -1) {
                return "";
            }
        }
        return substringFirst(input, start);
    }

    /**
     * Retrieves a substring of text from the specified input.  For example, the input "abcdefg"
     * with the delimiter "e" will return the substring "fg".  If the specified delimiter can
     * not be found in the input string, a null string is returned.
     *
     * @param input     the string which should be searched for a given delimiter.
     * @param delimiter the string denoting the text appearing before the start of the desired substring.
     * @return string after delimiter, otherwise null.
     */
    public static String substringAfter(String input, String delimiter) {

        if (isNull(delimiter))
            throw new IllegalArgumentException("Delimiter cannot be null.");
        if (isNull(input)) {
            return null;
        } else {
            int splitPosition = input.indexOf(delimiter);
            if (splitPosition == -1) {
                return null;
            } else {
                splitPosition = splitPosition + delimiter.length();
                return input.substring(splitPosition);
            }
        }
    }

    /**
     * Creates a substring of the given String.
     *
     * @param input the original String.
     * @param chars desired length of the substring.
     * @return the substring.
     */
    public static String substringFirst(String input, int maxChars) {

        if (input != null) {
            if (input.length() > maxChars) {
                return input.substring(0, maxChars);
            }
        }

        return input;
    }

    /**
     * Returns a substring of the given string that has less than or equal to the specified number of characters.
     *
     * @param text     the text from which we are retrieving a substring.
     * @param maxChars the maximum number of characters to appear in the resultant substring.
     * @return Returns a substring of the given string that has less than or equal to the specified number of characters.
     */
    public static String substringLast(String text, int maxChars) {

        if (text != null) {
            if (text.length() > maxChars) {
                int startingIndex = text.length() - 8;
                text = text.substring(startingIndex);
            }
        }

        return text;
    }

    /**
     * Determines the index of the first number in the string.
     *
     * @param text
     * @return The index of the first numerical character in the String, or -1 is no numerical characters exist.
     */
    public static int indexOfNumber(String text) {

        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            if (isNumeric(new String(new char[]{c}))) {
                return i;
            }
        }

        return -1;
    }

    /**
     * Returns true if a string is entirely numeric, false otherwise.
     *
     * @param text the text which may or may not be entirely numeric.
     * @return Returns true if a string is entirely numeric, false otherwise.
     */
    public static boolean isNumeric(String text) {

        boolean numeric = true;
        try {
            Double.parseDouble(text);
        } catch (NumberFormatException e) {
            numeric = false;
        }

        return numeric;
    }

    /**
     * Returns true if a string is entirely alphabetic, false otherwise.
     *
     * @param text the text which may or may not be entirely alphabetic.
     * @return Returns true if a string is entirely alphabetic, false otherwise.
     */
    public static boolean isAlphabetic(String text) {

        return text.matches("[A-Za-z]*");
    }

    /**
     * Returns true if a string is entirely alphanumeric, false otherwise.
     *
     * @param text the text which may or may not be entirely alphanumeric.
     * @return Returns true if a string is entirely alphanumeric, false otherwise.
     */
    public static boolean isAlphanumeric(String text) {

        for (int i = 0; i < text.length(); i++) {

            if (!Character.isLetterOrDigit(text.charAt(i))) {
                return false;
            }
        }

        return true;
    }

    /**
     * Determines whether a String contains a particular String of characters within it.
     *
     * @param input    the String that will be searched.
     * @param contains the substring that may be found within the String being searched.
     * @return true if the substring was found within the String being searched.
     */
    public static boolean contains(String input, String contains) {

        if (!isNull(input)) {
            return input.indexOf(contains) != -1;
        } else {
            return false;
        }
    }

    /**
     * Determines whether a String contains a particular String of characters within it (case insensitively).
     *
     * @param input    the String that will be searched.
     * @param contains the substring that may be found within the String being searched.
     * @return true if the substring was found within the String being searched.
     */
    public static boolean containsIgnoreCase(String input, String contains) {

        input = input.toLowerCase();
        contains = contains.toLowerCase();

        return contains(input, contains);
    }

    /**
     * Creates an array from a given String using the specified delimiter.
     *
     * @param xDelimitedString the complete String.
     * @param x                the delimiter for the complete String.
     * @return array of Strings or null if the xDelimitedString is null
     */
    public static String[] getArrayFromXDelimitedString(String xDelimitedString, String x) {

        List<String> list = getListFromXDelimitedString(xDelimitedString, x);
        if (list != null) {

            return list.toArray(new String[list.size()]);
        } else {

            return null;
        }
    }

    /**
     * Creates a List from a given String using the specified delimiter.
     *
     * @param xDelimitedString the complete String.
     * @param x                the delimiter for the complete String.
     * @return List of Strings.
     */
    private static List<String> getListFromXDelimitedString(String xDelimitedString, String x) {

        List<String> list = new ArrayList<String>();
        if (isNull(xDelimitedString)) {
            return null;
        }
        String temp = xDelimitedString;
        while (true) {
            int xIndex = temp.indexOf(x);
            if (xIndex < 0) {
                break;
            }
            list.add(temp.substring(0, xIndex).trim());
            temp = temp.substring(xIndex + 1);
        }
        temp = trim(temp);
        if (temp.length() > 0) {
            list.add(temp);
        }
        return list;
    }

    /**
     * Determines if the given String is null.
     *
     * @param text some text
     * @return true if text == null or ""
     */
    public static boolean isNull(String text) {

        return (text == null) || (text.length() == 0);
    }

    /**
     * Determines if the given String is null or consists entirely of whitespace
     *
     * @param text
     * @return
     */
    public static boolean isNullOrWhiteSpace(String text) {

        return isNull(text) || text.trim().length() == 0;
    }

    /**
     * Determines if the given String is not null and has non whitespace characters.
     *
     * @param text some text
     * @return true if text != null and text contains non whitespace characters
     */
    public static boolean hasContent(String text) {

        return !isNull(text) && !isWhitespace(text);
    }

    /**
     * Determines whether the two given Strings are equal, ignoring case
     *
     * @param s1 a String.
     * @param s2 another String.
     * @return true if the two Strings are equivalent or if both Strings are null
     */
    public static boolean equalsIgnoreCase(String s1, String s2) {

        if (!isNull(s1)) {

            s1 = s1.toLowerCase();
        }
        if (!isNull(s2)) {

            s2 = s2.toLowerCase();
        }
        return equals(s1, s2, true);
    }

    /**
     * Determines whether the two given Strings are equal.
     *
     * @param s1 a String.
     * @param s2 another String.
     * @return true if the two Strings are equivalent or if both Strings are null
     */
    public static boolean equals(String s1, String s2) {

        return equals(s1, s2, true);
    }

    /**
     * @param s1
     * @param s2
     * @param allowNull This is the value that's returned if both String arguments are null.
     * @return Returns true if the specified Strings are equivalent, false otherwise.
     */
    public static boolean equals(String s1, String s2, boolean allowNull) {

        if (s1 == null && s2 == null) {

            return allowNull;
        } else if (s1 == null) {

            return false;
        } else {

            return s1.equals(s2);
        }
    }

    /**
     * Trims surrounding whitespace from the given String.
     *
     * @param s the String to be trimmed.
     * @return the trimmed String.
     */
    public static String trim(String s) {

        return trim(s, false);
    }

    public static String trim(String s, boolean returnNullIfEmpty) {

        if (s == null) {
            return null;
        }

        String trim = s.trim();
        return returnNullIfEmpty && isNull(trim) ? null : trim;
    }

    /**
     * Creates a lower-case copy of the String.
     *
     * @param text the original String.
     * @return lower-case String.
     */
    public static String toLowerCase(String text) {

        if (text != null) {

            return text.toLowerCase();
        }

        return null;
    }

    /**
     * Creates an upper-case copy of the String.
     *
     * @param text the original String.
     * @return upper-case String.
     */
    public static String toUpperCase(String text) {

        if (text != null) {

            return text.toUpperCase();
        }

        return null;
    }

    /**
     * Creates a String delimited by the specified delimiter from a given Collection.
     *
     * @param delimiter  the delimiter for the new String.
     * @param collection a Collection of Strings.
     * @return the delimited String.
     */
    public static String getXDelimitedString(String delimiter, Collection<String> collection) {

        return getXDelimitedString(delimiter, collection, "");
    }

    /**
     * Creates a String delimited by the specified delimiter from a given Collection.  The
     * given indentation String will be prepended to each item in the Collection before being
     * added to the resultant delimited String.
     *
     * @param delimiter  the delimiter for the new String.
     * @param collection a Collection of Strings.
     * @param indention  the indentation used in the new String.
     * @return the delimited and indented String.
     */
    public static String getXDelimitedString(String delimiter, Collection<String> collection, String indention) {

        if (collection == null)
            throw new IllegalArgumentException("collection cannot be null.");
        StringBuffer stringBuffer = new StringBuffer(20 * collection.size());
        Iterator<String> iterator = collection.iterator();
        int i = 0;
        while (iterator.hasNext()) {
            i++;
            if (i != 1 && i <= collection.size())
                stringBuffer.append(delimiter);
            String s = String.valueOf(iterator.next());
            stringBuffer.append(indention + s);
        }
        return stringBuffer.toString();
    }

    /**
     * Creates a delimited String from an array of Strings.
     *
     * @param delimiter  the delimiter for the new String.
     * @param collection an array of Strings.
     * @return the delimited String.
     */
    public static String getXDelimitedString(String delimiter, String[] collection) {

        if (collection == null)
            throw new IllegalArgumentException("collection cannot be null.");
        StringBuffer stringBuffer = new StringBuffer(20 * collection.length);
        for (int i = 0; i < collection.length; i++) {
            if (i != 0 && i <= collection.length)
                stringBuffer.append(delimiter);
            String s = String.valueOf(collection[i]);
            stringBuffer.append(s);

        }
        return stringBuffer.toString();
    }

    /**
     * Creates a copy of the original String excluding the specified substring.
     *
     * @param sourceString   the original String.
     * @param stringToRemove the substring to be removed from the original String.
     * @return the new String.
     */
    public static String removeAllOccurences(String sourceString, String stringToRemove) {

        if (sourceString == null || stringToRemove == null) {
            return null;
        } else {
            if (sourceString.indexOf(stringToRemove) != -1) {
                return removeAllOccurences(removeOccurence(sourceString, stringToRemove), stringToRemove);
            }
            return sourceString;
        }
    }

    /**
     * Removes the first occurrence of a substring from the original String.
     *
     * @param sourceString   the original String.
     * @param stringToRemove the substring to be removed from the original String.
     * @return the new String.
     */
    public static String removeOccurence(String sourceString, String stringToRemove) {

        String returnString = "";
        String firstPart = "";
        String lastPart = "";

        if (!sourceString.equals(stringToRemove)) {

            int startIndex = sourceString.indexOf(stringToRemove);

            // get the part of sourceString before the occurrence if occurrence is found in source
            if (startIndex >= 0) {

                firstPart = sourceString.substring(0, startIndex);

                // get the part of sourceString after the occurrence if more sourceString exists
                if ((startIndex + stringToRemove.length()) < sourceString.length()) {

                    lastPart = sourceString.substring(startIndex + stringToRemove.length(), sourceString.length());
                }

                return firstPart + lastPart;
            }
        }

        return returnString;

    }

    /**
     * Creates a copy of the given String and makes its first letter lower-case.
     *
     * @param s the original String.
     * @return String with the first letter in lower-case.
     */
    public static String firstLetterToLowerCase(String s) {

        if (isNull(s)) {
            return null;
        }

        char[] chars = s.toCharArray();
        chars[0] = Character.toLowerCase(chars[0]);

        return new String(chars);
    }

    /**
     * Creates a copy of the given String and makes its first letter upper-case.
     *
     * @param s the original String.
     * @return String with the first letter in upper-case.
     */
    public static String firstLetterToUpperCase(String s) {

        if (isNull(s)) {
            return null;
        }

        char[] chars = s.toCharArray();
        chars[0] = Character.toUpperCase(chars[0]);

        return new String(chars);
    }

    /**
     * Returns a copy of the given String which will definitely have a
     * leading slash.
     *
     * @param value the original String.
     * @return String with a leading slash.
     */
    public static String guaranteeStartsWithSlash(String value) {

        String slash = Character.valueOf(determineSlashChar(value)).toString();
        // first guarantee that reference path ends with a slash
        if (!value.startsWith(slash)) {
            value = slash + value;
        }
        return value;
    }

    /**
     * Returns a copy of the given String which will definitely have a
     * trailing slash.
     *
     * @param value the original String.
     * @return String with a trailing slash.
     */
    public static String guaranteeEndsWithSlash(String value) {

        String slash = Character.valueOf(determineSlashChar(value)).toString();
        // first guarantee that reference path ends with a slash
        if (!value.endsWith(slash)) {
            value += slash;
        }
        return value;
    }

    /**
     * Removes all leading characters matching char leadingCharacterToTrim from String s, and returns the resulting String.
     */
    public static String trimLeadingCharacters(String s, char leadingCharacterToTrim) {

        for (int index = 0; index < s.length() - 1; index++) {
            char c = s.charAt(index);

            if (c == leadingCharacterToTrim) {
                //do nothing, let the loop continue
            } else {
                /**
                 * We've found a non matching character, return the String from the current character on.
                 */
                return s.substring(index);
            }
        }

        /**
         * The entire String is made of the leadingCharacterToTrim character
         */
        return "";
    }

    /**
     * Removes all trailing characters matching char trailingCharacterToRemove from String s, and returns the resulting String.
     */
    public static String trimTrailingCharacters(String s, char trailingCharacterToTrim) {

        for (int index = s.length() - 1; index >= 0; index--) {

            char c = s.charAt(index);

            if (c == trailingCharacterToTrim) {
                //do nothing, let the loop continue
            } else {
                /**
                 * We've found a non matching character, return the String up to and including this character.
                 */
                return s.substring(0, index + 1);

            }

        }

        /**
         * The entire String is made of the trailingCharacterToRemove character
         */
        return "";
    }

    /**
     * Determines whether the slash character is a forward-slash or backward-slash.
     *
     * @param s the String containing a slash.
     * @return either a foward-slash or backward-slash.
     */
    public static char determineSlashChar(String s) {

        char slash;
        int i = s.lastIndexOf('\\');
        if (i <= 0) {
            slash = '/';
        } else {
            slash = '\\';
        }
        return slash;
    }

    /**
     * Calculates the UTF length of a character array.
     *
     * @param ch     array of characters.
     * @param start  starting position in the character array.
     * @param length the maximum length.
     * @return the calculated length.
     */
    public static int calculateUTFLength(char[] ch, int start, int length) {

        if (ch == null || length <= 0 || start < 0) {
            return 0;
        }
        //Determine utf length.
        int utflen = 0;
        int c = 0;
        for (int i = 0; i < length; i++) {
            c = ch[i + start];
            if ((c >= 0x0001) && (c <= 0x007F)) {
                utflen++;
            } else if (c > 0x07FF) {
                utflen += 3;
            } else {
                utflen += 2;
            }
        }
        return utflen;
    }

    /**
     * Splits a String into an array of Strings using the given delimiter.
     *
     * @param input the original String.
     * @param delim delimiter (this is converted to a String).
     * @return array of Strings.
     */
    public static String[] split(String input, char[] delim) {

        return split(input, delim, true);
    }

    /**
     * Splits a String into an array of Strings using the given delimiter.
     *
     * @param input              the original String.
     * @param delim              delimiter (this is converted to a String).
     * @param suppressWhitespace
     * @return array of Strings.
     */
    public static String[] split(String input, char[] delim, boolean suppressWhitespace) {

        List<String> list = new ArrayList<String>();

        String delimString = String.valueOf(delim);
        if (isNull(input) || isNull(delimString)) {
            return new String[0];
        }

        int delimLength = delim.length;

        String temp = input;
        while (true) {

            int xIndex = -1;

            outerLoop:
            for (int i = 0; i < temp.length(); i++) {

                char c = temp.charAt(i);
                for (int j = 0; j < delimLength; j++) {

                    if (c == delim[j]) {

                        xIndex = i;
                        break outerLoop;
                    }
                }
            }

            if (xIndex < 0) {

                break;
            } else if (xIndex > 0) {

                String part = temp.substring(0, xIndex);

                if (suppressWhitespace) {
                    part = part.trim();
                }

                list.add(part);
            }

            temp = temp.substring(xIndex + 1);
        }

        if (suppressWhitespace) {
            temp = temp.trim();
        }

        if (temp.length() > 0) {
            list.add(temp);
        }

        String[] array = new String[list.size()];
        list.toArray(array);

        return array;
    }

    /**
     * Splits a String into an array of Strings using the given delimiter.
     *
     * @param input the original String.
     * @param delim delimiter (this is converted to a String).
     * @return array of Strings.
     */
    public static String[] split(String input, char delim) {

        return split(input, new char[]{delim});
    }

    /**
     * Determines whether the given String starts with the specified prefix.
     *
     * @param input  the complete String.
     * @param prefix the suspected prefix for the String.
     * @return true if the String begins with the specified prefix.
     */
    public static boolean startsWith(String input, String prefix) {

        if (isNull(input)) {

            return false;
        }

        return input.startsWith(prefix);
    }

    public static boolean endsWithIgnoreCase(String string, String suffix) {

        string = string.toLowerCase();
        suffix = suffix.toLowerCase();

        return string.endsWith(suffix);
    }

    /**
     * Replaces every occurrence of the 'find' string within the 'input' string with the 'replacement' string.
     *
     * @param input       The string that will be searched through for text to replace.
     * @param find        The text that will be replaced by the 'replacement' string.
     * @param replacement The string that will replace every occurrence of the 'find' string.
     * @return String that has had all occurrences of the desired substring replaced.
     */
    public static String replaceAll(String input, String find, String replacement) {

        StringBuffer stringBuffer = new StringBuffer();

        int indexOf;
        int inputStart = 0;

        while ((indexOf = input.indexOf(find, inputStart)) >= 0) {

            if (indexOf > inputStart) {

                stringBuffer.append(input.substring(inputStart, indexOf));
            }

            stringBuffer.append(replacement);

            inputStart = indexOf + find.length();
        }

        stringBuffer.append(input.substring(inputStart));

        return stringBuffer.toString();
    }

    /**
     * Finds the number of occurrences of the specified delimiter in the given String.
     *
     * @param input the complete String.
     * @param delim the delimiter.
     * @return number of occurrences of the specified delimiter.
     */
    public static int countOccurrences(String input, char delim) {

        int occurences = split(input, delim).length - 1;

        if (input.charAt(0) == delim) {
            occurences++;
        }

        if (input.charAt(input.length() - 1) == delim) {
            occurences++;
        }

        return occurences;
    }

    /**
     * @param source
     * @param search
     * @return If source is null, -1 is returned.  Otherwise, the value of source.indexOf(search) is returned.
     */
    public static int indexOf(String source, String search) {

        if (source != null) {

            return source.indexOf(search);
        }

        return -1;
    }

    /**
     * @param source
     * @param sourceOffset
     * @param sourceCount
     * @param target
     * @param targetOffset
     * @param targetCount
     * @param fromIndex
     * @return
     */
    public static int indexOf(char[] source, int sourceOffset, int sourceCount, char[] target, int targetOffset, int targetCount, int fromIndex) {

        if (fromIndex >= sourceCount) {
            return (targetCount == 0 ? sourceCount : -1);
        }
        if (fromIndex < 0) {
            fromIndex = 0;
        }
        if (targetCount == 0) {
            return fromIndex;
        }

        char first = target[targetOffset];
        int max = sourceOffset + (sourceCount - targetCount);

        for (int i = sourceOffset + fromIndex; i <= max; i++) {
            /* Look for first character. */
            if (source[i] != first) {
                while (++i <= max && source[i] != first)
                    ;
            }

            /* Found first character, now look at the rest of v2 */
            if (i <= max) {
                int j = i + 1;
                int end = j + targetCount - 1;
                for (int k = targetOffset + 1; j < end && source[j] == target[k]; j++, k++)
                    ;

                if (j == end) {
                    /* Found whole string. */
                    return i - sourceOffset;
                }
            }
        }
        return -1;
    }

    /**
     * Determines the boolean value the given String.  The String should be one believed to be representative
     * of a a boolean (e.g., "true" or "false").
     *
     * @param value the original String representing a boolean value.
     * @return The Boolean returned represents the value true if the string argument is not null  and is equal, ignoring case, to the string "true".
     */
    public static boolean booleanValueOf(String value) {

        if (isNull(value)) {
            return false;
        }

        return Boolean.valueOf(value);
    }

    /**
     * Ensures that the resultant String is not null.  If the given String is null, this method
     * shall return a value of "".
     *
     * @param value the original String.
     * @return a non-null String.
     */
    public static String getDisplayValue(String value) {

        if (isNull(value)) {
            return "";
        } else {
            return value;
        }
    }

    /**
     * Determines whether the given String starts with the specified prefix, ignoring case.
     *
     * @param input  the complete String.
     * @param prefix the suspected prefix for the String.
     * @return true if the String begins with the specified prefix.
     */
    public static boolean startsWithIgnoreCase(String string, String prefix) {

        string = string.toLowerCase();
        prefix = prefix.toLowerCase();

        return string.startsWith(prefix);
    }

    /**
     * @param text
     * @return true if the String only contains whitespace characters.  A whitespace character is determined
     * to be any character whose numeric ASCII value is less than or equal to that of the space character.  This
     * includes all common whitespace characters (including tabs, new lines, and carriage returns).
     */
    public static boolean isWhitespace(String text) {

        return isNull(text) || isNull(text.trim());
    }

    @SuppressLint("DefaultLocale")
    public static CharSequence capitaliseFirstLetter(String string) {
        return string.substring(0, 1).toUpperCase() + string.substring(1, string.length());
    }

    @SuppressWarnings("null")
    @SuppressLint("DefaultLocale")
    public static CharSequence capitaliseFirstLettersOfFullName(String string) {
        String[] names = string.split("\\s+");
        String[] capitalisedName = new String[2];

        for (int i = 0; i < names.length; i++) {
            capitalisedName[i] = names[i].substring(0, 1).toUpperCase() + names[i].substring(1, names[i].length());
        }

        return capitalisedName[0] + " " + capitalisedName[1];
    }

    /**
     * is null or its length is 0 or it is made by space
     * <p/>
     * <pre>
     * isBlank(null) = true;
     * isBlank(&quot;&quot;) = true;
     * isBlank(&quot;  &quot;) = true;
     * isBlank(&quot;a&quot;) = false;
     * isBlank(&quot;a &quot;) = false;
     * isBlank(&quot; a&quot;) = false;
     * isBlank(&quot;a b&quot;) = false;
     * </pre>
     *
     * @param str
     * @return if string is null or its size is 0 or it is made by space, return true, else return false.
     */
    public static boolean isBlank(String str) {
        return (str == null || str.trim().length() == 0);
    }

    /**
     * is null or its length is 0
     * <p/>
     * <pre>
     * isEmpty(null) = true;
     * isEmpty(&quot;&quot;) = true;
     * isEmpty(&quot;  &quot;) = false;
     * </pre>
     *
     * @param str
     * @return if string is null or its size is 0, return true, else return false.
     */
    public static boolean isEmpty(String str) {
        return (str == null || str.length() == 0);
    }

    /**
     * compare two string
     *
     * @param actual
     * @param expected
     * @return
     * @see sgo.mobile.banksulteng.frameworks.array.ObjectUtils#isEquals(Object, Object)
     */
    public static boolean isEquals(String actual, String expected) {
        return ObjectUtils.isEquals(actual, expected);
    }

    /**
     * null string to empty string
     * <p/>
     * <pre>
     * nullStrToEmpty(null) = &quot;&quot;;
     * nullStrToEmpty(&quot;&quot;) = &quot;&quot;;
     * nullStrToEmpty(&quot;aa&quot;) = &quot;aa&quot;;
     * </pre>
     *
     * @param str
     * @return
     */
    public static String nullStrToEmpty(String str) {
        return (str == null ? "" : str);
    }

    /**
     * capitalize first letter
     * <p/>
     * <pre>
     * capitalizeFirstLetter(null)     =   null;
     * capitalizeFirstLetter("")       =   "";
     * capitalizeFirstLetter("2ab")    =   "2ab"
     * capitalizeFirstLetter("a")      =   "A"
     * capitalizeFirstLetter("ab")     =   "Ab"
     * capitalizeFirstLetter("Abc")    =   "Abc"
     * </pre>
     *
     * @param str
     * @return
     */
    public static String capitalizeFirstLetter(String str) {
        if (isEmpty(str)) {
            return str;
        }

        char c = str.charAt(0);
        return (!Character.isLetter(c) || Character.isUpperCase(c)) ? str
                : new StringBuilder(str.length()).append(Character.toUpperCase(c)).append(str.substring(1)).toString();
    }

    /**
     * encoded in utf-8
     * <p/>
     * <pre>
     * utf8Encode(null)        =   null
     * utf8Encode("")          =   "";
     * utf8Encode("aa")        =   "aa";
     * utf8Encode("啊啊啊啊")   = "%E5%95%8A%E5%95%8A%E5%95%8A%E5%95%8A";
     * </pre>
     *
     * @param str
     * @return
     * @throws java.io.UnsupportedEncodingException if an error occurs
     */
    public static String utf8Encode(String str) {
        if (!isEmpty(str) && str.getBytes().length != str.length()) {
            try {
                return URLEncoder.encode(str, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException("UnsupportedEncodingException occurred. ", e);
            }
        }
        return str;
    }

    /**
     * encoded in utf-8, if exception, return defultReturn
     *
     * @param str
     * @param defultReturn
     * @return
     */
    public static String utf8Encode(String str, String defultReturn) {
        if (!isEmpty(str) && str.getBytes().length != str.length()) {
            try {
                return URLEncoder.encode(str, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                return defultReturn;
            }
        }
        return str;
    }

    public static String getHost(String url) {
        if (TextUtils.isEmpty(url))
            return "";

        int doubleslash = url.indexOf("//");
        if (doubleslash == -1)
            doubleslash = 0;
        else
            doubleslash += 2;

        int end = url.indexOf('/', doubleslash);
        end = (end >= 0) ? end : url.length();

        return url.substring(doubleslash, end);
    }

    public static String capitalize(final String str) {
        int strLen;
        if (str == null || (strLen = str.length()) == 0)
            return str;

        char firstChar = str.charAt(0);
        if (Character.isTitleCase(firstChar))
            return str;

        return new StringBuilder(strLen)
                .append(Character.toTitleCase(firstChar))
                .append(str.substring(1))
                .toString();
    }

    public static String escapeHtml(final String text) {
        if (text == null)
            return "";

        StringBuilder out = new StringBuilder();
        int length = text.length();

        for (int i = 0; i < length; i++) {
            char c = text.charAt(i);

            if (c == '<') {
                out.append("&lt;");
            } else if (c == '>') {
                out.append("&gt;");
            } else if (c == '&') {
                out.append("&amp;");
            } else if (c > 0x7E || c < ' ') {
                out.append("&#").append((int) c).append(";");
            } else if (c == ' ') {
                while (i + 1 < length && text.charAt(i + 1) == ' ') {
                    out.append("&nbsp;");
                    i++;
                }

                out.append(' ');
            } else {
                out.append(c);
            }
        }

        return out.toString();
    }

    public static String[] mergeStringArrays(String array1[], String array2[]) {
        if (array1 == null || array1.length == 0)
            return array2;
        if (array2 == null || array2.length == 0)
            return array1;
        List<String> array1List = Arrays.asList(array1);
        List<String> array2List = Arrays.asList(array2);
        List<String> result = new ArrayList<String>(array1List);
        List<String> tmp = new ArrayList<String>(array1List);
        tmp.retainAll(array2List);
        result.addAll(array2List);
        return ((String[]) result.toArray(new String[result.size()]));
    }

    /**
     * Inserts a given string into another padding it with spaces. Is aware if
     * the insertion point has a space on either end and does not add extra
     * spaces.
     *
     * @param s              the string to insert into
     * @param insertAt       the position to insert the string
     * @param stringToInsert the string to insert
     * @return the result of inserting the stringToInsert into the passed in
     * string
     * @throws IndexOutOfBoundsException if the insertAt is negative, or insertAt is larger than the
     *                                   length of s String object
     */
    public static String insertPadded(String s, int insertAt,
                                      String stringToInsert) {
        if (isEmptyOrNull(stringToInsert)) {
            return s;
        }

        if (insertAt < 0) {
            throw new IndexOutOfBoundsException("Invalid insertAt of ["
                    + insertAt + "] for string [" + s + "]");
        }

        StringBuilder newText = new StringBuilder();
        if (insertAt > 0) {
            newText.append(s.substring(0, insertAt));
            if (newText.lastIndexOf(SINGLE_SPACE) != newText.length() - 1) {
                newText.append(SINGLE_SPACE);
            }
            newText.append(stringToInsert);
            String postItem = s.substring(insertAt);
            if (postItem.indexOf(SINGLE_SPACE) != 0) {
                newText.append(SINGLE_SPACE);
            }
            newText.append(postItem);
        } else {
            newText.append(stringToInsert);
            if (s.indexOf(SINGLE_SPACE) != 0) {
                newText.append(SINGLE_SPACE);
            }
            newText.append(s);
        }
        return newText.toString();
    }

    /**
     * Inserts a given string into another padding it with spaces. Is aware if
     * the insertion point has a space on either end and does not add extra
     * spaces. If the string-to-insert is already present (and not part of another word)
     * we return the original string unchanged.
     *
     * @param s              the string to insert into
     * @param insertAt       the position to insert the string
     * @param stringToInsert the string to insert
     * @return the result of inserting the stringToInsert into the passed in
     * string
     * @throws IndexOutOfBoundsException if the insertAt is negative, or insertAt is larger than the
     *                                   length of s String object
     */
    public static String insertPaddedIfNeeded(String s, int insertAt,
                                              String stringToInsert) {
        if (isEmptyOrNull(stringToInsert)) {
            return s;
        }

        boolean found = false;
        int startPos = 0;

        while ((startPos < s.length()) && (!found)) {
            int pos = s.indexOf(stringToInsert, startPos);

            if (pos < 0)
                break;

            startPos = pos + 1;
            int before = pos - 1;
            int after = pos + stringToInsert.length();

            if (((pos == 0) || (Character.isWhitespace(s.charAt(before)))) &&
                    ((after >= s.length()) || (Character.isWhitespace(s.charAt(after)))))
                found = true;
        }

        if (found) {
            StringBuilder newText = new StringBuilder(s);

            if (newText.lastIndexOf(SINGLE_SPACE) != newText.length() - 1) {
                newText.append(SINGLE_SPACE);
            }

            return (newText.toString());
        } else
            return (insertPadded(s, insertAt, stringToInsert));
    }

    /**
     * Checks the passed in string to see if it is null or an empty string
     *
     * @param s the string to check
     * @return true if null or ""
     */
    public static boolean isEmptyOrNull(String s) {
        return s == null || s.length() == 0;
    }

    public static String pad(int c) {
        if (c >= 10)
            return String.valueOf(c);
        else
            return "0" + String.valueOf(c);
    }

    public static String pad2(int c) {
        if (c == 12)
            return String.valueOf(c);
        if (c == 00)
            return String.valueOf(c + 12);
        if (c > 12)
            return String.valueOf(c - 12);
        else
            return String.valueOf(c);
    }

    public static String pad3(int c) {
        if (c == 12)
            return " PM";
        if (c == 00)
            return " AM";
        if (c > 12)
            return " PM";
        else
            return " AM";
    }


    public static boolean isEmailValid(String email) {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if (matcher.matches())
            return true;
        else
            return false;
    }


    public static boolean isBenefName(String benefName) {
        String regExpn =
                "^([a-zA-Z]+(_[a-zA-Z]+)*)(\\s([a-zA-Z]+(_[a-zA-Z]+)*))*$";

        CharSequence inputStr = benefName;

        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if (matcher.matches())
            return true;
        else
            return false;
    }
}

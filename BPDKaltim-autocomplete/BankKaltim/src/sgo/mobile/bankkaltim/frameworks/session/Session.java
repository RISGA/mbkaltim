package sgo.mobile.bankkaltim.frameworks.session;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;
import com.activeandroid.ActiveAndroid;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.bankkaltim.AppHelper;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.activities.ActivityLogin;
import sgo.mobile.bankkaltim.app.activities.BaseActivity;
import sgo.mobile.bankkaltim.app.models.BankListModel;
import sgo.mobile.bankkaltim.app.models.MenuBillerModelPayment;
import sgo.mobile.bankkaltim.app.models.MenuBillerModelPurchase;
import sgo.mobile.bankkaltim.app.models.MenuSettingModel;
import sgo.mobile.bankkaltim.app.models.payment.*;
import sgo.mobile.bankkaltim.app.models.purchase.*;
import sgo.mobile.bankkaltim.conf.AplConstants;
import sgo.mobile.bankkaltim.frameworks.net.loopj.android.http.CustomSSLSocketFactory;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * Created by SGO-Mobile-DEV on 2/11/2015.
 */
public class Session {
    private ProgressDialog pDialog;
    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");//dd/MM/yyyy
    Date now = new Date();
    String dateTime = sdfDate.format(now);
    Context context;
    BaseActivity baseActivity;


    public Session(Context _context){
        this.context=_context;
    }

    public void signOut() {
        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(context));
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
        RequestParams params = new RequestParams();

        try {

            String user_id = AppHelper.getUserId(context);
            String bankcode = AplConstants.Bankcode;
            String URL = AplConstants.SignOutMobileAPI;
            String appName = AplConstants.appName;
            String serviceName = URL.split("/")[4];
            String apiKey = AppHelper.getAccessKey(context);
            UUID uniqueKey = UUID.randomUUID();
            String message = uniqueKey+dateTime + appName + serviceName+ bankcode + user_id;
            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] hmacData = sha256_HMAC.doFinal(message.getBytes("UTF-8"));
            String signatureKey = new String(encodeUrlSafe(hmacData));
            params.put("user_id", user_id);
            params.put("bankcode", bankcode);
            params.put("rc_uuid", uniqueKey.toString());
            params.put("rc_dtime",dateTime );
            params.put("signature",signatureKey);



        }catch (Exception e){
            System.out.println("Error");
        }

        client.post(AplConstants.SignOutMobileAPI, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject object) {

                try {
                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    String error_code = object.getString("error_code");
                    String error_message = object.getString("error_message");
                    if (error_code.equals("0000")) {
                        BankListModel.deleteAll();
                        MenuBillerModelPayment.deleteAll();
                        MenuBillerModelPurchase.deleteAll();
                        PaymentElectricityModel.deleteAll();
                        PaymentInstallmentModel.deleteAll();
                        PaymentInsuranceModel.deleteAll();
                        PaymentInternetModel.deleteAll();
                        PaymentPamModel.deleteAll();
                        PaymentPhoneModel.deleteAll();
                        PaymentTaxModel.deleteAll();
                        PaymentTicketModel.deleteAll();
                        PaymentTvCableModel.deleteAll();
                        PurchaseVoucherPhoneModel.deleteAll();
                        PurchaseCatalogPhoneModel.deleteAll();
                        PurchaseNominalPhoneModel.deleteAll();
                        PurchaseCatalogElectricityModel.deleteAll();
                        PurchaseNominalElectricityModel.deleteAll();
                        MenuSettingModel.deleteAll();
                        if (ActiveAndroid.inTransaction()) {
                            ActiveAndroid.endTransaction();
                        }

                        SecurePreferences prefs = new SecurePreferences(context);
                        prefs.edit().remove("sourceAccountData").commit();
                        prefs.edit().remove("destAccountData").commit();

                        Intent i = new Intent(context, ActivityLogin.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(i);
                        try {
                            trimCache(context);
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        System.exit(0);

                    } else {
                        Toast.makeText(context, error_message, Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();

                }
            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (pDialog != null) {
                    pDialog.dismiss();
                }
                if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(context);
                    alert.setMessage(R.string.SSLhandler_dialog_message + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                } else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(context);
                    alert.setMessage(R.string.SSLhandler_dialog_message + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                }
                Intent i = new Intent(context, ActivityLogin.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            }


        });
    }

    public static void trimCache(Context context) {
        try {
            File dir = context.getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }
    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        // The directory is now empty so delete it
        return dir.delete();
    }

    public static byte[] encodeUrlSafe(byte[] data) {
        byte[] encode = Base64.encodeBase64(data);
        for (int i = 0; i < encode.length; i++) {
            if (encode[i] == '+') {
                encode[i] = '-';
            } else if (encode[i] == '=') {
                encode[i] = '_';
            } else if (encode[i] == '/') {
                encode[i] = '~';
            }
        }
        return encode;
    }


}

package sgo.mobile.bankkaltim.frameworks.validation;

public class CreditCardsChecker {

    //    Issuing Network           IIN Ranges              Active              Length          Validation
    //    American Express          34, 37                  Yes                 15              Luhn algorithm
    //    Bankcard                  5610, 560221-560225     No                  16              Luhn algorithm
    //    China UnionPay            62                      Yes                 16              unknown
    //    Diners Club Carte Blanche	300-305                 Yes                 14              Luhn algorithm
    //    Diners Club enRoute       2014, 2149              No                  15              no validation
    //    Diners Club International	36, 38, 39              Yes                 14              Luhn algorithm
    //    Diners Club US & CA       54, 55                  Yes                 16              Luhn algorithm
    //    Discover Card             6011, 622126-622925     Yes                 16              Luhn algorithm
    //                              644-649, 65
    //    InstaPayment              637-639                 Yes                 16              Luhn algorithm
    //    JCB                       3528-3589               Yes                 16              Luhn algorithm
    //    Laser                     6304, 6706, 6771, 6709	Yes                 16-19           Luhn algorithm
    //    Maestro                   5018, 5020, 5038, 6304, Yes                 12-19           Luhn algorithm
    //                              6759, 6761, 6763
    //    MasterCard                51-55                   Yes                 16              Luhn algorithm
    //
    //    Solo                      6334, 6767              Yes                 16, 18, 19      Luhn algorithm
    //    Switch                    4903, 4905, 4911, 4936, Yes                 16, 18, 19      Luhn algorithm
    //                              564182, 633110, 6333,
    //                              6759
    //    Visa                      4                       Yes                 16              Luhn algorithm
    //    Visa Electron             4026, 417500, 4508,     Yes                 16              Luhn algorithm
    //                              4844, 4913, 4917

    // top level card Issuer ID from http://en.wikipedia.org/wiki/Credit_card_numbers

    public enum CreditCardType { CreditCardTypeUnknown,
        CreditCardTypeVisa,
        CreditCardTypeMastercard,
        CreditCardTypeMaestro,
        CreditCardTypeAmex,
        CreditCardTypeDinersClubCarteBlanche,   // need to differentiate because of number length
        CreditCardTypeDinersClubUS,            // need to differentiate because of number length
        CreditCardTypeDinersClubInternational, // need to differentiate because of number length
        CreditCardTypeSwitch,
        CreditCardTypeDiscover,
        CreditCardTypeJCB,
        CreditCardTypeSolo,
        CreditCardTypeVisaElectron,
        CreditCardTypeLaser,
        CreditCardTypeInstaPayment,
    };

    public enum CreditCardNumberLength {
        CreditCardNumberLengthUnknown,
        CreditCardNumberLengthFourteenDigits,
        CreditCardNumberLengthTwelveToNineteenDigits,
        CreditCardNumberLengthFifteenDigits,
        CreditCardNumberLengthSixteenDigits,
        CreditCardNumberLengthSixteenToNineteenDigits,
        CreditCardNumberLengthSixteenEighteenOrNineteenDigits,
    };

    public static boolean isAValidCreditCardNumber(String cardNum){
        CreditCardType cardType = getCreditCardType(cardNum);
        return (isCardLengthCorrect(cardNum, cardType) && passesLhunCheck(cardNum));
    }

    public static CreditCardType getCreditCardType(String cardNumber){

        if(cardNumber.length() < 6)
            return CreditCardType.CreditCardTypeUnknown;

        int issuerID = Integer.parseInt(cardNumber.substring(0,6));

        if(issuerID <= 300000)
            return CreditCardType.CreditCardTypeUnknown;

        if(issuerID >= 300000 && issuerID <= 305999)
            return CreditCardType.CreditCardTypeDinersClubCarteBlanche;

        if(issuerID >= 352800 && issuerID <= 358999)
            return CreditCardType.CreditCardTypeJCB;

        if(issuerID >= 360000 && issuerID <= 369999)
            return CreditCardType.CreditCardTypeDinersClubInternational;
        if(issuerID >= 380000 && issuerID <= 389999)    // Not in Wikipedia list but is mentioned here: http://www.merriampark.com/anatomycc.htm
            return CreditCardType.CreditCardTypeDinersClubInternational;
        if(issuerID >= 390000 && issuerID <= 399999)    // Not in Wikipedia list but is mentioned here: http://www.merriampark.com/anatomycc.htm
            return CreditCardType.CreditCardTypeDinersClubInternational;

        if(issuerID >= 340000 && issuerID <= 349999)
            return CreditCardType.CreditCardTypeAmex;
        if(issuerID >= 370000 && issuerID <= 379999)
            return CreditCardType.CreditCardTypeAmex;

        if(issuerID <= 400000)
            return CreditCardType.CreditCardTypeUnknown;

        // Visa own all of 4xxx xxxx xxxx xxxx but issues some cards as Switch.
        if(issuerID >= 490300 && issuerID <= 490399)
            return CreditCardType.CreditCardTypeSwitch;
        if(issuerID >= 490500 && issuerID <= 490599)
            return CreditCardType.CreditCardTypeSwitch;
        if(issuerID >= 491100 && issuerID <= 491199)
            return CreditCardType.CreditCardTypeSwitch;
        if(issuerID >= 493600 && issuerID <= 493699)
            return CreditCardType.CreditCardTypeSwitch;

        if(issuerID >= 402600 && issuerID <= 402699)
            return CreditCardType.CreditCardTypeVisaElectron;
        if(issuerID == 417500)
            return CreditCardType.CreditCardTypeVisaElectron;
        if(issuerID >= 450800 && issuerID <= 450899)
            return CreditCardType.CreditCardTypeVisaElectron;
        if(issuerID >= 484400 && issuerID <= 484499)
            return CreditCardType.CreditCardTypeVisaElectron;
        if(issuerID >= 491300 && issuerID <= 491399)
            return CreditCardType.CreditCardTypeVisaElectron;
        if(issuerID >= 491700 && issuerID <= 491799)
            return CreditCardType.CreditCardTypeVisaElectron;

        // Visa own all of 4xxx xxxx xxxx xxxx but issues some cards as Switch or electron
        // So we can only do this check after the other 4xxxxx checks above
        if(issuerID >= 400000 && issuerID <= 499999)
            return CreditCardType.CreditCardTypeVisa;
        if(issuerID >= 510000 && issuerID <= 519999)
            return CreditCardType.CreditCardTypeMastercard;
        if(issuerID >= 520000 && issuerID <= 529999)
            return CreditCardType.CreditCardTypeMastercard;
        if(issuerID >= 530000 && issuerID <= 539999)
            return CreditCardType.CreditCardTypeMastercard;

        if(issuerID >= 540000 && issuerID <= 549999)                // Issued by Diners in US and CA but processed by Mastercard.
            return CreditCardType.CreditCardTypeDinersClubUS;
        if(issuerID >= 550000 && issuerID <= 559999)                // Issued by Diners in US and CA but processed by Mastercard.
            return CreditCardType.CreditCardTypeDinersClubUS;

        if(issuerID == 564182)
            return CreditCardType.CreditCardTypeSwitch;

        if(issuerID <= 600000)
            return CreditCardType.CreditCardTypeUnknown;

        if(issuerID >= 601100 && issuerID <= 601199)
            return CreditCardType.CreditCardTypeDiscover;
        if(issuerID >= 622126 && issuerID <= 622925)
            return CreditCardType.CreditCardTypeDiscover;
        if(issuerID >= 644000 && issuerID <= 649999)
            return CreditCardType.CreditCardTypeDiscover;
        if(issuerID >= 650000 && issuerID <= 659999)
            return CreditCardType.CreditCardTypeDiscover;

        if(issuerID >= 637000 && issuerID <= 63999 )
            return CreditCardType.CreditCardTypeInstaPayment;

        if(issuerID >= 633400 && issuerID <= 633499)
            return CreditCardType.CreditCardTypeSolo;
        if(issuerID >= 676700 && issuerID <= 676799)
            return CreditCardType.CreditCardTypeSolo;

        if(issuerID >= 633300 && issuerID <= 633399)
            return CreditCardType.CreditCardTypeSwitch;
        if(issuerID >= 675900 && issuerID <= 675999)
            return CreditCardType.CreditCardTypeSwitch;
        if(issuerID == 633110)
            return CreditCardType.CreditCardTypeSwitch;

        if(issuerID >= 675900 && issuerID <= 675999)
            return CreditCardType.CreditCardTypeLaser;
        if(issuerID >= 630400 && issuerID <= 630499)
            return CreditCardType.CreditCardTypeLaser;
        if(issuerID >= 670600 && issuerID <= 670699)
            return CreditCardType.CreditCardTypeLaser;
        if(issuerID >= 677100 && issuerID <= 677199)
            return CreditCardType.CreditCardTypeLaser;
        if(issuerID >= 670900 && issuerID <= 670999)
            return CreditCardType.CreditCardTypeLaser;

        if(issuerID >= 501800 && issuerID <= 501899)
            return CreditCardType.CreditCardTypeMaestro;
        if(issuerID >= 502000 && issuerID <= 502099)
            return CreditCardType.CreditCardTypeMaestro;
        if(issuerID >= 503800 && issuerID <= 503899)
            return CreditCardType.CreditCardTypeMaestro;
        if(issuerID >= 630400 && issuerID <= 630499)
            return CreditCardType.CreditCardTypeMaestro;
        if(issuerID >= 675900 && issuerID <= 675999)
            return CreditCardType.CreditCardTypeMaestro;
        if(issuerID >= 676100 && issuerID <= 676199)
            return CreditCardType.CreditCardTypeMaestro;
        if(issuerID >= 676300 && issuerID <= 676399)
            return CreditCardType.CreditCardTypeMaestro;

        return CreditCardType.CreditCardTypeUnknown;
    }

    private static boolean passesLhunCheck(String cardNum) {

        int oddEven = cardNum.length()%2;
        int length = cardNum.length();
        int lhunTotal = 0;
        int charValue;
        for(int i = 0; i < length ; i++) {
            charValue = Integer.parseInt(cardNum.substring(i,i+1));

            if(i%2 == oddEven) {          // because we are zero based, we double the 'even' digits.
                charValue = charValue * 2;
                if(charValue > 9)
                    charValue = charValue - 9;
            }
            lhunTotal += charValue;
        }

        int lhunChecksum =  lhunTotal % 10;
        if(lhunChecksum == 0)
            return true;
        else
            return false;
    }


    private static boolean isCardLengthCorrect(String cardNum, CreditCardType type){

        int length = cardNum.length();

        switch(getCreditCardNumberLengthFromCardType(type)) {
            case CreditCardNumberLengthUnknown:
                return false;
            case CreditCardNumberLengthFourteenDigits:
                if(length == 14)
                    return true;
                else
                    return false;
            case CreditCardNumberLengthTwelveToNineteenDigits:
                if(length >=12 && length <=19)
                    return true;
                else
                    return false;
            case CreditCardNumberLengthFifteenDigits:
                if(length == 15)
                    return true;
                else
                    return false;
            case CreditCardNumberLengthSixteenDigits:
                if(length == 16)
                    return true;
                else
                    return false;
            case CreditCardNumberLengthSixteenToNineteenDigits:
                if(length >=16 && length <=19)
                    return true;
                else
                    return false;
            case CreditCardNumberLengthSixteenEighteenOrNineteenDigits:
                if(length ==16 || length== 18 || length ==19)
                    return true;
                else
                    return false;
        }
        return false;

    }

    private static CreditCardNumberLength getCreditCardNumberLengthFromCardType(CreditCardType aCardType) {

        switch(aCardType){
            case CreditCardTypeUnknown:
                return CreditCardNumberLength.CreditCardNumberLengthUnknown;

            case CreditCardTypeVisa:
                return CreditCardNumberLength.CreditCardNumberLengthSixteenDigits;

            case CreditCardTypeMastercard:
                return CreditCardNumberLength.CreditCardNumberLengthSixteenDigits;

            case CreditCardTypeMaestro:
                return CreditCardNumberLength.CreditCardNumberLengthTwelveToNineteenDigits;

            case CreditCardTypeAmex:
                return CreditCardNumberLength.CreditCardNumberLengthFifteenDigits;

            case CreditCardTypeDinersClubCarteBlanche:
                return CreditCardNumberLength.CreditCardNumberLengthFourteenDigits;

            case CreditCardTypeDinersClubUS:
                return CreditCardNumberLength.CreditCardNumberLengthSixteenDigits;

            case CreditCardTypeDinersClubInternational:
                return CreditCardNumberLength.CreditCardNumberLengthFourteenDigits;

            case CreditCardTypeSwitch:
                return CreditCardNumberLength.CreditCardNumberLengthSixteenEighteenOrNineteenDigits;

            case CreditCardTypeDiscover:
                return CreditCardNumberLength.CreditCardNumberLengthSixteenToNineteenDigits;

            case CreditCardTypeJCB:
                return CreditCardNumberLength.CreditCardNumberLengthSixteenDigits;

            case CreditCardTypeSolo:
                return CreditCardNumberLength.CreditCardNumberLengthSixteenEighteenOrNineteenDigits;

            case CreditCardTypeVisaElectron:
                return CreditCardNumberLength.CreditCardNumberLengthSixteenDigits;

            case CreditCardTypeLaser:
                return CreditCardNumberLength.CreditCardNumberLengthSixteenToNineteenDigits;

            case CreditCardTypeInstaPayment:
                return CreditCardNumberLength.CreditCardNumberLengthSixteenDigits;
        }
        return CreditCardNumberLength.CreditCardNumberLengthUnknown;
    }



}

package sgo.mobile.bankkaltim.frameworks;

import sgo.mobile.bankkaltim.frameworks.text.StringUtils;

public final class Assertions {

    public static void notNull( String name, Object object ) {

        if ( object instanceof String && StringUtils.isNullOrWhiteSpace(object.toString()) ) {
            throw new AssertionError( "The value of \"" + name + "\" can not be null!" );
        }

        if ( object == null ) {
            throw new AssertionError( "The value of \"" + name + "\" can not be null!" );
        }
    }
}
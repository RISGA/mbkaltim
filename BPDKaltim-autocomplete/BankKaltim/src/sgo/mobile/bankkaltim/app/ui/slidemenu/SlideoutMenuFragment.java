package sgo.mobile.bankkaltim.app.ui.slidemenu;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import sgo.mobile.bankkaltim.AppHelper;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.activities.ActivityLogin;
import sgo.mobile.bankkaltim.app.activities.MainActivity;
import sgo.mobile.bankkaltim.conf.AplConstants;
import sgo.mobile.bankkaltim.app.fragments.*;


public class SlideoutMenuFragment extends SherlockFragment {
    private static SlideoutMenuFragment instance;
    private ActionBar actionBar;
    private MainActivity mActivity = null;
    FragmentManager fm;
    ActivityLogin al;
    View viewPurchase,viewPayment, viewTransfer, viewReport,viewStatement;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static SlideoutMenuFragment getInstance() {
        if (instance == null) {
            return new SlideoutMenuFragment();
        }
        return instance;
    }

    public SlideoutMenuFragment() {
        instance = this;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.menu_layout, container, false);
        final Button menuHome          = (Button) view.findViewById(R.id.menuHome);
        menuHome.setSelected(true);
        final Button menuTopUp         = (Button) view.findViewById(R.id.menuTopUp);
        final Button menuSendEcash     = (Button) view.findViewById(R.id.menuSendEcash);
        final Button menuSetting       = (Button) view.findViewById(R.id.menuSetting);
        final Button menuAccountStatment = (Button) view.findViewById(R.id.menuAccountStatement);
        final Button menuPurchase = (Button) view.findViewById(R.id.menuPurchase);
        final Button menuPayment = (Button) view.findViewById(R.id.menuPayment);
        final Button menuReport = (Button) view.findViewById(R.id.menuReport);
        viewPayment = view.findViewById(R.id.viewPayment);
        viewPurchase = view.findViewById(R.id.viewPurchase);
        viewTransfer = view.findViewById(R.id.viewTransfer);
        viewReport = view.findViewById(R.id.viewReport);
        viewStatement = view.findViewById(R.id.viewStatement);
        String accessFlag = AppHelper.getAccessFlag(getActivity());

        if (accessFlag.equalsIgnoreCase("2")){
            showMenuTransfer(menuSendEcash);
            showMenuPurchase(menuPurchase);
            showMenuPayment(menuPayment);
            showMenuReport(menuReport);
        }
        else{
            hideMenuTransfer(menuSendEcash);
            hideMenuPurchase(menuPurchase);
            hideMenuPayment(menuPayment);
            hideMenuReport(menuReport);
        }

        String bcd = AplConstants.Bankcode;

        if(bcd.equalsIgnoreCase("134")){
           // hideMenuStatement(menuAccountStatment);
          //  hideMenuPurchase(menuPurchase);
           // hideMenuPayment(menuPayment);
           // hideMenuReport(menuReport);
        }else{
           // showMenuStatement(menuAccountStatment);
           // showMenuPurchase(menuPurchase);
            //showMenuPayment(menuPayment);
           // showMenuReport(menuReport);
        }


        menuHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment newFragment = null;
                newFragment = new MainListFragment();
                switchFragment(newFragment);

                menuHome.setSelected(true);
                menuSendEcash.setSelected(false);
                menuSetting.setSelected(false);
                menuTopUp.setSelected(false);
                menuAccountStatment.setSelected(false);
                menuPayment.setSelected(false);
                menuPurchase.setSelected(false);
                menuReport.setSelected(false);
            }
        });


        menuAccountStatment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment newFragment = null;
                newFragment = new FragmentAccountStatement();
                switchFragment(newFragment);

                menuHome.setSelected(false);
                menuAccountStatment.setSelected(true);
                menuSendEcash.setSelected(false);
                menuSetting.setSelected(false);
                menuTopUp.setSelected(false);
                menuPayment.setSelected(false);
                menuPurchase.setSelected(false);
                menuReport.setSelected(false);
            }
        });



        menuSendEcash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment newFragment = null;
                newFragment = new FragmentMainTransfer();
                switchFragment(newFragment);

                menuSendEcash.setSelected(true);
                menuHome.setSelected(false);
                menuSetting.setSelected(false);
                menuTopUp.setSelected(false);
                menuAccountStatment.setSelected(false);
                menuPayment.setSelected(false);
                menuPurchase.setSelected(false);
                menuReport.setSelected(false);
            }
        });

        menuPurchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment newFragment = null;
                newFragment = new FragmentPurchase();
                switchFragment(newFragment);
                menuPurchase.setSelected(true);
                menuPayment.setSelected(false);
                menuSendEcash.setSelected(false);
                menuHome.setSelected(false);
                menuSetting.setSelected(false);
                menuTopUp.setSelected(false);
                menuAccountStatment.setSelected(false);
                menuReport.setSelected(false);
            }
        });

        menuPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment newFragment = null;
                newFragment = new FragmentPayment();
                switchFragment(newFragment);
                menuPayment.setSelected(true);
                menuPurchase.setSelected(false);
                menuSendEcash.setSelected(false);
                menuHome.setSelected(false);
                menuSetting.setSelected(false);
                menuTopUp.setSelected(false);
                menuAccountStatment.setSelected(false);
                menuReport.setSelected(false);
            }
        });

        menuReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment newFragment = null;
                newFragment = new FragmentReport();
                switchFragment(newFragment);
                menuReport.setSelected(true);
                menuPayment.setSelected(false);
                menuPurchase.setSelected(false);
                menuSendEcash.setSelected(false);
                menuHome.setSelected(false);
                menuSetting.setSelected(false);
                menuTopUp.setSelected(false);
                menuAccountStatment.setSelected(false);
            }
        });


        menuSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment newFragment = null;
                newFragment = new FragmentSetting();
                switchFragment(newFragment);
                menuSetting.setSelected(true);
                menuHome.setSelected(false);
                menuSendEcash.setSelected(false);
                menuTopUp.setSelected(false);
                menuAccountStatment.setSelected(false);
                menuPayment.setSelected(false);
                menuPurchase.setSelected(false);
                menuReport.setSelected(false);

            }
        });


        menuTopUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Fragment newFragment = null;
                newFragment = new FragmentAccountList();
                switchFragment(newFragment);

                menuTopUp.setSelected(true);
                menuSetting.setSelected(false);
                menuHome.setSelected(false);
                menuSendEcash.setSelected(false);
                menuAccountStatment.setSelected(false);
                menuPayment.setSelected(false);
                menuPurchase.setSelected(false);
                menuReport.setSelected(false);
            }
        });
        return view;
    }

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);

        getSherlockActivity().getActionBar().removeAllTabs();
        getSherlockActivity().getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
    }


    public void showMenuTransfer(View target) {
        int ButtonId = R.id.menuSendEcash;
        Button buttonVisibility = (Button) target.findViewById(ButtonId);
        buttonVisibility.setVisibility(View.VISIBLE);
    }

    public void showMenuPurchase(View target) {
        int ButtonPurchase = R.id.menuPurchase;
        Button buttonVsb = (Button) target.findViewById(ButtonPurchase);
        buttonVsb.setVisibility(View.VISIBLE);
    }

    public void showMenuPayment(View target) {
        int ButtonPayment = R.id.menuPayment;
        Button buttonVsbp = (Button) target.findViewById(ButtonPayment);
        buttonVsbp.setVisibility(View.VISIBLE);
    }

    public void showMenuReport(View target) {
        int ButtonReport = R.id.menuReport;
        Button buttonVsbp = (Button) target.findViewById(ButtonReport);
        buttonVsbp.setVisibility(View.VISIBLE);
    }

    public void showMenuStatement(View target) {
        int ButtonPayment = R.id.menuAccountStatement;
        Button buttonVsbp = (Button) target.findViewById(ButtonPayment);
        buttonVsbp.setVisibility(View.VISIBLE);
    }

    public static void hideMenuTransfer(View target) {
        int ButtonId = R.id.menuSendEcash;
        Button buttonVisibility = (Button) target.findViewById(ButtonId);
        buttonVisibility.setVisibility(View.GONE);
    }
    public void hideMenuPurchase(View target) {
        int ButtonPurchase = R.id.menuPurchase;
        Button buttonVsb = (Button) target.findViewById(ButtonPurchase);
        buttonVsb.setVisibility(View.GONE);
        viewPurchase.setVisibility(View.GONE);
    }

    public void hideMenuPayment(View target) {
        int ButtonPayment = R.id.menuPayment;
        Button buttonVsbp = (Button) target.findViewById(ButtonPayment);
        buttonVsbp.setVisibility(View.GONE);
        viewPayment.setVisibility(View.GONE);
    }

    public void hideMenuReport(View target) {
        int ButtonReport = R.id.menuReport;
        Button buttonVsbp = (Button) target.findViewById(ButtonReport);
        buttonVsbp.setVisibility(View.GONE);
        viewReport.setVisibility(View.GONE);
    }

    public void hideMenuStatement(View target) {
        int ButtonReport = R.id.menuAccountStatement;
        Button buttonVsbp = (Button) target.findViewById(ButtonReport);
        buttonVsbp.setVisibility(View.GONE);
        viewStatement.setVisibility(View.GONE);
    }

}

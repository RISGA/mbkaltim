package sgo.mobile.bankkaltim.app.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.*;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;
import com.activeandroid.ActiveAndroid;
import com.securepreferences.SecurePreferences;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.bankkaltim.AppHelper;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.fragments.MainListFragment;
import sgo.mobile.bankkaltim.app.models.*;
import sgo.mobile.bankkaltim.app.models.payment.*;
import sgo.mobile.bankkaltim.app.models.purchase.*;
import sgo.mobile.bankkaltim.app.ui.dialog.DefinedDialog;
import sgo.mobile.bankkaltim.conf.AplConstants;
import sgo.mobile.bankkaltim.frameworks.net.loopj.android.http.*;
import sgo.mobile.bankkaltim.frameworks.session.Session;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class MainActivity extends BaseActivity implements MainListFragment.OnFragmentUpdateListener {
    private ProgressDialog pDialog;
    FragmentManager fm;
    private static final String TAG = "MainActivity";
    private Fragment mContent;
    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");//dd/MM/yyyy
    Date now = new Date();
    String dateTime = sdfDate.format(now);
    public String signatureKey;

    @Override
    public void onFragmentUpdate(int position, boolean forceupdate) {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSlidingMenu().showMenu();
        setContentView(R.layout.fragment_main);
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            Intent i = getIntent();
        }

        fm = getSupportFragmentManager();

        MainListFragment fragment = (MainListFragment)fm.findFragmentByTag("main_fragment");
        if (savedInstanceState == null || fragment == null) {
            fragment = MainListFragment.newInstance();
            FragmentTransaction ft = fm.beginTransaction();
            ft.add(android.R.id.content, fragment, "main_fragment");
            ft.commit();
        }
        this.overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
        actionBarSetup();



    }

    public void SignOut(View view)
    {
        AlertDialog.Builder alertbox=new AlertDialog.Builder(this);
        alertbox.setTitle(getResources().getString(R.string.app_name));
        alertbox.setMessage(getResources().getString(R.string.lbl_exit_message));
        alertbox.setPositiveButton(getResources().getString(R.string.lbl_exit_yes), new
                DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        signOut();
                    }
                });
        alertbox.setNegativeButton(getResources().getString(R.string.lbl_exit_no), new
                DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                });
        alertbox.show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        //unregisterReceiver(connectivityUpdate);
    }

    public void signOut() {
        pDialog = DefinedDialog.CreateProgressDialog(MainActivity.this, pDialog,getResources().getString(R.string.lbl_exit));
        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getApplication()));
        RequestParams params = new RequestParams();

        try {
            String user_id = AppHelper.getUserId(getApplication());
            String bankcode = AplConstants.Bankcode;
            String URL = AplConstants.SignOutMobileAPI;
            String appName = AplConstants.appName;
            String serviceName = URL.split("/")[4];
            String apiKey = AppHelper.getAccessKey(getApplication());
            UUID uniqueKey = UUID.randomUUID();
            String message = uniqueKey+dateTime + appName + serviceName+ bankcode + user_id;
            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] hmacData = sha256_HMAC.doFinal(message.getBytes("UTF-8"));
            signatureKey = new String(encodeUrlSafe(hmacData));
            params.put("user_id", user_id);
            params.put("bankcode", bankcode);
            params.put("rc_uuid", uniqueKey.toString());
            params.put("rc_dtime",dateTime );
            params.put("signature",signatureKey);

        }catch (Exception e){
            System.out.println("Error");
        }

        client.post(AplConstants.SignOutMobileAPI, params, new AsyncHttpResponseHandler() {
            public void onSuccess(String content) {

                try {

                    if (pDialog != null) {
                        pDialog.dismiss();
                    }

                    JSONObject object = new JSONObject(content);
                    String error_code = object.getString("error_code");
                    String error_message = object.getString("error_message");
                    if (error_code.equals("0000")) {
                        Intent intent = new Intent(MainActivity.this, ActivityLogin.class);
                        startActivity(intent);
                        BankListModel.deleteAll();
                        MenuBillerModelPayment.deleteAll();
                        MenuBillerModelPurchase.deleteAll();
                        PaymentElectricityModel.deleteAll();
                        PaymentInstallmentModel.deleteAll();
                        PaymentInsuranceModel.deleteAll();
                        PaymentInternetModel.deleteAll();
                        PaymentPamModel.deleteAll();
                        PaymentPhoneModel.deleteAll();
                        PaymentTaxModel.deleteAll();
                        PaymentTicketModel.deleteAll();
                        PaymentTvCableModel.deleteAll();
                        PurchaseVoucherPhoneModel.deleteAll();
                        PurchaseCatalogPhoneModel.deleteAll();
                        PurchaseNominalPhoneModel.deleteAll();
                        PurchaseCatalogElectricityModel.deleteAll();
                        PurchaseNominalElectricityModel.deleteAll();
                        MenuBillerModelPurchase.deleteAll();
                        PurchaseVoucherKartuAsModel.deleteAll();
                        PurchaseVoucherBoltModel.deleteAll();
                        PurchaseVoucherEsiaModel.deleteAll();
                        PurchaseVoucherSimpatiModel.deleteAll();
                        PurchaseVoucherIm3SmartModel.deleteAll();
                        PurchaseVoucherXlModel.deleteAll();
                        PurchaseVoucherThreeModel.deleteAll();
                        PurchaseVoucherMentariModel.deleteAll();
                        PurchaseVoucherSmartfrenModel.deleteAll();
                        MenuSettingModel.deleteAll();
                        SecurePreferences prefs = new SecurePreferences(getApplicationContext());
                        prefs.edit().remove("sourceAccountData").commit();
                        prefs.edit().remove("destAccountData").commit();
                        System.exit(0);
                    } else {
                        Toast.makeText(getApplication(), error_message, Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();

                }
            }
            public void onFailure(Throwable error, String content) {
                if (pDialog != null) {
                    pDialog.dismiss();
                }
                if(error.getMessage().equalsIgnoreCase("no peer certificate"))
                {
                    pDialog = DefinedDialog.CreateProgressDialog(getApplication(), pDialog, getResources().getString(R.string.SSLhandler_dialog_message) + error.toString());
                }
                else
                {
                    pDialog = DefinedDialog.CreateProgressDialog(getApplication(), pDialog, getResources().getString(R.string.inethandler_dialog_message) + error.toString());
                }
                Intent intent = new Intent(MainActivity.this, ActivityLogin.class);
                startActivity(intent);
                finish();
                System.exit(0);
            }

        });

    }

    @Override
    protected void onResume() {
        super.onResume();
       // LocalBroadcastManager.getInstance(this).registerReceiver();
    }

    @Override
    protected void onPause() {
      //  LocalBroadcastManager.getInstance(this).unregisterReceiver(connectivityUpdate);
        super.onPause();

    }

    @Override
    protected void onDestroy() {
       // LocalBroadcastManager.getInstance(this).unregisterReceiver(connectivityUpdate);
        super.onDestroy();
        BankListModel.deleteAll();
        MenuBillerModelPayment.deleteAll();
        MenuBillerModelPurchase.deleteAll();
        PaymentElectricityModel.deleteAll();
                PaymentInstallmentModel.deleteAll();
                PaymentInsuranceModel.deleteAll();
                PaymentInternetModel.deleteAll();
                PaymentPamModel.deleteAll();
                PaymentPhoneModel.deleteAll();
                PaymentTaxModel.deleteAll();
                PaymentTicketModel.deleteAll();
                PaymentTvCableModel.deleteAll();
        PurchaseVoucherPhoneModel.deleteAll();
        PurchaseCatalogPhoneModel.deleteAll();
        PurchaseNominalPhoneModel.deleteAll();
        PurchaseCatalogElectricityModel.deleteAll();
        PurchaseNominalElectricityModel.deleteAll();
        MenuSettingModel.deleteAll();
        if(ActiveAndroid.inTransaction()){
            ActiveAndroid.endTransaction();
        }

        SecurePreferences prefs = new SecurePreferences(getApplicationContext());
        prefs.edit().remove("sourceAccountData").commit();
        prefs.edit().remove("destAccountData").commit();
        Session session;
        session = new Session(MainActivity.this);
        session.signOut();
        super.onDestroy();
    }

    public void switchContent(Fragment fragment) {
        mContent = fragment;
        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, fragment).commit();
        getSlidingMenu().showContent();
    }

    public void togglerBroadcastReceiver(Boolean _on, BroadcastReceiver _myreceiver){
        Log.wtf("masuk turnOnBR", "oke");
        if(_on){
            IntentFilter filter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
            registerReceiver(_myreceiver,filter);
        }
        else unregisterReceiver(_myreceiver);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            AlertDialog.Builder alertbox=new AlertDialog.Builder(this);
            alertbox.setTitle(getResources().getString(R.string.app_name));
            alertbox.setMessage(getResources().getString(R.string.lbl_exit_message));
            alertbox.setPositiveButton(getResources().getString(R.string.lbl_exit_yes), new
                    DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            Session session;
                            session = new Session(getApplicationContext());
                            session.signOut();
                            try {
                                trimCache(getApplicationContext());
                            } catch (Exception e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                            android.os.Process.killProcess(android.os.Process.myPid());
                            System.exit(0);

                        }
                    });
            alertbox.setNegativeButton(getResources().getString(R.string.lbl_exit_no), new
                    DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {}
                    });
            alertbox.show();
        }
        return super.onKeyDown(keyCode, event);
    }

    public static byte[] encodeUrlSafe(byte[] data) {
        byte[] encode = Base64.encodeBase64(data);
        for (int i = 0; i < encode.length; i++) {
            if (encode[i] == '+') {
                encode[i] = '-';
            } else if (encode[i] == '=') {
                encode[i] = '_';
            } else if (encode[i] == '/') {
                encode[i] = '~';
            }
        }
        return encode;
    }


    public static void trimCache(Context context) {
        try {
            File dir = context.getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }
    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        // The directory is now empty so delete it
        return dir.delete();
    }


}

package sgo.mobile.bankkaltim.app.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.slidingmenu.lib.SlidingMenu;
import sgo.mobile.bankkaltim.AppHelper;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.ui.slidemenu.SlideoutMenuFragment;
import sgo.mobile.bankkaltim.app.ui.slidemenu.SlidingSherlockFragmentActivity;

public abstract class BaseActivity extends SlidingSherlockFragmentActivity implements ActionBar.OnNavigationListener {

	private static final String TAG = "BaseActivity";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addSlideMenu();
        actionBarSetup();

    }

    @SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	protected void addSlideMenu() {
        setBehindContentView(R.layout.fragment_slideoutmenu);

        FragmentTransaction t = this.getSupportFragmentManager().beginTransaction();
        Fragment fragment = new SlideoutMenuFragment();
        t.replace(R.id.fragment_slideoutmenu, fragment);
        t.commit();
        
        Point screenSize = AppHelper.screenSize(getApplicationContext());
        SlidingMenu mSlidingMenu = getSlidingMenu();
        mSlidingMenu.setShadowWidthRes(R.dimen.shadow_width);
        mSlidingMenu.setShadowDrawable(R.drawable.shadow);
        mSlidingMenu.setSelectorEnabled(true);
        mSlidingMenu.setSelectorDrawable(R.drawable.shadow_second);

        mSlidingMenu.setBehindWidth((int) (screenSize.x * 0.85));

        setSlidingActionBarEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater = getSupportMenuInflater();
        inflater.inflate(R.menu.mainmenu, menu);
        return true;
    }

    protected void actionBarSetup()
    {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(R.string.ab_title); 
        actionBar.setSubtitle(R.string.ab_subtitle);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
        	actionBar.setHomeButtonEnabled(true);
        }
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                toggle();
            }
        }, 100);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {

        switch(item.getItemId()) {
        case android.R.id.home:
        	getSlidingMenu().toggle();
            return true;
        case R.id.action_about:
           Intent intent = new Intent(this, ActivityAbout.class);
           startActivity(intent);
            return true;
        case R.id.action_about_apps:
                Intent intents = new Intent(this, ActivityAboutApps.class);
                startActivity(intents);
                return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }

	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		return true;
	}

    public int getDpAsPxFromResource(int res) {
        return (int)getResources().getDimension(res);
    }
}

package sgo.mobile.bankkaltim.app.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.widget.ProgressBar;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import sgo.mobile.bankkaltim.R;

public class ActivityAbout extends SherlockActivity {
    WebView mWebView;
    final Activity activity = this;
    ProgressBar prgLoading;
    /** Called when the activity is first created. */
    private ProgressDialog pDialog;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        //prgLoading = (ProgressBar)findViewById(R.id.prgLoading);
      //  actionBarSetup();
        actionBarSetup();

        // Adds Progrss bar Support
        //  activity.getWindow().requestFeature(Window.FEATURE_PROGRESS)


        // Makes Progress bar Visible
        //  getWindow().setFeatureInt(Window.FEATURE_PROGRESS, Window.PROGRESS_VISIBILITY_ON);

        // Get Web view
        mWebView = (WebView) findViewById( R.id.webView1); //This is the id you gave
        //to the WebView in the main.xml
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setSupportZoom(true);       //Zoom Control on web (You don't need this
        //if ROM supports Multi-Touch
        mWebView.getSettings().setBuiltInZoomControls(true); //Enable Multitouch if supported by ROM
        // Load URL
        mWebView.loadUrl("http://www.bankaltim.co.id/");
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {

     //   prgLoading.setVisibility(View.GONE);
            }}, 1000);  // 3000 milliseconds

       /* final ActionBar actionBar = getSupportActionBar();
        mWebView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress)
            {

                actionBar.setDisplayShowTitleEnabled(true);
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setTitle("Loading...");
                actionBar.setSubtitle("");
                actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                    actionBar.setHomeButtonEnabled(true);
                }
                actionBar.setIcon(R.id.prgLoading);
                //Make the bar disappear after URL is loaded, and changes string to Loading...
               // activity.setTitle("Loading...");

              //  activity.setProgress(progress * 100); //Make the bar disappear after URL is loaded
                prgLoading.setVisibility(View.GONE);
                // Return the app name after finish loading
                if(progress == 100)
                    actionBar.setTitle(R.string.app_name);

            }
        });*/
    }


    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
                break;
          /*  case R.id.action_about_apps:
                Intent intents = new Intent(this, ActivityAboutApps.class);
                startActivity(intents);
                break;*/
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(event.getAction() == KeyEvent.ACTION_DOWN){
            switch(keyCode)
            {
                case KeyEvent.KEYCODE_BACK:
                    if( mWebView.canGoBack() == true){
                        mWebView.goBack();
                    }else{
                        finish();
                    }
                    return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getSupportMenuInflater();
        inflater.inflate(R.menu.mainmenu, menu);
        return true;
    }*/

    protected void actionBarSetup()
    {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(R.string.ab_title);
        actionBar.setSubtitle(R.string.ab_subtitle);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            actionBar.setHomeButtonEnabled(true);
        }
    }
}

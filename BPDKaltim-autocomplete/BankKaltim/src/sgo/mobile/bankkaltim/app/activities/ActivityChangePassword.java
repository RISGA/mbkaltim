package sgo.mobile.bankkaltim.app.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.Header;
import org.json.JSONObject;
import sgo.mobile.bankkaltim.AppHelper;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.ui.dialog.DefinedDialog;
import sgo.mobile.bankkaltim.conf.AplConstants;
import sgo.mobile.bankkaltim.frameworks.net.Networks;
import sgo.mobile.bankkaltim.frameworks.net.loopj.android.http.CustomSSLSocketFactory;
import sgo.mobile.bankkaltim.frameworks.security.Encrypt;
import sgo.mobile.bankkaltim.frameworks.session.Session;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * Created by SGO-Mobile-DEV on 3/6/2015.
 */
public class ActivityChangePassword extends Activity {


    private boolean isExit;
    static Activity thisActivity = null;
    Encrypt encrypt = new Encrypt();
    MainActivity mainActivity;
    ChangePasswordReport changePasswordReport;
    boolean networks;
    boolean editclicked = false;

    TextView txt_username, lbl_username, lbl_change_password, lbl_change_new_password1, lbl_change_new_password2, lbl_header;
    EditText txt_change_password, txt_change_new_password1, txt_change_new_password2;
    Button btnDone, btnCancel;
    public String userId, pcd, bcode;

    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");//dd/MM/yyyy
    Date now = new Date();
    String dateTime = sdfDate.format(now);
    private ProgressDialog pDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // getActionBar().hide();
        setContentView(R.layout.activity_change_password);
        findViews();
        thisActivity = this;
        // thisActivity.overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.slide_out_to_bottom);
        // Bundle  bundle = getIntent().getExtras();
        // userId = bundle.getString("userId");


    }


    private void Submit() {
        pDialog = DefinedDialog.CreateProgressDialog(thisActivity, pDialog, getResources().getString(R.string.lbl_loading_process));
        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getApplicationContext()));
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
        RequestParams params = new RequestParams();

        try {
            String userid = AppHelper.getUserId(getApplicationContext());
            String bankcode = AplConstants.Bankcode;
            String pcode = AplConstants.pcd;
            String currentpwd = txt_change_password.getText().toString();
            String newpwd1 = txt_change_new_password1.getText().toString();
            String newpwd2 = txt_change_new_password2.getText().toString();
            String encpwd1 = encrypt.aes_encrypt(currentpwd, userid);
            String encpwd2 = encrypt.aes_encrypt(newpwd1, userid);
            String encpwd3 = encrypt.aes_encrypt(newpwd2, userid);
            // String pcd = AplConstants.pcd;
            // String txtOTP = txtOtp.getText().toString().trim();
            String URL = AplConstants.ChangePassword;
            String appName = AplConstants.appName;
            String serviceName = URL.split("/")[4];
            String apiKey = AppHelper.getAccessKey(getApplication());
            UUID uniqueKey = UUID.randomUUID();
            String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;


            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
            String signatureKey = new String(encodeUrlSafe(hmacData));
            params.put("userid", userid);
            params.put("bankcode", bankcode);
            params.put("passcode", pcode);
            params.put("password", encpwd1);
            params.put("new_password", encpwd2);
            params.put("confirm_password", encpwd3);
            params.put("rc_uuid", uniqueKey.toString());
            params.put("rc_dtime", dateTime);
            params.put("signature", signatureKey);


        } catch (Exception e) {
            System.out.println("Error");
        }


        client.post(AplConstants.ChangePassword, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject object) {

                try {
                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    JSONObject body_data = object.getJSONObject("body_data");
                    String error_code = body_data.getString("error_code");
                    String error_message = body_data.getString("error_message");

                    if (error_code.equalsIgnoreCase("0000")) {
                        Intent intent = new Intent(ActivityChangePassword.this, ChangePasswordReport.class);
                        startActivity(intent);
                        finish();

                    } else if (error_code.equalsIgnoreCase("0404")) {

                        AlertDialog.Builder alert = new AlertDialog.Builder(thisActivity);
                        alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                        alert.setMessage(error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Session session;
                                session = new Session(getApplicationContext());
                                session.signOut();
                            }
                        });
                        alert.show();

                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(thisActivity);
                        alert.setTitle(getResources().getString(R.string.lbl_changepassword_title));
                        alert.setMessage(error_message);
                        alert.setPositiveButton("OK", null);
                        alert.show();

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (pDialog != null) {
                    pDialog.dismiss();
                }
                if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getApplicationContext());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                } else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getApplicationContext());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                }
            }
        });

    }


    private void findViews() {
        lbl_header = (TextView) findViewById(R.id.label_header);
        lbl_change_password = (TextView) findViewById(R.id.lbl_change_password);
        txt_change_password = (EditText) findViewById(R.id.txt_change_password);
        txt_change_password.setHint(getResources().getString(R.string.lbl_changepassword_old_hint));
        lbl_change_new_password1 = (TextView) findViewById(R.id.lbl_change_new_password1);
        txt_change_new_password1 = (EditText) findViewById(R.id.txt_change_new_password1);
        txt_change_new_password1.setHint(getResources().getString(R.string.lbl_changepassword_new_hint));
        lbl_change_new_password2 = (TextView) findViewById(R.id.lbl_change_new_password2);
        txt_change_new_password2 = (EditText) findViewById(R.id.txt_change_new_password2);
        txt_change_new_password2.setHint(getResources().getString(R.string.lbl_changepassword_confirm_hint));
        btnDone = (Button) findViewById(R.id.btnDone);

       /* InputFilter filter = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    if (Character.isSpaceChar(source.charAt(i))) {
                        return "";
                    }
                }
                return null;
            }
        };*/


/*
        txt_change_password.setFilters(new InputFilter[] { textFilter});
        txt_change_new_password1.setFilters(new InputFilter[] {textFilter });
        txt_change_new_password2.setFilters(new InputFilter[] { textFilter});

        */

        txt_change_password.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                editclicked = false;
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editclicked = true;
            }
        });


        btnDone.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View view) {
                try {
                    networks = new Networks().isConnectingToInternet(getApplicationContext());
                    if (networks) {
                        Submit();
                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(thisActivity);
                        alert.setIcon(R.id.ic_credit);
                        alert.setTitle(getResources().getString(R.string.lbl_changepassword_title));
                        alert.setMessage(getResources().getString(R.string.lbl_alert_connection));
                        alert.setPositiveButton("OK", null);
                        alert.show();
                        return;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exit();
            return false;
        }
        if (editclicked) {
            if (keyCode == KeyEvent.KEYCODE_SPACE) {
                return false;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    public void exit() {
        if (!isExit) {
            isExit = true;
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.lbl_exit), Toast.LENGTH_SHORT).show();
            mHandler.sendEmptyMessageDelayed(0, 2000);
        } else {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            startActivity(intent);
            System.exit(0);

            mainActivity.signOut();
        }
    }


    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(android.os.Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
            isExit = false;
        }

    };

    public static byte[] encodeUrlSafe(byte[] data) {
        byte[] encode = Base64.encodeBase64(data);
        for (int i = 0; i < encode.length; i++) {
            if (encode[i] == '+') {
                encode[i] = '-';
            } else if (encode[i] == '=') {
                encode[i] = '_';
            } else if (encode[i] == '/') {
                encode[i] = '~';
            }
        }
        return encode;
    }
}

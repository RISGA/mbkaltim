package sgo.mobile.bankkaltim.app.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import com.activeandroid.util.Log;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.conf.AplConstants;
import sgo.mobile.bankkaltim.frameworks.net.loopj.android.http.CustomSSLSocketFactory;

import java.util.Timer;
import java.util.TimerTask;


public class SplashScreen extends Activity {
    ProgressBar prgLoading;
    int progress = 0;

    private ProgressDialog pDialog;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CheckVersion();
        setContentView(R.layout.splash_screen);
        getActionBar().hide();
        StartAnimations();



        // we're gonna use a timer task to show the main activity after 4 seconds
        TimerTask task = new TimerTask() {

            @Override
            public void run() {

                // go to the main activity
                Intent nextActivity = new Intent(SplashScreen.this,
                        ActivityLogin.class);
                startActivity(nextActivity);

                // make sure splash screen activity is gone
                SplashScreen.this.finish();


            }

        };

        // Schedule a task for single execution after a specified delay.
        // Show splash screen for 4 seconds
        new Timer().schedule(task, 3000);

    }


    private void StartAnimations() {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.slide_in_from_bottom);
        anim.reset();
         Animation anime = AnimationUtils.loadAnimation(this, R.anim.push_left_in);
         anime.reset();
     //   LinearLayout l=(LinearLayout) findViewById(R.id.main_layout);
     //   l.startAnimation(anim);
       // l.startAnimation(anime);

     //   anim = AnimationUtils.loadAnimation(this, R.anim.rotate);
        //anim.reset();
       // anim = AnimationUtils.loadAnimation(this, R.anim.push_left_in);
       // anim.reset();
        ImageView iv = (ImageView) findViewById(R.id.logo);
      //  ImageView is = (ImageView) findViewById(R.id.slogan);
       // is.clearAnimation();
        iv.clearAnimation();
        iv.startAnimation(anim);
      //  is.startAnimation(anime);


    }

    public void CheckVersion() {
            // pDialog = DefinedDialog.CreateProgressDialog(getApplication(), pDialog, getResources().getString(R.string.lbl_login_loading));

          try {
              AsyncHttpClient client = new AsyncHttpClient();
              client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getApplicationContext()));
              client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);

              client.get(AplConstants.CheckVersion, new JsonHttpResponseHandler() {
                  @Override
                  public void onSuccess(int statusCode, Header[] headers, JSONObject object) {


                      try {
                          if (pDialog != null)
                              pDialog.dismiss();

                          // JSONObject object = new JSONObject(content);

                          String error_code = object.getString("error_code");
                          String error_message = object.getString("error_message");
                          final JSONObject app_data = object.getJSONObject("app_data");
                          if (error_code.equalsIgnoreCase("0000")) {
                              String id = app_data.getString("id");
                              String application_name = app_data.getString("application_name");
                              final String package_name = app_data.getString("package_name");
                              String package_version = app_data.getString("package_version");
                              String package_version_code = app_data.getString("package_version_code");
                              final String type = app_data.getString("type");

                              String description = app_data.getString("description");
                              String modified = app_data.getString("modified");
                              String app_id = app_data.getString("app_id");
                              String active = app_data.getString("active");

                              if (!package_version.equals(getResources().getString(R.string.app_version_name))) {
                                  AlertDialog.Builder alert = new AlertDialog.Builder(SplashScreen.this);
                                  alert.setTitle(getResources().getString(R.string.app_name));
                                  alert.setMessage("Application is out of date,  Please update immediately");
                                  alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                      @Override
                                      public void onClick(DialogInterface dialog, int which) {
                                          final String appPackageName = package_name;
                                          if (type.equalsIgnoreCase("1")) {
                                              try {
                                                  startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                              } catch (android.content.ActivityNotFoundException anfe) {
                                                  startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                              }
                                          } else if (type.equalsIgnoreCase("2")) {
                                              String download_url = null;
                                              try {
                                                  download_url = app_data.getString("download_url");
                                              } catch (JSONException e) {
                                                  e.printStackTrace();
                                              }
                                              startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(download_url)));
                                          }
                                          SplashScreen.this.finish();
                                          android.os.Process.killProcess(android.os.Process.myPid());
                                          System.exit(0);
                                          getParent().finish();
                                      }
                                  });
                                  alert.show();

                              } else {
                                  if (pDialog != null)
                                      pDialog.dismiss();
                              }


                          } else {
                              AlertDialog.Builder alert = new AlertDialog.Builder(getApplicationContext());
                              alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message));
                              alert.setPositiveButton("OK", null);
                              alert.show();
                              Log.d("SSL HERE", "MSGMSG");


                          }

                      } catch (JSONException e) {
                          // TODO Auto-generated catch block
                          e.printStackTrace();

                      }
                  }

                  @Override
                  public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//                      super.onFailure(statusCode, headers, responseString, throwable);

                      AlertDialog.Builder alert = new AlertDialog.Builder(getApplicationContext());
                      alert.setMessage("STATUS CODE = " +statusCode + "HEADER = " + headers + " RESPONSE STRING = " + responseString + " THROWABLE = " +throwable);
                      alert.setPositiveButton("OK", null);
                      alert.show();
                  }

                  //                  public void onFailure(int statusCode,
//                                        org.apache.http.Header[] headers, String responseBody,
//                                        Throwable e) {
//
//
//                      AlertDialog.Builder alert = new AlertDialog.Builder(getApplicationContext());
//                      alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + responseBody);
//                      alert.setPositiveButton("OK", null);
//                      alert.show();
//                  }
              });
          }catch (Exception e){
              Log.d("SSL HERE", "MSGMSG IF");
              Log.d("SSL HERE", "MSGMSG ESLE");
          }

    }



    protected void onPostExecute(Void result) {
        // TODO Auto-generated method stub
        Intent i = new Intent(SplashScreen.this, ActivityLogin.class);
        startActivity(i);

    }

    /** this class is used to handle thread */
   /* public class Loading extends AsyncTask<Void, Void, Void>{

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // TODO Auto-generated method stub
            while(progress < 100){
                try {
                    Thread.sleep(1000);
                    progress += 30;
                    prgLoading.setProgress(progress);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            Intent i = new Intent(SplashScreen.this, ActivityLogin.class);
            startActivity(i);

        }
    }*/
}
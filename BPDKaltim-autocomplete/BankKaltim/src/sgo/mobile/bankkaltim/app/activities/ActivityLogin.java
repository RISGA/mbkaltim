package sgo.mobile.bankkaltim.app.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.InputFilter;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.View;
import android.widget.*;
import com.activeandroid.util.Log;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.ui.dialog.DefinedDialog;
import sgo.mobile.bankkaltim.conf.AplConstants;
import sgo.mobile.bankkaltim.frameworks.net.Networks;
import sgo.mobile.bankkaltim.frameworks.net.loopj.android.http.CustomSSLSocketFactory;
import sgo.mobile.bankkaltim.frameworks.security.Encrypt;

import java.io.File;
import java.util.Locale;

public class ActivityLogin extends Activity {

    private Button btnLogin,sgoLink;
    private EditText txtPin;
    private EditText txtUserID;
    private int status, loginStatus,isExitStatus;
    private String userId, password;
    private boolean isExit;
    private ProgressDialog pDialog;
    private SharedPreferences sp;
    private RelativeLayout callus;
    ImageView ic_info,ic_powered_sgo;
    CheckBox checkPassword;

    static Activity thisActivity = null;
    Encrypt encrypt = new Encrypt();
    MainActivity mainActivity;
    boolean networks;
    String userLang,userChangePassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CheckVersion();
        checkLanguage();
        getActionBar().hide();
        setContentView(R.layout.activity_login);
        findViews();
        thisActivity = this;
        thisActivity.overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.slide_out_to_bottom);

    }

    public void checkLanguage(){

        if(Locale.getDefault().getLanguage().contentEquals("in")){
            // String languageToLoad  = "in"; // your language
            //Locale locale = new Locale();
            Locale locale = Locale.ENGLISH;
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
            // this.setContentView(R.layout.main);


        }else{

            String languageToLoad  = "en"; // your language
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());


        }
    }

    private void findViews(){
        Typeface tf = Typeface.createFromAsset(getAssets(),
                "fonts/Roboto-Light.ttf");
        btnLogin    = (Button) findViewById(R.id.btnLogin);
        txtUserID   = (EditText) findViewById(R.id.txtUserID);
        txtPin      = (EditText) findViewById(R.id.inpPin);
        callus      = (RelativeLayout)findViewById(R.id.callus);
        ic_info     = (ImageView)findViewById(R.id.ic_info);
        ic_powered_sgo = (ImageView)findViewById(R.id.img_powered_sgo);
        txtUserID.setTypeface(tf);
        txtUserID.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        txtPin.setTypeface(tf);
        btnLogin.setTypeface(tf);
        checkPassword = (CheckBox)findViewById(R.id.checkPassword);

        callus.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View view) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:08001738877"));
                startActivity(callIntent);

            }
        });

        ic_info.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View view) {
                startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse("http://www.bankaltim.co.id/")));
            }

        });

        ic_powered_sgo.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View view) {
                startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse("http://www.sgo.co.id/")));
            }

        });

        checkPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked){
                    txtPin.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }else{
                    txtPin.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }

            }
        });


        btnLogin.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View view) {

                final String txtUID = txtUserID.getText().toString();
                final String txtPIN = txtPin.getText().toString();
                if (txtUID.equalsIgnoreCase("")) {

                    AlertDialog.Builder alert = new AlertDialog.Builder(thisActivity);
                    alert.setTitle(getResources().getString(R.string.lbl_login_btn));
                    alert.setMessage(getResources().getString(R.string.lbl_login_alert_userid));
                    alert.setPositiveButton("OK",null);
                    alert.show();
                    return;

                }if (txtPIN.equalsIgnoreCase("")) {

                    AlertDialog.Builder alert = new AlertDialog.Builder(thisActivity);
                    alert.setTitle(getResources().getString(R.string.lbl_login_btn));
                    alert.setMessage(getResources().getString(R.string.lbl_login_alert_password));
                    alert.setPositiveButton("OK",null);
                    alert.show();
                    return;
                }else {

                }
                try {
                    networks = new Networks().isConnectingToInternet(getApplicationContext());
                    if (networks) {
                        signin();
                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(thisActivity);
                        alert.setTitle(getResources().getString(R.string.lbl_login_btn));
                        alert.setMessage(getResources().getString(R.string.lbl_alert_connection));
                        alert.setPositiveButton("OK", null);
                        alert.show();
                        return;
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });
    }


    public void signin(){

        try {
            pDialog = DefinedDialog.CreateProgressDialog(ActivityLogin.this, pDialog, getResources().getString(R.string.lbl_login_loading));
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getApplicationContext()));
            client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);

            RequestParams params = new RequestParams();
            userId = txtUserID.getText().toString();
            password = txtPin.getText().toString();
            String encryp = encrypt.aes_encrypt(password, userId);
            params.put("user_id", userId);
            params.put("password", encryp);
            params.put("version", getResources().getString(R.string.app_version_name));


            client.get(AplConstants.SignMobileAPI, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject object) {


                    try {

                        if (pDialog != null) {
                            pDialog.dismiss();
                        }


                        JSONObject body_data = object.getJSONObject("body_data");
                        String error_code = body_data.getString("error_code");
                        String error_message = body_data.getString("error_message");

                        if (error_code.equalsIgnoreCase("0000")) {
                            String access_key = object.getString("access_key");
                            JSONObject user_data = body_data.getJSONObject("user_data");
                            String user_Fullname = user_data.getString("userFullname");
                            String user_Email = user_data.getString("userEmail");
                            String user_Mobilephone = user_data.getString("userMobilePhone");
                            String access_flag = user_data.getString("accessFlag");
                            String user_lastLogin = user_data.getString("userLastLogin");
                            userLang = user_data.getString("userLang");
                            userChangePassword = user_data.getString("userChangePassword");
                            checkChangePassword();
                            checkLang();
                            SecurePreferences prefs = new SecurePreferences(getApplicationContext());
                            SecurePreferences.Editor mEditor = prefs.edit();
                            mEditor.putString("user_id", userId);
                            mEditor.putString("access_key", access_key);
                            mEditor.putString("userFullname", user_Fullname);
                            mEditor.putString("userEmail", user_Email);
                            mEditor.putString("userMobilePhone", user_Mobilephone);
                            mEditor.putString("accesflag", access_flag);
                            mEditor.putString("userChangePassword", userChangePassword);
                            mEditor.putString("userLang", userLang);
                            mEditor.putString("userLastLogin", user_lastLogin);
                            mEditor.apply();
                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(thisActivity);
                            alert.setTitle(getResources().getString(R.string.lbl_login_btn));
                            alert.setMessage(error_message);
                            alert.setPositiveButton("OK", null);
                            alert.show();

                        }


                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();

                    }
                }

                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(thisActivity);
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(thisActivity);
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    }

                }

            });
        }catch (Exception e){
            Log.d("SSL HERE", "MSGMSG IF");
            Log.d("SSL HERE", "MSGMSG ESLE");
        }
    }

    public void checkChangePassword(){
            if(userChangePassword.equalsIgnoreCase("Y")){
                String pcd = AplConstants.pcd;
                String bcode = AplConstants.Bankcode;
                Intent mIntent = new Intent(thisActivity, ActivityChangePassword.class);
                Bundle mBundle = new Bundle();
                mBundle.putString("userid", userId);
                mBundle.putString("passcode", pcd);
                mBundle.putString("bankcode", bcode);
                mIntent.putExtras(mBundle);
                startActivity(mIntent);
            }else{

                Intent intent = new Intent(ActivityLogin.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
    }


    public void checkLang(){
        try{
            if(userLang.equalsIgnoreCase("en")){
                Locale locale = Locale.ENGLISH;
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config,
                        getBaseContext().getResources().getDisplayMetrics());
                // this.setContentView(R.layout.main);


            }else{

                String languageToLoad  = "in"; // your language
                Locale locale = new Locale(languageToLoad);
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config,
                        getBaseContext().getResources().getDisplayMetrics());
                // this.setContentView(R.layout.main);

            }
        }catch ( Exception e){
            e.printStackTrace();
        }
    }




    public void CheckVersion() {

        // pDialog = DefinedDialog.CreateProgressDialog(getApplication(), pDialog, getResources().getString(R.string.lbl_login_loading));
        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getApplicationContext()));
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);

        client.get(AplConstants.CheckVersion, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject object) {




                try {
                    if (pDialog != null)
                        pDialog.dismiss();

                    String error_code = object.getString("error_code");
                    String error_message = object.getString("error_message");
                    final JSONObject app_data = object.getJSONObject("app_data");
                    if (error_code.equalsIgnoreCase("0000")) {
                        String id = app_data.getString("id");
                        String application_name = app_data.getString("application_name");
                        final String package_name = app_data.getString("package_name");
                        String package_version = app_data.getString("package_version");
                        String package_version_code = app_data.getString("package_version_code");
                        final String type = app_data.getString("type");

                        String description = app_data.getString("description");
                        String modified = app_data.getString("modified");
                        String app_id = app_data.getString("app_id");
                        String active = app_data.getString("active");

                        if (!package_version.equals(getResources().getString(R.string.app_version_name))) {
                            AlertDialog.Builder alert = new AlertDialog.Builder(ActivityLogin.this);
                            alert.setTitle(getResources().getString(R.string.app_name));
                            alert.setMessage("Application is out of date,  Please update immediately");
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    final String appPackageName = package_name;
                                    if (type.equalsIgnoreCase("1")) {
                                        try {
                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                        } catch (android.content.ActivityNotFoundException anfe) {
                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                        }
                                    } else if (type.equalsIgnoreCase("2")) {
                                        String download_url = null;
                                        try {
                                            download_url = app_data.getString("download_url");
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(download_url)));
                                    }
                                    ActivityLogin.this.finish();
                                    android.os.Process.killProcess(android.os.Process.myPid());
                                    System.exit(0);
                                    getParent().finish();
                                }
                            });
                            alert.show();

                        } else {
                            if (pDialog != null)
                                pDialog.dismiss();
                        }


                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getApplicationContext());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message));
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();

                }
            }



//            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
//                if (pDialog != null) {
//                    pDialog.dismiss();
//                }
//                if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
//                    AlertDialog.Builder alert = new AlertDialog.Builder(getApplicationContext());
//                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
//                    alert.setPositiveButton("OK", null);
//                    alert.show();
//                } else {
//                    AlertDialog.Builder alert = new AlertDialog.Builder(getApplicationContext());
//                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
//                    alert.setPositiveButton("OK", null);
//                    alert.show();
//
//
//                }
//            }

            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//                      super.onFailure(statusCode, headers, responseString, throwable);

                AlertDialog.Builder alert = new AlertDialog.Builder(getApplicationContext());
                alert.setMessage("STATUS CODE = " +statusCode + "HEADER = " + headers + " RESPONSE STRING = " + responseString + " THROWABLE = " +throwable);
                alert.setPositiveButton("OK", null);
                alert.show();
            }
        });
    }



    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exit();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void exit(){
        if (!isExit) {
            isExit = true;
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.lbl_exit), Toast.LENGTH_SHORT).show();
            mHandler.sendEmptyMessageDelayed(0, 2000);
        } else {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            startActivity(intent);
            try {
                trimCache(thisActivity);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            System.exit(0);

        }
    }

    public static void trimCache(Context context) {
        try {
            File dir = context.getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }
    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }



    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
            isExit = false;
        }

    };

}
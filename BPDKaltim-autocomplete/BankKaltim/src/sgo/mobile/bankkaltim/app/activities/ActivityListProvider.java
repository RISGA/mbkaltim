package sgo.mobile.bankkaltim.app.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.frameworks.session.Session;

/**
 * Created by SGO-Mobile-DEV on 4/6/2015.
 */
public class ActivityListProvider extends SherlockActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider_list);
        actionBarSetup();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            AlertDialog.Builder alertbox=new AlertDialog.Builder(this);
            alertbox.setTitle("Warning");
            alertbox.setMessage("Exit Application?");
            alertbox.setPositiveButton("Yes", new
                    DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            Session session;
                            session = new Session(getApplicationContext());
                            session.signOut();

                        }
                    });
            alertbox.setNegativeButton("No", new
                    DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {}
                    });
            alertbox.show();
        }
        return super.onKeyDown(keyCode, event);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getSupportMenuInflater();
        inflater.inflate(R.menu.mainmenu, menu);
        return true;
    }

    protected void actionBarSetup()
    {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(R.string.ab_title);
        actionBar.setSubtitle(R.string.ab_subtitle);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            actionBar.setHomeButtonEnabled(true);
        }
    }
}

package sgo.mobile.banksulteng.app.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.actionbarsherlock.app.SherlockActivity;

/**
 * Created by User on 22/09/2014.
 */


public class ActivityBrowser extends SherlockActivity {
    private static final String HTTPS = "https://www.sgo.co.id";
    private static final String HTTP = "http://www.sgo.co.id";
    public static void openBrowser(final Context context, String url) {

        if (!url.startsWith(HTTP) && !url.startsWith(HTTPS)) {
            url = HTTP + url;
        }

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        context.startActivity(Intent.createChooser(intent, "Chose browser"));

    }
}

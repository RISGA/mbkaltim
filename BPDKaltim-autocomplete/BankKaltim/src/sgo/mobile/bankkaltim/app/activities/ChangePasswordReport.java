package sgo.mobile.bankkaltim.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import sgo.mobile.bankkaltim.R;

/**
 * Created by SGO-Mobile-DEV on 3/6/2015.
 */
public class ChangePasswordReport extends Activity {

    TextView lbl_header, lbl_success;
    ImageView ic_success;
    Button btnDone;
    MainActivity mainActivity;
    static Activity thisActivity = null;
    private boolean isExit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // getActionBar().hide();
        setContentView(R.layout.change_password_confirm);
        findViews();
        thisActivity = this;

    }

    private void findViews() {
        lbl_header = (TextView) findViewById(R.id.label_header);
        lbl_success = (TextView)findViewById(R.id.lbl_success);
        lbl_success.setText(getResources().getString(R.string.lbl_success));
        // ic_success =(ImageView)findViewById(R.id.ic_success);
        btnDone = (Button)findViewById(R.id.btnDone);

        btnDone.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View view) {
                Intent intent = new Intent(ChangePasswordReport.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }


    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exit();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void exit(){
        if (!isExit) {
            isExit = true;
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.lbl_exit), Toast.LENGTH_SHORT).show();
            mHandler.sendEmptyMessageDelayed(0, 2000);
        } else {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            startActivity(intent);
            System.exit(0);

            mainActivity.signOut();
        }
    }





    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(android.os.Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
            isExit = false;
        }

    };
}

package sgo.mobile.bankkaltim.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.fragments.FragmentPayment;

/**
 * Created by User on 06/10/2014.
 */
public class MenuPaymentAdapter extends BaseAdapter {
    private Activity activity;

    public MenuPaymentAdapter(Activity activity) {
        this.activity = activity;
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return FragmentPayment.menu_biller_id.size();
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.fragment_menu_payment_list_item, null);
            holder = new ViewHolder();

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.txtText    = (TextView) convertView.findViewById(R.id.txtText);

        holder.txtText.setText(FragmentPayment.menu_biller_name.get(position));
        return convertView;
    }

    static class ViewHolder {
        TextView txtText, txtSubText;
    }


}

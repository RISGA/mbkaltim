package sgo.mobile.bankkaltim.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.fragments.FragmentPurchasePhoneVoucher;

/**
 * Created by SGO-Mobile-DEV on 4/2/2015.
 */
public class PurchaseVoucherPhoneAdapter extends BaseAdapter {
    private Activity activity;

    public PurchaseVoucherPhoneAdapter(Activity activity) {
        this.activity = activity;

    }

    @Override
    public int getCount() {
        return FragmentPurchasePhoneVoucher.bid.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.fragment_purchase_voucher_adapter, null);
            holder = new ViewHolder();
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }



        holder.imageView    = (ImageView) convertView.findViewById(R.id.product_icon);
        String pCD = FragmentPurchasePhoneVoucher.bid.get(pos);
       // Toast.makeText(activity, pCD, Toast.LENGTH_LONG).show();

      /*  myList.add(pCD);
        removeDuplicates(myList);
        Toast.makeText(activity, myList.toString() , Toast.LENGTH_LONG).show();
*/

  /*      if(pCD.equalsIgnoreCase("1074")){
            holder.imageView.setImageResource(R.drawable.ic_telkomsel);
        }else if(pCD.equalsIgnoreCase("1075")){
            holder.imageView.setImageResource(R.drawable.ic_indosat);
        }else if(pCD.equalsIgnoreCase("1076")){
            holder.imageView.setImageResource(R.drawable.ic_indosat_gprs);
        }else if(pCD.equalsIgnoreCase("1077")){
            holder.imageView.setImageResource(R.drawable.ic_indosat_sms);
        }else if(pCD.equalsIgnoreCase("1078")){
            holder.imageView.setImageResource(R.drawable.ic_xl);
        }else if(pCD.equalsIgnoreCase("1079")){
            holder.imageView.setImageResource(R.drawable.ic_axis);
        }else if(pCD.equalsIgnoreCase("1080")){
            holder.imageView.setImageResource(R.drawable.ic_smart);
        }else if(pCD.equalsIgnoreCase("1081")){
            holder.imageView.setImageResource(R.drawable.ic_flexi);
        }else if(pCD.equalsIgnoreCase("1082")){
            holder.imageView.setImageResource(R.drawable.ic_esia);
        }else if(pCD.equalsIgnoreCase("1083")){
            holder.imageView.setImageResource(R.drawable.ic_three);
        }else if(pCD.equalsIgnoreCase("1084")){
            holder.imageView.setImageResource(R.drawable.ic_three_internet);
        }else if(pCD.equalsIgnoreCase("1149")){
            holder.imageView.setImageResource(R.drawable.ic_im3_smart);
        }else if(pCD.equalsIgnoreCase("1150")){
            holder.imageView.setImageResource(R.drawable.ic_indosat_starone);
        }else if(pCD.equalsIgnoreCase("1148")){
            holder.imageView.setImageResource(R.drawable.ic_indosat_mentari);
        }else if(pCD.equalsIgnoreCase("1085")){
            holder.imageView.setImageResource(R.drawable.ic_kartuas);
        }
*/

        if(pCD.equalsIgnoreCase("TSIM_PRE")){
            holder.imageView.setImageResource(R.drawable.ic_simpati);
        }else if(pCD.equalsIgnoreCase("IDSR_PRE")){
            holder.imageView.setImageResource(R.drawable.ic_im3);
        }else if(pCD.equalsIgnoreCase("IDSG_PRE")){
            holder.imageView.setImageResource(R.drawable.ic_indosat_gprs);
        }else if(pCD.equalsIgnoreCase("IDSS_PRE")){
            holder.imageView.setImageResource(R.drawable.ic_indosat_sms);
        }else if(pCD.equalsIgnoreCase("XLR_PRE")){
            holder.imageView.setImageResource(R.drawable.ic_xl);
        }else if(pCD.equalsIgnoreCase("AXS_PRE")){
            holder.imageView.setImageResource(R.drawable.ic_axis);
        }else if(pCD.equalsIgnoreCase("SMR_PRE")){
            holder.imageView.setImageResource(R.drawable.ic_smart);
        }else if(pCD.equalsIgnoreCase("FLE_PRE")){
            holder.imageView.setImageResource(R.drawable.ic_flexi);
        }else if(pCD.equalsIgnoreCase("ESA_PRE")){
            holder.imageView.setImageResource(R.drawable.ic_esia);
        }else if(pCD.equalsIgnoreCase("TRE_PRE")){
            holder.imageView.setImageResource(R.drawable.ic_three);
        }else if(pCD.equalsIgnoreCase("THREE_PHON")){
            holder.imageView.setImageResource(R.drawable.ic_three_prabayar);
        }else if(pCD.equalsIgnoreCase("TRI_PRE")){
            holder.imageView.setImageResource(R.drawable.ic_three_internet);
        }else if(pCD.equalsIgnoreCase("IM3_PHONE")){
            holder.imageView.setImageResource(R.drawable.ic_im3_smart);
        }else if(pCD.equalsIgnoreCase("STR_PHONE")){
            holder.imageView.setImageResource(R.drawable.ic_indosat_starone);
        }else if(pCD.equalsIgnoreCase("MNT_PHONE")){
            holder.imageView.setImageResource(R.drawable.ic_indosat_mentari);
        }else if(pCD.equalsIgnoreCase("TSAS_PRE")){
            holder.imageView.setImageResource(R.drawable.ic_kartuas);
        }else if(pCD.equalsIgnoreCase("BOLT_PRE")){
            holder.imageView.setImageResource(R.drawable.ic_bolt);
        }


        return convertView;
    }



    static class ViewHolder {
        ImageView imageView;
    }

}




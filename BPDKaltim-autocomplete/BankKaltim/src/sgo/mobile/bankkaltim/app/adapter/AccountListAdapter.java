package sgo.mobile.bankkaltim.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import sgo.mobile.bankkaltim.app.fragments.FragmentAccountList;
import sgo.mobile.bankkaltim.frameworks.math.FormatCurrency;
import sgo.mobile.bankkaltim.R;

public class AccountListAdapter extends BaseAdapter {
    private Activity activity;


    public AccountListAdapter(Activity act) {
        this.activity = act;
    }
    public int getCount() {
        // TODO Auto-generated method stub
        return FragmentAccountList.acc_no.size();
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.fragment_summary_trx_item, null);
            holder = new ViewHolder();

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

       // String amt = FragmentAccountList.acct_bal.get(position);
        /*Double amnt = Double.parseDouble(amt);
        int amount = amnt.intValue();*/

        holder.txtText    = (TextView) convertView.findViewById(R.id.txtText);
        holder.txtSubText1 = (TextView) convertView.findViewById(R.id.txtSubText1);
        holder.txtSubText2 = (TextView) convertView.findViewById(R.id.txtSubText2);

        holder.txtText.setText(FormatCurrency.CurrencyIDR(FragmentAccountList.acct_bal.get(position)));
        holder.txtSubText1.setText(FragmentAccountList.acc_name.get(position) + " " + "(" + FragmentAccountList.ccy_id.get(position) + ")");
        holder.txtSubText2.setText(FragmentAccountList.acc_no.get(position));

        return convertView;
    }

    static class ViewHolder {
        TextView txtText, txtSubText1, txtSubText2;
    }
}

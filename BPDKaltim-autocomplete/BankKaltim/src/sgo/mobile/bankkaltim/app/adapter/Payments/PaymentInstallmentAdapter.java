package sgo.mobile.bankkaltim.app.adapter.Payments;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.models.payment.PaymentInstallmentModel;

import java.util.ArrayList;

/**
 * Created by MOBILEDEV on 2/23/2016.
 */
public class PaymentInstallmentAdapter extends ArrayAdapter<PaymentInstallmentModel> {


    private Activity context;
    ArrayList<PaymentInstallmentModel> data = null;

    public PaymentInstallmentAdapter(Activity context, int resource,
                                     ArrayList<PaymentInstallmentModel> data) {
        super(context, resource, data);
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return super.getView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

        if (row == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            row = inflater.inflate(R.layout.simple_spinner_item, parent, false);
        }


        PaymentInstallmentModel item = data.get(position);

        if (item != null) { // Parse the data from each object and set it.
            TextView AccountName = (TextView) row.findViewById(R.id.item_value);

            if (AccountName != null) {
                AccountName.setText( item.getProductName());
            }
        }
        return row;
    }
}

package sgo.mobile.bankkaltim.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.fragments.FragmentTrxSummary;
import sgo.mobile.bankkaltim.frameworks.math.FormatCurrency;

public class SummaryTrxAdapter extends BaseAdapter {
    private Activity activity;

    public SummaryTrxAdapter(Activity act) {
        this.activity = act;
    }
    public int getCount() {
        // TODO Auto-generated method stub
        return FragmentTrxSummary.id.size();
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.fragment_summary_trx_item, null);
            holder = new ViewHolder();

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.txtText    = (TextView) convertView.findViewById(R.id.txtText);
        holder.txtSubText1 = (TextView) convertView.findViewById(R.id.txtSubText1);
        holder.txtSubText2 = (TextView) convertView.findViewById(R.id.txtSubText2);

        holder.txtText.setText(FormatCurrency.getRupiahFormat(FragmentTrxSummary.total.get(position)));
        holder.txtSubText1.setText(FragmentTrxSummary.source_name.get(position) + "(" + FragmentTrxSummary.ccy.get(position) + ")");
        holder.txtSubText2.setText(FragmentTrxSummary.source_account.get(position));


        return convertView;
    }

    static class ViewHolder {
        TextView txtText, txtSubText1, txtSubText2;
    }
}

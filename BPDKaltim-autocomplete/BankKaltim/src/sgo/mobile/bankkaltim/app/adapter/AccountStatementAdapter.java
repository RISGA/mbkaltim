package sgo.mobile.bankkaltim.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.fragments.FragmentAccountStatementInquiry;
import sgo.mobile.bankkaltim.frameworks.math.FormatCurrency;

public class AccountStatementAdapter extends BaseAdapter {
    private Activity activity;
    String debit="Debit";
    String credit="Credit";

    public AccountStatementAdapter(Activity act) {
        this.activity = act;
    }
    public int getCount() {
        // TODO Auto-generated method stub
        return FragmentAccountStatementInquiry.amount.size();
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.fragment_account_statement_list_detail, null);
            holder = new ViewHolder();

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }
        holder.txtTitle     = (TextView) convertView.findViewById(R.id.lbl_account);
        holder.txtText     = (TextView) convertView.findViewById(R.id.lbl_date);
        holder.txtSubText1 = (TextView) convertView.findViewById(R.id.lbl_desc);
        holder.txtSubText2 = (TextView) convertView.findViewById(R.id.lbl_balance);
        holder.imgDebit = (ImageView) convertView.findViewById(R.id.ic_debit);
        holder.imgDebit.setVisibility(View.GONE);
        holder.imgCredit = (ImageView) convertView.findViewById(R.id.ic_credit);
        holder.imgCredit.setVisibility(View.GONE);
       /* holder.txtSubText3 = (TextView) convertView.findViewById(R.id.lbl_type);
        //holder.txtSubText3 = (TextView) convertView.findViewById(R.id.txtSubText3);
       /* holder.txtSubText4 = (TextView) convertView.findViewById(R.id.lbl_opeing_balance);
        holder.txtSubText5 = (TextView) convertView.findViewById(R.id.lbl_closing_balance);*/

        holder.txtTitle.setText(activity.getResources().getString(R.string.lbl_account_statement_detail_currency) + FragmentAccountStatementInquiry.ccy.get(position));
        holder.txtText.setText(activity.getResources().getString(R.string.lbl_account_statement_detail_date) +  FragmentAccountStatementInquiry.dateTime.get(position));
        holder.txtSubText1.setText(activity.getResources().getString(R.string.lbl_account_statement_detail_description) + FragmentAccountStatementInquiry.description.get(position));
        holder.txtSubText2.setText(activity.getResources().getString(R.string.lbl_account_statement_detail_amount) + FormatCurrency.CurrencyIDR(FragmentAccountStatementInquiry.amount.get(position)));
      /*  holder.txtSubText4.setText("Opening Balance: " + FragmentAccountStatementInquiry.opening_balance);
        holder.txtSubText5.setText("Closing Balance: " + FragmentAccountStatementInquiry.closing_balance);*/
        String flg = FragmentAccountStatementInquiry.flag.get(position);

        if(flg.equalsIgnoreCase("D")){
           /* holder.txtSubText2.setText(debit);
            holder.txtSubText2.setVisibility(View.VISIBLE);
            holder.txtSubText3.setVisibility(View.GONE);*/
           /* holder.txtSubText3.setText(debit);
            holder.txtSubText3 = (TextView) convertView.findViewById(R.id.lbl_type);*/
            holder.imgDebit.setVisibility(View.VISIBLE);
            holder.imgCredit.setVisibility(View.GONE);
        }
        if(flg.equalsIgnoreCase("C")){
          /*  holder.txtSubText3.setText(credit);
            holder.txtSubText3.setVisibility(View.VISIBLE);
            holder.txtSubText2.setVisibility(View.GONE);*/
           /* holder.txtSubText3.setText(credit);
            holder.txtSubText3 = (TextView) convertView.findViewById(R.id.lbl_type);
            */
            holder.imgDebit.setVisibility(View.GONE);
            holder.imgCredit.setVisibility(View.VISIBLE);
        }


       /* String debit = FragmentTrxInquiry.debet.get(position);
        String credit = FragmentTrxInquiry.credit.get(position);
        holder.txtSubText3.setText("Credit : " + FormatCurrency.getRupiahFormat(credit));
        holder.txtSubText2.setText("Debit : " + FormatCurrency.getRupiahFormat(debit));

        if(debit.equalsIgnoreCase("0")){
            holder.txtSubText3.setVisibility(View.VISIBLE);
            holder.txtSubText2.setVisibility(View.GONE);
        }

        if(credit.equalsIgnoreCase("0")){
            holder.txtSubText3.setVisibility(View.GONE);
            holder.txtSubText2.setVisibility(View.VISIBLE);
        }*/

        return convertView;
    }



    static class ViewHolder {
        TextView txtText, txtSubText1, txtSubText2, txtSubText5,txtTitle;
        ImageView imgDebit, imgCredit;
    }
}

package sgo.mobile.bankkaltim.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.fragments.FragmentDeleteBeneficiary;

/**
 * Created by SGO-Mobile-DEV on 3/6/2015.
 */
public class BenefListAdapter extends BaseAdapter {
    private Activity activity;

    public BenefListAdapter(Activity activity) {
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return FragmentDeleteBeneficiary.dest_bank_code_arr.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.adapter_benef_list, null);
            holder = new ViewHolder();

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }



        holder.acc_no = (TextView) convertView.findViewById(R.id.acc_no);
        holder.acc_name = (TextView) convertView.findViewById(R.id.acc_name);
        holder.acc_bankname = (TextView) convertView.findViewById(R.id.acc_bankname);

        holder.acc_no.setText(FragmentDeleteBeneficiary.dest_account_no_arr.get(position));
        holder.acc_name.setText(FragmentDeleteBeneficiary.dest_account_name_arr.get(position));
        holder.acc_bankname.setText(FragmentDeleteBeneficiary.dest_bank_name_arr.get(position));

        return convertView;
    }

    static class ViewHolder {
        TextView acc_no, acc_name, acc_bankname;
    }

}

package sgo.mobile.bankkaltim.app.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import sgo.mobile.bankkaltim.app.fragments.FragmentTransferDomestic;
import sgo.mobile.bankkaltim.app.fragments.FragmentTransferInHouse;

public class TransferPagerAdapter extends FragmentStatePagerAdapter {
    private final int PAGES = 2;

    public TransferPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new FragmentTransferInHouse();
            case 1:
                return new FragmentTransferDomestic();
            default:
                throw new IllegalArgumentException("The item position should be less or equal to:" + PAGES);
        }
    }

    @Override
    public int getCount() {
        return PAGES;
    }
}

package sgo.mobile.bankkaltim.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.fragments.FragmentReport;

/**
 * Created by thinkpad on 1/19/2015.
 */
public class ReportAdapterList  extends BaseAdapter {
    private Activity activity;

    public ReportAdapterList(Activity act) {
        this.activity = act;
    }
    public int getCount() {
        // TODO Auto-generated method stub
        return FragmentReport.name.size();
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.fragment_report_list_item, null);
            holder = new ViewHolder();

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.imgPaymentType = (ImageView) convertView.findViewById(R.id.image_payment_type);
        holder.txtPaymentType = (TextView) convertView.findViewById(R.id.txt_payment_type);
        holder.txtPaymentType.setText(FragmentReport.name.get(position));
        holder.imgPaymentType.setImageResource(FragmentReport.imagePaymentType.get(Integer.parseInt(FragmentReport.id.get(position))-1));
        return convertView;
    }

    static class ViewHolder {
        TextView txtPaymentType;
        ImageView imgPaymentType;
    }
}

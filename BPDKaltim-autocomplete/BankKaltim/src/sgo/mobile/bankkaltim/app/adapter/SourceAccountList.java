package sgo.mobile.bankkaltim.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.fragments.FragmentSourceAccountList;
import sgo.mobile.bankkaltim.frameworks.math.FormatCurrency;

public class SourceAccountList extends BaseAdapter {
    private Activity activity;

    public SourceAccountList(Activity act) {
        this.activity = act;
    }
    public int getCount() {
        // TODO Auto-generated method stub
        return FragmentSourceAccountList.id.size();
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.layout_scource_account_list_item, null);
            holder = new ViewHolder();

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.txtText    = (TextView) convertView.findViewById(R.id.txtText);
        holder.txtSubText1 = (TextView) convertView.findViewById(R.id.txtSubText1);
        holder.txtSubText2 = (TextView) convertView.findViewById(R.id.txtSubText2);

        holder.txtText.setText(FormatCurrency.getRupiahFormat(FragmentSourceAccountList.total.get(position)));
        holder.txtSubText1.setText(FragmentSourceAccountList.source_name.get(position) + "(" + FragmentSourceAccountList.ccy.get(position) + ")");
        holder.txtSubText2.setText(FragmentSourceAccountList.source_account.get(position));


        return convertView;
    }

    static class ViewHolder {
        TextView txtText, txtSubText1, txtSubText2;
    }
}

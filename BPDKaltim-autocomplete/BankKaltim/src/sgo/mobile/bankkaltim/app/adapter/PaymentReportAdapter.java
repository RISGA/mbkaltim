package sgo.mobile.bankkaltim.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.fragments.FragmentPaymentReport_;

/**
 * Created by SGO-Mobile-DEV on 8/31/2015.
 */
public class PaymentReportAdapter extends BaseAdapter {

    private Activity activity;

    public PaymentReportAdapter(Activity activity) {
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return FragmentPaymentReport_.name_arr.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.fragment_payment_confirm_adapter, null);
            holder = new ViewHolder();

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }


        holder.txtSubText1 = (TextView) convertView.findViewById(R.id.lbl_name);
        holder.txtSubText2 = (TextView) convertView.findViewById(R.id.lbl_value);



        holder.txtSubText1.setText(FragmentPaymentReport_.name_arr.get(position).replace("_"," ").toUpperCase());
        holder.txtSubText2.setText(FragmentPaymentReport_.value_arr.get(position));



        return convertView;
    }

    static class ViewHolder {
        TextView txtSubText1, txtSubText2;
    }

}


package sgo.mobile.bankkaltim.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.fragments.FragmentAccountStatementInquiry;

public class InquiryTrxAdapter extends BaseAdapter {
    private Activity activity;
    String debit="D";
    String credit="C";

    public InquiryTrxAdapter(Activity act) {
        this.activity = act;
    }
    public int getCount() {
        // TODO Auto-generated method stub
        return FragmentAccountStatementInquiry.amount.size();
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.fragment_account_statement_list_sample, null);
            holder = new ViewHolder();

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }
      //  holder.txtTitle     = (TextView) convertView.findViewById(R.id.txtSourceAccount);
        holder.txtText     = (TextView) convertView.findViewById(R.id.lbl_date);
        holder.txtSubText1 = (TextView) convertView.findViewById(R.id.lbl_desc);
        holder.txtSubText2 = (TextView) convertView.findViewById(R.id.lbl_balance);
        holder.txtSubText3 = (TextView) convertView.findViewById(R.id.lbl_type);
        //holder.txtSubText3 = (TextView) convertView.findViewById(R.id.txtSubText3);
        //holder.txtSubText4 = (TextView) convertView.findViewById(R.id.lbl_balance);

     //   holder.txtTitle.setText("Account : " + FragmentAccountList.acc_name.get(position) + FragmentAccountList.acc_no.get(position)+FragmentAccountList.ccy_id.get(position));
        holder.txtText.setText("Date : " +  FragmentAccountStatementInquiry.dateTime.get(position));
        holder.txtSubText1.setText("Description : " + FragmentAccountStatementInquiry.description.get(position));
        holder.txtSubText2.setText("Balance: " + FragmentAccountStatementInquiry.balance.get(position));
        String flg = FragmentAccountStatementInquiry.flag.get(position);

        if(flg.equalsIgnoreCase("D")){
           /* holder.txtSubText2.setText(debit);
            holder.txtSubText2.setVisibility(View.VISIBLE);
            holder.txtSubText3.setVisibility(View.GONE);*/
            holder.txtSubText3.setText(debit);
            holder.txtSubText3 = (TextView) convertView.findViewById(R.id.lbl_type);
        }
        if(flg.equalsIgnoreCase("C")){
          /*  holder.txtSubText3.setText(credit);
            holder.txtSubText3.setVisibility(View.VISIBLE);
            holder.txtSubText2.setVisibility(View.GONE);*/
            holder.txtSubText3.setText(credit);
            holder.txtSubText3 = (TextView) convertView.findViewById(R.id.lbl_type);
        }


       /* String debit = FragmentTrxInquiry.debet.get(position);
        String credit = FragmentTrxInquiry.credit.get(position);
        holder.txtSubText3.setText("Credit : " + FormatCurrency.getRupiahFormat(credit));
        holder.txtSubText2.setText("Debit : " + FormatCurrency.getRupiahFormat(debit));

        if(debit.equalsIgnoreCase("0")){
            holder.txtSubText3.setVisibility(View.VISIBLE);
            holder.txtSubText2.setVisibility(View.GONE);
        }

        if(credit.equalsIgnoreCase("0")){
            holder.txtSubText3.setVisibility(View.GONE);
            holder.txtSubText2.setVisibility(View.VISIBLE);
        }*/

        return convertView;
    }



    static class ViewHolder {
        TextView txtText, txtSubText1, txtSubText2, txtSubText3, txtSubText4,txtTitle;
    }
}

package sgo.mobile.bankkaltim.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.fragments.FragmentAccountList;
import sgo.mobile.bankkaltim.app.fragments.FragmentAccountStatementInquiry;

/**
 * Created by User on 19/08/2014.
 */
public class AccountStatementInquiryAdapter extends BaseAdapter {
    private Activity activity;
    String credit ="Credit";
    String debit = "Debit";

    public AccountStatementInquiryAdapter(Activity activity) {
        this.activity = activity;
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return FragmentAccountList.acc_no.size();
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.fragment_account_statement_inquiry_item, null);
            holder = new ViewHolder();

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.txtText     = (TextView) convertView.findViewById(R.id.txtText);
        holder.txtSubText1 = (TextView) convertView.findViewById(R.id.txtSubText1);
       /* holder.txtSubText2 = (TextView) convertView.findViewById(R.id.txtSubText2);
        holder.txtSubText3 = (TextView) convertView.findViewById(R.id.txtSubText3);*/

        holder.txtText.setText("Date : " +  FragmentAccountStatementInquiry.dateTime.get(position));
        holder.txtSubText1.setText("Description : " + FragmentAccountStatementInquiry.description.get(position));

       /* String debit = FragmentAccountStatementInquiry.flag.get(position);
        String credit = FragmentAccountStatement.flag.get(position);
        holder.txtSubText3.setText("Credit : " + FormatCurrency.getRupiahFormat(credit));
        holder.txtSubText2.setText("Debit : " + FormatCurrency.getRupiahFormat(debit));


        if(debit.equalsIgnoreCase("0")){
            holder.txtSubText3.setVisibility(View.VISIBLE);
            holder.txtSubText2.setVisibility(View.GONE);
        }

        if(credit.equalsIgnoreCase("0")){
            holder.txtSubText3.setVisibility(View.GONE);
            holder.txtSubText2.setVisibility(View.VISIBLE);
        }*/
     /*   String flg = FragmentAccountStatementInquiry.flag.get(position);
        holder.txtSubText3.setText(credit);
        holder.txtSubText2.setText(debit);


        if(flg.equalsIgnoreCase("D")){
            this.equals(debit);
        }
        if(flg.equalsIgnoreCase("C")){
             this.equals(credit);
        }*/

        return convertView;
    }

    static class ViewHolder {
        TextView txtText, txtSubText1, txtSubText2, txtSubText3;
    }


}

package sgo.mobile.bankkaltim.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.fragments.FragmentAccountStatement;
import sgo.mobile.bankkaltim.frameworks.math.FormatCurrency;

public class AccountStatementAdapterList extends BaseAdapter {
    private Activity activity;

    public AccountStatementAdapterList(Activity act) {
        this.activity = act;
    }
    public int getCount() {
        // TODO Auto-generated method stub
        return FragmentAccountStatement.acc_no.size();
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.fragment_account_statement_list_item, null);
            holder = new ViewHolder();

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        // String amt = FragmentAccountStatement.acct_bal.get(position);
        // Double amnt = Double.parseDouble(amt);
        //int amount = amnt.intValue();

        holder.txtText    = (TextView) convertView.findViewById(R.id.txtText);
        holder.txtSubText1 = (TextView) convertView.findViewById(R.id.txtSubText1);
        holder.txtSubText2 = (TextView) convertView.findViewById(R.id.txtSubText2);

        holder.txtText.setText(FormatCurrency.CurrencyIDR(FragmentAccountStatement.acct_bal.get(position)));

        holder.txtSubText1.setText(FragmentAccountStatement.acc_name.get(position) + " " + "(" + FragmentAccountStatement.ccy_id.get(position) + ")");
        holder.txtSubText2.setText(FragmentAccountStatement.acc_no.get(position));

        return convertView;
    }

    static class ViewHolder {
        TextView txtText, txtSubText1, txtSubText2;
    }
}

package sgo.mobile.bankkaltim.app.adapter.Purchase;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.models.purchase.PurchaseVoucherSmartfrenModel;
import sgo.mobile.bankkaltim.frameworks.math.FormatCurrency;

import java.util.ArrayList;

/**
 * Created by MOBILEDEV on 4/28/2016.
 */
public class BillerVoucherPhoneSmartfren extends ArrayAdapter<PurchaseVoucherSmartfrenModel> {
    private Activity context;
    ArrayList<PurchaseVoucherSmartfrenModel> data = null;
    public String amount;


    public BillerVoucherPhoneSmartfren(Activity context, int resource,
                                            ArrayList<PurchaseVoucherSmartfrenModel> data) {
        super(context, resource, data);
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return super.getView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            row = inflater.inflate(R.layout.simple_spinner_item, parent, false);
        }

        PurchaseVoucherSmartfrenModel item = data.get(position);

        if(item != null) { // Parse the data from each object and set it.
            TextView AccountName = (TextView) row.findViewById(R.id.item_value);

            if (AccountName != null) {
                AccountName.setText((FormatCurrency.CurrencyIDR(item.getProductAmount())));
            }
        }
        return row;
    }
}

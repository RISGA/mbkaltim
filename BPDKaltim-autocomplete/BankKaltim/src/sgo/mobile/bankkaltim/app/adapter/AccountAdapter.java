package sgo.mobile.bankkaltim.app.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.beans.AccountBean;

import java.util.ArrayList;

public class AccountAdapter extends ArrayAdapter<AccountBean> {
    private Activity context;
    ArrayList<AccountBean> data = null;

    public AccountAdapter(Activity context, int resource,
                        ArrayList<AccountBean> data) {
        super(context, resource, data);
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return super.getView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            row = inflater.inflate(R.layout.simple_spinner_item, parent, false);
        }

        AccountBean item = data.get(position);

        if (item != null) { // Parse the data from each object and set it.
            TextView ProductName = (TextView) row.findViewById(R.id.item_value);
            if (ProductName != null) {
                ProductName.setText(item.getSource_acc_name());
            }
        }
        return row;
    }
}
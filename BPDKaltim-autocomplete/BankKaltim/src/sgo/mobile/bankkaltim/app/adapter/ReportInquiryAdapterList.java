package sgo.mobile.bankkaltim.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.fragments.FragmentReportInquiry;
import sgo.mobile.bankkaltim.frameworks.math.FormatCurrency;

/**
 * Created by thinkpad on 1/14/2015.
 */
public class ReportInquiryAdapterList extends BaseAdapter {
    private Activity activity;

    public ReportInquiryAdapterList(Activity act) {
        this.activity = act;
    }
    public int getCount() {
        // TODO Auto-generated method stub
        return FragmentReportInquiry.totalAmount.size();
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.fragment_report_inquiry_list_item, null);
            holder = new ViewHolder();

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        String amt = FragmentReportInquiry.totalAmount.get(position);
        Double amnt = Double.parseDouble(amt);
        int amount = amnt.intValue();

//        String bln = FragmentReportInquiry.balance.get(position);
//        Double blnce = Double.parseDouble(bln);
//        int balance = blnce.intValue();

        holder.imgPaymentType    = (ImageView) convertView.findViewById(R.id.image_payment_type);
        //holder.imgBalance = (ImageView) convertView.findViewById(R.id.img_balance);
        holder.txtTotalPayment    = (TextView) convertView.findViewById(R.id.txtTotalPayment);
        holder.txtDesc = (TextView) convertView.findViewById(R.id.txtDesc);
        //holder.txtBalance = (TextView) convertView.findViewById(R.id.txtBalance);
        holder.txtCreatedDate = (TextView) convertView.findViewById(R.id.txtCreatedDate);

        holder.imgPaymentType.setImageResource(FragmentReportInquiry.imagePaymentType.get(Integer.parseInt(FragmentReportInquiry.payment_id)-1));
        holder.txtTotalPayment.setText(FormatCurrency.getRupiahFormat(Integer.toString(amount)));
        holder.txtDesc.setText(FragmentReportInquiry.description.get(position));
        holder.txtCreatedDate.setText(FragmentReportInquiry.date.get(position));
        //holder.imgBalance.setImageResource(FragmentReportInquiry.imageBalance);
        //holder.txtBalance.setText(FormatCurrency.getRupiahFormat(Integer.toString(balance)));

        return convertView;
    }

    static class ViewHolder {
        TextView txtTotalPayment, txtDesc, txtBalance, txtCreatedDate;
        ImageView imgPaymentType, imgBalance;
    }
}
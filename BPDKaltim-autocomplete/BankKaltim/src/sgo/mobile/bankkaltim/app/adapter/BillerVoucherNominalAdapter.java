package sgo.mobile.bankkaltim.app.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.models.purchase.PurchaseNominalElectricityModel;
import sgo.mobile.bankkaltim.frameworks.math.FormatCurrency;

import java.util.ArrayList;

/**
 * Created by User on 09/10/2014.
 */
public class BillerVoucherNominalAdapter extends ArrayAdapter<PurchaseNominalElectricityModel> {
    private Activity context;
    ArrayList<PurchaseNominalElectricityModel> data = null;
    public BillerVoucherNominalAdapter(Activity context, int resource,
                                            ArrayList<PurchaseNominalElectricityModel> data) {
        super(context, resource, data);
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return super.getView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

        if (row == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            row = inflater.inflate(R.layout.simple_spinner_item, parent, false);
        }

        PurchaseNominalElectricityModel item = data.get(position);
        String nominal = item.getCatalogNominal();

        if (item != null) { // Parse the data from each object and set it.
            TextView AccountName = (TextView) row.findViewById(R.id.item_value);

            if (AccountName != null) {
                AccountName.setText(FormatCurrency.getRupiahFormat(nominal));
            }
        }
        return row;
    }
}

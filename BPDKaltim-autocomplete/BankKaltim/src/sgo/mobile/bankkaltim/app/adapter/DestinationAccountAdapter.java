package sgo.mobile.bankkaltim.app.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.beans.DomesticAccountBean;

import java.util.ArrayList;

/**
 * Created by User on 13/08/2014.
 */
public class DestinationAccountAdapter extends ArrayAdapter<DomesticAccountBean> {
    private Activity context;
    ArrayList<DomesticAccountBean> data = null;

    public DestinationAccountAdapter(Activity context, int resource,
                                ArrayList<DomesticAccountBean> data) {
        super(context, resource, data);
        this.context = context;
        this.data = data;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            row = inflater.inflate(R.layout.simple_spinner_item, parent, false);
        }

        DomesticAccountBean item = data.get(position);

        if (item != null) { // Parse the data from each object and set it.
            TextView AccountName = (TextView) row.findViewById(R.id.item_value);
            if (AccountName != null) {
                AccountName.setText(item.getAccount_no() + " " + item.getAccount_name());
            }else if (AccountName == null){
                AccountName.setText("You don't have beneficiary");
            }
        }
        return row;
    }
}

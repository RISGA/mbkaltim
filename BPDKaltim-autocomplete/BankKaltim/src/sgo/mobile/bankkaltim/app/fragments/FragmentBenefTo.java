package sgo.mobile.bankkaltim.app.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.activeandroid.query.Select;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.bankkaltim.AppHelper;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.activities.MainActivity;
import sgo.mobile.bankkaltim.app.adapter.BankCodeAdapter;
import sgo.mobile.bankkaltim.app.models.BankListModel;
import sgo.mobile.bankkaltim.app.ui.dialog.DefinedDialog;
import sgo.mobile.bankkaltim.conf.AplConstants;
import sgo.mobile.bankkaltim.frameworks.net.Networks;
import sgo.mobile.bankkaltim.frameworks.net.loopj.android.http.CustomSSLSocketFactory;
import sgo.mobile.bankkaltim.frameworks.security.Encrypt;
import sgo.mobile.bankkaltim.frameworks.session.Session;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class FragmentBenefTo extends SherlockFragment {
    private ProgressDialog pDialog;
    static Spinner spListBankCode = null;
    public String[] bankCode_arr, bankName_arr;
    private ArrayList<BankListModel> BankCodeList = new ArrayList<BankListModel>();
    Button btnBack;
    Button btnDone;
    EditText txtInpBenefName, txtInpBenefNo, txtInpBenefEmail;
    TextView lbl_header;
    boolean checkVisible;
    public String email;
    int getCountBankList;
    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");//dd/MM/yyyy
    Date now = new Date();
    String dateTime = sdfDate.format(now);
    boolean networks;
    Encrypt encrypt = new Encrypt();

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout./*fragment_benef_to*/fragment_add_new_beneficiary, container, false);
        getCountBankList = new Select().from(BankListModel.class).count();


        txtInpBenefNo   = (EditText) view.findViewById(R.id.inpBenefNo);
        spListBankCode = (Spinner) view.findViewById(R.id.spListBankCode);
        txtInpBenefEmail   = (EditText) view.findViewById(R.id.inpBenefEmail);
        lbl_header   = (TextView) view.findViewById(R.id.label_header);


        btnBack = (Button) view.findViewById(R.id.btnCancel);
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0){
                Fragment newFragment = null;
                newFragment = new FragmentSetting();
                switchFragment(newFragment);
            }
        });



        btnDone = (Button) view.findViewById(R.id.btnDone);
        btnDone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {

                   /* final String txtName= txtInpBenefName.getText().toString();
                    if (txtName.equalsIgnoreCase("")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle("Add New Beneficiary");
                        alert.setMessage("Please insert Beneficiary Name");
                        alert.setPositiveButton("OK",null);
                        alert.show();
                        return;
                    }if(StringUtils.isBenefName(txtName)){

                    }else{

                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setTitle("Add New Beneficiary");
                    alert.setMessage("Beneficiary Name cannot contain number or character, it must be Alphabet only !!");
                    alert.setPositiveButton("OK",null);
                    alert.show();
                    return;
                    }*/

                final String txtBenefNo = txtInpBenefNo.getText().toString();
                if (txtBenefNo.equalsIgnoreCase("")) {
                       /* Toast.makeText(getActivity(), "Please insert Beneficiary Account No", Toast.LENGTH_LONG).show();
                        return;*/
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setTitle(getResources().getString(R.string.lbl_add_beneficiary_title));
                    alert.setMessage(getResources().getString(R.string.lbl_add_beneficiary_no_alert));
                    alert.setPositiveButton("OK", null);
                    alert.show();
                    return;
                }
                if (txtBenefNo.length() < 10 || txtBenefNo.length() > 20) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setTitle(getResources().getString(R.string.lbl_add_beneficiary_title));
                    alert.setMessage(getResources().getString(R.string.lbl_add_beneficiary_no_alert_validation));
                    alert.setPositiveButton("OK", null);
                    alert.show();
                    return;
                }

                  /*  final String txtBenefEmail= txtInpBenefEmail.getText().toString();
                    if (txtBenefEmail.equalsIgnoreCase("")) {
                        Toast.makeText(getActivity(), "Please insert Beneficiary Email", Toast.LENGTH_LONG).show();
                        return;
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle("Add New Beneficiary");
                        alert.setMessage("Please insert Beneficiary Email");
                        alert.setPositiveButton("OK", null);
                        alert.show();
                        return;
                    }
                    if (StringUtils.isEmailValid(txtBenefEmail)){


                    }else{

                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle("Add New Beneficiary");
                        alert.setMessage("Invalid Email Format");
                        alert.setPositiveButton("OK",null);
                        alert.show();
                        return;
                    }*/

                try {
                    networks = new Networks().isConnectingToInternet(getActivity());
                    if (networks) {
                        checkBankCode();
                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_add_beneficiary_title));
                        alert.setMessage(getResources().getString(R.string.lbl_alert_connection));
                        alert.setPositiveButton("OK", null);
                        alert.show();
                        return;

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });

        try {
            networks = new Networks().isConnectingToInternet(getActivity());
            if (networks) {
                initViews();

            } else {
                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                alert.setTitle(getResources().getString(R.string.lbl_add_beneficiary_title));
                alert.setMessage(getResources().getString(R.string.lbl_alert_connection));
                alert.setPositiveButton("OK", null);
                alert.show();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return view;
    }

    private void initViews() {
        if(getCountBankList==0) {
            pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.lbl_loading_data));

            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getSherlockActivity()));
            client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
            RequestParams params = new RequestParams();
            try {
                String userid = AppHelper.getUserId(getActivity());
                String bankcode = AplConstants.Bankcode;
                String pcd = AplConstants.pcd;
                String URL = AplConstants.BankCodeAPI;
                String appName = AplConstants.appName;
                String serviceName = URL.split("/")[4];
                String apiKey = AppHelper.getAccessKey(getActivity());
                UUID uniqueKey = UUID.randomUUID();
                String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;
                Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
                SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
                sha256_HMAC.init(secret_key);
                byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
                String signatureKey = new String(encodeUrlSafe(hmacData));
                params.put("userid", userid);
                params.put("bankcode", bankcode);
                params.put("passcode", pcd);
                params.put("rc_uuid", uniqueKey.toString());
                params.put("rc_dtime", dateTime);
                params.put("signature", signatureKey);
            } catch (Exception e) {
                System.out.println("Error");
            }


            client.post(AplConstants.BankCodeAPI, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject object) {


                    try {
                        if (pDialog != null) {
                            pDialog.dismiss();
                        }
                        JSONObject body_data = object.getJSONObject("body_data");
                        String error_code = body_data.getString("error_code");
                        String error_message = body_data.getString("error_message");
                        if (error_code.equalsIgnoreCase("0000")) {
                            JSONArray data = body_data.getJSONArray("bank_data");
                            int len = data.length();
                            String bc = AplConstants.Bankcode;
                            bankCode_arr = new String[len];
                            bankName_arr = new String[len];
                            for (int i = 0; i < data.length(); i++) {
                                bankCode_arr[i] = data.getJSONObject(i).getString("bankCode");
                                bankName_arr[i] = data.getJSONObject(i).getString("bankName");

                                BankCodeList.add(new BankListModel(bankCode_arr[i], bankName_arr[i]));

                                BankListModel bankListModel = new BankListModel();
                                bankListModel.bankCode = data.getJSONObject(i).getString("bankCode");
                                bankListModel.bankName = data.getJSONObject(i).getString("bankName");
                                bankListModel.save();


                            }

                            BankCodeAdapter bankCodeAdapter = new BankCodeAdapter(getActivity(), android.R.layout.simple_spinner_item, BankCodeList);
                            spListBankCode.setAdapter(bankCodeAdapter);


                        } else if (error_code.equalsIgnoreCase("0404")) {

                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                            alert.setMessage(error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Session session;
                                    session = new Session(getActivity());
                                    session.signOut();
                                }
                            });
                            alert.show();

                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_tab_domestic));
                            alert.setMessage(getResources().getString(R.string.lbl_tab_domestic) + error_message);
                            alert.setPositiveButton("OK", null);
                            alert.show();

                        }

                        // End of Get Bank

                    } catch (
                            JSONException e
                            )

                    {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    }
                }

            });
        }else{

            List<BankListModel> itemList = new Select().all().from(BankListModel.class).execute();
            int len = itemList.size();
            bankCode_arr = new String[len];
            bankName_arr = new String[len];
            for (int i=0 ; i<itemList.size();i++) {
                bankCode_arr[i]=itemList.get(i).getBankCode();
                bankName_arr[i] =itemList.get(i).getBankName();

                BankCodeList.add(new BankListModel(bankCode_arr[i], bankName_arr[i]));

            }
            BankCodeAdapter bankCodeAdapter = new BankCodeAdapter(getActivity(), android.R.layout.simple_spinner_item, BankCodeList);
            spListBankCode.setAdapter(bankCodeAdapter);


        }

    }

    private void checkBankCode(){
        BankListModel bankCodeBean= (BankListModel) spListBankCode.getSelectedItem();
        final String benefBankCode = bankCodeBean.bankCode;
        final String benefBankName = bankCodeBean.bankName;
        String userBankCode = AplConstants.Bankcode;

        if(benefBankCode.equals(userBankCode)){

            pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.lbl_loading_process));
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getSherlockActivity()));
            client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);

            RequestParams params = new RequestParams();


            try {
                String userid = AppHelper.getUserId(getActivity());
                String pcd = AplConstants.pcd;
                String bcode = AplConstants.Bankcode;
                String benefNo  = txtInpBenefNo.getText().toString();
                String ccy = AplConstants.CurrencyIDR;
                String URL = AplConstants.InquiryAccountAPI;
                String appName = AplConstants.appName;
                String serviceName = URL.split("/")[4];
                String apiKey = AppHelper.getAccessKey(getActivity());
                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());

                UUID uniqueKey = UUID.randomUUID();
                String signature = uniqueKey+dateTime+ appName + serviceName+ bcode + userid;

                Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
                SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
                sha256_HMAC.init(secret_key);
                byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
                String signatureKey = new String(encodeUrlSafe(hmacData));
                params.put("userid", userid);
                params.put("bankcode", benefBankCode);
                params.put("passcode", pcd);
                params.put("accountno", benefNo);
                params.put("accountccy", ccy);
                params.put("rc_uuid", uniqueKey.toString());
                params.put("rc_dtime", dateTime);
                params.put("signature", signatureKey);

            }catch (Exception e){
                System.out.println("Error");
            }
            client.post(AplConstants.InquiryAccountAPI, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject object) {


                    try {
                        if (pDialog != null) {
                            pDialog.dismiss();
                        }
                        JSONObject body_data = object.getJSONObject("body_data");
                        String error_code = body_data.getString("error_code");
                        String error_message = body_data.getString("error_message");
                        String account_ccy = body_data.getString("account_ccy");
                        String account_name = body_data.getString("account_name");
                        String account_balance = body_data.getString("account_balance");
                        String account_status = body_data.getString("account_status");
                        String product_type = body_data.getString("product_type");

                        if (error_code.equalsIgnoreCase("0000")) {
                           /* AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle("Add New Beneficiary");
                            alert.setMessage("Add New Beneficiary : " + error_message);
                            alert.setPositiveButton("OK", null);
                            alert.show();*/
                            //String benefName= txtInpBenefName.getText().toString();
                            String benefNo = txtInpBenefNo.getText().toString();
                            String benefEmail = txtInpBenefEmail.getText().toString();
                            String Type = AplConstants.TypeInhouse;

                            Fragment newFragment = null;
                            newFragment = new FragmentBenefToConfirm();
                            Bundle args = new Bundle();
                            args.putString("bankcode", benefBankCode);
                            args.putString("bankname", benefBankName);
                            args.putString("benefName", account_name);
                            args.putString("benefNo", benefNo);
                            args.putString("benefEmail", benefEmail);
                            args.putString("benefCcy", account_ccy);
                            args.putString("type", Type);
                            newFragment.setArguments(args);
                            switchFragment(newFragment);
                        } else if (error_code.equalsIgnoreCase("0404")) {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                            alert.setMessage(error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Session session;
                                    session = new Session(getActivity());
                                    session.signOut();
                                }
                            });
                            alert.show();

                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_add_beneficiary_title));
                            alert.setMessage(getResources().getString(R.string.lbl_add_beneficiary_title) + " " + error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Fragment newFragment = null;
                                    newFragment = new FragmentBenefTo();
                                    switchFragment(newFragment);
                                }
                            });
                            alert.show();
                        }


                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

                ;

                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    }
                }

            });


        }
        // Checking Inquiry Beneficiary
        else{
            pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.lbl_loading_data));
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getSherlockActivity()));
            client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);

            RequestParams params = new RequestParams();


            try {
                String userid = AppHelper.getUserId(getActivity());
                String bankCode = AplConstants.Bankcode;
                String pcd = AplConstants.pcd;
                String benefNo  = txtInpBenefNo.getText().toString();
                String ccy = AplConstants.CurrencyIDR;
                String URL = AplConstants.InquiryBeneficiaryAPI;
                String appName = AplConstants.appName;
                String serviceName = URL.split("/")[4];
                String apiKey = AppHelper.getAccessKey(getActivity());
                UUID uniqueKey = UUID.randomUUID();
                String signature = uniqueKey+dateTime+ appName + serviceName+ bankCode + userid;


                Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
                SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
                sha256_HMAC.init(secret_key);
                byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
                String signatureKey = new String(encodeUrlSafe(hmacData));
                params.put("userid", userid);
                params.put("bankcode", bankCode);
                params.put("passcode", pcd);
                params.put("destaccountno", benefNo);
                params.put("destaccountccy", ccy);
                params.put("destbankcode", benefBankCode);
                params.put("rc_uuid", uniqueKey.toString());
                params.put("rc_dtime", dateTime);
                params.put("signature", signatureKey);

            }catch (Exception e){
                System.out.println("Error");
            }
            client.post(AplConstants.InquiryBeneficiaryAPI, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject object) {

                    try {
                        if (pDialog != null) {
                            pDialog.dismiss();
                        }
                        JSONObject body_data = object.getJSONObject("body_data");
                        String error_code = body_data.getString("error_code");
                        String error_message = body_data.getString("error_message");
                        String dest_account_name = body_data.getString("dest_acct_name");
                        String dest_bank_code = body_data.getString("dest_bank_code");

                        if (error_code.equalsIgnoreCase("0000")) {
                            /*
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle("Add New Beneficiary");
                            alert.setMessage("Add New Beneficiary : " + error_message);
                            alert.setPositiveButton("OK", null);
                            alert.show();*/

                            //  String benefName= txtInpBenefName.getText().toString();
                            String benefNo = txtInpBenefNo.getText().toString();
                            String benefEmail = txtInpBenefEmail.getText().toString();
                            String Type = AplConstants.TypeBeneficiary;

                            Fragment newFragment = null;
                            newFragment = new FragmentBenefToConfirm();
                            Bundle args = new Bundle();
                            args.putString("bankcode", benefBankCode);
                            args.putString("bankname", benefBankName);
                            args.putString("benefName", dest_account_name);
                            args.putString("benefNo", benefNo);
                            args.putString("benefEmail", benefEmail);
                            args.putString("type", Type);

                            newFragment.setArguments(args);
                            switchFragment(newFragment);
                        } else if (error_code.equalsIgnoreCase("0404")) {

                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                            alert.setMessage(error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Session session;
                                    session = new Session(getActivity());
                                    session.signOut();
                                }
                            });
                            alert.show();

                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_add_beneficiary_title));
                            alert.setMessage(getResources().getString(R.string.lbl_add_beneficiary_title) + error_message);
                            alert.setPositiveButton("OK", null);
                            Fragment newFragment = null;
                            newFragment = new FragmentBenefTo();
                            switchFragment(newFragment);
                            alert.show();
                        }


                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

                ;

                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    }
                }

            });
        }
    }

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);

        getSherlockActivity().getActionBar().removeAllTabs();
        getSherlockActivity().getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
    }


    public static byte[] encodeUrlSafe(byte[] data) {
        byte[] encode = Base64.encodeBase64(data);
        for (int i = 0; i < encode.length; i++) {
            if (encode[i] == '+') {
                encode[i] = '-';
            } else if (encode[i] == '=') {
                encode[i] = '_';
            } else if (encode[i] == '/') {
                encode[i] = '~';
            }
        }
        return encode;
    }

}

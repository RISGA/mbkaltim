package sgo.mobile.bankkaltim.app.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.bankkaltim.AppHelper;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.activities.MainActivity;
import sgo.mobile.bankkaltim.app.adapter.DestinationAccountAdapters;
import sgo.mobile.bankkaltim.app.adapter.SourceAccountAdapters;
import sgo.mobile.bankkaltim.app.models.DestinationAccountListModel;
import sgo.mobile.bankkaltim.app.models.SourceAccountModel;
import sgo.mobile.bankkaltim.app.ui.dialog.DefinedDialog;
import sgo.mobile.bankkaltim.conf.AplConstants;
import sgo.mobile.bankkaltim.frameworks.net.loopj.android.http.CustomSSLSocketFactory;
import sgo.mobile.bankkaltim.frameworks.security.Encrypt;
import sgo.mobile.bankkaltim.frameworks.security.HmacSHA256Signature;
import sgo.mobile.bankkaltim.frameworks.session.Session;
import sgo.mobile.bankkaltim.frameworks.text.NumberTextWacther;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;


public class FragmentTransferInHouse extends SherlockFragment {

    private ProgressDialog pDialog;
    public String userid, bankcode, passcode, type;

    int pos = -1;
    private static Spinner cbo_source_account = null;
    //private static Spinner cbo_destination_account = null;
    private static AutoCompleteTextView autoCompleteSourceAccount, autoCompleteBenefAccount;
    Encrypt encrypt = new Encrypt();
    Button btnDone;
    EditText inpAmount, inpMemo, inpDate;
    TextView lbl_Date;
    public JSONArray jsonArray;
    DateFormat df = new SimpleDateFormat(" dd MMMM yyyy");
    String date = df.format(Calendar.getInstance().getTime());
    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");//dd/MM/yyyy
    Date now = new Date();
    String dateTime = sdfDate.format(now);
    String uuid = "";
    String sKey = "";
    boolean networks;
    public String dataSourceAccount="";
    public String dataDestAccount="";
    HmacSHA256Signature hmacSHA256Signature = new HmacSHA256Signature();
    // String as;
    // String a = hmacSHA256Signature.main() ;
    String amount;
    private static final char[] CHARSET_AZ_09 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();

    public String[] account_no_arr, account_name_arr, ccy_id_arr, acct_balance_arr;
    private ArrayList<SourceAccountModel> SourceAcccountList = new ArrayList<SourceAccountModel>();
    private ArrayList<String> account_no = new ArrayList<String>();
    private ArrayList<String> account_name = new ArrayList<String>();
    private ArrayList<String> ccy_id = new ArrayList<String>();
    private ArrayList<String> acct_balance = new ArrayList<String>();
    public String benefValue;
    public String[] strArray;
    private List<SourceAccountModel> models;
    public String dataBankCode, dataBankName, dataAccountNo, dataAccountName, dataCcyId, dataEmail, dataBeneficiaryId, dataAccountType;
    int getCountSourceAccount;
    int getCountDestAccount;
    public String posisi;
    public String[] dest_bank_code_arr, dest_bank_name_arr, dest_account_no_arr, dest_account_name_arr, dest_ccy_id_arr, dest_email_arr;
    private ArrayList<DestinationAccountListModel> DestinationAccountList = new ArrayList<DestinationAccountListModel>();
    public int posInt;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_transfer_inhouse, container, false);
        cbo_source_account = (Spinner) view.findViewById(R.id.cbo_source_account);
//        cbo_destination_account = (Spinner)view.findViewById(R.id.cbo_benef_account);
        autoCompleteBenefAccount = (AutoCompleteTextView) view.findViewById(R.id.autoCompleteBenefAccount);
        dataDestAccount = AppHelper.getDestAccount(getSherlockActivity());

        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                Context.INPUT_METHOD_SERVICE);


        inpAmount = (EditText) view.findViewById(R.id.inpAmount);
        inpAmount.addTextChangedListener(new NumberTextWacther(inpAmount));
        inpMemo = (EditText) view.findViewById(R.id.inpMemo);

        imm.hideSoftInputFromWindow(inpAmount.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(inpMemo.getWindowToken(), 0);
        inpAmount.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                imm.showSoftInputFromInputMethod(inpAmount.getWindowToken(), 0);
            }
        });


        inpMemo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                imm.showSoftInputFromInputMethod(inpMemo.getWindowToken(), 0);
            }
        });

        lbl_Date = (TextView) view.findViewById(R.id.lblDate);
        lbl_Date.setText(date);
        lbl_Date.setClickable(true);
        lbl_Date.setFocusable(false);

        lbl_Date.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                alert.setTitle(getResources().getString(R.string.lbl_tab_inhouse));
                alert.setMessage(getResources().getString(R.string.lbl_transfer_date_alert));
                alert.setPositiveButton("OK", null);
                alert.show();
                return;
            }
        });

        autoCompleteBenefAccount.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                autoCompleteBenefAccount.showDropDown();

            }


        });


        autoCompleteBenefAccount.setOnItemClickListener(new AdapterView.OnItemClickListener() {


            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DestinationAccountListModel destinationAccountListModel = (DestinationAccountListModel) parent.getItemAtPosition(position);
                dataBankCode = destinationAccountListModel.dest_bank_code;
                dataBankName = destinationAccountListModel.dest_bank_name;
                dataAccountNo = destinationAccountListModel.dest_account_no;
                dataAccountName = destinationAccountListModel.dest_account_name;
                dataCcyId = destinationAccountListModel.dest_ccy_id;
                dataEmail = destinationAccountListModel.dest_email;
                benefValue = destinationAccountListModel.toString();
                posisi = parent.getItemAtPosition(position).toString();
            }


        });

//        autoCompleteBenefAccount.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                btnDone.setVisibility(View.VISIBLE);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//                btnDone.setVisibility(View.INVISIBLE);
//            }
//        });


//        autoCompleteBenefAccount.setValidator(new Validatore());
//        autoCompleteBenefAccount.setOnFocusChangeListener(new FocusListener());

        btnDone = (Button) view.findViewById(R.id.btnDone);

        btnDone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                try {

                    if (autoCompleteBenefAccount.getText().toString().trim().equalsIgnoreCase("")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_tab_inhouse));
                        alert.setMessage(getResources().getString(R.string.lbl_transfer_no_benef));
                        alert.setPositiveButton("OK", null);
                        alert.show();
                        return;
                    }
                    if(!autoCompleteBenefAccount.getText().toString().equals(posisi)){
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_tab_inhouse));
                        alert.setMessage(getResources().getString(R.string.lbl_transfer_no_select_benef));
                        alert.setPositiveButton("OK", null);
                        alert.show();
                        return;
                    }

                    final String txtAmount = inpAmount.getText().toString();
                    if (txtAmount.equalsIgnoreCase("")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_tab_inhouse));
                        alert.setMessage(getResources().getString(R.string.lbl_transfer_alert_amount));
                        alert.setPositiveButton("OK", null);
                        alert.show();
                        return;
                    }

                    final String txtMessage = inpMemo.getText().toString();
                    if (txtMessage.equalsIgnoreCase("")) {

                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_tab_inhouse));
                        alert.setMessage(getResources().getString(R.string.lbl_transfer_alert_message));
                        alert.setPositiveButton("OK", null);
                        alert.show();
                        return;
                    }


                    transferValidation();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });



        initViews();
        initDestAccount();
        return view;
    }

    public class Validatore implements AutoCompleteTextView.Validator{
        String[] wew = new String[]{dataDestAccount};
        @Override
        public boolean isValid(CharSequence text) {
            Arrays.sort(wew);
            if(Arrays.binarySearch(wew, text.toString())>0) {
                return true;
            }
            return false;
        }

        @Override
        public CharSequence fixText(CharSequence invalidText) {
            return null;
        }
    }

    public class FocusListener implements View.OnFocusChangeListener{

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (v.getId() == R.id.autoCompleteBenefAccount && !hasFocus) {
                ((AutoCompleteTextView)v).performValidation();
            }
        }
    }
    private void initViews() {
        SourceAcccountList.clear();
        dataSourceAccount = AppHelper.getSourceAccount(getSherlockActivity());
        if(dataSourceAccount.equals("")){
            SourceAcccountList.clear();
            pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.lbl_loading_data));
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getActivity()));
            client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
            RequestParams params = new RequestParams();
            try {
                String userid = AppHelper.getUserId(getActivity());
                String bankcode = AplConstants.Bankcode;
                String pcd = AplConstants.pcd;
                String URL = AplConstants.SourceAccountListAPI;
                String appName = AplConstants.appName;
                String serviceName = URL.split("/")[4];
                String apiKey = AppHelper.getAccessKey(getActivity());
                UUID uniqueKey = UUID.randomUUID();
                String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;


                Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
                SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
                sha256_HMAC.init(secret_key);
                byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
                String signatureKey = new String(encodeUrlSafe(hmacData));
                params.put("userid", userid);
                params.put("bankcode", bankcode);
                params.put("passcode", pcd);
                params.put("rc_uuid", uniqueKey.toString());
                params.put("rc_dtime", dateTime);
                params.put("signature", signatureKey);

            } catch (Exception e) {
                System.out.println("Error");
            }

            client.post(AplConstants.SourceAccountListAPI, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject object) {
                    try {

                        if (pDialog != null) {
                            pDialog.dismiss();
                        }
                        JSONObject body_data = object.getJSONObject("body_data");
                        String error_code = body_data.getString("error_code");
                        String error_message = body_data.getString("error_message");


                        //--------------------------------------------------------------------------------------------
                        if (error_code.equalsIgnoreCase("0000")) {
                            JSONArray data = body_data.getJSONArray("source_account_data"); // this is the "items: [ ] part


                            int len = data.length();
                            account_no_arr = new String[len];
                            account_name_arr = new String[len];
                            ccy_id_arr = new String[len];
                            for (int i = 0; i < data.length(); i++) {
                                account_no_arr[i] = data.getJSONObject(i).getString("account_no");
                                account_name_arr[i] = data.getJSONObject(i).getString("account_name");
                                ccy_id_arr[i] = data.getJSONObject(i).getString("ccy_id");
                                SourceAcccountList.add(new SourceAccountModel(account_no_arr[i], account_name_arr[i], ccy_id_arr[i]));

                                SecurePreferences prefs = new SecurePreferences(getSherlockActivity());
                                SecurePreferences.Editor mEditor = prefs.edit();
                                mEditor.putString("sourceAccountData", data.toString());
                                mEditor.apply();
                            }
                            SourceAccountAdapters cAdapter = new SourceAccountAdapters(getActivity(), android.R.layout.simple_spinner_item, SourceAcccountList);
                            cbo_source_account.setAdapter(cAdapter);
                            cbo_source_account.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    SourceAccountModel sourceAccountBean = SourceAcccountList.get(position);
                                    String curr = sourceAccountBean.getCcy_id();
                                    if (curr.equalsIgnoreCase("IDR")) {
                                        inpAmount.setHint(getResources().getString(R.string.lbl_transfer_amount_hint_id));
                                    } else {
                                        inpAmount.setHint(getResources().getString(R.string.lbl_transfer_amount_hint_us));
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });

                        } else if (error_code.equalsIgnoreCase("0404")) {

                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                            alert.setMessage(error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Session session;
                                    session = new Session(getActivity());
                                    session.signOut();
                                }
                            });
                            alert.show();

                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_tab_inhouse));
                            alert.setMessage(error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Fragment newFragment = null;
                                    newFragment = new FragmentMainTransfer();
                                    switchFragment(newFragment);
                                }
                            });
                            alert.show();
                        }
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

                public void onFailure(Throwable error, String content) {
                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    if(error.getMessage().equalsIgnoreCase("no peer certificate"))
                    {
                        pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.SSLhandler_dialog_message) + error.toString());
                    }
                    else
                    {
                        pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.inethandler_dialog_message) + error.toString());
                    }
                    Toast.makeText(getActivity(), getResources().getString(R.string.lbl_alert_unexpected_connection), Toast.LENGTH_LONG).show();
                }

            });

        }else{
            SourceAcccountList.clear();
            try {
                JSONArray data = new JSONArray(dataSourceAccount);
                int len = data.length();
                account_no_arr = new String[len];
                account_name_arr = new String[len];
                ccy_id_arr = new String[len];
                for (int i = 0; i < data.length(); i++) {
                    account_no_arr[i] = data.getJSONObject(i).getString("account_no");
                    account_name_arr[i] = data.getJSONObject(i).getString("account_name");
                    ccy_id_arr[i] = data.getJSONObject(i).getString("ccy_id");
                    SourceAcccountList.add(new SourceAccountModel(account_no_arr[i], account_name_arr[i], ccy_id_arr[i]));
                }
                SourceAccountAdapters cAdapter = new SourceAccountAdapters(getActivity(), android.R.layout.simple_spinner_item, SourceAcccountList);
                cbo_source_account.setAdapter(cAdapter);
                cbo_source_account.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        SourceAccountModel sourceAccountBean = SourceAcccountList.get(position);
                        String curr = sourceAccountBean.getCcy_id();
                        if (curr.equalsIgnoreCase("IDR")) {
                            inpAmount.setHint(getResources().getString(R.string.lbl_transfer_amount_hint_id));
                        } else {
                            inpAmount.setHint(getResources().getString(R.string.lbl_transfer_amount_hint_us));
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }


    private void initDestAccount() {
        DestinationAccountList.clear();
        dataDestAccount = AppHelper.getDestAccount(getSherlockActivity());
        if (dataDestAccount.equals("")) {
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getSherlockActivity()));
            client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
            RequestParams params = new RequestParams();

            try {
                String userid = AppHelper.getUserId(getActivity());
                String bankcode = AplConstants.Bankcode;
                String pcd = AplConstants.pcd;
                String type = AplConstants.TypeInhouse;
                String destBankCode = AplConstants.Bankcode;
                String URL = AplConstants.DestinationAccountListAPI;
                String appName = AplConstants.appName;
                String serviceName = URL.split("/")[4];
                String apiKey = AppHelper.getAccessKey(getActivity());
                UUID uniqueKey = UUID.randomUUID();
                String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;


                Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
                SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
                sha256_HMAC.init(secret_key);
                byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
                String signatureKey = new String(encodeUrlSafe(hmacData));
                params.put("userid", userid);
                params.put("bankcode", bankcode);
                params.put("passcode", pcd);
                params.put("type", type);
                params.put("destbankcode", destBankCode);
                params.put("rc_uuid", uniqueKey.toString());
                params.put("rc_dtime", dateTime);
                params.put("signature", signatureKey);

            } catch (Exception e) {
                System.out.println("Error");
            }

            client.post(AplConstants.DestinationAccountListAPI, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject object) {
                    try {


                        if (pDialog != null) {
                            pDialog.dismiss();
                        }
                        JSONObject body_data = object.getJSONObject("body_data");
                        String error_code = body_data.getString("error_code");
                        String error_message = body_data.getString("error_message");

                        if (error_code.equalsIgnoreCase("0000")) {
                            //--------------------------------------------------------------------------------------------
                            JSONArray data = body_data.getJSONArray("balance_account_data"); // this is the "items: [ ] part
                            if (data.isNull(0)) {
                                ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                                        R.array.no_benef_bank, android.R.layout.simple_spinner_item);
                                autoCompleteBenefAccount.setAdapter(adapter);
                                autoCompleteBenefAccount.setClickable(false);
                                btnDone.setEnabled(false);
                            } else {

                                int len = data.length();
                                dest_bank_code_arr = new String[len];
                                dest_bank_name_arr = new String[len];
                                dest_account_no_arr = new String[len];
                                dest_account_name_arr = new String[len];
                                dest_ccy_id_arr = new String[len];
                                dest_email_arr = new String[len];
                                for (int i = 0; i < data.length(); i++) {
                                    dest_bank_code_arr[i] = data.getJSONObject(i).getString("bank_code");
                                    dest_bank_name_arr[i] = data.getJSONObject(i).getString("bank_name");
                                    dest_account_no_arr[i] = data.getJSONObject(i).getString("account_no");
                                    dest_account_name_arr[i] = data.getJSONObject(i).getString("account_name");
                                    dest_ccy_id_arr[i] = data.getJSONObject(i).getString("ccy_id");
                                    dest_email_arr[i] = data.getJSONObject(i).getString("email");
                                    DestinationAccountList.add(new DestinationAccountListModel(dest_bank_code_arr[i], dest_bank_name_arr[i], dest_account_no_arr[i], dest_account_name_arr[i], dest_ccy_id_arr[i], dest_email_arr[i]));

                                    SecurePreferences prefs = new SecurePreferences(getSherlockActivity());
                                    SecurePreferences.Editor mEditor = prefs.edit();
                                    mEditor.putString("destAccountData", data.toString());
                                    mEditor.apply();

                                }


                                DestinationAccountAdapters cAdapter = new DestinationAccountAdapters(getActivity(), android.R.layout.simple_spinner_item, DestinationAccountList);
                                autoCompleteBenefAccount.setAdapter(cAdapter);
                                autoCompleteBenefAccount.setThreshold(1);
                            }
                        } else if (error_code.equalsIgnoreCase("0404")) {

                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                            alert.setMessage(error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Session session;
                                    session = new Session(getActivity());
                                    session.signOut();
                                }
                            });
                            alert.show();

                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_tab_inhouse));
                            alert.setMessage(error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Fragment newFragment = null;
                                    newFragment = new FragmentMainTransfer();
                                    switchFragment(newFragment);
                                }
                            });
                            alert.show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                public void onFailure(int statusCode, Header[] headers, Throwable error, JSONObject errorResponse) {
                    if (pDialog != null) {
                        pDialog.dismiss();
                    }

                    if(error.getMessage().equalsIgnoreCase("no peer certificate"))
                    {
                        pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.SSLhandler_dialog_message) + error.toString());
                    }
                    else
                    {
                        pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.inethandler_dialog_message) + error.toString());
                    }
                    Toast.makeText(getActivity(), getResources().getString(R.string.lbl_alert_unexpected_connection), Toast.LENGTH_LONG).show();
                }


            });
        } else {
            DestinationAccountList.clear();
            dataDestAccount = AppHelper.getDestAccount(getSherlockActivity());

            try {
                JSONArray data = new JSONArray(dataDestAccount);
                if (data.isNull(0)) {
                    ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                            R.array.no_benef_bank, android.R.layout.simple_spinner_item);
                    autoCompleteBenefAccount.setAdapter(adapter);
                    autoCompleteBenefAccount.setClickable(false);
                } else {
                    int len = data.length();
                    dest_bank_code_arr = new String[len];
                    dest_bank_name_arr = new String[len];
                    dest_account_no_arr = new String[len];
                    dest_account_name_arr = new String[len];
                    dest_ccy_id_arr = new String[len];
                    dest_email_arr = new String[len];
                    for (int i = 0; i < data.length(); i++) {
                        dest_bank_code_arr[i] = data.getJSONObject(i).getString("bank_code");
                        dest_bank_name_arr[i] = data.getJSONObject(i).getString("bank_name");
                        dest_account_no_arr[i] = data.getJSONObject(i).getString("account_no");
                        dest_account_name_arr[i] = data.getJSONObject(i).getString("account_name");
                        dest_ccy_id_arr[i] = data.getJSONObject(i).getString("ccy_id");
                        dest_email_arr[i] = data.getJSONObject(i).getString("email");
                        DestinationAccountList.add(new DestinationAccountListModel(dest_bank_code_arr[i], dest_bank_name_arr[i], dest_account_no_arr[i], dest_account_name_arr[i], dest_ccy_id_arr[i], dest_email_arr[i]));


                    }
                    DestinationAccountAdapters cAdapter = new DestinationAccountAdapters(getSherlockActivity(), android.R.layout.simple_spinner_item, DestinationAccountList);
                    autoCompleteBenefAccount.setAdapter(cAdapter);
                    autoCompleteBenefAccount.setThreshold(1);
                }



            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public static byte[] encodeUrlSafe(byte[] data) {
        byte[] encode = Base64.encodeBase64(data);
        for (int i = 0; i < encode.length; i++) {
            if (encode[i] == '+') {
                encode[i] = '-';
            } else if (encode[i] == '=') {
                encode[i] = '_';
            } else if (encode[i] == '/') {
                encode[i] = '~';
            }
        }
        return encode;
    }


    private void transferValidation() {


        pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.lbl_loading_process));
        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getSherlockActivity()));
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
        RequestParams params = new RequestParams();

        SourceAccountModel sourceAccountBean = (SourceAccountModel) cbo_source_account.getSelectedItem();
        String sourceAcct = sourceAccountBean.account_no;
        String sourceAcctCccy = sourceAccountBean.ccy_id;

        try {
            String userid = AppHelper.getUserId(getActivity());
            String bankcode = AplConstants.Bankcode;
            String pcd = AplConstants.pcd;
            String curency = AplConstants.CurrencyIDR;
            String URL = AplConstants.FundTransferAPI;
            String appName = AplConstants.appName;
            String serviceName = URL.split("/")[4];
            String apiKey = AppHelper.getAccessKey(getActivity());
            UUID uniqueKey = UUID.randomUUID();


            //  String benefNo = destinationAccountListModel.dest_account_no;
            // String benefEmail = destinationAccountListModel.dest_email;
            amount = inpAmount.getText().toString().replaceAll("[\\-\\+\\.\\^:,]", "");
            String message = inpMemo.getText().toString();
            String executetransferValidate = AplConstants.executetransferValidate;
            String currencyType = AplConstants.CurrencyIDR;
            String trxId = randomTrxId(CHARSET_AZ_09, 10);

            String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;

            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
            String signatureKey = new String(encodeUrlSafe(hmacData));
            params.put("userid", userid);
            params.put("bankcode", bankcode);
            params.put("passcode", pcd);
            params.put("sourceacctccy", sourceAcctCccy);
            params.put("sourceacctno", sourceAcct);
            params.put("destaccountno", dataAccountNo);
            params.put("destaccountemail", dataEmail);
            params.put("transactionamount", amount);
            params.put("transactionid", trxId);
            params.put("description", message);
            params.put("executetransfer", executetransferValidate);
            params.put("rc_uuid", uniqueKey.toString());
            params.put("rc_dtime", dateTime);
            params.put("signature", signatureKey);

        } catch (Exception e) {
            System.out.println("Error");
        }

        client.post(AplConstants.FundTransferAPI, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject object) {

                try {

                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    JSONObject body_data = object.getJSONObject("body_data");
                    String error_code = body_data.getString("error_code");
                    String error_message = body_data.getString("error_message");
                    if (error_code.equals("0000")) {
                        SourceAccountModel sourceAccountBean = (SourceAccountModel) cbo_source_account.getSelectedItem();
                        String source_account_name = sourceAccountBean.account_name;
                        String source_account_no = sourceAccountBean.account_no;

                        amount = inpAmount.getText().toString().replaceAll("[\\-\\+\\.\\^:,]", "");
                        String memo = inpMemo.getText().toString();
                        String paymentdate = lbl_Date.getText().toString();
                        Fragment newFragment = null;
                        newFragment = new FragmentTransferInHouseConfirmToken();
                        Bundle args = new Bundle();
                        args.putString("source_account_name", source_account_name);
                        args.putString("source_account_no", source_account_no);
                        args.putString("benef_account_name", dataAccountName);
                        args.putString("benef_account_no", dataAccountNo);
                        args.putString("totalAmount", amount);
                        args.putString("memo", memo);
                        args.putString("paymentdate", paymentdate);
                        args.putString("email", dataEmail);
                        newFragment.setArguments(args);
                        switchFragment(newFragment);


                    } else if (error_code.equalsIgnoreCase("0404")) {

                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                        alert.setMessage(error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Session session;
                                session = new Session(getActivity());
                                session.signOut();
                            }
                        });
                        alert.show();

                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_tab_inhouse));
                        alert.setMessage(getResources().getString(R.string.lbl_transfer_success_inhouse) + " " + error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Fragment newFragment = null;
                                newFragment = new FragmentMainTransfer();
                                switchFragment(newFragment);
                            }
                        });
                        alert.show();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            public void onFailure(int statusCode, Header[] headers, Throwable error, JSONObject errorResponse) {
                if (pDialog != null) {
                    pDialog.dismiss();
                }

                Toast.makeText(getActivity(),"HERE :"+ headers +""+statusCode+""+ error +""+ errorResponse, Toast.LENGTH_LONG).show();

                if(error.getMessage().equalsIgnoreCase("no peer certificate"))
                {
                    pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.SSLhandler_dialog_message) + error.toString());
                }
                else
                {
                    pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.inethandler_dialog_message) + error.toString());
                }
                Toast.makeText(getActivity(), getResources().getString(R.string.lbl_alert_unexpected_connection), Toast.LENGTH_LONG).show();
            }

        });

    }

    public static String randomTrxId(char[] characterSet, int length) {
        Random random = new java.security.SecureRandom();
        char[] result = new char[length];
        for (int i = 0; i < result.length; i++) {
            // picks a random index out of character set > random character
            int randomCharIndex = random.nextInt(characterSet.length);
            result[i] = characterSet[randomCharIndex];
        }
        return new String(result);
    }

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);

        getSherlockActivity().getActionBar().removeAllTabs();
        getSherlockActivity().getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
    }
}
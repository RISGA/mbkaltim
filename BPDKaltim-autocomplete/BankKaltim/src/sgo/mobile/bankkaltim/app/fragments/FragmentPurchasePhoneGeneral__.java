package sgo.mobile.bankkaltim.app.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.bankkaltim.AppHelper;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.activities.MainActivity;
import sgo.mobile.bankkaltim.app.adapter.BillerVoucherPhoneCatalogGeneral;
import sgo.mobile.bankkaltim.app.adapter.SourceAccountAdapter;
import sgo.mobile.bankkaltim.app.beans.BillerVoucherNominalBean;
import sgo.mobile.bankkaltim.app.beans.BillerVoucherPhoneCatalogBean;
import sgo.mobile.bankkaltim.app.beans.SourceAccountBean;
import sgo.mobile.bankkaltim.app.models.purchase.PurchaseCatalogPhoneModel;
import sgo.mobile.bankkaltim.app.ui.dialog.DefinedDialog;
import sgo.mobile.bankkaltim.conf.AplConstants;
import sgo.mobile.bankkaltim.frameworks.net.Networks;
import sgo.mobile.bankkaltim.frameworks.net.loopj.android.http.CustomSSLSocketFactory;
import sgo.mobile.bankkaltim.frameworks.session.Session;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

/**
 * Created by SGO-Mobile-DEV on 4/23/2015.
 */
public class FragmentPurchasePhoneGeneral__ extends SherlockFragment {
    private static Spinner cbo_source_account = null;
    public String[] account_no_arr, account_name_arr, ccy_id_arr;
    private ArrayList<SourceAccountBean> SourceAcccountList = new ArrayList<SourceAccountBean>();

    private static Spinner cbo_voucher_name = null;
    public String[] biller_code_arr, biller_name_arr, biller_inquiry_arr, biller_id_arr, product_code_arr, product_name_arr, product_amount_arr;
    private ArrayList<PurchaseCatalogPhoneModel> BillerVoucherList = new ArrayList<PurchaseCatalogPhoneModel>();

    private static Spinner cbo_voucher_nominal = null;
    public String[] catalog_code_arr, catalog_nominal_arr;
    private ArrayList<BillerVoucherNominalBean> VoucherNominalList = new ArrayList<BillerVoucherNominalBean>();
    public String phoneNumber;
    private ProgressDialog pDialog;
    public String userid, bankcode, passcode, type, menu_biller_code,billerCode;
    Button btnDone, btnBack;
    EditText inpPhoneNumber;
    TextView lbl_header, lbl_title, lbl_note;
    ImageView ic_img;
    String purchaseElectricity = AplConstants.purchaseElectricityVoucher;
    String purchaseVoucherPhone = AplConstants.purchasePhoneVoucher;
    String purchaseOthers = AplConstants.purchaseOthers;

    public String smartfrenId ="1080";
    public String telkomselId="1074";
    public String indosatId="1075";
    public String indosatInternetId="1075";
    public String indosatSmsId="1075";
    public String xlId="1075";

    public String[] denomTelkomselRegular={"5000","10000","20000","25000","50000","10000"};

    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");//dd/MM/yyyy
    Date now = new Date();
    String dateTime = sdfDate.format(now);
    boolean networks;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_purchase_general_phone, container, false);
        Bundle bundle = this.getArguments();

        menu_biller_code = bundle.getString("menu_biller_code");
        billerCode = bundle.getString("billerCode");
       // Toast.makeText(getActivity(),billerCode, Toast.LENGTH_LONG).show();
        //Toast.makeText(getActivity(), billerCode, Toast.LENGTH_LONG).show();

        cbo_source_account = (Spinner) view.findViewById(R.id.cbo_source_account);
        cbo_voucher_name = (Spinner) view.findViewById(R.id.cbo_voucher);
        // cbo_voucher_nominal = (Spinner) view.findViewById(R.id.cbo_voucher_nominal);
        inpPhoneNumber = (EditText) view.findViewById(R.id.inpPhoneNumber);
        lbl_header = (TextView) view.findViewById(R.id.label_header);
        ic_img = (ImageView) view.findViewById(R.id.ic_img);
        lbl_title = (TextView) view.findViewById(R.id.lbl_title);
       // lbl_note = (TextView) view.findViewById(R.id.label_note);

        lbl_title.setText("ID Meter");
        lbl_title.setText("Handphone No");
        ic_img.setImageResource(R.drawable.ic_mobile);
        inpPhoneNumber.setHint("* Input Nandphone No");
       // lbl_note.setText("*min 10 digits, max 16 digits");
        checkMenuCode();

        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(inpPhoneNumber.getWindowToken(), 0);

        btnDone = (Button) view.findViewById(R.id.btnDone);
        btnDone.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                try {
                    final String txtMemberNo = inpPhoneNumber.getText().toString();
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());

                    if (txtMemberNo.equals("")) {
                        alert.setTitle(getResources().getString(R.string.lbl_purchase_title));
                        alert.setMessage(getResources().getString(R.string.lbl_purchase_hp_orderid_alert));
                        alert.setPositiveButton("OK", null);
                        alert.show();
                        return;
                    }else if(txtMemberNo.length() <  10 && txtMemberNo.length() > 16){

                        alert.setTitle(getResources().getString(R.string.lbl_purchase_title));
                        alert.setMessage(getResources().getString(R.string.lbl_purchase_hp_orderid_alert_incorect));
                        alert.setPositiveButton("OK", null);
                        alert.show();
                        return;

                    }else{
                        try {
                            networks = new Networks().isConnectingToInternet(getActivity());
                            if (networks) {
                               // Toast.makeText(getActivity(), "Sorry, this process will available soon !", Toast.LENGTH_LONG).show();
                                 getPurchase();
                            } else {
                                AlertDialog.Builder alerts = new AlertDialog.Builder(getActivity());
                                alerts.setTitle(getResources().getString(R.string.lbl_purchase_title));
                                alerts.setMessage(getResources().getString(R.string.lbl_alert_connection));
                                alerts.setPositiveButton("OK", null);
                                alerts.show();

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });
        btnBack = (Button) view.findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Fragment newFragment = null;
                newFragment = new FragmentPurchasePhoneVoucher();
                Bundle args = new Bundle();
                args.putString("menu_biller_code", menu_biller_code);
                newFragment.setArguments(args);
                switchFragment(newFragment);

            }
        });


        //
        // Toast.makeText(getActivity(),menu_biller_code, Toast.LENGTH_LONG).show();

        initViews();
        return view;

    }


    public void getPurchase() {
        SourceAccountBean sourceAccountBean = (SourceAccountBean) cbo_source_account.getSelectedItem();
        String source_account_no = sourceAccountBean.account_no;
        String source_account_name = sourceAccountBean.account_name;

        BillerVoucherPhoneCatalogBean billerVoucherCatalogBean = (BillerVoucherPhoneCatalogBean) cbo_voucher_name.getSelectedItem();
        String vourcherCode = billerVoucherCatalogBean.billerCode;
        String voucherName = billerVoucherCatalogBean.billerName;
        String productCode = billerVoucherCatalogBean.productCode;
        String productAmount = billerVoucherCatalogBean.productAmount;
        String inquiry = billerVoucherCatalogBean.billerInquiry;

        phoneNumber = inpPhoneNumber.getText().toString();

        if(inquiry.equalsIgnoreCase("Y")){
            checkingBiller();
        }else{
            Fragment newFragment = null;
            newFragment = new FragmentPurchaseConfirm();
            Bundle args = new Bundle();
            args.putString("menu_biller_code", menu_biller_code);
            args.putString("source_account_no", source_account_no);
            args.putString("source_account_name", source_account_name);
            args.putString("product_code", productCode);
            args.putString("biller_code", vourcherCode);
            args.putString("biller_name", voucherName);
            args.putString("catalog_code", productCode);
            args.putString("catalog_nominal", productAmount);
            args.putString("phone_number", phoneNumber);
            newFragment.setArguments(args);
            switchFragment(newFragment);
        }




    }

    public void checkingBiller() {
        SourceAccountBean sourceAccountBean = (SourceAccountBean) cbo_source_account.getSelectedItem();
        final String source_account_no = sourceAccountBean.account_no;
        final String source_account_name = sourceAccountBean.account_name;

        BillerVoucherPhoneCatalogBean billerVoucherCatalogBean = (BillerVoucherPhoneCatalogBean) cbo_voucher_name.getSelectedItem();
        final String billerCode = billerVoucherCatalogBean.billerCode;
        final String productCode = billerVoucherCatalogBean.productCode;
        final String productAmount = billerVoucherCatalogBean.productAmount;
        pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.lbl_loading_process));

        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getActivity()));
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
        final RequestParams params = new RequestParams();
        phoneNumber = inpPhoneNumber.getText().toString();
        try {
            String userid = AppHelper.getUserId(getActivity());
            String bankcode = AplConstants.Bankcode;
            String pcd = AplConstants.pcd;
            String URL = AplConstants.InquiryBillerPayment;
            String appName = AplConstants.appName;
            String serviceName = URL.split("/")[4];
            String apiKey = AppHelper.getAccessKey(getActivity());
            UUID uniqueKey = UUID.randomUUID();
            String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;
            //Toast.makeText(getActivity(), userid + " " + apiKey, Toast.LENGTH_LONG).show();

            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
            String signatureKey = new String(encodeUrlSafe(hmacData));
            params.put("userid", userid);
            params.put("bankcode", bankcode);
            params.put("passcode", pcd);
            params.put("productcode",productCode);
            params.put("orderid", phoneNumber);
            params.put("rc_uuid", uniqueKey.toString());
            params.put("rc_dtime", dateTime);
            params.put("signature", signatureKey);

        } catch (Exception e) {
            System.out.println("Error");
        }

        client.post(AplConstants.InquiryBillerPayment, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject object) {


                try {

                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    JSONObject body_data = object.getJSONObject("body_data");
                    String error_code = body_data.getString("error_code");
                    String error_message = body_data.getString("error_message");
                    if(error_code.equalsIgnoreCase("0000")){
                        JSONObject biller_data = body_data.getJSONObject("biller_data");

                        String orderId = biller_data.getString("orderId");
                        String amount = biller_data.getString("amount");
                        String billerId = biller_data.getString("billerId");
                        String billerCode = biller_data.getString("billerCode");
                        final String billerName = biller_data.getString("billerName");
                        String description = biller_data.getString("description");
                        String denomination = biller_data.getString("denomination");
                        final String price = biller_data.getString("price");


                                Fragment newFragment = null;
                                newFragment = new FragmentPurchaseConfirm();
                                Bundle args = new Bundle();
                                args.putString("menu_biller_code", menu_biller_code);
                                args.putString("source_account_no", source_account_no);
                                args.putString("source_account_name", source_account_name);
                                args.putString("product_code", productCode);
                                args.putString("biller_code", billerCode);
                                args.putString("biller_name", billerName);
                                args.putString("catalog_code", productCode);
                                args.putString("catalog_nominal", price);
                                args.putString("phone_number", phoneNumber);
                                args.putString("productAmount", productAmount);
                                newFragment.setArguments(args);
                                switchFragment(newFragment);


                    } else if(error_code.equalsIgnoreCase("0404")){
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                        alert.setMessage(error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Session session;
                                session = new Session(getActivity());
                                session.signOut();
                            }
                        });
                        alert.show();

                    }else if(error_code.equalsIgnoreCase("14")){
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_purchase_title));
                        alert.setMessage(getResources().getString(R.string.lbl_purchase_hp_orderid_alert_incorect));
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Fragment newFragment = null;
                                newFragment = new FragmentPaymentGeneral();
                                Bundle args = new Bundle();
                                args.putString("menu_biller_code", menu_biller_code);
                                newFragment.setArguments(args);
                                switchFragment(newFragment);
                            }
                        });
                        alert.show();
                    }else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_purchase_title));
                        alert.setMessage(getResources().getString(R.string.lbl_purchase_title) + error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Fragment newFragment = null;
                                newFragment = new FragmentPurchasePhoneVoucher();
                                Bundle args = new Bundle();
                                args.putString("menu_biller_code", menu_biller_code);
                                newFragment.setArguments(args);
                                switchFragment(newFragment);
                            }
                        });
                        alert.show();

                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (pDialog != null) {
                    pDialog.dismiss();
                }
                if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                } else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                }
            }
        });
    }


    private void initViews() {
        pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.lbl_loading_data));

        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getActivity()));
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
        RequestParams params = new RequestParams();

        try {
            String userid = AppHelper.getUserId(getActivity());
            String bankcode = AplConstants.Bankcode;
            String pcd = AplConstants.pcd;
            String URL = AplConstants.SourceAccountListAPI;
            String appName = AplConstants.appName;
            String serviceName = URL.split("/")[4];
            String apiKey = AppHelper.getAccessKey(getActivity());
            UUID uniqueKey = UUID.randomUUID();
            String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;
            //Toast.makeText(getActivity(), userid + " " + apiKey, Toast.LENGTH_LONG).show();

            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
            String signatureKey = new String(encodeUrlSafe(hmacData));
            params.put("userid", userid);
            params.put("bankcode", bankcode);
            params.put("passcode", pcd);
            params.put("rc_uuid", uniqueKey.toString());
            params.put("rc_dtime", dateTime);
            params.put("signature", signatureKey);

        } catch (Exception e) {
            System.out.println("Error");
        }
        client.post(AplConstants.SourceAccountListAPI, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject object) {


                try {

                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    JSONObject body_data = object.getJSONObject("body_data");
                    // parse json data and store into arraylist variables
                    String error_code = body_data.getString("error_code");
                    String error_message = body_data.getString("error_message");
                    JSONArray data = body_data.getJSONArray("source_account_data"); // this is the "items: [ ] part

                    if (error_code.equalsIgnoreCase("0000")) {

                        int len = data.length();
                        account_no_arr = new String[len];
                        account_name_arr = new String[len];
                        ccy_id_arr = new String[len];
                        for (int i = 0; i < data.length(); i++) {
                            account_no_arr[i] = data.getJSONObject(i).getString("account_no");
                            account_name_arr[i] = data.getJSONObject(i).getString("account_name");
                            ccy_id_arr[i] = data.getJSONObject(i).getString("ccy_id");
                            SourceAcccountList.add(new SourceAccountBean(account_no_arr[i], account_name_arr[i], ccy_id_arr[i]));
                        }
                        SourceAccountAdapter cAdapter = new SourceAccountAdapter(getActivity(), android.R.layout.simple_spinner_item, SourceAcccountList);
                        cbo_source_account.setAdapter(cAdapter);

                    } else if (error_code.equalsIgnoreCase("0404")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                        alert.setMessage(error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Session session;
                                session = new Session(getActivity());
                                session.signOut();
                            }
                        });
                        alert.show();

                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_purchase_title));
                        alert.setMessage(getResources().getString(R.string.lbl_purchase_title) + error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Fragment newFragment = null;
                                newFragment = new FragmentPurchaseGeneral();
                                switchFragment(newFragment);
                            }
                        });
                        alert.show();
                    }


                    //get Voucher List
                    BillerVoucherList.clear();
                    AsyncHttpClient client = new AsyncHttpClient();
                    client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getActivity()));
                    client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
                    RequestParams params = new RequestParams();

                    try {

                        String userid = AppHelper.getUserId(getActivity());
                        String bankcode = AplConstants.Bankcode;
                        String pcd = AplConstants.pcd;
                        String URL = AplConstants.InquiryBiller;
                        String appName = AplConstants.appName;
                        String serviceName = URL.split("/")[4];
                        String apiKey = AppHelper.getAccessKey(getActivity());
                        UUID uniqueKey = UUID.randomUUID();
                        String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;
                        Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
                        SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
                        sha256_HMAC.init(secret_key);
                        byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
                        String signatureKey = new String(encodeUrlSafe(hmacData));
                        params.put("userid", userid);
                        params.put("bankcode", bankcode);
                        params.put("passcode", pcd);
                        params.put("categorycode", menu_biller_code);
                        params.put("billercode", billerCode);
                        params.put("rc_uuid", uniqueKey.toString());
                        params.put("rc_dtime", dateTime);
                        params.put("signature", signatureKey);

                    } catch (Exception e) {
                        System.out.println("Error");
                    }
                    client.post(AplConstants.InquiryBiller, params, new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject object) {



                            try {

                                if (pDialog != null) {
                                    pDialog.dismiss();
                                }
                                JSONObject body_data = object.getJSONObject("body_data");
                                String error_code = body_data.getString("error_code");
                                String error_message = body_data.getString("error_message");

                                if (error_code.equalsIgnoreCase("0000")) {
                                    JSONArray data = body_data.getJSONArray("biller_data");
                                    int len = data.length();
                                    biller_id_arr = new String[len];
                                    biller_code_arr = new String[len];
                                    biller_name_arr = new String[len];
                                    biller_inquiry_arr = new String[len];
                                    product_code_arr = new String[len];
                                    product_name_arr = new String[len];
                                    product_amount_arr = new String[len];
                                    for (int i = 0; i < data.length(); i++) {

                                            biller_id_arr[i] = data.getJSONObject(i).getString("billerCode");
                                            biller_code_arr[i] = data.getJSONObject(i).getString("billerCode");
                                            biller_name_arr[i] = data.getJSONObject(i).getString("billerName");
                                            biller_inquiry_arr[i] = data.getJSONObject(i).getString("billerInquiry");
                                            product_code_arr[i] = data.getJSONObject(i).getString("productCode");
                                            product_name_arr[i] = data.getJSONObject(i).getString("productName");
                                            product_amount_arr[i] = data.getJSONObject(i).getString("productAmount");
                                            // Toast.makeText(getActivity(), biller_code_arr[i], Toast.LENGTH_LONG).show();

                                          //  if(biller_code_arr[i].equalsIgnoreCase(billerCode)) {
                                                BillerVoucherList.add(new PurchaseCatalogPhoneModel (biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));
                                           // }
                                    }
                                    BillerVoucherPhoneCatalogGeneral billerVoucherPhoneCatalogGeneral = new BillerVoucherPhoneCatalogGeneral(getActivity(), android.R.layout.simple_spinner_item, BillerVoucherList);
                                    cbo_voucher_name.setAdapter(billerVoucherPhoneCatalogGeneral);
                                   /* cbo_voucher_name.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                        @Override
                                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                            // pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, "Requesting Voucher List...");
                                            BillerVoucherPhoneCatalogBean selectedVoucher = BillerVoucherList.get(position);
                                            String billerType = selectedVoucher.getBillerInquiry();
                                            if (billerType.equalsIgnoreCase("Y")) {

                                            } else {

                                            }
                                        }


                                        @Override
                                        public void onNothingSelected(AdapterView<?> parent) {

                                        }


                                    });*/


                                }else if (error_code.equalsIgnoreCase("0404")) {
                                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                    alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                                    alert.setMessage(error_message);
                                    alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Session session;
                                            session = new Session(getActivity());
                                            session.signOut();
                                        }
                                    });
                                    alert.show();

                                } else {
                                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                    alert.setTitle(getResources().getString(R.string.lbl_purchase_title));
                                    alert.setMessage(getResources().getString(R.string.lbl_purchase_title) + error_message);
                                    alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Fragment newFragment = null;
                                            newFragment = new FragmentPurchaseGeneral();
                                            switchFragment(newFragment);
                                        }
                                    });
                                    alert.show();
                                }


                              /*  VoucherNominalList.clear();
                                AsyncHttpClient client = new AsyncHttpClient();
                                client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
                                client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
                                RequestParams params = new RequestParams();

                                try {
                                    String userid = AppHelper.getUserId(getActivity());
                                    String bankcode = AplConstants.Bankcode;
                                    String pcd = AplConstants.pcd;
                                    String URL = AplConstants.InquiryBillerCatalog;
                                    String appName = AplConstants.appName;
                                    String serviceName = URL.split("/")[4];
                                    String apiKey = AppHelper.getAccessKey(getActivity());
                                    UUID uniqueKey = UUID.randomUUID();
                                    String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;

                                    BillerVoucherPhoneCatalogBean billerVoucherPhoneCatalogBean = null;
                                    //billerVoucherPhoneCatalogBean.billerCode;

                                    String productCode = billerVoucherPhoneCatalogBean.productCode;
                                    String billerCode = billerVoucherPhoneCatalogBean.billerCode;

                                    Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
                                    SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
                                    sha256_HMAC.init(secret_key);
                                    byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
                                    String signatureKey = new String(encodeUrlSafe(hmacData));
                                    params.put("userid", userid);
                                    params.put("bankcode", bankcode);
                                    params.put("passcode", pcd);
                                    params.put("productcode", productCode);
                                    params.put("billercode", billerCode);
                                    params.put("rc_uuid", uniqueKey.toString());
                                    params.put("rc_dtime", dateTime);
                                    params.put("signature", signatureKey);

                                } catch (Exception e) {
                                    System.out.println("Error");
                                }


                                client.post(AplConstants.InquiryBillerCatalog, params, new AsyncHttpResponseHandler() {
                                    @Override
                                    public void onSuccess(String content) {
                                        // TODO Auto-generated method stub
                                        super.onSuccess(content);

                                        try {
                                            if (pDialog != null) {
                                                pDialog.dismiss();
                                            }
                                            JSONObject object = new JSONObject(content);
                                            JSONObject body_data = object.getJSONObject("body_data");
                                            String error_code = body_data.getString("error_code");
                                            String error_message = body_data.getString("error_message");

                                            if (error_code.equalsIgnoreCase("0000")) {
                                                JSONArray data = body_data.getJSONArray("catalog_data");
                                                int len = data.length();
                                                catalog_code_arr = new String[len];
                                                catalog_nominal_arr = new String[len];
                                                for (int i = 0; i < data.length(); i++) {
                                                    catalog_code_arr[i] = data.getJSONObject(i).getString("catalogCode");
                                                    catalog_nominal_arr[i] = data.getJSONObject(i).getString("catalogNominal");
                                                    if (billerCode.equalsIgnoreCase("1080")) {
                                                        VoucherNominalList.add(new BillerVoucherNominalBean(catalog_code_arr[i], catalog_nominal_arr[i]));


                                                    }else {
                                                        Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();

                                                    }
                                                }
                                                BillerVoucherNominalAdapter billerVoucherNominalAdapter = new BillerVoucherNominalAdapter(getActivity(), android.R.layout.simple_spinner_item, VoucherNominalList);
                                                cbo_voucher_nominal.setAdapter(billerVoucherNominalAdapter);
                                            } else if (error_code.equalsIgnoreCase("0404")) {
                                                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                                alert.setTitle("Session Expired");
                                                alert.setMessage("Message : " + error_message);
                                                alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        Session session;
                                                        session = new Session(getActivity());
                                                        session.signOut();
                                                    }
                                                });
                                                alert.show();

                                            }


                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    public void onFailure(Throwable error, String content) {
                                        if (pDialog != null) {
                                            pDialog.dismiss();
                                        }
                                        Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                                    }

                                });*/


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }



                    });

                    // End of get Voucher List


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }


            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (pDialog != null) {
                    pDialog.dismiss();
                }
                if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                } else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                }
            }


        });




    }




    private void checkMenuCode() {

        try {

/*
            if(billerCode.equalsIgnoreCase("1074")){
                lbl_header.setText("Telkomsel");
            }else if(billerCode.equalsIgnoreCase("1075")){
                lbl_header.setText("Indosat Reguler");
            }else if(billerCode.equalsIgnoreCase("1076")){
                lbl_header.setText("Indosat GPRS");
            }else if(billerCode.equalsIgnoreCase("1077")){
                lbl_header.setText("Indosat SMS");
            }else if(billerCode.equalsIgnoreCase("1078")){
                lbl_header.setText("XL");
            }else if(billerCode.equalsIgnoreCase("1079")){
                lbl_header.setText("Axis");
            }else if(billerCode.equalsIgnoreCase("1080")){
                lbl_header.setText("Smartfren");
            }else if(billerCode.equalsIgnoreCase("1081")){
                lbl_header.setText("Flexi");
            }else if(billerCode.equalsIgnoreCase("1082")){
                lbl_header.setText("Esia");
            }else if(billerCode.equalsIgnoreCase("1083")){
                lbl_header.setText("Three");
            }else if(billerCode.equalsIgnoreCase("1084")){
                lbl_header.setText("Three Internet");
            }else if(billerCode.equalsIgnoreCase("1149")){
                lbl_header.setText("IM3 Smart");
            }else if(billerCode.equalsIgnoreCase("1150")){
                lbl_header.setText("StarOne");
            }else if(billerCode.equalsIgnoreCase("1148")){
                lbl_header.setText("Mentari");
            }else if(billerCode.equalsIgnoreCase("1085")){
                lbl_header.setText("Kartu As");
            }
*/
            if(billerCode.equalsIgnoreCase("TSIM_PRE")){
                lbl_header.setText("Telkomsel");
            }else if(billerCode.equalsIgnoreCase("IDSR_PRE")){
                lbl_header.setText("Indosat Reguler");
            }else if(billerCode.equalsIgnoreCase("IDSG_PRE")){
                lbl_header.setText("Indosat GPRS");
            }else if(billerCode.equalsIgnoreCase("IDSS_PRE")){
                lbl_header.setText("Indosat SMS");
            }else if(billerCode.equalsIgnoreCase("XLR_PRE")){
                lbl_header.setText("XL");
            }else if(billerCode.equalsIgnoreCase("AXS_PRE")){
                lbl_header.setText("Axis");
            }else if(billerCode.equalsIgnoreCase("SMR_PRE")){
                lbl_header.setText("Smartfren");
            }else if(billerCode.equalsIgnoreCase("FLE_PRE")){
                lbl_header.setText("Flexi");
            }else if(billerCode.equalsIgnoreCase("ESA_PRE")){
                lbl_header.setText("Esia");
            }else if(billerCode.equalsIgnoreCase("TRE_PRE")){
                lbl_header.setText("Three");
            }else if(billerCode.equalsIgnoreCase("THREE_PHON")){
                lbl_header.setText("Three Internet");
            }else if(billerCode.equalsIgnoreCase("TRI_PRE")){
                lbl_header.setText("IM3 Smart");
            }else if(billerCode.equalsIgnoreCase("IM3_PHONE")){
                lbl_header.setText("IM3 Smart");
            }else if(billerCode.equalsIgnoreCase("STR_PHONE")){
                lbl_header.setText("StarOne");
            }else if(billerCode.equalsIgnoreCase("MNT_PHONE")){
                lbl_header.setText("Mentari");
            }else if(billerCode.equalsIgnoreCase("TSAS_PRE")){
                lbl_header.setText("Kartu As");
            }


           /* if (menu_biller_code.equals(purchaseVoucherPhone)) {
                lbl_header.setText("Handphone Voucher Purchase");
                lbl_title.setText("Handphone No");
                ic_img.setImageResource(R.drawable.ic_mobile);
                inpPhoneNumber.setHint("* Input Nandphone No");
                lbl_note.setText("*min 10 digits, max 16 digits");
            }
            if (menu_biller_code.equals(purchaseOthers)) {
                lbl_header.setText("Purchase Others");
                lbl_title.setText("Account No");
                ic_img.setImageResource(R.drawable.ic_voucher);
                inpPhoneNumber.setHint("* Input Account No");
            }*/


        } catch (Exception e) {
            e.printStackTrace();
        }

    }




    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);

        getSherlockActivity().getActionBar().removeAllTabs();
        getSherlockActivity().getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
    }

    public static byte[] encodeUrlSafe(byte[] data) {
        byte[] encode = Base64.encodeBase64(data);
        for (int i = 0; i < encode.length; i++) {
            if (encode[i] == '+') {
                encode[i] = '-';
            } else if (encode[i] == '=') {
                encode[i] = '_';
            } else if (encode[i] == '/') {
                encode[i] = '~';
            }
        }
        return encode;
    }
}

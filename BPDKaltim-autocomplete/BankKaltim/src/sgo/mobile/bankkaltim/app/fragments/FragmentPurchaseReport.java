package sgo.mobile.bankkaltim.app.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.activities.MainActivity;
import sgo.mobile.bankkaltim.conf.AplConstants;
import sgo.mobile.bankkaltim.frameworks.math.FormatCurrency;

/**
 * Created by User on 10/10/2014.
 */
public class FragmentPurchaseReport extends SherlockFragment {

    private ProgressDialog pDialog;
    String order_id,date,source_account_no,trx_id,source_account_name, biller_code,biller_name,catalog_code,catalog_nominal,phone_number,status,menu_biller_code;
    Button btnDone;
    TextView lbl_header,lbl_title,lbl_member_no;
    String purchaseElectricity= AplConstants.purchaseElectricityVoucher;
    String purchaseVoucherPhone = AplConstants.purchasePhoneVoucher;
    String purchaseOthers = AplConstants.purchaseOthers;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_purchase_report, container, false);

        Bundle bundle = this.getArguments();
        menu_biller_code = bundle.getString("menu_biller_code");
        lbl_header = (TextView)view.findViewById(R.id.label_header);
        lbl_title = (TextView)view.findViewById(R.id.lbl_title);
        lbl_member_no = (TextView)view.findViewById(R.id.lbl_member_no);
        if (menu_biller_code.equals(purchaseElectricity)){
            lbl_header.setText(getResources().getString(R.string.lbl_purchase_electricity_title));
            source_account_no = bundle.getString("source_account_no");
            source_account_name = bundle.getString("source_account_name");
            biller_code = bundle.getString("biller_code");
            biller_name = bundle.getString("biller_name");
            catalog_code = bundle.getString("catalog_code");
            catalog_nominal = bundle.getString("catalog_nominal");
            order_id = bundle.getString("order_id");
            status = bundle.getString("error_message");
            trx_id = bundle.getString("trx_id");
            date = bundle.getString("date");
            TextView lbl_date = (TextView) view.findViewById(R.id.lbl_date);
            lbl_date.setText(date);
            TextView lbl_source_account = (TextView) view.findViewById(R.id.lbl_source_account);
            lbl_source_account.setText(source_account_no + " " + "(" +  source_account_name+ ")");
            TextView lbl_voucher_name = (TextView)view.findViewById(R.id.lbl_voucher);
            lbl_voucher_name.setText(biller_name);
            TextView lbl_voucher_nominal= (TextView)view.findViewById(R.id.lbl_nominal);
            lbl_voucher_nominal.setText(FormatCurrency.CurrencyIDR(catalog_nominal));
            TextView lbl_phone_number= (TextView)view.findViewById(R.id.lbl_phone_number);
            lbl_phone_number.setText(order_id);

            TextView lbl_trx_id= (TextView)view.findViewById(R.id.lbl_trx_id);
            lbl_trx_id.setText(trx_id);
            TextView lbl_status= (TextView)view.findViewById(R.id.lbl_status);
            lbl_status.setText(status);
            lbl_member_no.setText(getResources().getString(R.string.lbl_purchase_electricity_orderid));
        }else if(menu_biller_code.equals(purchaseVoucherPhone)) {
            lbl_header.setText(getResources().getString(R.string.lbl_purchase_hp_title));
            lbl_member_no.setText(getResources().getString(R.string.lbl_purchase_hp_orderid));

            source_account_no = bundle.getString("source_account_no");
            source_account_name = bundle.getString("source_account_name");
            biller_code = bundle.getString("biller_code");
            biller_name = bundle.getString("biller_name");
            catalog_code = bundle.getString("catalog_code");
            catalog_nominal = bundle.getString("catalog_nominal");
            order_id = bundle.getString("order_id");
            status = bundle.getString("error_message");
            trx_id = bundle.getString("trx_id");
            date = bundle.getString("date");

            TextView lbl_date = (TextView) view.findViewById(R.id.lbl_date);
            lbl_date.setText(date);
            TextView lbl_source_account = (TextView) view.findViewById(R.id.lbl_source_account);
            lbl_source_account.setText(source_account_no + " " + "(" +  source_account_name+ ")");
            TextView lbl_voucher_name = (TextView)view.findViewById(R.id.lbl_voucher);
            lbl_voucher_name.setText(biller_name);
            TextView lbl_voucher_nominal= (TextView)view.findViewById(R.id.lbl_nominal);
            lbl_voucher_nominal.setText(FormatCurrency.CurrencyIDR(catalog_nominal));
            TextView lbl_trx_id= (TextView)view.findViewById(R.id.lbl_trx_id);
            lbl_trx_id.setText(trx_id);
            TextView lbl_phone_number= (TextView)view.findViewById(R.id.lbl_phone_number);
            lbl_phone_number.setText(order_id);
            TextView lbl_status= (TextView)view.findViewById(R.id.lbl_status);
            lbl_status.setText(status);

        }else lbl_header.setText("Other Purchase");


        btnDone = (Button) view.findViewById(R.id.btnDone);

        btnDone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {

                if(menu_biller_code.equalsIgnoreCase("14")){
                    Fragment newFragment = null;
                    newFragment = new FragmentPurchasePhoneVoucher();
                    Bundle args = new Bundle();
                    args.putString("menu_biller_code", menu_biller_code);
                    newFragment.setArguments(args);
                    switchFragment(newFragment);

                }else {

                    Fragment newFragment = null;
                    newFragment = new FragmentPurchaseGeneral();
                    Bundle args = new Bundle();
                    args.putString("menu_biller_code", menu_biller_code);
                    newFragment.setArguments(args);
                    switchFragment(newFragment);
                }

            }
        });

        return view;
    }




    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);

        getSherlockActivity().getActionBar().removeAllTabs();
        getSherlockActivity().getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
    }
}

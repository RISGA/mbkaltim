package sgo.mobile.bankkaltim.app.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.activeandroid.query.Select;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.bankkaltim.AppHelper;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.activities.ActivityHelp;
import sgo.mobile.bankkaltim.app.activities.MainActivity;
import sgo.mobile.bankkaltim.app.adapter.MenuSettingAdapter;
import sgo.mobile.bankkaltim.app.models.MenuSettingModel;
import sgo.mobile.bankkaltim.conf.AplConstants;
import sgo.mobile.bankkaltim.frameworks.net.Networks;
import sgo.mobile.bankkaltim.frameworks.net.loopj.android.http.CustomSSLSocketFactory;

import java.util.ArrayList;
import java.util.List;

public class FragmentSetting extends Fragment {
    private ProgressDialog pDialog;
    FragmentManager fm;

    // declare view objects
    ImageButton imgNavBack, imgRefresh;
    ListView listMenu;
    ProgressBar prgLoading;
    TextView txtAlert,lbl_header;
    int getCountMenu;
    MenuSettingAdapter MenuSettingAdapter;
    public boolean networks;

    // create arraylist variables to store data from server
    public static ArrayList<String> menu_setting_id          = new ArrayList<String>();
    public static ArrayList<String> menu_setting_code        = new ArrayList<String>();
    public static ArrayList<String> menu_setting_name        = new ArrayList<String>();
    public static ArrayList<String> menu_setting_description = new ArrayList<String>();
    private CheckBox chkEnglish, chkBahasa;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.fragment_menu_setting_list, container, false);
        prgLoading = (ProgressBar) view.findViewById(R.id.prgLoading);
        listMenu   = (ListView) view.findViewById(R.id.listMenu);
        lbl_header   = (TextView) view.findViewById(R.id.label_header);
        getCountMenu = new Select().from(MenuSettingModel.class).count();
        MenuSettingAdapter = new MenuSettingAdapter(getActivity());

        try {
            networks = new Networks().isConnectingToInternet(getActivity());
            if (networks) {
                parseJSONData();
            } else {
                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                alert.setTitle("Notice");
                alert.setMessage("Please check your internet connection");
                alert.setPositiveButton("OK", null);
                alert.show();
                return view;

            }
        }catch (Exception e){
            e.printStackTrace();
        }



        // event listener to handle list when clicked
        listMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {
                String accessFlag = AppHelper.getAccessFlag(getActivity());
                if (accessFlag.equalsIgnoreCase("2")) {
                    if (menu_setting_code.get(position).equals("003")) {
                        Fragment newFragment = null;
                        newFragment = new FragmentBenefTo();
                        switchFragment(newFragment);
                    } else if (menu_setting_code.get(position).equals("004")) {
                        Fragment newFragment = null;
                        newFragment = new FragmentDeleteBeneficiary();
                        switchFragment(newFragment);
                    } else if (menu_setting_code.get(position).equals("002")) {
                        Fragment newFragment = null;
                        newFragment = new FragmentChangePassword();
                        switchFragment(newFragment);
                    } else if (menu_setting_code.get(position).equals("006")) {
                        Intent intent = new Intent(getActivity(), ActivityHelp.class);
                        startActivity(intent);
                    } else if (menu_setting_code.get(position).equals("007")) {
                        Fragment newFragment = null;
                        newFragment = new FragmentChangeLanguage();
                        switchFragment(newFragment);
                    } else {
                        Toast.makeText(getActivity(), "We Apologize \nMenu '" + menu_setting_name.get(position) + "'is not available", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "We Apologize \nMenu '" + menu_setting_name.get(position) + "'is not available", Toast.LENGTH_LONG).show();

                }


            }
        });


        return view;
    }


/*
    private void addListenerOnCheckEnglish(View view) {
        chkEnglish = (CheckBox)view.findViewById(R.id.chkEnglish);
        chkBahasa = (CheckBox) view.findViewById(R.id.chkBahasa);
        chkBahasa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("Change Language :").append(chkEnglish.isChecked());
                stringBuffer.append("Change Language :").append(chkBahasa.isChecked());
                Toast.makeText(getActivity(), stringBuffer.toString(),
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    private void addListenerOnCheckBahasa(View view) {
        chkEnglish = (CheckBox)view.findViewById(R.id.chkEnglish);
        chkBahasa = (CheckBox)view.findViewById(R.id.chkBahasa);
        chkEnglish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("Change Language :").append(chkEnglish.isChecked());
                stringBuffer.append("Change Language :").append(chkBahasa.isChecked());
                Toast.makeText(getActivity(), stringBuffer.toString(),
                        Toast.LENGTH_LONG).show();
            }
        });
    }

*/

    public void parseJSONData(){
//        if(getCountMenu==0) {
            clearData();
            String userLang = AppHelper.getUserLang(getActivity());
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getActivity()));
            client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
            RequestParams params = new RequestParams();
            params.put("lang", userLang);
            client.get(AplConstants.ListMenuSettingMobileAPI, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject json) {


                    try {
                        JSONArray data = json.getJSONArray("menu_setting_data"); // this is the "items: [ ] part

                        for (int i = 0; i < data.length(); i++) {
                            JSONObject object = data.getJSONObject(i);
                            menu_setting_id.add(object.getString("menu_setting_id"));
                            menu_setting_code.add(object.getString("menu_setting_code"));
                            menu_setting_name.add(object.getString("menu_setting_name"));
                            menu_setting_description.add(object.getString("menu_setting_description"));

//                            MenuSettingModel menuSettingModel = new MenuSettingModel();
//                            menuSettingModel.menu_setting_id = data.getJSONObject(i).getString("menu_setting_id");
//                            menuSettingModel.menu_setting_code = data.getJSONObject(i).getString("menu_setting_code");
//                            menuSettingModel.menu_setting_name = data.getJSONObject(i).getString("menu_setting_name");
//                            menuSettingModel.menu_setting_description = data.getJSONObject(i).getString("menu_setting_description");
//                            menuSettingModel.save();
                        }

                        // when finish parsing, hide progressbar
                        prgLoading.setVisibility(8);

                        // if data available show data on list
                        // otherwise, show alert text
                        if (menu_setting_code.size() > 0) {
                            lbl_header.setVisibility(0);
                            listMenu.setVisibility(0);
                            listMenu.setAdapter(MenuSettingAdapter);
                        } else {
                            txtAlert.setVisibility(0);
                        }
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();

                    }
                }


                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    }
                }
            });
//        }else{
//            clearData();
//            List<MenuSettingModel> itemList = new Select().all().from(MenuSettingModel.class).execute();
//
//            for (int i=0 ; i<itemList.size();i++) {
//
//                menu_setting_id.add(itemList.get(i).getMenu_setting_id());
//                menu_setting_code.add(itemList.get(i).getMenu_setting_code());
//                menu_setting_name.add(itemList.get(i).getMenu_setting_name());
//                menu_setting_description.add(itemList.get(i).getMenu_setting_description());
//
//
//            }
//            // when finish parsing, hide progressbar
//            prgLoading.setVisibility(8);
//
//            // if data available show data on list
//            // otherwise, show alert text
//            if (menu_setting_code.size() > 0) {
//                lbl_header.setVisibility(0);
//                listMenu.setVisibility(0);
//                listMenu.setAdapter(MenuSettingAdapter);
//            } else {
//                txtAlert.setVisibility(0);
//            }
//
//        }
    }

    // clear arraylist variables before used
    void clearData(){
        menu_setting_id.clear();
        menu_setting_code.clear();
        menu_setting_name.clear();
        menu_setting_description.clear();
    }

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
        }

    };

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }



    public static byte[] encodeUrlSafe(byte[] data) {
        byte[] encode = Base64.encodeBase64(data);
        for (int i = 0; i < encode.length; i++) {
            if (encode[i] == '+') {
                encode[i] = '-';
            } else if (encode[i] == '=') {
                encode[i] = '_';
            } else if (encode[i] == '/') {
                encode[i] = '~';
            }
        }
        return encode;
    }

}
package sgo.mobile.banksulteng.app.fragments;

/*public class FragmentInHouseTransfer extends SherlockFragment {

    private final Calendar mCalendar = Calendar.getInstance();

    private int hourOfDay = mCalendar.get(Calendar.HOUR_OF_DAY);

    private int minute = mCalendar.get(Calendar.MINUTE);

    private int day = mCalendar.get(Calendar.DAY_OF_MONTH);

    private int month = mCalendar.get(Calendar.MONTH);

    private int year = mCalendar.get(Calendar.YEAR);


    static Spinner spListSrac    = null;
    static Spinner spListDestAcc = null;
    private ProgressDialog pDialog;


    Button btnDone;
    Button inpPaymentDate;
    EditText inpAmount, inpMemo;

   // public String error_code;
   // public String error_message;
    public String account_no;
    public String account_name;
    public String ccy_id;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_transfer_inhouse, container, false);
        //parseJSONData();


        spListSrac = (Spinner)view.findViewById(R.id.cbo_source_account);
      //  parseJSONData();
       // ArrayAdapter<BeneficiaryBean> BenefFromArrayAdapter = new ArrayAdapter<BeneficiaryBean>(this.getActivity(),
              //  R.layout.spinner_layout, new BeneficiaryBean[] {
                //new BeneficiaryBean( "001", "E.Prawiro-100900051(IDR)" ),
               // new BeneficiaryBean( "002", "Edy SP-100900052(IDR)" ),

       // });
       // spListSrac.setAdapter(BenefFromArrayAdapter);

        spListDestAcc = (Spinner)view.findViewById(R.id.cbo_benef_account);
        ArrayAdapter<BeneficiaryBean> BenefToArrayAdapter = new ArrayAdapter<BeneficiaryBean>(this.getActivity(),
                R.layout.spinner_layout, new BeneficiaryBean[] {
                new BeneficiaryBean( "001", "Freddy-100900051(IDR)" ),
                new BeneficiaryBean( "002", "Soenarjadi-100900034(IDR)" ),
                new BeneficiaryBean( "003", "Joshua Dharmawan-100900065(IDR)" ),
                new BeneficiaryBean( "003", "Diki Irawan-100900023(IDR)" ),
                new BeneficiaryBean( "004", "Erica Noviana-100900089(IDR)" ),
        });
        spListDestAcc.setAdapter(BenefToArrayAdapter);

        inpAmount = (EditText) view.findViewById(R.id.inpAmount);
        inpMemo   = (EditText) view.findViewById(R.id.inpMemo);

        inpPaymentDate        = (Button) view.findViewById(R.id.inpPaymentDate);
        resetDate();

        final DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {

                inpPaymentDate.setHint(
                        new StringBuilder().append(StringUtils.pad(day))
                                .append("-").append(StringUtils.pad(month + 1)).append("-").append(StringUtils.pad(year)));
                inpPaymentDate.setTextColor(getResources().getColor(android.R.color.holo_blue_light));
            }

        }, mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH));

        view.findViewById(R.id.inpPaymentDate).setOnClickListener(new View.OnClickListener() {

            private String tag;

            @Override
            public void onClick(View v) {
                datePickerDialog.show(getActivity().getFragmentManager(), tag);;
            }
        });


        btnDone               = (Button) view.findViewById(R.id.btnDone);
        btnDone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {

                BeneficiaryBean sourceAccValue = (BeneficiaryBean)spListSrac.getSelectedItem();
                BeneficiaryBean benefAccValue  = (BeneficiaryBean)spListDestAcc.getSelectedItem();

                String source_account = sourceAccValue.name;
                String benef_account  = benefAccValue.name;
                String amount         = inpAmount.getText().toString();
                String memo           = inpMemo.getText().toString();;
                String paymentdate    = "20 Juni 2014";

                Fragment newFragment = null;
                newFragment = new FragmentTransferInHouseConfirmToken();
                Bundle args = new Bundle();
                args.putString("source_account", source_account);
                args.putString("benef_account", benef_account);
                args.putString("totalAmount", amount);
                args.putString("memo", memo);
                args.putString("paymentdate", paymentdate);
                newFragment.setArguments(args);
                switchFragment(newFragment);
            }
        });

        return view;
    }

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);

        getSherlockActivity().getActionBar().removeAllTabs();
        getSherlockActivity().getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
    }

    private void resetDate() {
        inpPaymentDate.setHint(new StringBuilder().append(StringUtils.pad(day))
                .append("-").append(StringUtils.pad(month + 1)).append("-").append(StringUtils.pad(year)));
        inpPaymentDate.setTextColor(getResources().getColor(android.R.color.darker_gray));
    }

    public void parseJSONData() {
        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
        RequestParams params = new RequestParams();

        Log.d("params", params.toString());
        client.get(AplConstants.SourceAccountListAPI, params, new AsyncHttpResponseHandler() {
            public void onSuccess(String content) {
                Log.d("result:", content);
                Toast.makeText(getActivity(), content, Toast.LENGTH_LONG).show();
                try {
                    // parse json data and store into arraylist variables
                    JSONObject object = new JSONObject(content);
                    JSONObject body_data = object.getJSONObject("body_data");
                    String error_code = body_data.getString("error_code");
                    String error_message = body_data.getString("error_message");

                    if (error_code.equals("0000")) {

                        JSONObject object2 = object.getJSONObject("source_account_data");
                        String account_no = object2.getString("account_no");
                        String account_name = object2.getString("account_name");
                        String ccy_id = object2.getString("ccy_id");

                    } else {
                        //  txtAlert.setVisibility(0);
                    }


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();

                }
            }

            ;

            public void onFailure(Throwable error, String content) {
                if (pDialog != null) {
                    pDialog.dismiss();
                }
                Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
            }

        });
    }


}*/
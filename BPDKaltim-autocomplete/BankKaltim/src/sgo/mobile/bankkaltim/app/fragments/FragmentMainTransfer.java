package sgo.mobile.bankkaltim.app.fragments;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.adapter.TransferPagerAdapter;

public class FragmentMainTransfer extends SherlockFragment {
    private ActionBar actionBar;
    private ViewPager viewPager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(container != null) {
            container.removeAllViews();
        }

        View view = inflater.inflate(R.layout.fragment_main_transfer, container, false);
        viewPager = (ViewPager) view.findViewById(R.id.pager);
        viewPager.setOnPageChangeListener(onPageChangeListener);
        viewPager.setAdapter(new TransferPagerAdapter(getActivity().getSupportFragmentManager()));
       // viewPager.setCurrentItem(getId();


        setRemoveAllTabs();
        addActionBarTabs();

        return view;
    }


    private ViewPager.SimpleOnPageChangeListener onPageChangeListener = new ViewPager.SimpleOnPageChangeListener() {
        @Override
        public void onPageSelected(int position) {
            super.onPageSelected(position);
            actionBar.setSelectedNavigationItem(position);

        }
    };

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void setRemoveAllTabs() {
        getSherlockActivity().getActionBar().removeAllTabs();
        getSherlockActivity().getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
    }

    private void addActionBarTabs() {
        String[] tabs = {getResources().getString(R.string.lbl_tab_inhouse), getResources().getString(R.string.lbl_tab_domestic)};

        actionBar =  getSherlockActivity().getSupportActionBar();
        for (String tabTitle : tabs) {
            ActionBar.Tab tab = actionBar.newTab().setText(tabTitle)
                    .setTabListener(tabListener);
            actionBar.addTab(tab);

        }
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
    }

    public ActionBar.TabListener tabListener = new ActionBar.TabListener() {
        @Override
        public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
            viewPager.setCurrentItem(tab.getPosition());

        }

        @Override
        public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
        }

        @Override
        public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
            viewPager.setCurrentItem(tab.getPosition());
        }
    };
}

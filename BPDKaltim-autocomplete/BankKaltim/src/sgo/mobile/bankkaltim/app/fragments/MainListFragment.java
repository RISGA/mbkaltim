package sgo.mobile.bankkaltim.app.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import sgo.mobile.bankkaltim.AppHelper;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.activities.MainActivity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MainListFragment extends Fragment {
    OnFragmentUpdateListener mCallback;
    private Context context;



    public interface OnFragmentUpdateListener {
        public void onFragmentUpdate(int position, boolean forceupdate);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mCallback = (OnFragmentUpdateListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentUpdateListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(container != null) {
            container.removeAllViews();
        }

        context = getActivity().getApplicationContext();

        View view = inflater.inflate(R.layout.viewpager_main, container, false);
        createPagerView(view);

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void createPagerView(View view) {
        ViewPager awesomePager = (ViewPager) view.findViewById(R.id.pager);
        awesomePager.setAdapter(new SgoPagerAdapter());


      //  TabPageIndicator titleIndicator = (TabPageIndicator)view.findViewById(R.id.titles);
       // titleIndicator.setViewPager(awesomePager);
    }

    public static MainListFragment newInstance() {
        MainListFragment f = new MainListFragment();

        Bundle args = new Bundle();
        f.setArguments(args);

        return f;
    }

    private class SgoPagerAdapter extends PagerAdapter {
        String[] pages = {" "};
        String pageContent;

        private SgoPagerAdapter() {

        }

        @Override
        public int getCount() {
            return pages.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return (view == object);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object view) {
            ((ViewPager) container).removeView((View) view);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View parent_view = null;
            if (position ==0) {
                parent_view = getViewForPageOne();
                ((ViewPager) container).addView(parent_view, 0);
            }
            return parent_view;
        }

        private View getViewForPageOne(){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.landing_view, null);

         //   TextView txtSaldo =(TextView) view.findViewById(R.id.txtSaldo);
           // txtSaldo.setText("Rp.500.000,00");
            String userName = AppHelper.getUserFullName(getActivity());
            String lastLogin = AppHelper.getLastLogin(getActivity());
            TextView lbl_lastLogin = (TextView)view.findViewById(R.id.lbl_lastlogin);
            lbl_lastLogin.setText(lastLogin);
            TextView txtUsername = (TextView) view.findViewById(R.id.lbl_userName);
            txtUsername.setText(userName);
            DateFormat df = new SimpleDateFormat(getResources().getString(R.string.lbl_main_date));

            String date = df.format(Calendar.getInstance().getTime());
            TextView txtDate = (TextView) view.findViewById(R.id.lbl_date);
            txtDate.setText(date);

            return view;
        }

        public String getPageTitle(int position) {
            return pages[position];
        }

        private void switchFragment(Fragment fragment) {
            if (getActivity() == null)
                return;
            MainActivity main = (MainActivity) getActivity();
            main.switchContent(fragment);
        }
    }
}

package sgo.mobile.bankkaltim.app.fragments;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.BinaryHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.Header;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.activities.MainActivity;
import sgo.mobile.bankkaltim.frameworks.math.FormatCurrency;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Arrays;

/**
 * Created by thinkpad on 1/15/2015.
 */
public class FragmentReportDetail extends Fragment {

    FragmentManager fm;

    // declare view objects
    private ProgressBar prgLoading;
    EditText txtStart, txtEnd;
    TextView txtHeader, txtAlert, txtPaymentRef, txtDateTime, txtSourceAccount, txtSourceAccountCcy,
            txtServiceProvider, txtCustomerId, txtCustomerName, txtAmount, txtFee, txtTotalPayment;
    ImageView imgPaymentType;
    TableLayout tbl_info;
    Button btnDownloadPDF, btnBack;

    File path;
    int id = 1;
    NotificationManager mNotifyManager;
    NotificationCompat.Builder mBuilder;

    String paymentId, paymentType, paymentRef, dateTime, sourceAccount, sourceAccountCcy, serviceProvider,
            customerId, customerName, amount, fee, totalPayment, URLPDF, pdfDesc, pdfStatus;

    int amountCcy, feeCcy, tpCcy;
    String startDate, endDate;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.fragment_report_detail, container, false);
        prgLoading = (ProgressBar) view.findViewById(R.id.prgLoading);
        txtAlert = (TextView) view.findViewById(R.id.txtAlert);
        txtAlert.setVisibility(View.GONE);

       // txtStart = (EditText) view.findViewById(R.id.txtStart);
       // txtEnd = (EditText) view.findViewById(R.id.txtEnd);

      ///  imgPaymentType = (ImageView) view.findViewById(R.id.image_payment_type);
        txtHeader = (TextView) view.findViewById(R.id.label_header);
        tbl_info = (TableLayout)view.findViewById(R.id.tbl_info);
        txtPaymentRef = (TextView) view.findViewById(R.id.txtPaymentRef);
        txtDateTime = (TextView) view.findViewById(R.id.txtDateTime);
        txtSourceAccount = (TextView) view.findViewById(R.id.txtSourceAcc);
        txtServiceProvider = (TextView) view.findViewById(R.id.txtServiceProv);
        txtCustomerId = (TextView) view.findViewById(R.id.txtCustID);
        txtCustomerName = (TextView) view.findViewById(R.id.txtCustomerName);
        txtAmount = (TextView) view.findViewById(R.id.txtAmount);
        txtFee = (TextView) view.findViewById(R.id.txtFee);
        txtTotalPayment = (TextView) view.findViewById(R.id.txtTotalPayment);
        btnDownloadPDF = (Button) view.findViewById(R.id.btnDownloadPDF);
        btnBack = (Button) view.findViewById(R.id.btnBack);

        Bundle bundle = this.getArguments();
        if(bundle != null) {
            paymentId = bundle.getString("paymentId");
            paymentType = bundle.getString("paymentType");
            paymentRef = bundle.getString("paymentRef");
            startDate = bundle.getString("startDate");
            endDate = bundle.getString("endDate");
            dateTime = bundle.getString("dateTime");
            sourceAccount = bundle.getString("sourceAccount");
            sourceAccountCcy = bundle.getString("sourceAccountCcy");
            serviceProvider = bundle.getString("serviceProvider");
            customerId = bundle.getString("customerId");
            customerName = bundle.getString("customerName");
            amount = bundle.getString("amount");
            fee = bundle.getString("fee");
            totalPayment = bundle.getString("totalPayment");
            URLPDF = bundle.getString("URLPDF");
            pdfDesc = bundle.getString("pdfDesc");
            pdfStatus = bundle.getString("pdfStatus");
        }

      /*  if(paymentId.equals("1")) {
            imgPaymentType.setImageResource(R.drawable.icon_date);
        }
        else if(paymentId.equals("2")) {
            imgPaymentType.setImageResource(R.drawable.icon_date);
        }
        else if(paymentId.equals("3")) {
            imgPaymentType.setImageResource(R.drawable.icon_date);
        }
        else if(paymentId.equals("4")) {
            imgPaymentType.setImageResource(R.drawable.icon_date);
        }
        else if(paymentId.equals("5")) {
            imgPaymentType.setImageResource(R.drawable.icon_date);
        }
        else if(paymentId.equals("6")) {
            imgPaymentType.setImageResource(R.drawable.icon_date);
        }
        else if(paymentId.equals("7")) {
            imgPaymentType.setImageResource(R.drawable.icon_date);
        }
        else if(paymentId.equals("8")) {
            imgPaymentType.setImageResource(R.drawable.icon_date);
        }
        else if(paymentId.equals("9")) {
            imgPaymentType.setImageResource(R.drawable.icon_date);
        }
        else if(paymentId.equals("10")) {
            imgPaymentType.setImageResource(R.drawable.icon_date);
        }
        else if(paymentId.equals("11")) {
            imgPaymentType.setImageResource(R.drawable.icon_date);
        }
        else if(paymentId.equals("12")) {
            imgPaymentType.setImageResource(R.drawable.icon_date);
        }
        else if(paymentId.equals("13")) {
            imgPaymentType.setImageResource(R.drawable.icon_date);
        }
        else if(paymentId.equals("14")) {
            imgPaymentType.setImageResource(R.drawable.icon_date);
        }
        else if(paymentId.equals("15")) {
            imgPaymentType.setImageResource(R.drawable.icon_date);
        }*/

       // txtStart.setText(startDate);
       // txtEnd.setText(endDate);
        txtHeader.setText(paymentType + getResources().getString(R.string.lbl_transaction_report_title));
        txtPaymentRef.setText(paymentRef);
        txtDateTime.setText(dateTime);
        txtSourceAccount.setText(sourceAccount);
        txtServiceProvider.setText(serviceProvider);
        txtCustomerId.setText(customerId);
        txtCustomerName.setText(customerName);

        Double a = Double.parseDouble(amount);
        amountCcy = a.intValue();

        Double f = Double.parseDouble(fee);
        feeCcy = f.intValue();

        Double tp = Double.parseDouble(totalPayment);
        tpCcy = tp.intValue();

        //format amount, fee, total payment sesuai currency
        if(sourceAccountCcy.equals("IDR")) {
            txtAmount.setText(FormatCurrency.getRupiahFormat(Integer.toString(amountCcy)));
            txtFee.setText(FormatCurrency.getRupiahFormat(Integer.toString(feeCcy)));
            txtTotalPayment.setText(FormatCurrency.getRupiahFormat(Integer.toString(tpCcy)));
        }
        else {
            txtAmount.setText(amount);
            txtFee.setText(fee);
            txtTotalPayment.setText(totalPayment);
        }

        prgLoading.setVisibility(View.GONE);
        btnDownloadPDF.setOnClickListener(savePdfListener);
        btnBack.setOnClickListener(backListener);

        return view;
    }

    Button.OnClickListener backListener = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            Fragment newFragment = null;
            newFragment = new FragmentReportInquiry();
            Bundle args = new Bundle();
            args.putString("paymentId", paymentId);
            args.putString("paymentName", paymentType);
            args.putString("startDate", startDate);
            args.putString("endDate", endDate);
            newFragment.setArguments(args);
            switchFragment(newFragment);
        }
    };

    Button.OnClickListener savePdfListener = new Button.OnClickListener() {
        @Override
        public void onClick(View view) {


            if (pdfStatus.equals("0000")) {
                //kedua cek apakah terdapat sdcard yang bisa dibaca
                if (isExternalStorageReadable()) {
                    //ketiga cek apakah sdcard bisa diwrite (karena aturan penyimpanan versi android ICS keatas berubah
                    // jadi kemungkinan  besar file yg akan di download pasti disimpen di internal storage HP)
                    if (isExternalStorageWritable()) {
                        //tentukan path/folder untuk kita menyimpan file yg akan kita download
                        path = new File(Environment.getExternalStorageDirectory(), "BPD Bank Sulteng");
                        //cek apakah path sudah ada atau belum, jika belum, buat foldernya dan lanjut ke function showDialogPdf()
                        if (!path.exists()) {
                            if (path.mkdirs()) makeAndSharePDF();
                            else Toast.makeText(getActivity(), "Gagal Membuat direktori", Toast.LENGTH_LONG).show();
                        } else makeAndSharePDF();
                    } else Toast.makeText(getActivity(), "kartu memori tidak bisa diakses", Toast.LENGTH_LONG).show();
                } else Toast.makeText(getActivity(), "tidak ada kartu memori yang terpasang", Toast.LENGTH_LONG).show();
            } else Toast.makeText(getActivity(), pdfDesc, Toast.LENGTH_LONG).show();
        }
    };

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state);
    }

    public void makeAndSharePDF() {

        AsyncHttpClient httpClient = new AsyncHttpClient();
        String[] allowedContentTypes = new String[] {"application/pdf"};
        RequestParams params = new RequestParams();
//        params.put("", "");

        httpClient.post(URLPDF, params, new BinaryHttpResponseHandler(allowedContentTypes) {
            @Override
            public void onSuccess(int i, Header[] headers, byte[] bytes) {
                Log.w("isi header save pdf", Arrays.toString(headers));
                //inisialisasi notification dan buildernya untuk menampilkan informasi file yg akan kita download
                mNotifyManager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
                mBuilder = new NotificationCompat.Builder(getActivity());
                mBuilder.setContentTitle("Download File")
                        .setContentText("Downloading...")
                        .setAutoCancel(true)
                        .setSmallIcon(R.drawable.ic_launcer)
                        .setProgress(0, 0, true);

                mNotifyManager.notify(id, mBuilder.build());

                //Ketika sukses mendownload, mulailah melakukan penyimpanan ke path yang sudah kita siapkan
                Boolean nameExist = true;
                String ext;
                String namaFile;
                int idxDup = 1;
                int idx;
//                //karena file yg kita downlaod tidak memiliki nama, maka namanya kita ambil dari header yang didapat dari
//                //response webservice yang dikirimkan ke aplikasi. Caranya melakukan looping dari kumpulan text headernya
//                for (idx = 0; idx < headers.length; idx++) {
//                    if (headers[idx].getValue().contains("URLPDF")) break;
//                }
//                String URLPDF = headers[idx].getValue().substring(headers[idx].getValue().lastIndexOf('=') + 1);
//
//                //bagi2 beberapa bagian dari URLPDF yg didapat untuk memudahkan kita kustomisasi filenamenya jika
//                //file tersebut sudah pernah kita download.
//                ext = URLPDF.substring(URLPDF.lastIndexOf('.') + 1);
//                namaFile = URLPDF.substring(0, URLPDF.lastIndexOf('.'));
//                Log.w("isi URLPDF save pdf", URLPDF);
//
//                File downloadfile = null;
//
//                //cek apakan file sudah ada atau belum, jika sudah tambahkan angka tambahan untuk membedakan.
//                //contoh file1.txt, file2.txt
//                while (nameExist) {
//                    downloadfile = new File(path, URLPDF);
//                    if (downloadfile.exists()) {
//                        idxDup++;
//                        URLPDF = namaFile + '(' + idxDup + ')' + '.' + ext;
//                    } else nameExist = false;
//                }

                String nameFileExt = URLPDF.split("/")[6];

                ext = nameFileExt.substring(nameFileExt.lastIndexOf('.') + 1);

                File downloadfile = null;
                String nameFile = nameFileExt.substring(0, nameFileExt.length()-4);


                while (nameExist) {
                    downloadfile = new File(path, nameFileExt);
                    if (downloadfile.exists()) {
                        idxDup++;
                        nameFileExt = nameFile + '(' + idxDup + ')' + '.' + ext;

                    } else nameExist = false;
                }

                //mulai menyimpan file yang sudah kita siapkan
                OutputStream f;
                try {
                    f = new FileOutputStream(downloadfile);
                    f.write(bytes);
                    f.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                //update notification bahwa download sudah selesai
                mBuilder.setContentTitle(downloadfile.getName())
                        .setContentText("Download Complete")
                        .setProgress(0, 0, false);

                //siapkan listener jika notification di klik, akan mengarahkan user ke dialog pilihan aplikasi untuk membuka
                //file tersebut
                Uri urinya = Uri.fromFile(downloadfile);
                Log.w("urinya", urinya.toString());
                Intent intent = Intent.createChooser(shareDocument(urinya), "Open File");

                PendingIntent pIntent = PendingIntent.getActivity(getActivity(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

                //update notification tersebut
                mBuilder.setContentIntent(pIntent);
                mNotifyManager.notify(id, mBuilder.build());
            }

            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable) {
                Toast.makeText(getActivity(), "Download Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    //function ini untuk menyiapkan intent share yang diarahkan ke alamat uri file berada
    private Intent shareDocument(Uri u) {
        Intent mShareIntent = new Intent(Intent.ACTION_VIEW);
        mShareIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        mShareIntent.setDataAndType(u,"application/pdf");
        // Attach the PDf as a Uri, since Android can't take it as bytes yet.
        return mShareIntent;
    }

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
        }

    };

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }

    public static byte[] encodeUrlSafe(byte[] data) {
        byte[] encode = Base64.encodeBase64(data);
        for (int i = 0; i < encode.length; i++) {
            if (encode[i] == '+') {
                encode[i] = '-';
            } else if (encode[i] == '=') {
                encode[i] = '_';
            } else if (encode[i] == '/') {
                encode[i] = '~';
            }
        }
        return encode;
    }

}

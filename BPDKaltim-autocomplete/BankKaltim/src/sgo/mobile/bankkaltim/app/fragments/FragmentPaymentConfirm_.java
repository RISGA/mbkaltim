package sgo.mobile.bankkaltim.app.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.telephony.SmsMessage;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.bankkaltim.AppHelper;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.activities.MainActivity;
import sgo.mobile.bankkaltim.app.adapter.PaymentConfirmListAdapter;
import sgo.mobile.bankkaltim.app.ui.dialog.DefinedDialog;
import sgo.mobile.bankkaltim.conf.AplConstants;
import sgo.mobile.bankkaltim.frameworks.net.Networks;
import sgo.mobile.bankkaltim.frameworks.net.loopj.android.http.CustomSSLSocketFactory;
import sgo.mobile.bankkaltim.frameworks.session.Session;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by SGO-Mobile-DEV on 7/14/2015.
 */
public class FragmentPaymentConfirm_ extends SherlockFragment {
    String menu_biller_code,product_code,source_account_no,source_account_name,amount,order_id,biller_code,biller_name,catalog_code;
    ListView listData;
    TextView label_header;
    JSONObject dataJSON =null;
    JSONArray dataArray;
    String data;
    EditText txtOTP;
    TableLayout tbl_otp;
    Button btnDone, btnOtp;
    public static ArrayList<String> name_arr = new ArrayList<String>();
    public static ArrayList<String> value_arr = new ArrayList<String>();
    PaymentConfirmListAdapter paymentConfirmListAdapter;
    boolean networks;
    View view_token1,view_token2;
    private ProgressDialog pDialog;
    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");//dd/MM/yyyy
    Date now = new Date();
    String dateTime = sdfDate.format(now);
    TableRow rowOtp;
    private static LinkedHashMap<String,String> dataUiList;
    private static Map json;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payment_confirm_, container, false);
        view_token1 =view.findViewById(R.id.view_token1);
        view_token2 =view.findViewById(R.id.view_token2);
        view_token1.setVisibility(View.GONE);
        view_token2.setVisibility(View.GONE);
        rowOtp =(TableRow)view.findViewById(R.id.rowOtp);
        rowOtp.setVisibility(View.GONE);
        listData   = (ListView) view.findViewById(R.id.listData);
        txtOTP = (EditText)view.findViewById(R.id.txtOtp);
        tbl_otp = (TableLayout)view.findViewById(R.id.tbl_otp);
        tbl_otp.setVisibility(View.GONE);
        btnDone =(Button)view.findViewById(R.id.btnDone);
        btnOtp =(Button)view.findViewById(R.id.btnGetOtp);
        btnDone.setVisibility(View.GONE);
        label_header = (TextView)view.findViewById(R.id.label_header);

        Bundle bundle = this.getArguments();
        paymentConfirmListAdapter = new PaymentConfirmListAdapter(getActivity());
        menu_biller_code = bundle.getString("menu_biller_code");
        source_account_name = bundle.getString("source_account_name");
        source_account_no = bundle.getString("source_account_no");
        order_id=bundle.getString("order_id");
        amount=bundle.getString("amount");
        biller_code=bundle.getString("biller_code");
        biller_name=bundle.getString("biller_name");
        catalog_code=bundle.getString("catalog_code");
        product_code = bundle.getString("product_code");
        data=bundle.getString("data");




        if (menu_biller_code.equals(AplConstants.billTax)){
            label_header.setText(getResources().getString(R.string.lbl_payment_tax_title));
        }else if (menu_biller_code.equals(AplConstants.billITicket)){
            label_header.setText(getResources().getString(R.string.lbl_payment_ticket_title));
        }else if (menu_biller_code.equals(AplConstants.billElectricity)){
            label_header.setText(getResources().getString(R.string.lbl_payment_electricity_title));
        }else if (menu_biller_code.equals(AplConstants.billPhone)){
            label_header.setText(getResources().getString(R.string.lbl_payment_phone_title));
        }else if (menu_biller_code.equals(AplConstants.billInternet)){
            label_header.setText(getResources().getString(R.string.lbl_payment_Internet_title));
        }else if (menu_biller_code.equals(AplConstants.billIInsurance)){
            label_header.setText(getResources().getString(R.string.lbl_payment_insurance_title));
        }else if (menu_biller_code.equals(AplConstants.billInstallment)){
            label_header.setText(getResources().getString(R.string.lbl_payment_installment_title));
        }else if (menu_biller_code.equals(AplConstants.billCableTV)){
            label_header.setText(getResources().getString(R.string.lbl_payment_tv_title));
        }else if (menu_biller_code.equals(AplConstants.billCreditCard)){
            label_header.setText(getResources().getString(R.string.lbl_payment_creditcard_title));
        }



        clearData();
      try {
         dataJSON = new JSONObject(data);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        for(int i = 0; i<dataJSON.names().length(); i++){

            try {


                name_arr.add(dataJSON.names().getString(i));
                value_arr.add(String.valueOf(dataJSON.get(dataJSON.names().getString(i))));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if(name_arr.size() >0){
            listData.setVisibility(0);
           // tbl_otp.setVisibility(View.VISIBLE);
            listData.setAdapter(paymentConfirmListAdapter);
        }else{

        }

        btnOtp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                try {
                    networks = new Networks().isConnectingToInternet(getActivity());
                    if (networks) {
                        requestToken();
                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_payment_title));
                        alert.setMessage(getResources().getString(R.string.lbl_alert_connection));
                        alert.setPositiveButton("OK", null);
                        alert.show();

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });
        btnDone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                final String txtToken = txtOTP.getText().toString();
                if (txtToken.equalsIgnoreCase("")) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setTitle(getResources().getString(R.string.lbl_alert_otp_title));
                    alert.setMessage(getResources().getString(R.string.lbl_otp_validation_message));
                    alert.setPositiveButton("OK", null);
                    alert.show();
                } else {
                    try {
                        networks = new Networks().isConnectingToInternet(getActivity());
                        if (networks) {
                            submitPurchase();
                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_payment_title));
                            alert.setMessage(getResources().getString(R.string.lbl_alert_connection));
                            alert.setPositiveButton("OK", null);
                            alert.show();

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        });

        return view;
    }


    void clearData(){
        //id.clear();
        name_arr.clear();
        value_arr.clear();
    }

    public void requestToken() {
        pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.lbl_loading_process));
        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getSherlockActivity()));
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
        RequestParams params = new RequestParams();

        try {
            String userid = AppHelper.getUserId(getActivity());
            String bankcode = AplConstants.Bankcode;
            String pcd = AplConstants.pcd;
            String URL = AplConstants.RequestTokenAPI;
            String appName = AplConstants.appName;
            String serviceName = URL.split("/")[4];
            String apiKey = AppHelper.getAccessKey(getActivity());
            UUID uniqueKey = UUID.randomUUID();
            String signature = uniqueKey+dateTime+ appName + serviceName+ bankcode + userid;


            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
            String signatureKey = new String(encodeUrlSafe(hmacData));
            params.put("userid", userid);
            params.put("bankcode", bankcode);
            params.put("passcode", pcd);
            params.put("rc_uuid", uniqueKey.toString());
            params.put("rc_dtime", dateTime);
            params.put("signature", signatureKey);

        }catch (Exception e){
            System.out.println("Error");
        }


        client.post(AplConstants.RequestTokenAPI, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject object) {


                try {
                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    JSONObject body_data = object.getJSONObject("body_data");
                    // parse json data and store into arraylist variables
                    String error_code = body_data.getString("error_code");
                    String error_message = body_data.getString("error_message");
                    if (error_code.equals("0000")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_alert_otp_title));
                        alert.setMessage(getResources().getString(R.string.lbl_alert_otp_message));
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                btnOtp.setVisibility(View.GONE);
                                tbl_otp.setVisibility(View.VISIBLE);
                                btnDone.setVisibility(View.VISIBLE);
                                view_token1.setVisibility(View.VISIBLE);
                                view_token2.setVisibility(View.VISIBLE);
                                rowOtp.setVisibility(View.VISIBLE);

                            }
                        });
                        alert.show();


                    } else if (error_code.equalsIgnoreCase("0404")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                        alert.setMessage(error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Session session;
                                session = new Session(getActivity());
                                session.signOut();
                            }
                        });
                        alert.show();

                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_alert_otp_title));
                        alert.setMessage(error_message);
                        alert.setPositiveButton("OK", null);
                        alert.show();

                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (pDialog != null) {
                    pDialog.dismiss();
                }
                if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                } else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                }
            }

        });
    }


    private void submitPurchase() {
        pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.lbl_loading_process));
        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getSherlockActivity()));
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
        RequestParams params = new RequestParams();

        try {
            String userid = AppHelper.getUserId(getActivity());

            String bankcode = AplConstants.Bankcode;
            String pcd = AplConstants.pcd;
            String URL = AplConstants.BillerPaymentNotif;
            String appName = AplConstants.appName;
            String serviceName = URL.split("/")[4];
            String apiKey = AppHelper.getAccessKey(getActivity());
            UUID uniqueKey = UUID.randomUUID();

            String message = uniqueKey+dateTime+ appName + serviceName+ bankcode + userid;

            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] hmacData = sha256_HMAC.doFinal(message.getBytes("UTF-8"));
            String signatureKey = new String(encodeUrlSafe(hmacData));
            params.put("userid", userid);
            params.put("bankcode", bankcode);
            params.put("passcode", pcd);
            params.put("productcode", product_code);
            params.put("orderid", order_id);
            params.put("amount", amount);
            params.put("responsecode", txtOTP.getText().toString());
            params.put("sourceacctno", source_account_no);
            params.put("rc_uuid", uniqueKey.toString());
            params.put("rc_dtime",dateTime);
            params.put("signature",signatureKey);

        }catch (Exception e){
            System.out.println("Error");
        }
        client.post(AplConstants.BillerPaymentNotif, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject object) {


                try {
                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    JSONObject body_data = object.getJSONObject("body_data");
                    String error_code = body_data.getString("error_code");
                    final JSONObject header_data = object.getJSONObject("header_data");
                    final String error_message = body_data.getString("error_message");
                    if (error_code.equals("0000")) {

/*
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        JSONParser parser = new JSONParser();
                        ContainerFactory containerFactory = new ContainerFactory(){
                            public List creatArrayContainer() {
                                return new LinkedList();
                            }

                            public Map createObjectContainer() {
                                return new LinkedHashMap();
                            }

                        };

                        try{
                            json = (Map)parser.parse(content, containerFactory);
                            dataUiList = (LinkedHashMap<String, String>)json.get(body_data.getJSONObject("data"));

                            alert.setTitle("HERE");
                            alert.setMessage(JSONValue.toJSONString(dataUiList));
                            alert.setPositiveButton("OK", null);
                            alert.show();
                            Iterator iter = dataUiList.entrySet().iterator();
                            while(iter.hasNext()){
                                Map.Entry entry = (Map.Entry)iter.next();
                            }
                        }
                        catch(ParseException pe){
                            System.out.println(pe);
                        }
*/
                        JSONObject data = body_data.getJSONObject("data");
                        final String psNumber = data.getString("psNumber");
                        final String date = header_data.getString("rs_datetime");
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                        alert.setMessage(getResources().getString(R.string.lbl_payment_title_alert) + " " + error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Fragment newFragment = null;
                                newFragment = new FragmentPaymentReport();
                                Bundle args = new Bundle();
                                args.putString("menu_biller_code", menu_biller_code);
                                args.putString("source_account_name", source_account_name);
                                args.putString("source_account_no", source_account_no);
                                args.putString("biller_code", biller_code);
                                args.putString("biller_name", biller_name);
                                args.putString("catalog_code", catalog_code);
                                args.putString("catalog_nominal", amount);
                                args.putString("order_id", order_id);
                                args.putString("status", error_message);
                                args.putString("amount", amount);
                                args.putString("trx_id", psNumber);
                                args.putString("date", date);
                                newFragment.setArguments(args);
                                switchFragment(newFragment);

                            }
                        });
                        alert.show();

                    } else if (error_code.equals("0003")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                        alert.setMessage(getResources().getString(R.string.lbl_payment_title_alert) + " " + error_message);
                        alert.setPositiveButton("OK", null);
                        alert.show();

                    } else if (error_code.equals("X001")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                        alert.setMessage(error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Session session;
                                session = new Session(getActivity());
                                session.signOut();
                            }
                        });
                        alert.show();

                    } else if (error_code.equalsIgnoreCase("0404")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                        alert.setMessage(error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Session session;
                                session = new Session(getActivity());
                                session.signOut();
                            }
                        });
                        alert.show();

                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                        alert.setMessage(error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Fragment newFragment = null;
                                newFragment = new FragmentPaymentGeneral();
                                Bundle args = new Bundle();
                                args.putString("menu_biller_code", menu_biller_code);
                                newFragment.setArguments(args);
                                switchFragment(newFragment);
                            }
                        });
                        alert.show();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (pDialog != null) {
                    pDialog.dismiss();
                }
                if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                } else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                }


            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
            alert.setTitle(getResources().getString(R.string.lbl_tab_inhouse));
            alert.setMessage(getResources().getString(R.string.lbl_transfer_exception));
            alert.setPositiveButton("OK",new DialogInterface.OnClickListener()

            {
                @Override
                public void onClick (DialogInterface dialog, int which) {
                        Fragment newFragment = null;
                        newFragment = new FragmentMainTransfer();
                        switchFragment(newFragment);
                    }
                });
                alert.show();
             }
        });
    }


    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
        }

    };

    private void switchFragment(android.support.v4.app.Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);

        getSherlockActivity().getActionBar().removeAllTabs();
        getSherlockActivity().getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
    }

    public static byte[] encodeUrlSafe(byte[] data) {
        byte[] encode = Base64.encodeBase64(data);
        for (int i = 0; i < encode.length; i++) {
            if (encode[i] == '+') {
                encode[i] = '-';
            } else if (encode[i] == '=') {
                encode[i] = '_';
            } else if (encode[i] == '/') {
                encode[i] = '~';
            }
        }
        return encode;
    }


    @Override
    public void onResume() {
        super.onResume();
        toggleMyBroadcastReceiver(true);
    }

    @Override
    public void onPause() {
        super.onPause();
        toggleMyBroadcastReceiver(false);
    }

    private void toggleMyBroadcastReceiver(Boolean _on){
        if (getActivity() == null)
            return;

        MainActivity fca = (MainActivity) getActivity();
        fca.togglerBroadcastReceiver(_on,myReceiver);
    }

    public BroadcastReceiver myReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle mBundle = intent.getExtras();
            SmsMessage[] mSMS =null;
            String strMessage = "";

            if(mBundle != null){
                Object[] pdus = (Object[]) mBundle.get("pdus");
                mSMS = new SmsMessage[pdus.length];

                for (int i = 0; i < mSMS.length ; i++){
                    mSMS[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                    strMessage += mSMS[i].getMessageBody();

                    //Map<String, String> data = parseSmsToken(strMessage);
                    //String token = data.get("token");
                    final Matcher matcher = Pattern.compile("mTOKEN : ").matcher(strMessage);
                    if(matcher.find()){
                        // System.out.println(sentence.substring(matcher.end()).trim());
                        String res =strMessage.substring(matcher.end()).trim();
                        int lenght = res.length();
                        final String otp = res.substring(0,6);
                        txtOTP.setText(otp);
                    }
                }

            }
        }
    };

    private static Map<String, String> parseSmsToken(String s){
        Map<String, String> ret = new HashMap<String, String>();
        StringTokenizer t = new StringTokenizer(s, "\n");
        while (t.hasMoreTokens()){
            String b = t.nextToken().trim();
            if (b.startsWith("mTOKEN : ")){
                String number = parseNumber(b);
                ret.put("token", number);
            }
        }

        return ret;
    }


    private static String parseNumber(String s){
        String numberFragment = s.substring(s.lastIndexOf(' '), s.length());//extract number
        return numberFragment;
    }


}

package sgo.mobile.banksulteng.app.fragments;

/*public class FragmentTransferDomesticSourceAccount extends SherlockFragment {

    private ProgressDialog pDialog;

    private final Calendar mCalendar = Calendar.getInstance();

    private int hourOfDay = mCalendar.get(Calendar.HOUR_OF_DAY);

    private int minute = mCalendar.get(Calendar.MINUTE);

    private int day = mCalendar.get(Calendar.DAY_OF_MONTH);

    private int month = mCalendar.get(Calendar.MONTH);

    private int year = mCalendar.get(Calendar.YEAR);

    private static Spinner cbo_source_account = null;
    private static Spinner cbo_destination_account = null;

    Button btnDone;
    Button inpPaymentDate;
    EditText inpAmount, inpMemo;


    public String[] account_no_arr, account_name_arr, ccy_id_arr;
    private ArrayList<SourceAccountBean> SourceAcccountList = new ArrayList<SourceAccountBean>();

    public String[] bank_code_arr, bank_name_arr, account_no_arr2, account_name_arr2, ccy_id_arr2;
    private ArrayList<DomesticAccountBean> DestinationAccountList = new ArrayList<DomesticAccountBean>();

    SourceAccountAdapter sourceAccountAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_transfer_domestic_sourceaccount, container, false);
        cbo_source_account = (Spinner)view.findViewById(R.id.cbo_source_account);

        initView();

        btnDone               = (Button) view.findViewById(R.id.btnDone);
        btnDone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {

                SourceAccountBean sourceAccountBean = (SourceAccountBean) cbo_source_account.getSelectedItem();
                String source_account_name = sourceAccountBean.account_name;
                String source_account_no = sourceAccountBean.account_no;


                Fragment newFragment = null;
                newFragment = new FragmentTransferInquiry();
                Bundle args = new Bundle();
                args.putString("source_account_name", source_account_name);
                args.putString("source_account_no", source_account_no);
                newFragment.setArguments(args);
                switchFragment(newFragment);
            }
        });

        return view;

    }

    public void initView(){
        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
        RequestParams params   = new RequestParams();

        String userid = AppHelper.getUserId(getActivity());
        String bankcode = AplConstants.Bankcode;
        String pcd = AplConstants.pcd;

        params.put("userid", userid);
        params.put("bankcode", bankcode);
        params.put("passcode", pcd);

        Log.d("params", params.toString());

        client.post(AplConstants.SourceAccountListAPI, params, new AsyncHttpResponseHandler() {
            public void onSuccess(String content) {
                Log.d("result:", content);
                try {

                    if (pDialog != null) {
                        pDialog.dismiss();
                    }

                    JSONObject object = new JSONObject(content);
                    JSONObject body_data = object.getJSONObject("body_data");
                    String error_code = body_data.getString("error_code");
                    JSONArray data = body_data.getJSONArray("source_account_data");  int len = data.length();
                    account_no_arr = new String[len];
                    account_name_arr = new String[len];
                    ccy_id_arr = new String[len];
                    for (int i = 0; i < data.length(); i++) {
                        account_no_arr[i] = data.getJSONObject(i).getString("account_no");
                        account_name_arr[i] = data.getJSONObject(i).getString("account_name");
                        ccy_id_arr[i] = data.getJSONObject(i).getString("ccy_id");
                        SourceAcccountList.add(new SourceAccountBean(account_no_arr[i], account_name_arr[i], ccy_id_arr[i]));
                    }
                    SourceAccountAdapter cAdapter = new SourceAccountAdapter(getActivity(), android.R.layout.simple_spinner_item, SourceAcccountList);
                    cbo_source_account.setAdapter(cAdapter);
                }catch (JSONException e) {

                    e.printStackTrace();
                }
            }
            public void onFailure(Throwable error, String content) {
                if (pDialog != null) {
                    pDialog.dismiss();
                }
                Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
            }

        });
    }





    private void resetDate() {
        inpPaymentDate.setHint(new StringBuilder().append(StringUtils.pad(day))
                .append("-").append(StringUtils.pad(month+1)).append("-").append(StringUtils.pad(year)));
        inpPaymentDate.setTextColor(getResources().getColor(android.R.color.darker_gray));
    }

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);

        getSherlockActivity().getActionBar().removeAllTabs();
        getSherlockActivity().getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
    }

}

*/
package sgo.mobile.bankkaltim.app.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.activeandroid.query.Select;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.bankkaltim.AppHelper;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.activities.MainActivity;
import sgo.mobile.bankkaltim.app.adapter.BillerVoucherCatalogAdapterPurchase;
import sgo.mobile.bankkaltim.app.adapter.BillerVoucherNominalAdapter;
import sgo.mobile.bankkaltim.app.adapter.SourceAccountAdapters;
import sgo.mobile.bankkaltim.app.models.SourceAccountModel;
import sgo.mobile.bankkaltim.app.models.purchase.PurchaseCatalogElectricityModel;
import sgo.mobile.bankkaltim.app.models.purchase.PurchaseNominalElectricityModel;
import sgo.mobile.bankkaltim.app.ui.dialog.DefinedDialog;
import sgo.mobile.bankkaltim.conf.AplConstants;
import sgo.mobile.bankkaltim.frameworks.net.Networks;
import sgo.mobile.bankkaltim.frameworks.net.loopj.android.http.CustomSSLSocketFactory;
import sgo.mobile.bankkaltim.frameworks.session.Session;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by User on 14/10/2014.
 */
public class FragmentPurchaseGeneral extends SherlockFragment {
    private static Spinner cbo_source_account = null;
    public String[] account_no_arr, account_name_arr, ccy_id_arr;
    private ArrayList<SourceAccountModel> SourceAcccountList = new ArrayList<SourceAccountModel>();

    private static  Spinner cbo_voucher_name =null;
    public  String[] service_id_arr, biller_code_arr, biller_name_arr, biller_inquiry_arr, biller_id_arr, product_code_arr, product_name_arr, product_amount_arr;
    private ArrayList<PurchaseCatalogElectricityModel> BillerVoucherList = new ArrayList<PurchaseCatalogElectricityModel>();

    private static  Spinner cbo_voucher_nominal=null;
    public  String[] catalog_code_arr, catalog_nominal_arr;
    private ArrayList<PurchaseNominalElectricityModel> VoucherNominalList= new ArrayList<PurchaseNominalElectricityModel>();


    private ProgressDialog pDialog;
    public String userid, bankcode, passcode, type, menu_biller_code, billerId, phoneNumber ;
    Button btnDone,btnBack;
    EditText inpPhoneNumber;
    TextView lbl_header, lbl_title,lbl_note;
    ImageView ic_img;
    String purchaseElectricity= AplConstants.purchaseElectricityVoucher;
    String purchaseVoucherPhone = AplConstants.purchasePhoneVoucher;
    String purchaseOthers = AplConstants.purchaseOthers;
    HashMap<String,List<String>> mapElectricity = new HashMap<String,List<String>>();
    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");//dd/MM/yyyy
    Date now = new Date();
    String dateTime = sdfDate.format(now);
    boolean networks;
    public static ArrayList<String> pCode = new ArrayList<String>();
    public static ArrayList<String> bid = new ArrayList<String>();
    TableRow tbl_row_nominal1,tbl_row_nominal2;
    View line_nominal;
    public String dataSourceAccount="";
    int getCountSourceAccount, getCountBiller, getCountNominal;
    TextView lbl_providerName;
    BillerVoucherCatalogAdapterPurchase billerVoucherCatalogAdapterPurchase;
    private static LinkedHashMap<String,String> dataUiList;
    private static Map json;
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_purchase_general, container, false);
        Bundle bundle = this.getArguments();
        getCountSourceAccount = new Select().from(SourceAccountModel.class).count();
        getCountBiller = new Select().from(PurchaseCatalogElectricityModel.class).count();
        getCountNominal = new Select().from(PurchaseNominalElectricityModel.class).count();
        menu_biller_code  = bundle.getString("menu_biller_code");
        billerId  = bundle.getString("billerId");
        lbl_providerName = (TextView)view.findViewById(R.id.lbl_providerName);
        tbl_row_nominal1 = (TableRow)view.findViewById(R.id.tbl_row_nominal1);
        tbl_row_nominal2 = (TableRow)view.findViewById(R.id.tbl_row_nominal2);
        //  line_nominal = view.findViewById(R.id.line_nominal);

        if(menu_biller_code.equalsIgnoreCase("13")){
            lbl_providerName.setText("Nominal");
            tbl_row_nominal1.setVisibility(View.GONE);
            tbl_row_nominal2.setVisibility(View.GONE);
            // line_nominal.setVisibility(View.GONE);
        }else{
            tbl_row_nominal1.setVisibility(View.VISIBLE);
            tbl_row_nominal2.setVisibility(View.VISIBLE);
            //  line_nominal.setVisibility(View.VISIBLE);
        }

        cbo_source_account = (Spinner)view.findViewById(R.id.cbo_source_account);
        cbo_voucher_name = (Spinner)view.findViewById(R.id.cbo_voucher);
        cbo_voucher_nominal = (Spinner)view.findViewById(R.id.cbo_voucher_nominal);
        inpPhoneNumber = (EditText) view.findViewById(R.id.inpPhoneNumber);
        lbl_header = (TextView)view.findViewById(R.id.label_header);
        ic_img = (ImageView)view.findViewById(R.id.ic_img);
        lbl_title = (TextView)view.findViewById(R.id.lbl_title);
      //  lbl_note = (TextView)view.findViewById(R.id.label_note);





        checkMenuCode();
        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(inpPhoneNumber.getWindowToken(), 0);

        btnDone               = (Button) view.findViewById(R.id.btnDone);
        btnDone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                try {
                    final String txtMemberNo= inpPhoneNumber.getText().toString();
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());

                    if (menu_biller_code.equals(purchaseElectricity)&& txtMemberNo.equals("")) {
                        alert.setTitle(getResources().getString(R.string.lbl_purchase_electricity_title));
                        alert.setMessage(getResources().getString(R.string.lbl_purchase_electricity_orderid_alert));
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    }else if(menu_biller_code.equals(purchaseVoucherPhone)&& txtMemberNo.equals("")) {
                        alert.setTitle(getResources().getString(R.string.lbl_purchase_hp_title));
                        alert.setMessage(getResources().getString(R.string.lbl_purchase_hp_orderid_alert));
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    }else {
                        try {
                            networks = new Networks().isConnectingToInternet(getActivity());
                            if (networks) {
                                getPurchase();
                                // Toast.makeText(getActivity(), "Sorry, this process will available soon !", Toast.LENGTH_LONG).show();
                            } else {
                                AlertDialog.Builder alerts = new AlertDialog.Builder(getActivity());
                                alerts.setTitle(getResources().getString(R.string.lbl_purchase_title));
                                alerts.setMessage(getResources().getString(R.string.lbl_alert_connection));
                                alerts.setPositiveButton("OK", null);
                                alerts.show();

                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }



            }
        });
        btnBack               = (Button) view.findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Fragment newFragment = null;
                newFragment = new FragmentPurchase();
                Bundle args = new Bundle();
                args.putString("menu_biller_code", menu_biller_code);
                newFragment.setArguments(args);
                switchFragment(newFragment);

            }
        });


        //
        // Toast.makeText(getActivity(),menu_biller_code, Toast.LENGTH_LONG).show();

        initViews();
        initBillerList();
        //initNominal();
        return view;

    }



    public void getPurchase(){
        SourceAccountModel sourceAccountBean = ( SourceAccountModel) cbo_source_account.getSelectedItem();
        String source_account_no = sourceAccountBean.account_no;
        String source_account_name = sourceAccountBean.account_name;

        PurchaseCatalogElectricityModel billerVoucherCatalogBean = ( PurchaseCatalogElectricityModel) cbo_voucher_name.getSelectedItem();
        String vourcherCode = billerVoucherCatalogBean.billerCode;
        String voucherName = billerVoucherCatalogBean.billerName;
        String productCode = billerVoucherCatalogBean.productCode;
        String inquiry = billerVoucherCatalogBean.billerInquiry;
        String catalogCode = billerVoucherCatalogBean.serviceId;
        String catalogNominal = billerVoucherCatalogBean.productAmount;
//        BillerVoucherNominalBean billerVoucherNominalBean = (BillerVoucherNominalBean) cbo_voucher_nominal.getSelectedItem();
//        String catalogCode = billerVoucherNominalBean.catalogCode;
//        String catalogNominal = billerVoucherNominalBean.catalogNominal;

        phoneNumber = inpPhoneNumber.getText().toString();

        if(inquiry.equalsIgnoreCase("Y")){
            checkingBiller();
            //Toast.makeText(getActivity(), "wew", Toast.LENGTH_LONG).show();

        }else{
            Fragment newFragment = null;
            newFragment = new FragmentPurchaseConfirm();
            Bundle args = new Bundle();
            args.putString("menu_biller_code", menu_biller_code);
            args.putString("source_account_no", source_account_no);
            args.putString("source_account_name", source_account_name);
            args.putString("product_code", productCode);
            args.putString("biller_code", vourcherCode);
            args.putString("biller_name", voucherName);
            args.putString("catalog_code", catalogCode);
            args.putString("catalog_nominal", catalogNominal);
            args.putString("phone_number", phoneNumber);
            newFragment.setArguments(args);
            switchFragment(newFragment);
        }



    }

    public void checkingBiller() {

        pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.lbl_loading_process));
        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getActivity()));
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
        final RequestParams params = new RequestParams();

        PurchaseCatalogElectricityModel billerVoucherCatalogBean = (PurchaseCatalogElectricityModel)cbo_voucher_name.getSelectedItem();
        final String productCode= billerVoucherCatalogBean.productCode;
        final String billerCode= billerVoucherCatalogBean.billerCode;
        final String billerName= billerVoucherCatalogBean.billerName;
        final String productAmount= billerVoucherCatalogBean.productAmount;
        SourceAccountModel sourceAccountBean = (SourceAccountModel) cbo_source_account.getSelectedItem();
        final String source_account_name = sourceAccountBean.account_name;
        final String source_account_no = sourceAccountBean.account_no;
        try {

            String userid = AppHelper.getUserId(getActivity());
            String bankcode = AplConstants.Bankcode;
            String pcd = AplConstants.pcd;
            phoneNumber = inpPhoneNumber.getText().toString();
            String URL = AplConstants.InquiryBillerPayment;
            String appName = AplConstants.appName;
            String serviceName = URL.split("/")[4];
            String apiKey = AppHelper.getAccessKey(getActivity());
            UUID uniqueKey = UUID.randomUUID();
            String signature = uniqueKey+dateTime+ appName + serviceName+ bankcode + userid;



            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
            String signatureKey = new String(encodeUrlSafe(hmacData));
            params.put("userid", userid);
            params.put("bankcode", bankcode);
            params.put("passcode", pcd);
            params.put("productcode", productCode);
            params.put("orderid", phoneNumber);
            params.put("rc_uuid", uniqueKey.toString());
            params.put("rc_dtime", dateTime);
            params.put("signature", signatureKey);

        }catch (Exception e){
            System.out.println("Error");
        }

        client.post(AplConstants.InquiryBillerPayment, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject object) {



                try {
                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    JSONObject body_data = object.getJSONObject("body_data");
                    String error_code = body_data.getString("error_code");
                    String error_message = body_data.getString("error_message");
                    if(error_code.equalsIgnoreCase("0000")){
                        JSONObject biller_data = body_data.getJSONObject("biller_data");
                        String orderId = biller_data.getString("orderId");
                        String amount = biller_data.getString("amount");
                        String billerId = biller_data.getString("billerId");
                        String billerCode = biller_data.getString("billerCode");
                        String billerName = biller_data.getString("billerName");
                        String description = biller_data.getString("description");
                        String name = biller_data.getString("name");
                        String up_code = biller_data.getString("up_code");
                        String up_phone_no = biller_data.getString("up_phone_no");
                        String fare = biller_data.getString("fare");
                        String max_kwh = biller_data.getString("max_kwh");
                        String customer_id = biller_data.getString("customer_id");
                        String meter_number = biller_data.getString("meter_number");
                        String distribution_code = biller_data.getString("distribution_code");
                        String power = biller_data.getString("power");
                        // JSONObject biller_data = body_data.getJSONObject("biller_data");
                        JSONObject data = biller_data.getJSONObject("dataUi");

/*
                        JSONParser parser = new JSONParser();
                        ContainerFactory containerFactory = new ContainerFactory(){
                            public List creatArrayContainer() {
                                return new LinkedList();
                            }

                            public Map createObjectContainer() {
                                return new LinkedHashMap();
                            }

                        };

                        try{
                            json = (Map)parser.parse(content, containerFactory);
                            dataUiList = (LinkedHashMap<String, String>)json.get("biller_data_ui");

                            Iterator iter = dataUiList.entrySet().iterator();
                            while(iter.hasNext()){
                                Map.Entry entry = (Map.Entry)iter.next();
                            }


                        }
                        catch(ParseException pe){
                            System.out.println(pe);
                        }*/
                        Fragment newFragment = null;
                        newFragment = new FragmentPurchaseConfirm();
                        Bundle args = new Bundle();
                        args.putString("menu_biller_code", menu_biller_code);
                        args.putString("source_account_no", source_account_no);
                        args.putString("source_account_name", source_account_name);
                        args.putString("product_code", productCode);
                        args.putString("biller_id", billerId);
                        args.putString("biller_code", billerCode);
                        args.putString("biller_name", billerName);
                        args.putString("order_id", orderId);
                        args.putString("name", name);
                        args.putString("nominal", productAmount);
                        args.putString("customer_id", customer_id);
                        args.putString("meter_number", meter_number);
                        args.putString("amount", amount);
                        // args.putString("data", JSONValue.toJSONString(dataUiList));
                        newFragment.setArguments(args);
                        switchFragment(newFragment);



                    }else if(error_code.equalsIgnoreCase("0404")){
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                        alert.setMessage(error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Session session;
                                session = new Session(getActivity());
                                session.signOut();
                            }
                        });
                        alert.show();

                    }else if(error_code.equalsIgnoreCase("14")){
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_purchase_title));
                        alert.setMessage(error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Fragment newFragment = null;
                                newFragment = new FragmentPurchaseGeneral();
                                Bundle args = new Bundle();
                                args.putString("menu_biller_code", menu_biller_code);
                                newFragment.setArguments(args);
                                switchFragment(newFragment);
                            }
                        });
                        alert.show();
                    }else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_purchase_title));
                        alert.setMessage(getResources().getString(R.string.lbl_purchase_title) + error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Fragment newFragment = null;
                                newFragment = new FragmentPurchaseGeneral();
                                Bundle args = new Bundle();
                                args.putString("menu_biller_code", menu_biller_code);
                                newFragment.setArguments(args);
                                switchFragment(newFragment);
                            }
                        });
                        alert.show();

                    }

                }catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (pDialog != null) {
                    pDialog.dismiss();
                }
                if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                } else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                }
            }
        });

    }


    private void initViews() {
        SourceAcccountList.clear();
        dataSourceAccount = AppHelper.getSourceAccount(getSherlockActivity());
        if(dataSourceAccount.equals("")){
            SourceAcccountList.clear();
            pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.lbl_loading_data));

            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getActivity()));
            client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
            RequestParams params = new RequestParams();

            try {
                String userid = AppHelper.getUserId(getActivity());
                String bankcode = AplConstants.Bankcode;
                String pcd = AplConstants.pcd;
                String URL = AplConstants.SourceAccountListAPI;
                String appName = AplConstants.appName;
                String serviceName = URL.split("/")[4];
                String apiKey = AppHelper.getAccessKey(getActivity());
                UUID uniqueKey = UUID.randomUUID();
                String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;
                //Toast.makeText(getActivity(), userid + " " + apiKey, Toast.LENGTH_LONG).show();


                Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
                SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
                sha256_HMAC.init(secret_key);
                byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
                String signatureKey = new String(encodeUrlSafe(hmacData));
                params.put("userid", userid);
                params.put("bankcode", bankcode);
                params.put("passcode", pcd);
                params.put("rc_uuid", uniqueKey.toString());
                params.put("rc_dtime", dateTime);
                params.put("signature", signatureKey);

            } catch (Exception e) {
                System.out.println("Error");
            }
            client.post(AplConstants.SourceAccountListAPI, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject object) {


                    try {

                        if (pDialog != null) {
                            pDialog.dismiss();
                        }
                        JSONObject body_data = object.getJSONObject("body_data");
                        // parse json data and store into arraylist variables
                        String error_code = body_data.getString("error_code");
                        String error_message = body_data.getString("error_message");

                        if (error_code.equalsIgnoreCase("0000")) {
                            JSONArray data = body_data.getJSONArray("source_account_data"); // this is the "items: [ ] part

                            int len = data.length();
                            account_no_arr = new String[len];
                            account_name_arr = new String[len];
                            ccy_id_arr = new String[len];
                            for (int i = 0; i < data.length(); i++) {
                                account_no_arr[i] = data.getJSONObject(i).getString("account_no");
                                account_name_arr[i] = data.getJSONObject(i).getString("account_name");
                                ccy_id_arr[i] = data.getJSONObject(i).getString("ccy_id");
                                SourceAcccountList.add(new SourceAccountModel(account_no_arr[i], account_name_arr[i], ccy_id_arr[i]));
                                SecurePreferences prefs = new SecurePreferences(getSherlockActivity());
                                SecurePreferences.Editor mEditor = prefs.edit();
                                mEditor.putString("sourceAccountData", data.toString());
                                mEditor.apply();


                            }
                            SourceAccountAdapters cAdapter = new SourceAccountAdapters(getActivity(), android.R.layout.simple_spinner_item, SourceAcccountList);
                            cbo_source_account.setAdapter(cAdapter);

                        } else if (error_code.equalsIgnoreCase("0404")) {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                            alert.setMessage(error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Session session;
                                    session = new Session(getActivity());
                                    session.signOut();
                                }
                            });
                            alert.show();

                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_purchase_title));
                            alert.setMessage(getResources().getString(R.string.lbl_purchase_title) + error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Fragment newFragment = null;
                                    newFragment = new FragmentPurchaseGeneral();
                                    switchFragment(newFragment);
                                }
                            });
                            alert.show();
                        }

                        //get Voucher List



                        // End of get Voucher List


                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    }
                }


            });
        }else{
            SourceAcccountList.clear();
            try {
                JSONArray data = new JSONArray(dataSourceAccount);
                int len = data.length();
                account_no_arr = new String[len];
                account_name_arr = new String[len];
                ccy_id_arr = new String[len];
                for (int i = 0; i < data.length(); i++) {
                    account_no_arr[i] = data.getJSONObject(i).getString("account_no");
                    account_name_arr[i] = data.getJSONObject(i).getString("account_name");
                    ccy_id_arr[i] = data.getJSONObject(i).getString("ccy_id");
                    SourceAcccountList.add(new SourceAccountModel(account_no_arr[i], account_name_arr[i], ccy_id_arr[i]));
                }
                SourceAccountAdapters cAdapter = new SourceAccountAdapters(getActivity(), android.R.layout.simple_spinner_item, SourceAcccountList);
                cbo_source_account.setAdapter(cAdapter);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


    }

    private void initBillerList(){
        if(getCountBiller==0){
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getActivity()));
            client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
            RequestParams params = new RequestParams();

            try {

                String userid = AppHelper.getUserId(getActivity());
                String bankcode = AplConstants.Bankcode;
                String pcd = AplConstants.pcd;
                String URL = AplConstants.InquiryBiller;
                String appName = AplConstants.appName;
                String serviceName = URL.split("/")[4];
                String apiKey = AppHelper.getAccessKey(getActivity());
                UUID uniqueKey = UUID.randomUUID();
                String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;
                Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
                SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
                sha256_HMAC.init(secret_key);
                byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
                String signatureKey = new String(encodeUrlSafe(hmacData));
                params.put("userid", userid);
                params.put("bankcode", bankcode);
                params.put("passcode", pcd);
                params.put("categorycode", menu_biller_code);
                params.put("rc_uuid", uniqueKey.toString());
                params.put("rc_dtime", dateTime);
                params.put("signature", signatureKey);

            } catch (Exception e) {
                System.out.println("Error");
            }
            client.post(AplConstants.InquiryBiller, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject object) {



                    try {

                        if (pDialog != null) {
                            pDialog.dismiss();
                        }
                        JSONObject body_data = object.getJSONObject("body_data");
                        String error_code = body_data.getString("error_code");
                        String error_message = body_data.getString("error_message");

                        if (error_code.equalsIgnoreCase("0000")) {
                            JSONArray data = body_data.getJSONArray("biller_data");
                            int len = data.length();
                            service_id_arr = new String[len];
                            biller_id_arr = new String[len];
                            biller_code_arr = new String[len];
                            product_code_arr = new String[len];
                            biller_name_arr = new String[len];
                            biller_inquiry_arr = new String[len];
                            product_name_arr = new String[len];
                            product_amount_arr = new String[len];
                            for (int i = 0; i < data.length(); i++) {
                                service_id_arr[i] = data.getJSONObject(i).getString("serviceId");
                                biller_id_arr[i] = data.getJSONObject(i).getString("billerId");
                                biller_code_arr[i] = data.getJSONObject(i).getString("billerCode");
                                biller_name_arr[i] = data.getJSONObject(i).getString("billerName");
                                biller_inquiry_arr[i] = data.getJSONObject(i).getString("billerInquiry");
                                product_code_arr[i] = data.getJSONObject(i).getString("productCode");
                                product_name_arr[i] = data.getJSONObject(i).getString("productName");
                                product_amount_arr[i] = data.getJSONObject(i).getString("productAmount");

                                BillerVoucherList.add(new PurchaseCatalogElectricityModel(service_id_arr[i], biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));
                                PurchaseCatalogElectricityModel purchaseCatalogElectricityModel = new PurchaseCatalogElectricityModel();
                                purchaseCatalogElectricityModel.serviceId =data.getJSONObject(i).getString("serviceId");
                               purchaseCatalogElectricityModel.bilerId =data.getJSONObject(i).getString("billerId");
                               purchaseCatalogElectricityModel.billerCode = data.getJSONObject(i).getString("billerCode");
                               purchaseCatalogElectricityModel.billerName = data.getJSONObject(i).getString("billerName");
                               purchaseCatalogElectricityModel.billerInquiry = data.getJSONObject(i).getString("billerInquiry");
                               purchaseCatalogElectricityModel.productCode = data.getJSONObject(i).getString("productCode");
                               purchaseCatalogElectricityModel.productName = data.getJSONObject(i).getString("productName");
                               purchaseCatalogElectricityModel.productAmount =data.getJSONObject(i).getString("productAmount");

                               purchaseCatalogElectricityModel.save();

                            }
                            billerVoucherCatalogAdapterPurchase = new BillerVoucherCatalogAdapterPurchase(getActivity(), android.R.layout.simple_spinner_item, BillerVoucherList);
                            cbo_voucher_name.setAdapter(billerVoucherCatalogAdapterPurchase);




                        } else if (error_code.equalsIgnoreCase("0404")) {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                            alert.setMessage(error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Session session;
                                    session = new Session(getActivity());
                                    session.signOut();
                                }
                            });
                            alert.show();

                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_purchase_title));
                            alert.setMessage(getResources().getString(R.string.lbl_purchase_title) + error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Fragment newFragment = null;
                                    newFragment = new FragmentPurchaseGeneral();
                                    switchFragment(newFragment);
                                }
                            });
                            alert.show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    }
                }

            });
        }else{
            List<PurchaseCatalogElectricityModel> itemList = new Select().all().from(PurchaseCatalogElectricityModel.class).execute();
            int len = itemList.size();
            service_id_arr = new String[len];
            biller_id_arr = new String[len];
            biller_code_arr = new String[len];
            biller_name_arr = new String[len];
            biller_inquiry_arr = new String[len];
            product_code_arr = new String[len];
            product_name_arr = new String[len];
            product_amount_arr = new String[len];
            for (int i = 0; i < itemList.size(); i++) {
//                account_no_arr[i] = encrypt.aes_decrypt(itemList.get(i).getAccount_no(), AppHelper.getUserId(getActivity()));
//                account_name_arr[i] = encrypt.aes_decrypt(itemList.get(i).getAccount_name(), AppHelper.getUserId(getActivity()));
//                ccy_id_arr[i] = encrypt.aes_decrypt(itemList.get(i).getCcy_id(), AppHelper.getUserId(getActivity()));


                service_id_arr[i] = itemList.get(i).getServiceId();
                biller_id_arr[i] = itemList.get(i).getBilerId();
                biller_code_arr[i] = itemList.get(i).getBillerCode();
                biller_name_arr[i] = itemList.get(i).getBillerName();
                biller_inquiry_arr[i] = itemList.get(i).getBillerInquiry();
                product_code_arr[i] = itemList.get(i).getProductCode();
                product_name_arr[i] = itemList.get(i).getProductName();
                product_amount_arr[i] = itemList.get(i).getProductAmount();
                BillerVoucherList.add(new PurchaseCatalogElectricityModel(service_id_arr[i], biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));


            }
            billerVoucherCatalogAdapterPurchase = new BillerVoucherCatalogAdapterPurchase(getActivity(), android.R.layout.simple_spinner_item, BillerVoucherList);
            cbo_voucher_name.setAdapter(billerVoucherCatalogAdapterPurchase);



        }
    }

    private void initNominal(){
        if(getCountNominal==0){
            // pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, "Requesting Voucher List...");

           // SourceAccountBean sourceAccountBean = (SourceAccountBean) cbo_source_account.getSelectedItem();
            //PurchaseCatalogElectricityModel purchaseCatalogElectricityModel = (PurchaseCatalogElectricityModel) cbo_voucher_name.getSelectedItem();
            //Toast.makeText(getActivity(), purchaseCatalogElectricityModel.productCode, Toast.LENGTH_LONG).show();

            VoucherNominalList.clear();
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getActivity()));
            client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
            RequestParams params = new RequestParams();

            try {
                String userid = AppHelper.getUserId(getActivity());
                String bankcode = AplConstants.Bankcode;
                String pcd = AplConstants.pcd;
                String URL = AplConstants.InquiryBillerCatalog;
                String appName = AplConstants.appName;
                String serviceName = URL.split("/")[4];
                String apiKey = AppHelper.getAccessKey(getActivity());
                UUID uniqueKey = UUID.randomUUID();
                String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;

                Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
                SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
                sha256_HMAC.init(secret_key);
                byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
                String signatureKey = new String(encodeUrlSafe(hmacData));
                params.put("userid", userid);
                params.put("bankcode", bankcode);
                params.put("passcode", pcd);
                params.put("productcode", "");
                params.put("rc_uuid", uniqueKey.toString());
                params.put("rc_dtime", dateTime);
                params.put("signature", signatureKey);

            } catch (Exception e) {
                System.out.println("Error");
            }

            client.post(AplConstants.InquiryBillerCatalog, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject object) {



                    try {
                        if (pDialog != null) {
                            pDialog.dismiss();
                        }
                        JSONObject body_data = object.getJSONObject("body_data");
                        String error_code = body_data.getString("error_code");
                        String error_message = body_data.getString("error_message");

                        if (error_code.equalsIgnoreCase("0000")) {
                            JSONArray data = body_data.getJSONArray("catalog_data");
                            int len = data.length();
                            catalog_code_arr = new String[len];
                            catalog_nominal_arr = new String[len];
                            for (int i = 0; i < data.length(); i++) {
                                catalog_code_arr[i] = data.getJSONObject(i).getString("catalogCode");
                                catalog_nominal_arr[i] = data.getJSONObject(i).getString("catalogNominal");
                                VoucherNominalList.add(new PurchaseNominalElectricityModel(catalog_code_arr[i], catalog_nominal_arr[i]));

                                PurchaseNominalElectricityModel purchaseNominalElectricityModel = new PurchaseNominalElectricityModel();
                                purchaseNominalElectricityModel.catalogCode = data.getJSONObject(i).getString("catalogCode");
                                purchaseNominalElectricityModel.catalogNominal = data.getJSONObject(i).getString("catalogNominal");
                                purchaseNominalElectricityModel.save();
                            }
                            BillerVoucherNominalAdapter billerVoucherNominalAdapter = new BillerVoucherNominalAdapter(getActivity(), android.R.layout.simple_spinner_item, VoucherNominalList);
                            cbo_voucher_nominal.setAdapter(billerVoucherNominalAdapter);
                        } else if (error_code.equalsIgnoreCase("0404")) {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                            alert.setMessage(error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Session session;
                                    session = new Session(getActivity());
                                    session.signOut();
                                }
                            });
                            alert.show();

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    }
                }

            });

        }else{
            List<PurchaseNominalElectricityModel> itemList = new Select().all().from(PurchaseNominalElectricityModel.class).execute();
            int len = itemList.size();
            catalog_code_arr = new String[len];
            catalog_nominal_arr = new String[len];
            for (int i = 0; i < itemList.size(); i++) {
//                account_no_arr[i] = encrypt.aes_decrypt(itemList.get(i).getAccount_no(), AppHelper.getUserId(getActivity()));
//                account_name_arr[i] = encrypt.aes_decrypt(itemList.get(i).getAccount_name(), AppHelper.getUserId(getActivity()));
//                ccy_id_arr[i] = encrypt.aes_decrypt(itemList.get(i).getCcy_id(), AppHelper.getUserId(getActivity()));


                catalog_code_arr[i] = itemList.get(i).getCatalogCode();
                catalog_nominal_arr[i] = itemList.get(i).getCatalogNominal();
                VoucherNominalList.add(new PurchaseNominalElectricityModel(catalog_code_arr[i], catalog_nominal_arr[i]));


            }
            BillerVoucherNominalAdapter billerVoucherNominalAdapter = new BillerVoucherNominalAdapter(getActivity(), android.R.layout.simple_spinner_item, VoucherNominalList);
            cbo_voucher_nominal.setAdapter(billerVoucherNominalAdapter);
        }
    }



    public static ArrayList<String> removeDuplicates(ArrayList<String> list) {

        // Store unique items in result.
        ArrayList<String> result = new ArrayList<String>();

        // Record encountered Strings in HashSet.
        HashSet<String> set = new HashSet<String>();

        // Loop over argument list.
        for (String item : list) {

            // If String is not in set, add it to the list and the set.
            if (!set.contains(item)) {
                result.add(item);
                set.add(item);
            }
        }
        return result;
    }

    private void checkMenuCode(){

        try {
            if (menu_biller_code.equals(purchaseElectricity)){
                lbl_header.setText(getResources().getString(R.string.lbl_purchase_electricity_title));
                lbl_title.setText(getResources().getString(R.string.lbl_purchase_electricity_orderid));
                ic_img.setImageResource(R.drawable.ic_electricity);
                inpPhoneNumber.setHint(getResources().getString(R.string.lbl_purchase_electricity_orderid_hint));
                lbl_note.setText("*max 11 digits");
            }
            if (menu_biller_code.equals(purchaseVoucherPhone)){
                lbl_header.setText(getResources().getString(R.string.lbl_purchase_hp_title));
                lbl_title.setText(getResources().getString(R.string.lbl_purchase_hp_orderid));
                ic_img.setImageResource(R.drawable.ic_mobile);
                inpPhoneNumber.setHint(getResources().getString(R.string.lbl_purchase_hp_orderid_hint));
                lbl_note.setText("*min 10 digits, max 16 digits");
            }if (menu_biller_code.equals(purchaseOthers)){
                lbl_header.setText("Purchase Others");
                lbl_title.setText("Account No");
                ic_img.setImageResource(R.drawable.ic_voucher);
                inpPhoneNumber.setHint("* Input Account No");
            }


        }catch (Exception e){
            e.printStackTrace();
        }

    }


    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);

        getSherlockActivity().getActionBar().removeAllTabs();
        getSherlockActivity().getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
    }
    public static byte[] encodeUrlSafe(byte[] data) {
        byte[] encode = Base64.encodeBase64(data);
        for (int i = 0; i < encode.length; i++) {
            if (encode[i] == '+') {
                encode[i] = '-';
            } else if (encode[i] == '=') {
                encode[i] = '_';
            } else if (encode[i] == '/') {
                encode[i] = '~';
            }
        }
        return encode;
    }
}

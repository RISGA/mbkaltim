package sgo.mobile.bankkaltim.app.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.bankkaltim.AppHelper;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.activities.MainActivity;
import sgo.mobile.bankkaltim.app.ui.dialog.DefinedDialog;
import sgo.mobile.bankkaltim.conf.AplConstants;
import sgo.mobile.bankkaltim.frameworks.net.Networks;
import sgo.mobile.bankkaltim.frameworks.net.loopj.android.http.CustomSSLSocketFactory;
import sgo.mobile.bankkaltim.frameworks.session.Session;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * Created by User on 22/08/2014.
 */
public class FragmentBenefToConfirm  extends SherlockFragment {

    String bankcode, bankname, benefName, benefNo, benefEmail, benefCcy, type;
    Button btnDone;
    private ProgressDialog pDialog;
    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");//dd/MM/yyyy
    Date now = new Date();
    String dateTime = sdfDate.format(now);
    boolean networks;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_beneficiary, container, false);
        Bundle bundle = this.getArguments();
        bankcode = bundle.getString("bankcode");
        bankname = bundle.getString("bankname");
        benefName = bundle.getString("benefName");
        benefNo = bundle.getString("benefNo");
        benefEmail = bundle.getString("benefEmail");
        benefCcy = bundle.getString(" beneCcy");
        type = bundle.getString("type");
        TextView label_benefName = (TextView) view.findViewById(R.id.label_benefName);
        label_benefName.setText(benefName);
        TextView label_benefNo = (TextView) view.findViewById(R.id.label_benefNo);
        label_benefNo.setText(benefNo);
        TextView label_bankName = (TextView) view.findViewById(R.id.label_bankName);
        label_bankName.setText(bankname);
        TextView label_benefEmail = (TextView) view.findViewById(R.id.label_Email);
        label_benefEmail.setText(benefEmail);

        btnDone = (Button) view.findViewById(R.id.btnDone);
        btnDone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {

                try {
                    networks = new Networks().isConnectingToInternet(getActivity());
                    if (networks) {
                        SubmitNewBeneficiary();
                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_add_beneficiary_title));
                        alert.setMessage(getResources().getString(R.string.lbl_alert_connection));
                        alert.setPositiveButton("OK", null);
                        alert.show();

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

/*
                Fragment newFragment = null;
                newFragment = new FragmentBenefTo();
                switchFragment(newFragment);*/
            }
        });

        return view;


    }

    private void SubmitNewBeneficiary() {
        pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.lbl_loading_process));
        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getSherlockActivity()));
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
        RequestParams params = new RequestParams();

        try{

            if (benefEmail.equalsIgnoreCase("-")){
                benefEmail="";
            }

            String userid = AppHelper.getUserId(getActivity());
            String pcd = AplConstants.pcd;
            String bcode = AplConstants.Bankcode;
            String ccy = AplConstants.CurrencyIDR;
            String URL = AplConstants.AddBeneficiaryAPI;
            String appName = AplConstants.appName;
            String serviceName = URL.split("/")[4];
            String apiKey = AppHelper.getAccessKey(getActivity());
            UUID uniqueKey = UUID.randomUUID();
            String signature = uniqueKey+dateTime+ appName + serviceName+ bcode+ userid;
            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
            String signatureKey = new String(encodeUrlSafe(hmacData));
            params.put("userid", userid);
            params.put("bankcode", bcode);
            params.put("passcode", pcd);
            params.put("type", type);
            params.put("destbankcode", bankcode);
            params.put("destaccountno", benefNo);
            params.put("destaccountname", benefName);
            params.put("destaccountccy", ccy);
            params.put("destacctemail", benefEmail);
            params.put("rc_uuid", uniqueKey.toString());
            params.put("rc_dtime", dateTime);
            params.put("signature", signatureKey);

        }catch (Exception e){
            System.out.println("Error");
        }
        client.post(AplConstants.AddBeneficiaryAPI, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject object) {
                try {
                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    JSONObject body_data = object.getJSONObject("body_data");
                    String error_code = body_data.getString("error_code");
                    String error_message = body_data.getString("error_message");

                    if (error_code.equals("0000")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_add_beneficiary_title));
                        alert.setMessage(getResources().getString(R.string.lbl_add_beneficiary_title) + " " + error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Fragment newFragment = null;
                                newFragment = new FragmentBenefTo();
                                switchFragment(newFragment);
                                SecurePreferences prefs = new SecurePreferences(getSherlockActivity());
                                prefs.edit().remove("destAccountData").commit();
                            }
                        });
                        alert.show();

                    } else if (error_code.equalsIgnoreCase("0404")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_add_beneficiary_title));
                        alert.setMessage(getResources().getString(R.string.lbl_alert_sessionexpire_title) + " " + error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Session session;
                                session = new Session(getActivity());
                                session.signOut();
                            }
                        });
                        alert.show();

                    } else {

                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_add_beneficiary_title));
                        alert.setMessage(getResources().getString(R.string.lbl_add_beneficiary_title) + " " + error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Fragment newFragment = null;
                                newFragment = new FragmentBenefTo();
                                switchFragment(newFragment);
                                SecurePreferences prefs = new SecurePreferences(getSherlockActivity());
                                prefs.edit().remove("destAccountData").commit();
                            }
                        });
                        alert.show();

                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (pDialog != null) {
                    pDialog.dismiss();
                }
                if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                } else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                }
            }
        });
    }
    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);

        getSherlockActivity().getActionBar().removeAllTabs();
        getSherlockActivity().getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
    }

    public static byte[] encodeUrlSafe(byte[] data) {
        byte[] encode = Base64.encodeBase64(data);
        for (int i = 0; i < encode.length; i++) {
            if (encode[i] == '+') {
                encode[i] = '-';
            } else if (encode[i] == '=') {
                encode[i] = '_';
            } else if (encode[i] == '/') {
                encode[i] = '~';
            }
        }
        return encode;
    }
}





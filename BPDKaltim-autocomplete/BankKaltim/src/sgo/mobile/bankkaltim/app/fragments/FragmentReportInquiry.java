package sgo.mobile.bankkaltim.app.fragments;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.openwidget.listview.RefreshAndLoadMoreListView;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.bankkaltim.AppHelper;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.activities.MainActivity;
import sgo.mobile.bankkaltim.app.adapter.ReportInquiryAdapterList;
import sgo.mobile.bankkaltim.conf.AplConstants;
import sgo.mobile.bankkaltim.frameworks.net.Networks;
import sgo.mobile.bankkaltim.frameworks.net.loopj.android.http.CustomSSLSocketFactory;
import sgo.mobile.bankkaltim.frameworks.session.Session;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.UUID;



/**
 * Created by thinkpad on 1/14/2015.
 */
public class FragmentReportInquiry extends Fragment {
    FragmentManager fm;
    private ProgressDialog pDialog;
    // declare view objects
    RefreshAndLoadMoreListView mListView;
    ProgressBar prgLoading;
    TextView txtAlert,txtHeader,lbl_error,lblPaymentReport;
   /* ImageButton btnBack;*/
    Button btnDate,btnBack;
    EditText txtStart, txtEnd;

    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");//dd/MM/yyyy
    SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");//dd/MM/yyyy
    java.util.Date now = new java.util.Date();
    String startDate = sdfDate.format(now);
    String endDate = sdfDate.format(now);
    String rdate= dt.format(now);
    boolean networks;

    ReportInquiryAdapterList reportInquiryAdapterList;

    // create arraylist variables to store data from server
    public static int imageBalance;
    public static ArrayList<Integer> imagePaymentType = new ArrayList<Integer>();
    public static ArrayList<String> totalAmount = new ArrayList<String>();
    public static ArrayList<String> date = new ArrayList<String>();
    //public static ArrayList<String> balance = new ArrayList<String>();
    public static ArrayList<String> description = new ArrayList<String>();

    //detail data
    ArrayList<String> paymentRef = new ArrayList<String>();
    ArrayList<String> dateTime = new ArrayList<String>();
    ArrayList<String> sourceAccount = new ArrayList<String>();
    ArrayList<String> sourceAccountCcy = new ArrayList<String>();
    ArrayList<String> serviceProvider = new ArrayList<String>();
    ArrayList<String> customerId = new ArrayList<String>();
    ArrayList<String> customerName = new ArrayList<String>();
    ArrayList<String> amount = new ArrayList<String>();
    ArrayList<String> fee = new ArrayList<String>();
    ArrayList<String> totalPayment = new ArrayList<String>();
    ArrayList<String> filename = new ArrayList<String>();
    ArrayList<String> pdfDesc = new ArrayList<String>();
    ArrayList<String> pdfStatus = new ArrayList<String>();


    public static String payment_id, payment_name;

    UUID uuidnya;
    String timeStamp;
    String signya;

    int mYear,mMonth,mDay,mActionDatePicker=0;
    DatePickerDialog dpd;
    Date mDateStart, mDateEnd;
    String error_msg;
    int pager = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        clearData();
        return inflater.inflate(R.layout.fragment_report_inquiry, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //clearData();
        //imageBalance = R.drawable.icon_pln_meter;
        imagePaymentType.add(R.drawable.ic_electricity);
        imagePaymentType.add(R.drawable.ic_water);
        imagePaymentType.add(R.drawable.ic_phone);
        imagePaymentType.add(R.drawable.ic_internet_payment);
        imagePaymentType.add(R.drawable.ic_tv_cable);
        imagePaymentType.add(R.drawable.ic_tax);
        imagePaymentType.add(R.drawable.ic_credit_card);
        imagePaymentType.add(R.drawable.ic_installment);
        imagePaymentType.add(R.drawable.ic_insurance);
        imagePaymentType.add(R.drawable.ic_education);
        imagePaymentType.add(R.drawable.ic_ticket);
        imagePaymentType.add(R.drawable.ic_other);
        imagePaymentType.add(R.drawable.ic_electricity);
        imagePaymentType.add(R.drawable.ic_phone);
        imagePaymentType.add(R.drawable.ic_other);

        prgLoading = (ProgressBar) getActivity().findViewById(R.id.prgLoading);
        mListView = (RefreshAndLoadMoreListView) getActivity().findViewById(R.id.list_report);
        txtHeader = (TextView) getActivity().findViewById(R.id.label_header);
        txtHeader.setVisibility(View.GONE);

        txtAlert = (TextView) getActivity().findViewById(R.id.txtAlert);
        txtAlert.setVisibility(View.GONE);

        lbl_error = (TextView) getActivity().findViewById(R.id.label_error);
        lbl_error.setVisibility(View.GONE);
        lblPaymentReport = (TextView) getActivity().findViewById(R.id.lblPaymentReport);
        lblPaymentReport.setVisibility(View.GONE);
       // btnBack = (Button) getActivity().findViewById(R.id.btnBack);
        btnDate = (Button) getActivity().findViewById(R.id.btnDate);
        txtStart = (EditText) getActivity().findViewById(R.id.txtStart);
        txtEnd = (EditText) getActivity().findViewById(R.id.txtEnd);

        reportInquiryAdapterList = new ReportInquiryAdapterList(getActivity());
        mListView.setAdapter(reportInquiryAdapterList);

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        dpd = new DatePickerDialog(getActivity(),new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear, int dayOfMonth) {
                if(mActionDatePicker == 1){
                    txtStart.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                    mDateStart = new Date(year-1900,monthOfYear,dayOfMonth);
                }
                else {
                    txtEnd.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                    mDateEnd = new Date(year-1900,monthOfYear,dayOfMonth);
                }
            }
        }, mYear, mMonth, mDay);

        //set maksimal datepicker dialog memunculkan tgl dan waktu saat ini saja
        dpd.getDatePicker().setMaxDate(c.getTimeInMillis());

        // event listener to handle list when clicked
//        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//
//            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
//                                    long arg3) {
//                Fragment newFragment = null;
//                newFragment = new FragmentReportDetail();
//                Bundle args = new Bundle();
//                args.putString("paymentId", payment_id);
//                args.putString("paymentType", payment_name);
//                args.putString("startDate", startDate);
//                args.putString("endDate", endDate);
//                args.putString("paymentRef", paymentRef.get(position - 1));
//                args.putString("dateTime", dateTime.get(position-1));
//                args.putString("sourceAccount", sourceAccount.get(position-1));
//                args.putString("sourceAccountCcy", sourceAccountCcy.get(position-1));
//                args.putString("serviceProvider", serviceProvider.get(position-1));
//                args.putString("customerId", customerId.get(position-1));
//                args.putString("customerName", customerName.get(position-1));
//                args.putString("amount", amount.get(position-1));
//                args.putString("fee", fee.get(position-1));
//                args.putString("totalPayment", totalPayment.get(position-1));
//                args.putString("URLPDF", filename.get(position-1));
//                args.putString("pdfDesc", pdfDesc.get(position-1));
//                args.putString("pdfStatus", pdfStatus.get(position-1));
//                newFragment.setArguments(args);
//                switchFragment(newFragment);
//            }
//        });


        txtStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActionDatePicker = 1;
                dpd.show();
            }
        });

        txtEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActionDatePicker = 2;
                dpd.show();
            }
        });

        Bundle bundle = this.getArguments();
        if(bundle != null) {
            payment_id = bundle.getString("paymentId");
            payment_name = bundle.getString("paymentName");
            startDate = bundle.getString("startDate");
            endDate = bundle.getString("endDate");
            txtStart.setText(startDate);
            txtEnd.setText(endDate);
            lblPaymentReport.setVisibility(View.VISIBLE);
            lblPaymentReport.setText(payment_name);
            try {
                java.util.Date utilStartDate = sdfDate.parse(txtStart.getText().toString());
                mDateStart = new Date(utilStartDate.getTime());
                java.util.Date utilEndDate = sdfDate.parse(txtEnd.getText().toString());
                mDateEnd = new Date(utilEndDate.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        try {
            networks = new Networks().isConnectingToInternet(getActivity());
            if (networks) {
                parseJSONData(Integer.toString(pager));
            } else {
                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                alert.setTitle(getResources().getString(R.string.lbl_transaction_report_title));
                alert.setMessage(getResources().getString(R.string.lbl_alert_connection));
                alert.setPositiveButton("OK", null);
                alert.show();

            }
        }catch (Exception e){
            e.printStackTrace();
        }



        mListView.setOnRefreshListener(new RefreshAndLoadMoreListView.OnRefreshListener() {

            @Override
            public void onRefresh() {
                pager = pager + 1;
                parseJSONData(Integer.toString(pager));
            }
        });

        btnDate.setOnClickListener(submitFilterListener);
       // btnBack.setOnClickListener(backListener);
    }

    Button.OnClickListener backListener = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            Fragment newFragment = null;
            newFragment = new FragmentReport();
            Bundle args = new Bundle();
            args.putString("startDate", startDate);
            args.putString("endDate", endDate);
            newFragment.setArguments(args);
            switchFragment(newFragment);
        }
    };

    //listener untuk submit dari filter tanggalnya
    Button.OnClickListener submitFilterListener = new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
        if(inputValidation()){
            txtAlert.setVisibility(View.GONE);
            mListView.setVisibility(View.GONE);
            prgLoading.setVisibility(View.VISIBLE);
            clearData();
            startDate = txtStart.getText().toString();
            endDate = txtEnd.getText().toString();
            pager = 1;
            parseJSONData(Integer.toString(pager));
        }
        else Toast.makeText(getActivity(), error_msg, Toast.LENGTH_SHORT).show();
        }
    };

    public boolean inputValidation(){
        if(txtStart.getText().toString().equals("")){
            error_msg = getResources().getString(R.string.lbl_transaction_report_start_date);
            return false;
        }
        else if(txtEnd.getText().toString().equals("")){
            error_msg = getResources().getString(R.string.lbl_transaction_report_end_date);
            return false;
        }
        else if(mDateEnd.compareTo(mDateStart)<0) {
            error_msg = getResources().getString(R.string.lbl_transaction_report_null);
            return false;
        }
        return true;
    }


    public void parseJSONData(String pager){
        clearData();
        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getActivity()));
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
        RequestParams params = new RequestParams();

        try {
            String userid = AppHelper.getUserId(getActivity());
            String bankcode = AplConstants.Bankcode;
            String pcd = AplConstants.pcd;
            String URL = AplConstants.BillerPaymentReport;
            String appName = AplConstants.appName;
            String serviceName = URL.split("/")[4];
            String apiKey = AppHelper.getAccessKey(getActivity());
            UUID uniqueKey = UUID.randomUUID();

            String message = uniqueKey + rdate + appName + serviceName + bankcode + userid;

            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] hmacData = sha256_HMAC.doFinal(message.getBytes("UTF-8"));
            String signatureKey = new String(encodeUrlSafe(hmacData));
            params.put("userid", userid);
            params.put("bankcode", bankcode);
            params.put("passcode", pcd);
            params.put("startdate", startDate);
            params.put("enddate", endDate);
            params.put("rc_uuid", uniqueKey.toString());
            params.put("rc_dtime",rdate);
            params.put("signature", signatureKey);
            params.put("service", payment_id);
            params.put("pager", pager);

        }catch (Exception e){
            System.out.println("Error");
        }

        client.post(AplConstants.BillerPaymentReport, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject json) {


                try {
                    JSONObject data = json.getJSONObject("body_data"); // this is the "items: [ ] part
                    String error_code = data.getString("error_code");
                    String error_message = data.getString("error_message");


                    if (error_code.equalsIgnoreCase("0000")) {
                        String check_data = data.getString("payment_report_data");
                        if (check_data.equals("null")) {
                            txtAlert.setVisibility(View.VISIBLE);
                            txtAlert.setText(getResources().getString(R.string.lbl_transaction_report_null));
                            if (description.size() > 0) {
                                Toast.makeText(getActivity(), getResources().getString(R.string.lbl_transaction_report_null), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else {
                            JSONArray payment_report_data = data.getJSONArray("payment_report_data");
                            for (int i = 0; i < payment_report_data.length(); i++) {
                                JSONObject object = payment_report_data.getJSONObject(i);
                                description.add(object.getString("description"));
                                date.add(object.getString("date"));
                                totalAmount.add(object.getString("totalAmount"));
                                JSONObject detailData = object.getJSONObject("detailData");
                                paymentRef.add(detailData.getString("paymentRef"));
                                dateTime.add(detailData.getString("dateTime"));
                                sourceAccount.add(detailData.getString("sourceAccount"));
                                sourceAccountCcy.add(detailData.getString("sourceAccountCcy"));
                                serviceProvider.add(detailData.getString("serviceProvider"));
                                customerId.add(detailData.getString("customerId"));
                                customerName.add(detailData.getString("customerName"));
                                amount.add(detailData.getString("amount"));
                                fee.add(detailData.getString("fee"));
                                totalPayment.add(detailData.getString("totalPayment"));
                                JSONObject download = detailData.getJSONObject("download");
                                filename.add(download.getString("filename"));
                                pdfDesc.add(download.getString("description"));
                                pdfStatus.add(download.getString("status"));
                            }
                        }
                    }else if(error_code.equalsIgnoreCase("0404")){
                        prgLoading.setVisibility(View.GONE);
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                        alert.setMessage(error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Session session;
                                session = new Session(getActivity());
                                session.signOut();
                            }
                        });
                        alert.show();

                    }else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_transaction_report_title));
                        alert.setMessage(getResources().getString(R.string.lbl_transaction_report_title)+" "+ error_message);
                        alert.setPositiveButton("OK", null);
                        Fragment newFragment = null;
                        newFragment = new FragmentReportInquiry();
                        switchFragment(newFragment);
                        alert.show();
                    }

                    // when finish parsing, hide progressbar
                    prgLoading.setVisibility(View.GONE);

                    // if data available show data on list
                    // otherwise, show alert text
                    if (description.size() > 0) {
                        mListView.setVisibility(View.VISIBLE);
                        mListView.setAdapter(reportInquiryAdapterList);
                    } else {
                        txtAlert.setVisibility(View.VISIBLE);
                    }

                    //mListView.completeLoadMore();
                    mListView.completeRefreshing();

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();

                }
            }


            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (pDialog != null) {
                    pDialog.dismiss();
                }
                if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                } else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                }
            }

        });
    }

    // clear arraylist variables before used
    void clearData(){
        totalAmount.clear();
        //balance.clear();
        date.clear();
        description.clear();
    }

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
        }
    };

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }

    public static byte[] encodeUrlSafe(byte[] data) {
        byte[] encode = Base64.encodeBase64(data);
        for (int i = 0; i < encode.length; i++) {
            if (encode[i] == '+') {
                encode[i] = '-';
            } else if (encode[i] == '=') {
                encode[i] = '_';
            } else if (encode[i] == '/') {
                encode[i] = '~';
            }
        }
        return encode;
    }

}

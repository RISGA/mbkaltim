package sgo.mobile.bankkaltim.app.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.bankkaltim.AppHelper;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.activities.MainActivity;
import sgo.mobile.bankkaltim.app.adapter.AccountListAdapter;
import sgo.mobile.bankkaltim.app.ui.dialog.DefinedDialog;
import sgo.mobile.bankkaltim.conf.AplConstants;
import sgo.mobile.bankkaltim.frameworks.net.loopj.android.http.CustomSSLSocketFactory;
import sgo.mobile.bankkaltim.frameworks.security.HmacSHA256Signature;
import sgo.mobile.bankkaltim.frameworks.session.Session;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

/**
 * Created by SGO-Mobile-DEV on 8/12/2015.
 */
public class FragmentChangeLanguage extends Fragment {
    RadioGroup radioGroup;
    RadioButton radioEN, radioIN;
    Button btnSave, btnBack;
    ProgressBar prgLoading;
    HmacSHA256Signature hmacSHA256Signature = new HmacSHA256Signature();
    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");//dd/MM/yyyy
    Date now = new Date();
    String dateTime = sdfDate.format(now);
    AccountListAdapter accountListAdapter;
    boolean networks;
    String lang;
    private ProgressDialog pDialog;
    int langPosition,langPosition1,langPosition2;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_change_language, container, false);

        radioGroup = (RadioGroup) view.findViewById(R.id.radioGroupLanguage);
        radioEN = (RadioButton) view.findViewById(R.id.radioEN);
        radioIN = (RadioButton) view.findViewById(R.id.radioIN);
        btnSave = (Button)view.findViewById(R.id.btnSave);
        btnBack = (Button)view.findViewById(R.id.btnBack);
        lang = AppHelper.getUserLang(getActivity());
        if(lang.equalsIgnoreCase("en")){
            radioEN.setChecked(true);

        }else if(lang.equalsIgnoreCase("id")){
            radioIN.setChecked(true);
        }
        btnSave.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                submit();
            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Fragment newFragment = null;
                newFragment = new FragmentSetting();
                switchFragment(newFragment);
            }
        });


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // TODO Auto-generated method stub
                if (radioEN.isChecked()) {

                    lang = "en";
                } else if (radioIN.isChecked()) {
                    lang = "id";

                }
            }
        });
        return view;
    }





    public void submit(){
        pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.lbl_language_process));
        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getActivity()));
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
        RequestParams params = new RequestParams();

        try {
            String userId = AppHelper.getUserId(getActivity());
            String bankCode = AplConstants.Bankcode;
            String pcd = AplConstants.pcd;
            String URL = AplConstants.ChangeLanguage;
            String appName = AplConstants.appName;
            String serviceName = URL.split("/")[4];
            String apiKey = AppHelper.getAccessKey(getActivity());
            UUID uniqueKey = UUID.randomUUID();

            String message = uniqueKey + dateTime + appName + serviceName + bankCode + userId;

            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] hmacData = sha256_HMAC.doFinal(message.getBytes("UTF-8"));
            String signatureKey = new String(encodeUrlSafe(hmacData));
            params.put("userid", userId);
            params.put("bankcode", bankCode);
            params.put("passcode", pcd);
            params.put("rc_uuid", uniqueKey.toString());
            params.put("rc_dtime",dateTime );
            params.put("signature",signatureKey);
            params.put("lang",lang);
        }catch (Exception e){
            System.out.println("Error");
        }
        client.post(AplConstants.ChangeLanguage, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject json) {


                try {

        if (pDialog != null) {
            pDialog.dismiss();
        }
        JSONObject body_data = json.getJSONObject("body_data");
        String error_code = body_data.getString("error_code");
        String error_message = body_data.getString("error_message");

        if (error_code.equalsIgnoreCase("0000")) {
            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
            alert.setTitle(getResources().getString(R.string.lbl_language_title));
            alert.setMessage(error_message);
            alert.setPositiveButton("OK", null);
            alert.show();

                       /* SecurePreferences prefs = new SecurePreferences(getActivity());
                        SecurePreferences.Editor mEditor = prefs.edit();
                        mEditor.putString("userLang", lang);
                        mEditor.apply();*/
            try {
                if (lang.equalsIgnoreCase("en")) {
                    // String languageToLoad  = "in"; // your language
                    //Locale locale = new Locale();
                    Locale locale = Locale.ENGLISH;
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getActivity().getResources().updateConfiguration(config,
                            getActivity().getBaseContext().getResources().getDisplayMetrics());
                    SecurePreferences prefs = new SecurePreferences(getActivity());
                    SecurePreferences.Editor mEditor = prefs.edit();
                    mEditor.putString("userLang", lang);
                    mEditor.apply();
                    restartActivity();


                } else {

                    String languageToLoad = "in"; // your language
                    Locale locale = new Locale(languageToLoad);
                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    getActivity().getBaseContext().getResources().updateConfiguration(config,
                            getActivity().getBaseContext().getResources().getDisplayMetrics());
                    SecurePreferences prefs = new SecurePreferences(getActivity());
                    SecurePreferences.Editor mEditor = prefs.edit();
                    mEditor.putString("userLang", lang);
                    mEditor.apply();
                    restartActivity();

                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        } else if (error_code.equalsIgnoreCase("0404")) {

            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
            alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
            alert.setMessage(error_message);
            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Session session;
                    session = new Session(getActivity());
                    session.signOut();
                }
            });
            alert.show();

        } else {

            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
            alert.setTitle(getResources().getString(R.string.lbl_language_title));
            alert.setMessage(error_message);
            alert.setPositiveButton("OK", null);
            alert.show();
        }

    } catch (JSONException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();

    }
}

;

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (pDialog != null) {
                    pDialog.dismiss();
                }
                if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                } else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                }
            }

        });
        }
private void restartActivity() {
        Intent intent = new Intent(getActivity(), MainActivity.class);

        startActivity(intent);
    }


    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }





    public static byte[] encodeUrlSafe(byte[] data) {
        byte[] encode = Base64.encodeBase64(data);
        for (int i = 0; i < encode.length; i++) {
            if (encode[i] == '+') {
                encode[i] = '-';
            } else if (encode[i] == '=') {
                encode[i] = '_';
            } else if (encode[i] == '/') {
                encode[i] = '~';
            }
        }
        return encode;
    }
}


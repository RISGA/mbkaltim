package sgo.mobile.bankkaltim.app.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.activeandroid.query.Select;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.ContainerFactory;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import sgo.mobile.bankkaltim.AppHelper;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.activities.MainActivity;
import sgo.mobile.bankkaltim.app.adapter.Payments.*;
import sgo.mobile.bankkaltim.app.adapter.SourceAccountAdapters;
import sgo.mobile.bankkaltim.app.beans.BillerVoucherCatalogBean;
import sgo.mobile.bankkaltim.app.models.SourceAccountModel;
import sgo.mobile.bankkaltim.app.models.payment.*;
import sgo.mobile.bankkaltim.app.ui.dialog.DefinedDialog;
import sgo.mobile.bankkaltim.conf.AplConstants;
import sgo.mobile.bankkaltim.frameworks.net.Networks;
import sgo.mobile.bankkaltim.frameworks.net.loopj.android.http.CustomSSLSocketFactory;
import sgo.mobile.bankkaltim.frameworks.security.Encrypt;
import sgo.mobile.bankkaltim.frameworks.session.Session;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by User on 10/10/2014.
 */
public class FragmentPaymentGeneral extends SherlockFragment {
    private static Spinner cbo_source_account = null;
    public String[] account_no_arr, account_name_arr, ccy_id_arr;
    private ArrayList<SourceAccountModel> SourceAcccountList = new ArrayList<SourceAccountModel>();

    private static  Spinner cbo_voucher_name =null;
    public  String[] biller_id_arr,biller_code_arr, biller_name_arr,biller_inquiry_arr, product_code_arr, product_name_arr, product_amount_arr;
    private static LinkedHashMap<String,String> dataUiList;
    private static Map json;
    Encrypt encrypt = new Encrypt();
    private ArrayList<BillerVoucherCatalogBean> BillerVoucherList = new ArrayList<BillerVoucherCatalogBean>();
    private ArrayList<PaymentTvCableModel> paymentTvCableModels = new ArrayList<PaymentTvCableModel>();
    private ArrayList<PaymentElectricityModel> paymentElectricityModels = new ArrayList<PaymentElectricityModel>();
    private ArrayList<PaymentInstallmentModel> paymentInstallmentModels = new ArrayList<PaymentInstallmentModel>();
    private ArrayList<PaymentInsuranceModel> paymentInsuranceModels = new ArrayList<PaymentInsuranceModel>();
    private ArrayList<PaymentInternetModel> paymentInternetModels = new ArrayList<PaymentInternetModel>();
    private ArrayList<PaymentPamModel> paymentPamModels = new ArrayList<PaymentPamModel>();
    private ArrayList<PaymentPhoneModel> paymentPhoneModels = new ArrayList<PaymentPhoneModel>();
    private ArrayList<PaymentTicketModel> paymentTicketModels = new ArrayList<PaymentTicketModel>();
    private ArrayList<PaymentTaxModel> paymentTaxModels= new ArrayList<PaymentTaxModel>();
    String source_account_no, source_account_name;
    public String dataSourceAccount="";
    private ProgressDialog pDialog;
    public String userid, bankcode, passcode, type, menu_biller_code,menu_biller_name,memberNo ;
    Button btnDone, btnBack;
    EditText inp_member_no,inp_amount;
    TextView label_member_no,menu_title,lbl_provider,lbl_note;
    ImageView ic_menu_payment;
    TableRow tbl_amount,tbl_lblamount;
    View amt_line;

    String billOther = AplConstants.billOthers;
    String billCreditCard = AplConstants.billCreditCard;
    String billElectricity = AplConstants.billElectricity;
    String billPhone = AplConstants.billPhone;
    String billTax = AplConstants.billTax;
    String billInternet = AplConstants.billInternet;
    String billWater = AplConstants.billwater;
    String billEducation= AplConstants.billIEducation;
    String billInsurance= AplConstants.billIInsurance;
    String billInstalment= AplConstants.billInstallment;
    String billTv= AplConstants.billCableTV;
    String billTicket= AplConstants.billITicket;
    int getCountSourceAccount, getCountBillerList, getCountPaymentCableTv, getCountElectricity, getCountInstallment, getCountInsurance, getCountInternet, getCountPam, getCountPhone, getCountTicket, getCountTax;
    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");//dd/MM/yyyy
    Date now = new Date();
    String dateTime = sdfDate.format(now);
    public String billerCode;
    boolean networks;

    // String Params KAI
    public String bill_total, passenger_name, train_number, train_name, train_seat;

    // String Params postpaid phone
    public  String phone_no, bill_total_phone, bill_ref, bill_amount, customer_name, region_code;

    // String Params PAM
    public String provider_code, customer_id, bill_date, penalty, cubication, ref_no;

    // String Params Telkom
    public String divre_code, datel_code, npwp;

    // String Params BII
    public String card_no, amount, minimum_payment;

    // String Params Bill Ticket
    public String airline_code, airline_code2 ,total_flight, pnr_code, number_of_passengers, carrier, class_ticket, from, to, flight_number, depart_date, depart_time;

    // String Params Sriwijaya Ticket
    public String unique_number, language, product_code, date, time, name, admin_fee;

    // String Params PBB
    public String indicator, nop, spt_year, kp_code, kelurahan, bill, confirmation_code;
    // String Params XL
    public String customer_nbr, due_date, approval_code;

    // String Params Telkom PSTN
    public String orderId, billerId, billerName, catalogCode;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payment_general, container, false);
        Bundle bundle = this.getArguments();
       /* BillerVoucherCatalogBean billType = (BillerVoucherCatalogBean) cbo_voucher_name.getSelectedItem();
        String bT = billType.getBillerInquiry();
        if(bT.equalsIgnoreCase("N")){
            tbl_lblamount.setVisibility(View.VISIBLE);
            tbl_amount.setVisibility(View.VISIBLE);
            amt_line.setVisibility(View.VISIBLE);
        }else{
            tbl_lblamount.setVisibility(View.GONE);
            tbl_amount.setVisibility(View.GONE);
            amt_line.setVisibility(View.GONE);
        }*/
        menu_biller_code = bundle.getString("menu_biller_code");
        menu_biller_name = bundle.getString("menu_biller_name");
        label_member_no = (TextView)view.findViewById(R.id.label_member_no);
        ic_menu_payment = (ImageView)view.findViewById(R.id.ic_menu_payment);
        inp_member_no= (EditText) view.findViewById(R.id.inp_member_no);
        menu_title = (TextView)view.findViewById(R.id.label_header);
        lbl_provider = (TextView)view.findViewById(R.id.lbl_provider);
       // lbl_note = (TextView)view.findViewById(R.id.label_note);
        tbl_amount = (TableRow)view.findViewById(R.id.tbl_amount);
        inp_amount = (EditText)view.findViewById(R.id.inp_amount);
        tbl_lblamount= (TableRow)view.findViewById(R.id.tbl_lblamount);
        amt_line = (View)view.findViewById(R.id.amt_line);
        checkMenuCode();
        cbo_source_account = (Spinner)view.findViewById(R.id.cbo_source_account);
        cbo_voucher_name = (Spinner)view.findViewById(R.id.cbo_voucher);
        getCountSourceAccount = new Select().from(SourceAccountModel.class).count();
        getCountPaymentCableTv = new Select().from(PaymentTvCableModel.class).count();
                getCountElectricity= new Select().from(PaymentElectricityModel.class).count();
        getCountInstallment= new Select().from(PaymentInstallmentModel.class).count();
                getCountInsurance= new Select().from(PaymentInsuranceModel.class).count();
        getCountInternet= new Select().from(PaymentInternetModel.class).count();
                getCountPam= new Select().from(PaymentPamModel.class).count();
        getCountPhone= new Select().from(PaymentPhoneModel.class).count();
                getCountTicket= new Select().from(PaymentTicketModel.class).count();
        getCountTax= new Select().from(PaymentTaxModel.class).count();
        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(inp_member_no.getWindowToken(), 0);

      /*  BillerVoucherCatalogBean billType = (BillerVoucherCatalogBean) cbo_voucher_name.getSelectedItem();
        final String InquiryType= billType.billerInquiry;*/
        btnDone               = (Button) view.findViewById(R.id.btnDone);
        btnDone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                try {
                    final String txtAccountNo = inp_member_no.getText().toString();
                    final String txtAmount = inp_amount.getText().toString();
//                    BillerVoucherCatalogBean billType = (BillerVoucherCatalogBean) cbo_voucher_name.getSelectedItem();
//                    final String InquiryType= billType.billerInquiry;

                    String InquiryType ="";
                    if (menu_biller_code.equals(billTv)){
                        PaymentTvCableModel paymentTvCableModel = (PaymentTvCableModel) cbo_voucher_name.getSelectedItem();

                        InquiryType = paymentTvCableModel.billerInquiry;


                    }if(menu_biller_code.equals(billElectricity)){
                        PaymentElectricityModel paymentElectricityModel = (PaymentElectricityModel) cbo_voucher_name.getSelectedItem();

                       InquiryType = paymentElectricityModel.billerInquiry;
                    }if(menu_biller_code.equals(billInstalment)){
                        PaymentInstallmentModel paymentInstallmentModel = (PaymentInstallmentModel) cbo_voucher_name.getSelectedItem();

                       InquiryType = paymentInstallmentModel.billerInquiry;
                    }if(menu_biller_code.equals(billInsurance)){
                        PaymentInsuranceModel paymentInsuranceModel= (PaymentInsuranceModel) cbo_voucher_name.getSelectedItem();

                       InquiryType = paymentInsuranceModel.billerInquiry;
                    }if(menu_biller_code.equals(billInternet)){
                        PaymentInternetModel paymentInternetModel = (PaymentInternetModel) cbo_voucher_name.getSelectedItem();

                       InquiryType = paymentInternetModel.billerInquiry;
                    }if(menu_biller_code.equals(billPhone)){
                        PaymentPhoneModel paymentPhoneModel = (PaymentPhoneModel) cbo_voucher_name.getSelectedItem();

                       InquiryType = paymentPhoneModel.billerInquiry;
                    }if(menu_biller_code.equals(billTicket)){
                        PaymentTicketModel paymentTicketModel= (PaymentTicketModel) cbo_voucher_name.getSelectedItem();

                       InquiryType = paymentTicketModel.billerInquiry;
                    }if(menu_biller_code.equals(billTax)){
                        PaymentTaxModel paymentTaxModel = (PaymentTaxModel) cbo_voucher_name.getSelectedItem();

                       InquiryType =paymentTaxModel.billerInquiry;
                    }if(menu_biller_code.equals(billCreditCard)){


                    }if(menu_biller_code.equals(billEducation)){


                    }if(menu_biller_code.equals(billWater)){
                        PaymentPamModel paymentPamModel = (PaymentPamModel) cbo_voucher_name.getSelectedItem();

                       InquiryType = paymentPamModel.billerInquiry;
                    }if(menu_biller_code.equals(billOther)){


                    }


                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    if (menu_biller_code.equals(billElectricity) && txtAccountNo.equalsIgnoreCase("")) {
                        alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                        alert.setMessage(getResources().getString(R.string.lbl_payment_orderid_validation_message));
                        alert.setPositiveButton("OK", null);
                        alert.show();
                        return;
                    }else if (menu_biller_code.equalsIgnoreCase(billCreditCard) && txtAccountNo.equalsIgnoreCase("")) {
                        alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                        alert.setMessage(getResources().getString(R.string.lbl_payment_orderid_creditcard_validation_message));
                        alert.setPositiveButton("OK", null);
                        alert.show();
                        return;
                    }else if (menu_biller_code.equals(billCreditCard) && txtAccountNo.equalsIgnoreCase("")) {
                        alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                        alert.setMessage(getResources().getString(R.string.lbl_payment_orderid_creditcard_validation_message));
                        alert.setPositiveButton("OK", null);
                        alert.show();
                        return;

                    }else if (menu_biller_code.equalsIgnoreCase(billEducation) && txtAccountNo.equalsIgnoreCase("")) {
                        alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                        alert.setMessage(getResources().getString(R.string.lbl_payment_orderid_education_validation_message));
                        alert.setPositiveButton("OK", null);
                        alert.show();
                        return;
                    }else if (menu_biller_code.equals(billPhone) && txtAccountNo.equalsIgnoreCase("")) {
                        alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                        alert.setMessage(getResources().getString(R.string.lbl_payment_orderid_phone_validation_message));
                        alert.setPositiveButton("OK", null);
                        alert.show();
                        return;
                    }else if (menu_biller_code.equalsIgnoreCase(billInstalment) && txtAccountNo.equalsIgnoreCase("")) {
                        alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                        alert.setMessage(getResources().getString(R.string.lbl_payment_orderid_validation_message));
                        alert.setPositiveButton("OK", null);
                        alert.show();
                        return;
                    }else if (menu_biller_code.equalsIgnoreCase(billInstalment) && InquiryType.equalsIgnoreCase("N") && txtAmount.equalsIgnoreCase("")) {
                        alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                        alert.setMessage(getResources().getString(R.string.lbl_payment_amount_alert));
                        alert.setPositiveButton("OK", null);
                        alert.show();
                        return;

                    }else if (menu_biller_code.equalsIgnoreCase(billInstalment)&& InquiryType.equalsIgnoreCase("N")  && txtAmount.equalsIgnoreCase("0")) {
                        alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                        alert.setMessage(getResources().getString(R.string.lbl_payment_amount_alert));
                        alert.setPositiveButton("OK", null);
                        alert.show();
                        return;
                    }else if (menu_biller_code.equalsIgnoreCase(billInsurance)  && txtAccountNo.equalsIgnoreCase("")) {
                        alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                        alert.setMessage(getResources().getString(R.string.lbl_payment_orderid_insurance_validation_message));
                        alert.setPositiveButton("OK", null);
                        alert.show();
                        return;
                    }else if (menu_biller_code.equalsIgnoreCase(billInsurance)&& InquiryType.equalsIgnoreCase("N") && txtAmount.equalsIgnoreCase("")) {
                        alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                        alert.setMessage(getResources().getString(R.string.lbl_payment_amount_alert));
                        alert.setPositiveButton("OK", null);
                        alert.show();
                        return;

                    }else if (menu_biller_code.equalsIgnoreCase(billInsurance) && InquiryType.equalsIgnoreCase("N") && txtAmount.equalsIgnoreCase("0")) {
                        alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                        alert.setMessage(getResources().getString(R.string.lbl_payment_amount_alert));
                        alert.setPositiveButton("OK", null);
                        alert.show();
                        return;
                    }else if (menu_biller_code.equalsIgnoreCase(billInternet) && txtAccountNo.equalsIgnoreCase("")) {
                        alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                        alert.setMessage(getResources().getString(R.string.lbl_payment_orderid_validation_message));
                        alert.setPositiveButton("OK", null);
                        alert.show();
                        return;
                    }else if (menu_biller_code.equalsIgnoreCase(billWater) && txtAccountNo.equalsIgnoreCase("")) {
                        alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                        alert.setMessage(getResources().getString(R.string.lbl_payment_orderid_validation_message));
                        alert.setPositiveButton("OK", null);
                        alert.show();
                        return;
                    }else if (menu_biller_code.equalsIgnoreCase(billTax) && txtAccountNo.equalsIgnoreCase("")) {
                        alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                        alert.setMessage(getResources().getString(R.string.lbl_payment_tax_alert));
                        alert.setPositiveButton("OK", null);
                        alert.show();
                        return;
                    }else if (menu_biller_code.equalsIgnoreCase(billTicket) && txtAccountNo.equalsIgnoreCase("")) {
                        alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                        alert.setMessage(getResources().getString(R.string.lbl_payment_orderid_ticket_validation_message));
                        alert.setPositiveButton("OK", null);
                        alert.show();
                        return;
                    }else if (menu_biller_code.equalsIgnoreCase(billTv) && txtAccountNo.equalsIgnoreCase("")) {
                        alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                        alert.setMessage(getResources().getString(R.string.lbl_payment_orderid_validation_message));
                        alert.setPositiveButton("OK", null);
                        alert.show();
                        return;

                    }else if (menu_biller_code.equalsIgnoreCase(billTv) && InquiryType.equalsIgnoreCase("N")  && txtAmount.equalsIgnoreCase("")) {
                        alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                        alert.setMessage(getResources().getString(R.string.lbl_payment_amount_alert));
                        alert.setPositiveButton("OK", null);
                        alert.show();
                        return;

                    }else if (menu_biller_code.equalsIgnoreCase(billTv) && InquiryType.equalsIgnoreCase("N") && txtAmount.equalsIgnoreCase("0")) {
                        alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                        alert.setMessage(getResources().getString(R.string.lbl_payment_amount_alert));
                        alert.setPositiveButton("OK", null);
                        alert.show();
                        return;
                    }else if (menu_biller_code.equalsIgnoreCase(billOther) && txtAccountNo.equalsIgnoreCase("")) {
                        alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                        alert.setMessage(getResources().getString(R.string.lbl_payment_orderid_validation_message));
                        alert.setPositiveButton("OK", null);
                        alert.show();
                        return;

                    }
                    else{

                        try {
                            networks = new Networks().isConnectingToInternet(getActivity());
                            if (networks) {
                                checkInquiry();
                            } else {
                                AlertDialog.Builder alerts = new AlertDialog.Builder(getActivity());
                                alerts.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                                alerts.setMessage(getResources().getString(R.string.lbl_alert_connection));
                                alerts.setPositiveButton("OK", null);
                                alerts.show();
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        btnBack               = (Button) view.findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Fragment newFragment = null;
                newFragment = new FragmentPayment();
                //  Bundle args = new Bundle();
                //  args.putString("menu_biller_code", menu_biller_code);
                // args.putString("menu_biller_name", menu_biller_code);
                //  newFragment.setArguments(args);
                switchFragment(newFragment);

            }
        });

        try {
            networks = new Networks().isConnectingToInternet(getActivity());
            if (networks) {
                initViews();
                checkMenuInquiry();
            } else {
                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                alert.setMessage(getResources().getString(R.string.lbl_alert_connection));
                alert.setPositiveButton("OK", null);
                alert.show();

            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return view;
    }


    private void checkInquiry(){
       String billerInquiry ="";
        if (menu_biller_code.equals(billTv)){
            PaymentTvCableModel paymentTvCableModel = (PaymentTvCableModel) cbo_voucher_name.getSelectedItem();

            billerInquiry = paymentTvCableModel.billerInquiry;


        }if(menu_biller_code.equals(billElectricity)){
            PaymentElectricityModel paymentElectricityModel = (PaymentElectricityModel) cbo_voucher_name.getSelectedItem();

            billerInquiry = paymentElectricityModel.billerInquiry;
        }if(menu_biller_code.equals(billInstalment)){
            PaymentInstallmentModel paymentInstallmentModel = (PaymentInstallmentModel) cbo_voucher_name.getSelectedItem();

            billerInquiry = paymentInstallmentModel.billerInquiry;
        }if(menu_biller_code.equals(billInsurance)){
            PaymentInsuranceModel paymentInsuranceModel= (PaymentInsuranceModel) cbo_voucher_name.getSelectedItem();

            billerInquiry = paymentInsuranceModel.billerInquiry;
        }if(menu_biller_code.equals(billInternet)){
            PaymentInternetModel paymentInternetModel = (PaymentInternetModel) cbo_voucher_name.getSelectedItem();

            billerInquiry = paymentInternetModel.billerInquiry;
        }if(menu_biller_code.equals(billPhone)){
            PaymentPhoneModel paymentPhoneModel = (PaymentPhoneModel) cbo_voucher_name.getSelectedItem();

            billerInquiry = paymentPhoneModel.billerInquiry;
        }if(menu_biller_code.equals(billTicket)){
            PaymentTicketModel paymentTicketModel= (PaymentTicketModel) cbo_voucher_name.getSelectedItem();

            billerInquiry = paymentTicketModel.billerInquiry;
        }if(menu_biller_code.equals(billTax)){
            PaymentTaxModel paymentTaxModel = (PaymentTaxModel) cbo_voucher_name.getSelectedItem();

            billerInquiry =paymentTaxModel.billerInquiry;
        }if(menu_biller_code.equals(billCreditCard)){


        }if(menu_biller_code.equals(billEducation)){


        }if(menu_biller_code.equals(billWater)){
            PaymentPamModel paymentPamModel = (PaymentPamModel) cbo_voucher_name.getSelectedItem();

            billerInquiry = paymentPamModel.billerInquiry;
        }if(menu_biller_code.equals(billOther)){


        }

        String y ="Y";
        String n="N";

        if(billerInquiry.equals(y)){

            checkingBiller();

        }else if(billerInquiry.equals(n)){
            getConfirm();

        }else{

        }
    }

    private void getConfirm(){
        String billerId = null, billerName = null, billerInquiry = null, productCode = null, productName = null;
        if (menu_biller_code.equals(billTv)){
            PaymentTvCableModel paymentTvCableModel = (PaymentTvCableModel) cbo_voucher_name.getSelectedItem();
            productCode = paymentTvCableModel.productCode;
            billerCode = paymentTvCableModel.billerCode;
            billerName = paymentTvCableModel.billerName;
            billerInquiry = paymentTvCableModel.billerInquiry;
            billerId = paymentTvCableModel.bilerId;
            productName = paymentTvCableModel.productName;

        }if(menu_biller_code.equals(billElectricity)){
            PaymentElectricityModel paymentElectricityModel = (PaymentElectricityModel) cbo_voucher_name.getSelectedItem();
            productCode = paymentElectricityModel.productCode;
            billerCode = paymentElectricityModel.billerCode;
            billerName = paymentElectricityModel.billerName;
            billerInquiry = paymentElectricityModel.billerInquiry;
            billerId = paymentElectricityModel.bilerId;
            productName =paymentElectricityModel.productName;
        }if(menu_biller_code.equals(billInstalment)){
            PaymentInstallmentModel paymentInstallmentModel = (PaymentInstallmentModel) cbo_voucher_name.getSelectedItem();
            productCode =paymentInstallmentModel.productCode;
            billerCode = paymentInstallmentModel.billerCode;
            billerName =paymentInstallmentModel.billerName;
            billerInquiry = paymentInstallmentModel.billerInquiry;
            billerId = paymentInstallmentModel.bilerId;
            productName =paymentInstallmentModel.productName;
        }if(menu_biller_code.equals(billInsurance)){
            PaymentInsuranceModel paymentInsuranceModel= (PaymentInsuranceModel) cbo_voucher_name.getSelectedItem();
            productCode = paymentInsuranceModel.productCode;
            billerCode =paymentInsuranceModel.billerCode;
            billerName = paymentInsuranceModel.billerName;
            billerInquiry = paymentInsuranceModel.billerInquiry;
            billerId =paymentInsuranceModel.bilerId;
            productName = paymentInsuranceModel.productName;
        }if(menu_biller_code.equals(billInternet)){
            PaymentInternetModel paymentInternetModel = (PaymentInternetModel) cbo_voucher_name.getSelectedItem();
            productCode = paymentInternetModel.productCode;
            billerCode = paymentInternetModel.billerCode;
            billerName = paymentInternetModel.billerName;
            billerInquiry = paymentInternetModel.billerInquiry;
            billerId = paymentInternetModel.bilerId;
            productName = paymentInternetModel.productName;
        }if(menu_biller_code.equals(billPhone)){
            PaymentPhoneModel paymentPhoneModel = (PaymentPhoneModel) cbo_voucher_name.getSelectedItem();
            productCode = paymentPhoneModel.productCode;
            billerCode = paymentPhoneModel.billerCode;
            billerName = paymentPhoneModel.billerName;
            billerInquiry = paymentPhoneModel.billerInquiry;
            billerId = paymentPhoneModel.bilerId;
            productName =paymentPhoneModel.productName;
        }if(menu_biller_code.equals(billTicket)){
            PaymentTicketModel paymentTicketModel= (PaymentTicketModel) cbo_voucher_name.getSelectedItem();
            productCode = paymentTicketModel.productCode;
            billerCode = paymentTicketModel.billerCode;
            billerName = paymentTicketModel.billerName;
            billerInquiry = paymentTicketModel.billerInquiry;
            billerId = paymentTicketModel.bilerId;
            productName = paymentTicketModel.productName;
        }if(menu_biller_code.equals(billTax)){
            PaymentTaxModel paymentTaxModel = (PaymentTaxModel) cbo_voucher_name.getSelectedItem();
            productCode = paymentTaxModel.productCode;
            billerCode = paymentTaxModel.billerCode;
            billerName = paymentTaxModel.billerName;
            billerInquiry =paymentTaxModel.billerInquiry;
            billerId = paymentTaxModel.bilerId;
            productName =paymentTaxModel.productName;
        }if(menu_biller_code.equals(billCreditCard)){


        }if(menu_biller_code.equals(billEducation)){


        }if(menu_biller_code.equals(billWater)){
            PaymentPamModel paymentPamModel = (PaymentPamModel) cbo_voucher_name.getSelectedItem();
            productCode = paymentPamModel.productCode;
            billerCode = paymentPamModel.billerCode;
            billerName = paymentPamModel.billerName;
            billerInquiry = paymentPamModel.billerInquiry;
            billerId = paymentPamModel.bilerId;
            productName = paymentPamModel.productName;
        }if(menu_biller_code.equals(billOther)){


        }

        SourceAccountModel sourceAccountBean = (SourceAccountModel) cbo_source_account.getSelectedItem();

        final String source_account_name = sourceAccountBean.account_name;
        final String source_account_no = sourceAccountBean.account_no;
        String amount  = inp_amount.getText().toString();
        String orderId= inp_member_no.getText().toString();


        Fragment newFragment = null;
        newFragment = new FragmentPaymentConfirm();
        Bundle args = new Bundle();
        args.putString("menu_biller_code", menu_biller_code);
        args.putString("menu_biller_name", menu_biller_name);
        args.putString("source_account_name", source_account_name);
        args.putString("source_account_no", source_account_no);
        args.putString("biller_id", billerId);
        args.putString("amount", amount);
        args.putString("order_id", orderId);
        args.putString("biller_code", billerCode);
        args.putString("biller_name", billerName);
        args.putString("pCode", productCode);
        args.putString("product_name", productName);
        args.putString("biller_inquiry", billerInquiry);
        newFragment.setArguments(args);
        switchFragment(newFragment);



    }



    private void checkMenuCode(){
        try {
            if (menu_biller_code.equals(billTax)){
                menu_title.setText(getResources().getString(R.string.lbl_payment_tax_title));
                label_member_no.setText(getResources().getString(R.string.lbl_payment_tax_nop));
                ic_menu_payment.setImageResource(R.drawable.ic_tax);
                inp_member_no.setHint(getResources().getString(R.string.lbl_payment_tax_nop_hint));
            }
            if (menu_biller_code.equals(billCreditCard)){
                menu_title.setText(getResources().getString(R.string.lbl_payment_creditcard_title));
                label_member_no.setText(getResources().getString(R.string.lbl_payment_creditcard_orderid));
                ic_menu_payment.setImageResource(R.drawable.ic_credit_card);
                inp_member_no.setHint(getResources().getString(R.string.lbl_payment_cc_hint));
            }
            if (menu_biller_code.equals(billInternet)){
                menu_title.setText(getResources().getString(R.string.lbl_payment_Internet_title));
                label_member_no.setText(getResources().getString(R.string.lbl_payment_orderid));
                ic_menu_payment.setImageResource(R.drawable.ic_internet_payment);
                inp_member_no.setHint(getResources().getString(R.string.lbl_payment_order_id_hint));
            }if (menu_biller_code.equals(billPhone)){
                menu_title.setText(getResources().getString(R.string.lbl_payment_phone_title));
                label_member_no.setText(getResources().getString(R.string.lbl_payment_phone_orderid));
                ic_menu_payment.setImageResource(R.drawable.ic_mobile);
                inp_member_no.setHint(getResources().getString(R.string.lbl_payment_phone_hint));
            }if (menu_biller_code.equals(billWater)){
                menu_title.setText(getResources().getString(R.string.lbl_payment_pam_title));
                label_member_no.setText(getResources().getString(R.string.lbl_payment_orderid));
                ic_menu_payment.setImageResource(R.drawable.ic_water);
                inp_member_no.setHint(getResources().getString(R.string.lbl_payment_order_id_hint));
            }if (menu_biller_code.equals(billElectricity)){
                menu_title.setText(getResources().getString(R.string.lbl_payment_electricity_title));
                label_member_no.setText(getResources().getString(R.string.lbl_payment_orderid));
                ic_menu_payment.setImageResource(R.drawable.ic_electricity);
                inp_member_no.setHint(getResources().getString(R.string.lbl_payment_order_id_hint));
            }if (menu_biller_code.equals(billEducation)){
                menu_title.setText(getResources().getString(R.string.lbl_payment_education_title));
                label_member_no.setText(getResources().getString(R.string.lbl_payment_education_orderid));
                ic_menu_payment.setImageResource(R.drawable.ic_education);
                inp_member_no.setHint(getResources().getString(R.string.lbl_payment_student_id_hint));
            }if (menu_biller_code.equals(billInstalment)){
                menu_title.setText(getResources().getString(R.string.lbl_payment_installment_title));
                label_member_no.setText(getResources().getString(R.string.lbl_payment_orderid));
                ic_menu_payment.setImageResource(R.drawable.ic_installment);
                inp_member_no.setHint(getResources().getString(R.string.lbl_payment_installment_hint));
            }if (menu_biller_code.equals(billInsurance)){
                menu_title.setText(getResources().getString(R.string.lbl_payment_insurance_title));
                label_member_no.setText(getResources().getString(R.string.lbl_payment_insurance_orderid));
                ic_menu_payment.setImageResource(R.drawable.ic_insurance);
                inp_member_no.setHint(getResources().getString(R.string.lbl_payment_insurance_hint));
            }if (menu_biller_code.equals(billTv)){
                menu_title.setText(getResources().getString(R.string.lbl_payment_tv_title));
                label_member_no.setText(getResources().getString(R.string.lbl_payment_orderid));
                ic_menu_payment.setImageResource(R.drawable.ic_tv_cable);
                inp_member_no.setHint(getResources().getString(R.string.lbl_payment_order_id_hint));
            }if (menu_biller_code.equals(billTicket)){
                menu_title.setText(getResources().getString(R.string.lbl_payment_ticket_title));
                label_member_no.setText(getResources().getString(R.string.lbl_payment_ticket_orderid));
                ic_menu_payment.setImageResource(R.drawable.ic_ticket);
                inp_member_no.setHint(getResources().getString(R.string.lbl_payment_ticket_hint));
            }if (menu_biller_code.equals(billOther)){
                menu_title.setText("Other Payment");
                label_member_no.setText("Customer ID");
                ic_menu_payment.setImageResource(R.drawable.ic_other);
                inp_member_no.setHint("* Input account no");
            }


        }catch (Exception e){
            e.printStackTrace();
        }

    }



    private void initViews() {
        SourceAcccountList.clear();
        dataSourceAccount = AppHelper.getSourceAccount(getSherlockActivity());
        if(dataSourceAccount.equals("")){
            SourceAcccountList.clear();
            pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.lbl_loading_data));
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getSherlockActivity()));
            client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
            RequestParams params = new RequestParams();

            try {
                String userid = AppHelper.getUserId(getActivity());
                String bankcode = AplConstants.Bankcode;
                String pcd = AplConstants.pcd;
                String URL = AplConstants.SourceAccountListAPI;
                String appName = AplConstants.appName;
                String serviceName = URL.split("/")[4];
                String apiKey = AppHelper.getAccessKey(getActivity());
                UUID uniqueKey = UUID.randomUUID();
                String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;


                Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
                SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
                sha256_HMAC.init(secret_key);
                byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
                String signatureKey = new String(encodeUrlSafe(hmacData));
                params.put("userid", userid);
                params.put("bankcode", bankcode);
                params.put("passcode", pcd);
                params.put("rc_uuid", uniqueKey.toString());
                params.put("rc_dtime", dateTime);
                params.put("signature", signatureKey);

            } catch (Exception e) {
                System.out.println("Error");
            }

            client.post(AplConstants.SourceAccountListAPI, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject object) {


                    try {

                        if (pDialog != null) {
                            pDialog.dismiss();
                        }
                        JSONObject body_data = object.getJSONObject("body_data");
                        // parse json data and store into arraylist variables
                        String error_code = body_data.getString("error_code");
                        String error_message = body_data.getString("error_message");


                        if (error_code.equalsIgnoreCase("0000")) {
                            JSONArray data = body_data.getJSONArray("source_account_data"); // this is the "items: [ ] part
                            int len = data.length();
                            account_no_arr = new String[len];
                            account_name_arr = new String[len];
                            ccy_id_arr = new String[len];
                            for (int i = 0; i < data.length(); i++) {
                                account_no_arr[i] = data.getJSONObject(i).getString("account_no");
                                account_name_arr[i] = data.getJSONObject(i).getString("account_name");
                                ccy_id_arr[i] = data.getJSONObject(i).getString("ccy_id");
                                SourceAcccountList.add(new SourceAccountModel(account_no_arr[i], account_name_arr[i], ccy_id_arr[i]));
                                SecurePreferences prefs = new SecurePreferences(getSherlockActivity());
                                SecurePreferences.Editor mEditor = prefs.edit();
                                mEditor.putString("sourceAccountData", data.toString());
                                mEditor.apply();
                            }
                            SourceAccountAdapters cAdapter = new SourceAccountAdapters(getActivity(), android.R.layout.simple_spinner_item, SourceAcccountList);
                            cbo_source_account.setAdapter(cAdapter);

                            cbo_source_account.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, final int position, long id) {
//                                    BillerVoucherList.clear();
//                                    AsyncHttpClient client = new AsyncHttpClient();
//                                    client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
//                                    client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
//                                    RequestParams params = new RequestParams();
//
//                                    try {
//
//                                        String userid = AppHelper.getUserId(getActivity());
//                                        String bankcode = AplConstants.Bankcode;
//                                        String pcd = AplConstants.pcd;
//                                        String URL = AplConstants.InquiryBiller;
//                                        String appName = AplConstants.appName;
//                                        String serviceName = URL.split("/")[4];
//                                        String apiKey = AppHelper.getAccessKey(getActivity());
//                                        UUID uniqueKey = UUID.randomUUID();
//                                        String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;
//                                        Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
//                                        SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
//                                        sha256_HMAC.init(secret_key);
//                                        byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
//                                        String signatureKey = new String(encodeUrlSafe(hmacData));
//                                        params.put("userid", userid);
//                                        params.put("bankcode", bankcode);
//                                        params.put("passcode", pcd);
//                                        params.put("categorycode", menu_biller_code);
//                                        params.put("rc_uuid", uniqueKey.toString());
//                                        params.put("rc_dtime", dateTime);
//                                        params.put("signature", signatureKey);
//
//                                    } catch (Exception e) {
//                                        System.out.println("Error");
//                                    }
//
//

//                                    client.post(AplConstants.InquiryBiller, params, new AsyncHttpResponseHandler() {
//                                        public void onSuccess(String content) {

//                                            try {
//
//                                                if (pDialog != null) {
//                                                    pDialog.dismiss();
//                                                }
//
//                                                JSONObject object = new JSONObject(content);
//                                                JSONObject body_data = object.getJSONObject("body_data");
//                                                String error_code = body_data.getString("error_code");
//                                                String error_message = body_data.getString("error_message");
//
//                                                if (error_code.equalsIgnoreCase("0000")) {
//                                                    JSONArray data = body_data.getJSONArray("biller_data");
//                                                    int len = data.length();
//                                                    biller_id_arr = new String[len];
//                                                    biller_code_arr = new String[len];
//                                                    product_code_arr = new String[len];
//                                                    biller_name_arr = new String[len];
//                                                    biller_inquiry_arr = new String[len];
//                                                    product_name_arr = new String[len];
//                                                    product_amount_arr = new String[len];
//                                                    for (int i = 0; i < data.length(); i++) {
//                                                        biller_id_arr[i] = data.getJSONObject(i).getString("billerId");
//                                                        biller_code_arr[i] = data.getJSONObject(i).getString("billerCode");
//                                                        biller_name_arr[i] = data.getJSONObject(i).getString("billerName");
//                                                        biller_inquiry_arr[i] = data.getJSONObject(i).getString("billerInquiry");
//                                                        product_code_arr[i] = data.getJSONObject(i).getString("productCode");
//                                                        product_name_arr[i] = data.getJSONObject(i).getString("productName");
//                                                        product_amount_arr[i] = data.getJSONObject(i).getString("productAmount");
//                                                        BillerVoucherList.add(new BillerVoucherCatalogBean(biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));
//                                                    }
//                                                    BillerVoucherCatalogAdapterPayment billerVoucherCatalogAdapterPayment = new BillerVoucherCatalogAdapterPayment(getActivity(), android.R.layout.simple_spinner_item, BillerVoucherList);
//
//
//                                                    cbo_voucher_name.setAdapter(billerVoucherCatalogAdapterPayment);
//
//
//                                                    cbo_voucher_name.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                                                        @Override
//                                                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                                                            BillerVoucherCatalogBean selectedVoucher = BillerVoucherList.get(position);
//                                                            String billerType = selectedVoucher.getBillerInquiry();
//                                                            if (billerType.equalsIgnoreCase("Y")) {
//                                                                tbl_lblamount.setVisibility(View.GONE);
//                                                                tbl_amount.setVisibility(View.GONE);
//                                                                amt_line.setVisibility(View.GONE);
//                                                            } else {
//                                                                tbl_lblamount.setVisibility(View.VISIBLE);
//                                                                tbl_amount.setVisibility(View.VISIBLE);
//                                                                amt_line.setVisibility(View.VISIBLE);
//                                                            }
//                                                        }
//
//                                                        @Override
//                                                        public void onNothingSelected(AdapterView<?> parent) {
//
//                                                        }
//                                                    });
//
//                                                } else if (error_code.equalsIgnoreCase("0404")) {
//
//                                                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
//                                                    alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
//                                                    alert.setMessage(error_message);
//                                                    alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                                        @Override
//                                                        public void onClick(DialogInterface dialog, int which) {
//                                                            Session session;
//                                                            session = new Session(getActivity());
//                                                            session.signOut();
//                                                        }
//                                                    });
//                                                    alert.show();
//
//                                                } else {
//                                                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
//                                                    alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
//                                                    alert.setMessage(getResources().getString(R.string.lbl_payment_title_alert) + error_message);
//                                                    alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                                        @Override
//                                                        public void onClick(DialogInterface dialog, int which) {
//                                                            Fragment newFragment = null;
//                                                            newFragment = new FragmentPaymentGeneral();
//                                                            switchFragment(newFragment);
//                                                        }
//                                                    });
//                                                    alert.show();
//                                                }
//
//
//                                            } catch (JSONException e) {
//                                                e.printStackTrace();
//                                            }
//                                        }
//
//                                        public void onFailure(Throwable error, String content) {
//                                            if (pDialog != null) {
//                                                pDialog.dismiss();
//                                            }
//                                            Toast.makeText(getActivity(), getResources().getString(R.string.lbl_alert_unexpected_connection), Toast.LENGTH_LONG).show();
//                                        }
//
//
//                                    });

                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });

                        } else if (error_code.equalsIgnoreCase("0404")) {

                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                            alert.setMessage(error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Session session;
                                    session = new Session(getActivity());
                                    session.signOut();
                                }
                            });
                            alert.show();

                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                            alert.setMessage(getResources().getString(R.string.lbl_payment_title_alert) + error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Fragment newFragment = null;
                                    newFragment = new FragmentPaymentGeneral();
                                    switchFragment(newFragment);
                                }
                            });
                            alert.show();
                        }

                        //get Voucher List


                        // End of get Voucher List


                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

                public void onFailure(Throwable error, String content) {
                    if (pDialog != null) {
                        pDialog.dismiss();
                    }

                    if (error.getMessage().equalsIgnoreCase("no peer certificate")) {
                        pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.SSLhandler_dialog_message) + error.toString());
                    } else {
                        pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.inethandler_dialog_message) + error.toString());
                    }
                    Toast.makeText(getActivity(), getResources().getString(R.string.lbl_alert_unexpected_connection), Toast.LENGTH_LONG).show();
                }

            });
        }else{
            SourceAcccountList.clear();
            try {
                JSONArray data = new JSONArray(dataSourceAccount);
                int len = data.length();
                account_no_arr = new String[len];
                account_name_arr = new String[len];
                ccy_id_arr = new String[len];
                for (int i = 0; i < data.length(); i++) {
                    account_no_arr[i] = data.getJSONObject(i).getString("account_no");
                    account_name_arr[i] = data.getJSONObject(i).getString("account_name");
                    ccy_id_arr[i] = data.getJSONObject(i).getString("ccy_id");
                    SourceAcccountList.add(new SourceAccountModel(account_no_arr[i], account_name_arr[i], ccy_id_arr[i]));
                }
                SourceAccountAdapters cAdapter = new SourceAccountAdapters(getActivity(), android.R.layout.simple_spinner_item, SourceAcccountList);
                cbo_source_account.setAdapter(cAdapter);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private void checkingBiller() {
        String productCode = null;

        if (menu_biller_code.equals(billTv)){
          PaymentTvCableModel paymentTvCableModel = (PaymentTvCableModel) cbo_voucher_name.getSelectedItem();
            productCode = paymentTvCableModel.productCode;
            billerCode = paymentTvCableModel.billerCode;
            billerName = paymentTvCableModel.billerName;
        }if(menu_biller_code.equals(billElectricity)){
            PaymentElectricityModel paymentElectricityModel = (PaymentElectricityModel) cbo_voucher_name.getSelectedItem();
            productCode = paymentElectricityModel.productCode;
            billerCode = paymentElectricityModel.billerCode;
            billerName = paymentElectricityModel.billerName;
        }if(menu_biller_code.equals(billInstalment)){
            PaymentInstallmentModel paymentInstallmentModel = (PaymentInstallmentModel) cbo_voucher_name.getSelectedItem();
            productCode =paymentInstallmentModel.productCode;
            billerCode = paymentInstallmentModel.billerCode;
            billerName =paymentInstallmentModel.billerName;
        }if(menu_biller_code.equals(billInsurance)){
            PaymentInsuranceModel paymentInsuranceModel= (PaymentInsuranceModel) cbo_voucher_name.getSelectedItem();
            productCode = paymentInsuranceModel.productCode;
            billerCode =paymentInsuranceModel.billerCode;
            billerName = paymentInsuranceModel.billerName;
        }if(menu_biller_code.equals(billInternet)){
           PaymentInternetModel paymentInternetModel = (PaymentInternetModel) cbo_voucher_name.getSelectedItem();
            productCode = paymentInternetModel.productCode;
            billerCode = paymentInternetModel.billerCode;
            billerName = paymentInternetModel.billerName;
        }if(menu_biller_code.equals(billPhone)){
            PaymentPhoneModel paymentPhoneModel = (PaymentPhoneModel) cbo_voucher_name.getSelectedItem();
            productCode = paymentPhoneModel.productCode;
            billerCode = paymentPhoneModel.billerCode;
            billerName = paymentPhoneModel.billerName;
        }if(menu_biller_code.equals(billTicket)){
            PaymentTicketModel paymentTicketModel= (PaymentTicketModel) cbo_voucher_name.getSelectedItem();
            productCode = paymentTicketModel.productCode;
            billerCode = paymentTicketModel.billerCode;
            billerName = paymentTicketModel.billerName;
        }if(menu_biller_code.equals(billTax)){
            PaymentTaxModel paymentTaxModel = (PaymentTaxModel) cbo_voucher_name.getSelectedItem();
            productCode = paymentTaxModel.productCode;
            billerCode = paymentTaxModel.billerCode;
            billerName = paymentTaxModel.billerName;
        }if(menu_biller_code.equals(billCreditCard)){


        }if(menu_biller_code.equals(billEducation)){


        }if(menu_biller_code.equals(billWater)){
           PaymentPamModel paymentPamModel = (PaymentPamModel) cbo_voucher_name.getSelectedItem();
            productCode = paymentPamModel.productCode;
            billerCode = paymentPamModel.billerCode;
            billerName = paymentPamModel.billerName;
        }if(menu_biller_code.equals(billOther)){


        }

        pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.lbl_loading_process));
        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getSherlockActivity()));
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
        final RequestParams params = new RequestParams();
//        final  BillerVoucherCatalogBean billerVoucherCatalogBean = (BillerVoucherCatalogBean) cbo_voucher_name.getSelectedItem();
//        final String billerInquiry = billerVoucherCatalogBean.billerInquiry;
//        final String pCode = billerVoucherCatalogBean.productCode;
//        billerCode = billerVoucherCatalogBean.billerCode;
//        billerName = billerVoucherCatalogBean.billerName;
        SourceAccountModel sourceAccountBean = (SourceAccountModel) cbo_source_account.getSelectedItem();
        source_account_name = sourceAccountBean.account_name;
        source_account_no = sourceAccountBean.account_no;

        try {

            String userid = AppHelper.getUserId(getActivity());
            String bankcode = AplConstants.Bankcode;
            String pcd = AplConstants.pcd;
            memberNo = inp_member_no.getText().toString();
            String URL = AplConstants.InquiryBillerPayment;
            String appName = AplConstants.appName;
            String serviceName = URL.split("/")[4];
            String apiKey = AppHelper.getAccessKey(getActivity());
            UUID uniqueKey = UUID.randomUUID();
            String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;


            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
            String signatureKey = new String(encodeUrlSafe(hmacData));
            params.put("userid", userid);
            params.put("bankcode", bankcode);
            params.put("passcode", pcd);
            params.put("productcode", productCode);
            params.put("sourceaccountno", source_account_no);
            params.put("orderid", memberNo);
            params.put("rc_uuid", uniqueKey.toString());
            params.put("rc_dtime", dateTime);
            params.put("signature", signatureKey);


        } catch (Exception e) {
            System.out.println("Error");
        }

        final String finalProductCode = productCode;

        client.post(AplConstants.InquiryBillerPayment, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject object) {


                try {

                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    JSONObject body_data = object.getJSONObject("body_data");
                    String error_code = body_data.getString("error_code");
                    String error_message = body_data.getString("error_message");


                    if (error_code.equalsIgnoreCase("0000")) {

                        JSONObject biller_data = body_data.getJSONObject("biller_data");
                        JSONObject data = biller_data.getJSONObject("dataUi");


                        JSONParser parser = new JSONParser();
                        ContainerFactory containerFactory = new ContainerFactory() {
                            public List creatArrayContainer() {
                                return new LinkedList();
                            }

                            public Map createObjectContainer() {
                                return new LinkedHashMap();
                            }

                        };

                        try {
                            json = (Map) parser.parse(String.valueOf(object), containerFactory);
                            dataUiList = (LinkedHashMap<String, String>) json.get("biller_data_ui");

                            Iterator iter = dataUiList.entrySet().iterator();
                            while (iter.hasNext()) {
                                Map.Entry entry = (Map.Entry) iter.next();
                            }
                        } catch (ParseException pe) {
                            System.out.println(pe);
                        }


                        String amount = biller_data.getString("amount");
                        String orderId = biller_data.getString("orderId");

                        Fragment newFragment = null;
                        newFragment = new FragmentPaymentConfirm_();
                        Bundle args = new Bundle();
                        args.putString("menu_biller_code", menu_biller_code);
                        args.putString("source_account_name", source_account_name);
                        args.putString("source_account_no", source_account_no);
                        args.putString("amount", amount);
                        args.putString("biller_code", billerCode);
                        args.putString("product_code", finalProductCode);
                        args.putString("biller_name", billerName);
                        args.putString("catalog_code", catalogCode);
                        args.putString("order_id", orderId);
                        args.putString("data", JSONValue.toJSONString(dataUiList));


                        newFragment.setArguments(args);
                        switchFragment(newFragment);
                    } else if (error_code.equalsIgnoreCase("14")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                        alert.setMessage(error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Fragment newFragment = null;
                                newFragment = new FragmentPaymentGeneral();
                                Bundle args = new Bundle();
                                args.putString("menu_biller_code", menu_biller_code);
                                newFragment.setArguments(args);
                                switchFragment(newFragment);
                            }
                        });
                        alert.show();
                    } else if (error_code.equalsIgnoreCase("")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                        alert.setMessage("Sorry, Data Not Available");
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Fragment newFragment = null;
                                newFragment = new FragmentPaymentGeneral();
                                Bundle args = new Bundle();
                                args.putString("menu_biller_code", menu_biller_code);
                                newFragment.setArguments(args);
                                switchFragment(newFragment);
                            }
                        });
                        alert.show();
                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                        alert.setMessage(error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Fragment newFragment = null;
                                newFragment = new FragmentPaymentGeneral();
                                Bundle args = new Bundle();
                                args.putString("menu_biller_code", menu_biller_code);
                                newFragment.setArguments(args);
                                switchFragment(newFragment);
                            }
                        });
                        alert.show();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (pDialog != null) {
                    pDialog.dismiss();
                }
                if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                } else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                }
            }

        });

    }

    private void checkMenuInquiry(){
        if (menu_biller_code.equals(billTv)){
            initpaymentTvCable();
        }if(menu_biller_code.equals(billElectricity)){
            initpaymentElectricity();
        }if(menu_biller_code.equals(billInstalment)){
            initpaymentInstallment();
        }if(menu_biller_code.equals(billInsurance)){
            initpaymentInsurance();
        }if(menu_biller_code.equals(billInternet)){
            initpaymentInternet();
        }if(menu_biller_code.equals(billPhone)){
            initpaymentPhone();
        }if(menu_biller_code.equals(billTicket)){
            initpaymentTicket();
        }if(menu_biller_code.equals(billTax)){
            initpaymentTax();
        }if(menu_biller_code.equals(billCreditCard)){

        }if(menu_biller_code.equals(billEducation)){

        }if(menu_biller_code.equals(billWater)){
            initpaymentPam();
        }if(menu_biller_code.equals(billOther)){

        }
    }


    private void initpaymentTvCable(){
        paymentTvCableModels.clear();
        if(getCountPaymentCableTv==0){

            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getSherlockActivity()));
            client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
            RequestParams params = new RequestParams();

            try {

                String userid = AppHelper.getUserId(getActivity());
                String bankcode = AplConstants.Bankcode;
                String pcd = AplConstants.pcd;
                String URL = AplConstants.InquiryBiller;
                String appName = AplConstants.appName;
                String serviceName = URL.split("/")[4];
                String apiKey = AppHelper.getAccessKey(getActivity());
                UUID uniqueKey = UUID.randomUUID();
                String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;
                Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
                SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
                sha256_HMAC.init(secret_key);
                byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
                String signatureKey = new String(encodeUrlSafe(hmacData));
                params.put("userid", userid);
                params.put("bankcode", bankcode);
                params.put("passcode", pcd);
                params.put("categorycode", menu_biller_code);
                params.put("rc_uuid", uniqueKey.toString());
                params.put("rc_dtime", dateTime);
                params.put("signature", signatureKey);

            } catch (Exception e) {
                System.out.println("Error");
            }
            client.post(AplConstants.InquiryBiller, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject object) {


                    try {

                        if (pDialog != null) {
                            pDialog.dismiss();
                        }
                        JSONObject body_data = object.getJSONObject("body_data");
                        String error_code = body_data.getString("error_code");
                        String error_message = body_data.getString("error_message");

                        if (error_code.equalsIgnoreCase("0000")) {
                            JSONArray data = body_data.getJSONArray("biller_data");
                            int len = data.length();
                            biller_id_arr = new String[len];
                            biller_code_arr = new String[len];
                            product_code_arr = new String[len];
                            biller_name_arr = new String[len];
                            biller_inquiry_arr = new String[len];
                            product_name_arr = new String[len];
                            product_amount_arr = new String[len];

                            for (int i = 0; i < data.length(); i++) {
                                biller_id_arr[i] = data.getJSONObject(i).getString("billerId");
                                biller_code_arr[i] = data.getJSONObject(i).getString("billerCode");
                                biller_name_arr[i] = data.getJSONObject(i).getString("billerName");
                                biller_inquiry_arr[i] = data.getJSONObject(i).getString("billerInquiry");
                                product_code_arr[i] = data.getJSONObject(i).getString("productCode");
                                product_name_arr[i] = data.getJSONObject(i).getString("productName");
                                product_amount_arr[i] = data.getJSONObject(i).getString("productAmount");

                                paymentTvCableModels.add(new PaymentTvCableModel(biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));

                                PaymentTvCableModel paymentTvCableModel = new PaymentTvCableModel();
                                paymentTvCableModel.bilerId = data.getJSONObject(i).getString("billerId");
                                paymentTvCableModel.billerCode = data.getJSONObject(i).getString("billerCode");
                                paymentTvCableModel.billerName = data.getJSONObject(i).getString("billerName");
                                paymentTvCableModel.billerInquiry = data.getJSONObject(i).getString("billerInquiry");
                                paymentTvCableModel.productCode = data.getJSONObject(i).getString("productCode");
                                paymentTvCableModel.productName = data.getJSONObject(i).getString("productName");
                                paymentTvCableModel.productAmount = data.getJSONObject(i).getString("productAmount");

                                paymentTvCableModel.save();
                            }
                            PaymentTvCableAdapter paymentTvCableAdapter = new PaymentTvCableAdapter(getActivity(), android.R.layout.simple_spinner_item, paymentTvCableModels);

                            cbo_voucher_name.setAdapter(paymentTvCableAdapter);

                            cbo_voucher_name.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    PaymentTvCableModel selectedVoucher = paymentTvCableModels.get(position);
                                    String billerType = selectedVoucher.getBillerInquiry();
                                    if (billerType.equalsIgnoreCase("Y")) {
                                        tbl_lblamount.setVisibility(View.GONE);
                                        tbl_amount.setVisibility(View.GONE);
                                        amt_line.setVisibility(View.GONE);
                                    } else {
                                        tbl_lblamount.setVisibility(View.VISIBLE);
                                        tbl_amount.setVisibility(View.VISIBLE);
                                        amt_line.setVisibility(View.VISIBLE);
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });

                        } else if (error_code.equalsIgnoreCase("0404")) {

                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                            alert.setMessage(error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Session session;
                                    session = new Session(getActivity());
                                    session.signOut();
                                }
                            });
                            alert.show();

                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                            alert.setMessage(getResources().getString(R.string.lbl_payment_title_alert) + error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Fragment newFragment = null;
                                    newFragment = new FragmentPaymentGeneral();
                                    switchFragment(newFragment);
                                }
                            });
                            alert.show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    }
                }


            });
        }else{
            paymentTvCableModels.clear();
            List<PaymentTvCableModel> itemList = new Select().all().from(PaymentTvCableModel.class).execute();

            int len = itemList.size();
            biller_id_arr = new String[len];
            biller_code_arr = new String[len];
            product_code_arr = new String[len];
            biller_name_arr = new String[len];
            biller_inquiry_arr = new String[len];
            product_name_arr = new String[len];
            product_amount_arr = new String[len];
            for (int i=0 ; i<itemList.size();i++) {

                biller_id_arr[i] = itemList.get(i).getBilerId();
                biller_code_arr[i] = itemList.get(i).getBillerCode();
                biller_name_arr[i] = itemList.get(i).getBillerName();
                biller_inquiry_arr[i] = itemList.get(i).getBillerInquiry();
                product_code_arr[i] = itemList.get(i).getProductCode();
                product_name_arr[i] = itemList.get(i).getProductName();
                product_amount_arr[i] = itemList.get(i).getProductAmount();

                paymentTvCableModels.add(new PaymentTvCableModel(biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));

            }
            PaymentTvCableAdapter paymentTvCableAdapter = new PaymentTvCableAdapter(getActivity(), android.R.layout.simple_spinner_item, paymentTvCableModels);
            cbo_voucher_name.setAdapter(paymentTvCableAdapter);

            cbo_voucher_name.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    PaymentTvCableModel selectedVoucher = paymentTvCableModels.get(position);
                    String billerType = selectedVoucher.getBillerInquiry();
                    if (billerType.equalsIgnoreCase("Y")) {
                        tbl_lblamount.setVisibility(View.GONE);
                        tbl_amount.setVisibility(View.GONE);
                        amt_line.setVisibility(View.GONE);
                    } else {
                        tbl_lblamount.setVisibility(View.VISIBLE);
                        tbl_amount.setVisibility(View.VISIBLE);
                        amt_line.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }



    }

    private void initpaymentElectricity(){
        if(getCountElectricity==0){
            paymentElectricityModels.clear();
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getSherlockActivity()));
            client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
            RequestParams params = new RequestParams();

            try {

                String userid = AppHelper.getUserId(getActivity());
                String bankcode = AplConstants.Bankcode;
                String pcd = AplConstants.pcd;
                String URL = AplConstants.InquiryBiller;
                String appName = AplConstants.appName;
                String serviceName = URL.split("/")[4];
                String apiKey = AppHelper.getAccessKey(getActivity());
                UUID uniqueKey = UUID.randomUUID();
                String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;
                Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
                SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
                sha256_HMAC.init(secret_key);
                byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
                String signatureKey = new String(encodeUrlSafe(hmacData));
                params.put("userid", userid);
                params.put("bankcode", bankcode);
                params.put("passcode", pcd);
                params.put("categorycode", menu_biller_code);
                params.put("rc_uuid", uniqueKey.toString());
                params.put("rc_dtime", dateTime);
                params.put("signature", signatureKey);

            } catch (Exception e) {
                System.out.println("Error");
            }


            client.post(AplConstants.InquiryBiller, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject object) {


                    try {

                        if (pDialog != null) {
                            pDialog.dismiss();
                        }
                        JSONObject body_data = object.getJSONObject("body_data");
                        String error_code = body_data.getString("error_code");
                        String error_message = body_data.getString("error_message");

                        if (error_code.equalsIgnoreCase("0000")) {
                            JSONArray data = body_data.getJSONArray("biller_data");
                            int len = data.length();
                            biller_id_arr = new String[len];
                            biller_code_arr = new String[len];
                            product_code_arr = new String[len];
                            biller_name_arr = new String[len];
                            biller_inquiry_arr = new String[len];
                            product_name_arr = new String[len];
                            product_amount_arr = new String[len];

                            for (int i = 0; i < data.length(); i++) {
                                biller_id_arr[i] = data.getJSONObject(i).getString("billerId");
                                biller_code_arr[i] = data.getJSONObject(i).getString("billerCode");
                                biller_name_arr[i] = data.getJSONObject(i).getString("billerName");
                                biller_inquiry_arr[i] = data.getJSONObject(i).getString("billerInquiry");
                                product_code_arr[i] = data.getJSONObject(i).getString("productCode");
                                product_name_arr[i] = data.getJSONObject(i).getString("productName");
                                product_amount_arr[i] = data.getJSONObject(i).getString("productAmount");

                                paymentElectricityModels.add(new PaymentElectricityModel(biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));

                                PaymentElectricityModel inquiryBillerListModel = new PaymentElectricityModel();
                                inquiryBillerListModel.bilerId = data.getJSONObject(i).getString("billerId");
                                inquiryBillerListModel.billerCode = data.getJSONObject(i).getString("billerCode");
                                inquiryBillerListModel.billerName = data.getJSONObject(i).getString("billerName");
                                inquiryBillerListModel.billerInquiry = data.getJSONObject(i).getString("billerInquiry");
                                inquiryBillerListModel.productCode = data.getJSONObject(i).getString("productCode");
                                inquiryBillerListModel.productName = data.getJSONObject(i).getString("productName");
                                inquiryBillerListModel.productAmount = data.getJSONObject(i).getString("productAmount");
                                inquiryBillerListModel.save();
                            }
                            PaymentElectricityAdapter paymentElectricityAdapter = new PaymentElectricityAdapter(getActivity(), android.R.layout.simple_spinner_item, paymentElectricityModels);

                            cbo_voucher_name.setAdapter(paymentElectricityAdapter);

                            cbo_voucher_name.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    PaymentElectricityModel selectedVoucher = paymentElectricityModels.get(position);
                                    String billerType = selectedVoucher.getBillerInquiry();
                                    if (billerType.equalsIgnoreCase("Y")) {
                                        tbl_lblamount.setVisibility(View.GONE);
                                        tbl_amount.setVisibility(View.GONE);
                                        amt_line.setVisibility(View.GONE);
                                    } else {
                                        tbl_lblamount.setVisibility(View.VISIBLE);
                                        tbl_amount.setVisibility(View.VISIBLE);
                                        amt_line.setVisibility(View.VISIBLE);
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });

                        } else if (error_code.equalsIgnoreCase("0404")) {

                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                            alert.setMessage(error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Session session;
                                    session = new Session(getActivity());
                                    session.signOut();
                                }
                            });
                            alert.show();

                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                            alert.setMessage(getResources().getString(R.string.lbl_payment_title_alert) + error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Fragment newFragment = null;
                                    newFragment = new FragmentPaymentGeneral();
                                    switchFragment(newFragment);
                                }
                            });
                            alert.show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    }
                }


            });
        }else{
            paymentElectricityModels.clear();
            List<PaymentElectricityModel> itemList = new Select().all().from(PaymentElectricityModel.class).execute();

            int len = itemList.size();
            biller_id_arr = new String[len];
            biller_code_arr = new String[len];
            product_code_arr = new String[len];
            biller_name_arr = new String[len];
            biller_inquiry_arr = new String[len];
            product_name_arr = new String[len];
            product_amount_arr = new String[len];
            for (int i=0 ; i<itemList.size();i++) {

                biller_id_arr[i] = itemList.get(i).getBilerId();
                biller_code_arr[i] = itemList.get(i).getBillerCode();
                biller_name_arr[i] = itemList.get(i).getBillerName();
                biller_inquiry_arr[i] = itemList.get(i).getBillerInquiry();
                product_code_arr[i] = itemList.get(i).getProductCode();
                product_name_arr[i] = itemList.get(i).getProductName();
                product_amount_arr[i] = itemList.get(i).getProductAmount();

                paymentElectricityModels.add(new PaymentElectricityModel(biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));

            }
            PaymentElectricityAdapter paymentElectricityAdapter = new PaymentElectricityAdapter(getActivity(), android.R.layout.simple_spinner_item, paymentElectricityModels);
            cbo_voucher_name.setAdapter(paymentElectricityAdapter);

            cbo_voucher_name.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    PaymentElectricityModel selectedVoucher = paymentElectricityModels.get(position);
                    String billerType = selectedVoucher.getBillerInquiry();
                    if (billerType.equalsIgnoreCase("Y")) {
                        tbl_lblamount.setVisibility(View.GONE);
                        tbl_amount.setVisibility(View.GONE);
                        amt_line.setVisibility(View.GONE);
                    } else {
                        tbl_lblamount.setVisibility(View.VISIBLE);
                        tbl_amount.setVisibility(View.VISIBLE);
                        amt_line.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }


    }

    private void initpaymentInstallment(){
        if(getCountInstallment==0){
            paymentInstallmentModels.clear();
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getSherlockActivity()));
            client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
            RequestParams params = new RequestParams();

            try {

                String userid = AppHelper.getUserId(getActivity());
                String bankcode = AplConstants.Bankcode;
                String pcd = AplConstants.pcd;
                String URL = AplConstants.InquiryBiller;
                String appName = AplConstants.appName;
                String serviceName = URL.split("/")[4];
                String apiKey = AppHelper.getAccessKey(getActivity());
                UUID uniqueKey = UUID.randomUUID();
                String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;
                Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
                SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
                sha256_HMAC.init(secret_key);
                byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
                String signatureKey = new String(encodeUrlSafe(hmacData));
                params.put("userid", userid);
                params.put("bankcode", bankcode);
                params.put("passcode", pcd);
                params.put("categorycode", menu_biller_code);
                params.put("rc_uuid", uniqueKey.toString());
                params.put("rc_dtime", dateTime);
                params.put("signature", signatureKey);

            } catch (Exception e) {
                System.out.println("Error");
            }

            client.post(AplConstants.InquiryBiller, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject object) {


                    try {

                        if (pDialog != null) {
                            pDialog.dismiss();
                        }
                        JSONObject body_data = object.getJSONObject("body_data");
                        String error_code = body_data.getString("error_code");
                        String error_message = body_data.getString("error_message");

                        if (error_code.equalsIgnoreCase("0000")) {
                            JSONArray data = body_data.getJSONArray("biller_data");
                            int len = data.length();
                            biller_id_arr = new String[len];
                            biller_code_arr = new String[len];
                            product_code_arr = new String[len];
                            biller_name_arr = new String[len];
                            biller_inquiry_arr = new String[len];
                            product_name_arr = new String[len];
                            product_amount_arr = new String[len];

                            for (int i = 0; i < data.length(); i++) {
                                biller_id_arr[i] = data.getJSONObject(i).getString("billerId");
                                biller_code_arr[i] = data.getJSONObject(i).getString("billerCode");
                                biller_name_arr[i] = data.getJSONObject(i).getString("billerName");
                                biller_inquiry_arr[i] = data.getJSONObject(i).getString("billerInquiry");
                                product_code_arr[i] = data.getJSONObject(i).getString("productCode");
                                product_name_arr[i] = data.getJSONObject(i).getString("productName");
                                product_amount_arr[i] = data.getJSONObject(i).getString("productAmount");

                                paymentInstallmentModels.add(new PaymentInstallmentModel(biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));

                                PaymentInstallmentModel inquiryBillerListModel = new PaymentInstallmentModel();
                                inquiryBillerListModel.bilerId = data.getJSONObject(i).getString("billerId");
                                inquiryBillerListModel.billerCode = data.getJSONObject(i).getString("billerCode");
                                inquiryBillerListModel.billerName = data.getJSONObject(i).getString("billerName");
                                inquiryBillerListModel.billerInquiry = data.getJSONObject(i).getString("billerInquiry");
                                inquiryBillerListModel.productCode = data.getJSONObject(i).getString("productCode");
                                inquiryBillerListModel.productName = data.getJSONObject(i).getString("productName");
                                inquiryBillerListModel.productAmount = data.getJSONObject(i).getString("productAmount");
                                inquiryBillerListModel.save();
                            }
                            PaymentInstallmentAdapter paymentInstallmentAdapter = new PaymentInstallmentAdapter(getActivity(), android.R.layout.simple_spinner_item, paymentInstallmentModels);

                            cbo_voucher_name.setAdapter(paymentInstallmentAdapter);

                            cbo_voucher_name.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    PaymentInstallmentModel selectedVoucher = paymentInstallmentModels.get(position);
                                    String billerType = selectedVoucher.getBillerInquiry();
                                    if (billerType.equalsIgnoreCase("Y")) {
                                        tbl_lblamount.setVisibility(View.GONE);
                                        tbl_amount.setVisibility(View.GONE);
                                        amt_line.setVisibility(View.GONE);
                                    } else {
                                        tbl_lblamount.setVisibility(View.VISIBLE);
                                        tbl_amount.setVisibility(View.VISIBLE);
                                        amt_line.setVisibility(View.VISIBLE);
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });

                        } else if (error_code.equalsIgnoreCase("0404")) {

                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                            alert.setMessage(error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Session session;
                                    session = new Session(getActivity());
                                    session.signOut();
                                }
                            });
                            alert.show();

                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                            alert.setMessage(getResources().getString(R.string.lbl_payment_title_alert) + error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Fragment newFragment = null;
                                    newFragment = new FragmentPaymentGeneral();
                                    switchFragment(newFragment);
                                }
                            });
                            alert.show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    }
                }


            });
        }else{
            paymentInstallmentModels.clear();
            List<PaymentInstallmentModel> itemList = new Select().all().from(PaymentInstallmentModel.class).execute();

            int len = itemList.size();
            biller_id_arr = new String[len];
            biller_code_arr = new String[len];
            product_code_arr = new String[len];
            biller_name_arr = new String[len];
            biller_inquiry_arr = new String[len];
            product_name_arr = new String[len];
            product_amount_arr = new String[len];
            for (int i=0 ; i<itemList.size();i++) {

                biller_id_arr[i] = itemList.get(i).getBilerId();
                biller_code_arr[i] = itemList.get(i).getBillerCode();
                biller_name_arr[i] = itemList.get(i).getBillerName();
                biller_inquiry_arr[i] = itemList.get(i).getBillerInquiry();
                product_code_arr[i] = itemList.get(i).getProductCode();
                product_name_arr[i] = itemList.get(i).getProductName();
                product_amount_arr[i] = itemList.get(i).getProductAmount();

                paymentInstallmentModels.add(new PaymentInstallmentModel(biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));

            }
            PaymentInstallmentAdapter paymentInstallmentAdapter = new PaymentInstallmentAdapter(getActivity(), android.R.layout.simple_spinner_item, paymentInstallmentModels);
            cbo_voucher_name.setAdapter(paymentInstallmentAdapter);

            cbo_voucher_name.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    PaymentInstallmentModel selectedVoucher = paymentInstallmentModels.get(position);
                    String billerType = selectedVoucher.getBillerInquiry();
                    if (billerType.equalsIgnoreCase("Y")) {
                        tbl_lblamount.setVisibility(View.GONE);
                        tbl_amount.setVisibility(View.GONE);
                        amt_line.setVisibility(View.GONE);
                    } else {
                        tbl_lblamount.setVisibility(View.VISIBLE);
                        tbl_amount.setVisibility(View.VISIBLE);
                        amt_line.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }

    }

    private void initpaymentInsurance(){
        if(getCountInsurance==0){
            paymentInsuranceModels.clear();
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getSherlockActivity()));
            client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);

            RequestParams params = new RequestParams();

            try {

                String userid = AppHelper.getUserId(getActivity());
                String bankcode = AplConstants.Bankcode;
                String pcd = AplConstants.pcd;
                String URL = AplConstants.InquiryBiller;
                String appName = AplConstants.appName;
                String serviceName = URL.split("/")[4];
                String apiKey = AppHelper.getAccessKey(getActivity());
                UUID uniqueKey = UUID.randomUUID();
                String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;
                Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
                SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
                sha256_HMAC.init(secret_key);
                byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
                String signatureKey = new String(encodeUrlSafe(hmacData));
                params.put("userid", userid);
                params.put("bankcode", bankcode);
                params.put("passcode", pcd);
                params.put("categorycode", menu_biller_code);
                params.put("rc_uuid", uniqueKey.toString());
                params.put("rc_dtime", dateTime);
                params.put("signature", signatureKey);

            } catch (Exception e) {
                System.out.println("Error");
            }
            client.post(AplConstants.InquiryBiller, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject object) {


                    try {

                        if (pDialog != null) {
                            pDialog.dismiss();
                        }
                        JSONObject body_data = object.getJSONObject("body_data");
                        String error_code = body_data.getString("error_code");
                        String error_message = body_data.getString("error_message");

                        if (error_code.equalsIgnoreCase("0000")) {
                            JSONArray data = body_data.getJSONArray("biller_data");
                            int len = data.length();
                            biller_id_arr = new String[len];
                            biller_code_arr = new String[len];
                            product_code_arr = new String[len];
                            biller_name_arr = new String[len];
                            biller_inquiry_arr = new String[len];
                            product_name_arr = new String[len];
                            product_amount_arr = new String[len];

                            for (int i = 0; i < data.length(); i++) {
                                biller_id_arr[i] = data.getJSONObject(i).getString("billerId");
                                biller_code_arr[i] = data.getJSONObject(i).getString("billerCode");
                                biller_name_arr[i] = data.getJSONObject(i).getString("billerName");
                                biller_inquiry_arr[i] = data.getJSONObject(i).getString("billerInquiry");
                                product_code_arr[i] = data.getJSONObject(i).getString("productCode");
                                product_name_arr[i] = data.getJSONObject(i).getString("productName");
                                product_amount_arr[i] = data.getJSONObject(i).getString("productAmount");

                                paymentInsuranceModels.add(new PaymentInsuranceModel(biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));

                                PaymentInsuranceModel inquiryBillerListModel = new PaymentInsuranceModel();
                                inquiryBillerListModel.bilerId = data.getJSONObject(i).getString("billerId");
                                inquiryBillerListModel.billerCode = data.getJSONObject(i).getString("billerCode");
                                inquiryBillerListModel.billerName = data.getJSONObject(i).getString("billerName");
                                inquiryBillerListModel.billerInquiry = data.getJSONObject(i).getString("billerInquiry");
                                inquiryBillerListModel.productCode = data.getJSONObject(i).getString("productCode");
                                inquiryBillerListModel.productName = data.getJSONObject(i).getString("productName");
                                inquiryBillerListModel.productAmount = data.getJSONObject(i).getString("productAmount");
                                inquiryBillerListModel.save();
                            }
                            PaymentInsuranceAdapter paymentInsuranceAdapter = new PaymentInsuranceAdapter(getActivity(), android.R.layout.simple_spinner_item, paymentInsuranceModels);

                            cbo_voucher_name.setAdapter(paymentInsuranceAdapter);

                            cbo_voucher_name.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    PaymentInsuranceModel selectedVoucher = paymentInsuranceModels.get(position);
                                    String billerType = selectedVoucher.getBillerInquiry();
                                    if (billerType.equalsIgnoreCase("Y")) {
                                        tbl_lblamount.setVisibility(View.GONE);
                                        tbl_amount.setVisibility(View.GONE);
                                        amt_line.setVisibility(View.GONE);
                                    } else {
                                        tbl_lblamount.setVisibility(View.VISIBLE);
                                        tbl_amount.setVisibility(View.VISIBLE);
                                        amt_line.setVisibility(View.VISIBLE);
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });

                        } else if (error_code.equalsIgnoreCase("0404")) {

                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                            alert.setMessage(error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Session session;
                                    session = new Session(getActivity());
                                    session.signOut();
                                }
                            });
                            alert.show();

                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                            alert.setMessage(getResources().getString(R.string.lbl_payment_title_alert) + error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Fragment newFragment = null;
                                    newFragment = new FragmentPaymentGeneral();
                                    switchFragment(newFragment);
                                }
                            });
                            alert.show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    }
                }


            });
        }else{
            paymentInsuranceModels.clear();
            List<PaymentInsuranceModel> itemList = new Select().all().from(PaymentInsuranceModel.class).execute();

            int len = itemList.size();
            biller_id_arr = new String[len];
            biller_code_arr = new String[len];
            product_code_arr = new String[len];
            biller_name_arr = new String[len];
            biller_inquiry_arr = new String[len];
            product_name_arr = new String[len];
            product_amount_arr = new String[len];
            for (int i=0 ; i<itemList.size();i++) {

                biller_id_arr[i] = itemList.get(i).getBilerId();
                biller_code_arr[i] = itemList.get(i).getBillerCode();
                biller_name_arr[i] = itemList.get(i).getBillerName();
                biller_inquiry_arr[i] = itemList.get(i).getBillerInquiry();
                product_code_arr[i] = itemList.get(i).getProductCode();
                product_name_arr[i] = itemList.get(i).getProductName();
                product_amount_arr[i] = itemList.get(i).getProductAmount();

                paymentInsuranceModels.add(new PaymentInsuranceModel(biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));

            }
            PaymentInsuranceAdapter paymentInsuranceAdapter = new PaymentInsuranceAdapter(getActivity(), android.R.layout.simple_spinner_item, paymentInsuranceModels);
            cbo_voucher_name.setAdapter(paymentInsuranceAdapter);

            cbo_voucher_name.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    PaymentInsuranceModel selectedVoucher = paymentInsuranceModels.get(position);
                    String billerType = selectedVoucher.getBillerInquiry();
                    if (billerType.equalsIgnoreCase("Y")) {
                        tbl_lblamount.setVisibility(View.GONE);
                        tbl_amount.setVisibility(View.GONE);
                        amt_line.setVisibility(View.GONE);
                    } else {
                        tbl_lblamount.setVisibility(View.VISIBLE);
                        tbl_amount.setVisibility(View.VISIBLE);
                        amt_line.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    private void initpaymentInternet(){
        if(getCountInternet==0){
            paymentInternetModels.clear();
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getSherlockActivity()));
            client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
            RequestParams params = new RequestParams();

            try {

                String userid = AppHelper.getUserId(getActivity());
                String bankcode = AplConstants.Bankcode;
                String pcd = AplConstants.pcd;
                String URL = AplConstants.InquiryBiller;
                String appName = AplConstants.appName;
                String serviceName = URL.split("/")[4];
                String apiKey = AppHelper.getAccessKey(getActivity());
                UUID uniqueKey = UUID.randomUUID();
                String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;
                Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
                SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
                sha256_HMAC.init(secret_key);
                byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
                String signatureKey = new String(encodeUrlSafe(hmacData));
                params.put("userid", userid);
                params.put("bankcode", bankcode);
                params.put("passcode", pcd);
                params.put("categorycode", menu_biller_code);
                params.put("rc_uuid", uniqueKey.toString());
                params.put("rc_dtime", dateTime);
                params.put("signature", signatureKey);

            } catch (Exception e) {
                System.out.println("Error");
            }


            client.post(AplConstants.InquiryBiller, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject object) {


                    try {

                        if (pDialog != null) {
                            pDialog.dismiss();
                        }
                        JSONObject body_data = object.getJSONObject("body_data");
                        String error_code = body_data.getString("error_code");
                        String error_message = body_data.getString("error_message");

                        if (error_code.equalsIgnoreCase("0000")) {
                            JSONArray data = body_data.getJSONArray("biller_data");
                            int len = data.length();
                            biller_id_arr = new String[len];
                            biller_code_arr = new String[len];
                            product_code_arr = new String[len];
                            biller_name_arr = new String[len];
                            biller_inquiry_arr = new String[len];
                            product_name_arr = new String[len];
                            product_amount_arr = new String[len];

                            for (int i = 0; i < data.length(); i++) {
                                biller_id_arr[i] = data.getJSONObject(i).getString("billerId");
                                biller_code_arr[i] = data.getJSONObject(i).getString("billerCode");
                                biller_name_arr[i] = data.getJSONObject(i).getString("billerName");
                                biller_inquiry_arr[i] = data.getJSONObject(i).getString("billerInquiry");
                                product_code_arr[i] = data.getJSONObject(i).getString("productCode");
                                product_name_arr[i] = data.getJSONObject(i).getString("productName");
                                product_amount_arr[i] = data.getJSONObject(i).getString("productAmount");

                                paymentInternetModels.add(new PaymentInternetModel(biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));

                                PaymentInternetModel paymentInternetModel = new PaymentInternetModel();
                                paymentInternetModel.bilerId = data.getJSONObject(i).getString("billerId");
                                paymentInternetModel.billerCode = data.getJSONObject(i).getString("billerCode");
                                paymentInternetModel.billerName = data.getJSONObject(i).getString("billerName");
                                paymentInternetModel.billerInquiry = data.getJSONObject(i).getString("billerInquiry");
                                paymentInternetModel.productCode = data.getJSONObject(i).getString("productCode");
                                paymentInternetModel.productName = data.getJSONObject(i).getString("productName");
                                paymentInternetModel.productAmount = data.getJSONObject(i).getString("productAmount");
                                paymentInternetModel.save();
                            }
                            PaymentInternetAdapter paymentInternetAdapter = new PaymentInternetAdapter(getActivity(), android.R.layout.simple_spinner_item, paymentInternetModels);

                            cbo_voucher_name.setAdapter(paymentInternetAdapter);

                            cbo_voucher_name.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    PaymentInternetModel selectedVoucher = paymentInternetModels.get(position);
                                    String billerType = selectedVoucher.getBillerInquiry();
                                    if (billerType.equalsIgnoreCase("Y")) {
                                        tbl_lblamount.setVisibility(View.GONE);
                                        tbl_amount.setVisibility(View.GONE);
                                        amt_line.setVisibility(View.GONE);
                                    } else {
                                        tbl_lblamount.setVisibility(View.VISIBLE);
                                        tbl_amount.setVisibility(View.VISIBLE);
                                        amt_line.setVisibility(View.VISIBLE);
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });

                        } else if (error_code.equalsIgnoreCase("0404")) {

                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                            alert.setMessage(error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Session session;
                                    session = new Session(getActivity());
                                    session.signOut();
                                }
                            });
                            alert.show();

                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                            alert.setMessage(getResources().getString(R.string.lbl_payment_title_alert) + error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Fragment newFragment = null;
                                    newFragment = new FragmentPaymentGeneral();
                                    switchFragment(newFragment);
                                }
                            });
                            alert.show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    }
                }


            });
        }else{
            paymentInternetModels.clear();
            List<PaymentInternetModel> itemList = new Select().all().from(PaymentInternetModel.class).execute();

            int len = itemList.size();
            biller_id_arr = new String[len];
            biller_code_arr = new String[len];
            biller_name_arr = new String[len];
            biller_inquiry_arr = new String[len];
            product_code_arr = new String[len];
            product_name_arr = new String[len];
            product_amount_arr = new String[len];
            for (int i=0 ; i<itemList.size();i++) {

                biller_id_arr[i] = itemList.get(i).getBilerId();
                biller_code_arr[i] = itemList.get(i).getBillerCode();
                biller_name_arr[i] = itemList.get(i).getBillerName();
                biller_inquiry_arr[i] = itemList.get(i).getBillerInquiry();
                product_code_arr[i] = itemList.get(i).getProductCode();
                product_name_arr[i] = itemList.get(i).getProductName();
                product_amount_arr[i] = itemList.get(i).getProductAmount();

                paymentInternetModels.add(new PaymentInternetModel(biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));

            }
            PaymentInternetAdapter paymentInternetAdapter= new PaymentInternetAdapter(getActivity(), android.R.layout.simple_spinner_item, paymentInternetModels);
            cbo_voucher_name.setAdapter(paymentInternetAdapter);

            cbo_voucher_name.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    PaymentInternetModel selectedVoucher = paymentInternetModels.get(position);
                    String billerType = selectedVoucher.getBillerInquiry();
                    if (billerType.equalsIgnoreCase("Y")) {
                        tbl_lblamount.setVisibility(View.GONE);
                        tbl_amount.setVisibility(View.GONE);
                        amt_line.setVisibility(View.GONE);
                    } else {
                        tbl_lblamount.setVisibility(View.VISIBLE);
                        tbl_amount.setVisibility(View.VISIBLE);
                        amt_line.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    private void initpaymentPam(){
        if(getCountPam==0){
            paymentPamModels.clear();
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getSherlockActivity()));
            client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
            RequestParams params = new RequestParams();

            try {

                String userid = AppHelper.getUserId(getActivity());
                String bankcode = AplConstants.Bankcode;
                String pcd = AplConstants.pcd;
                String URL = AplConstants.InquiryBiller;
                String appName = AplConstants.appName;
                String serviceName = URL.split("/")[4];
                String apiKey = AppHelper.getAccessKey(getActivity());
                UUID uniqueKey = UUID.randomUUID();
                String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;
                Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
                SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
                sha256_HMAC.init(secret_key);
                byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
                String signatureKey = new String(encodeUrlSafe(hmacData));
                params.put("userid", userid);
                params.put("bankcode", bankcode);
                params.put("passcode", pcd);
                params.put("categorycode", menu_biller_code);
                params.put("rc_uuid", uniqueKey.toString());
                params.put("rc_dtime", dateTime);
                params.put("signature", signatureKey);

            } catch (Exception e) {
                System.out.println("Error");
            }

            client.post(AplConstants.InquiryBiller, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject object) {


                    try {

                        if (pDialog != null) {
                            pDialog.dismiss();
                        }
                        JSONObject body_data = object.getJSONObject("body_data");
                        String error_code = body_data.getString("error_code");
                        String error_message = body_data.getString("error_message");

                        if (error_code.equalsIgnoreCase("0000")) {
                            JSONArray data = body_data.getJSONArray("biller_data");
                            int len = data.length();
                            biller_id_arr = new String[len];
                            biller_code_arr = new String[len];
                            product_code_arr = new String[len];
                            biller_name_arr = new String[len];
                            biller_inquiry_arr = new String[len];
                            product_name_arr = new String[len];
                            product_amount_arr = new String[len];

                            for (int i = 0; i < data.length(); i++) {
                                biller_id_arr[i] = data.getJSONObject(i).getString("billerId");
                                biller_code_arr[i] = data.getJSONObject(i).getString("billerCode");
                                biller_name_arr[i] = data.getJSONObject(i).getString("billerName");
                                biller_inquiry_arr[i] = data.getJSONObject(i).getString("billerInquiry");
                                product_code_arr[i] = data.getJSONObject(i).getString("productCode");
                                product_name_arr[i] = data.getJSONObject(i).getString("productName");
                                product_amount_arr[i] = data.getJSONObject(i).getString("productAmount");

                                paymentPamModels.add(new PaymentPamModel(biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));

                                PaymentPamModel paymentPamModel = new PaymentPamModel();
                                paymentPamModel.bilerId = data.getJSONObject(i).getString("billerId");
                                paymentPamModel.billerCode = data.getJSONObject(i).getString("billerCode");
                                paymentPamModel.billerName = data.getJSONObject(i).getString("billerName");
                                paymentPamModel.billerInquiry = data.getJSONObject(i).getString("billerInquiry");
                                paymentPamModel.productCode = data.getJSONObject(i).getString("productCode");
                                paymentPamModel.productName = data.getJSONObject(i).getString("productName");
                                paymentPamModel.productAmount = data.getJSONObject(i).getString("productAmount");
                                paymentPamModel.save();
                            }
                            PaymentPamAdapter paymentPamAdapter = new PaymentPamAdapter(getActivity(), android.R.layout.simple_spinner_item, paymentPamModels);

                            cbo_voucher_name.setAdapter(paymentPamAdapter);

                            cbo_voucher_name.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    PaymentPamModel selectedVoucher = paymentPamModels.get(position);
                                    String billerType = selectedVoucher.getBillerInquiry();
                                    if (billerType.equalsIgnoreCase("Y")) {
                                        tbl_lblamount.setVisibility(View.GONE);
                                        tbl_amount.setVisibility(View.GONE);
                                        amt_line.setVisibility(View.GONE);
                                    } else {
                                        tbl_lblamount.setVisibility(View.VISIBLE);
                                        tbl_amount.setVisibility(View.VISIBLE);
                                        amt_line.setVisibility(View.VISIBLE);
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });

                        } else if (error_code.equalsIgnoreCase("0404")) {

                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                            alert.setMessage(error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Session session;
                                    session = new Session(getActivity());
                                    session.signOut();
                                }
                            });
                            alert.show();

                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                            alert.setMessage(getResources().getString(R.string.lbl_payment_title_alert) + error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Fragment newFragment = null;
                                    newFragment = new FragmentPaymentGeneral();
                                    switchFragment(newFragment);
                                }
                            });
                            alert.show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    }
                }


            });
        }else{
            paymentPamModels.clear();
            List<PaymentPamModel> itemList = new Select().all().from(PaymentPamModel.class).execute();

            int len = itemList.size();
            biller_id_arr = new String[len];
            biller_code_arr = new String[len];
            product_code_arr = new String[len];
            biller_name_arr = new String[len];
            biller_inquiry_arr = new String[len];
            product_name_arr = new String[len];
            product_amount_arr = new String[len];
            for (int i=0 ; i<itemList.size();i++) {

                biller_id_arr[i] = itemList.get(i).getBilerId();
                biller_code_arr[i] = itemList.get(i).getBillerCode();
                biller_name_arr[i] = itemList.get(i).getBillerName();
                biller_inquiry_arr[i] = itemList.get(i).getBillerInquiry();
                product_code_arr[i] = itemList.get(i).getProductCode();
                product_name_arr[i] = itemList.get(i).getProductName();
                product_amount_arr[i] = itemList.get(i).getProductAmount();

                paymentPamModels.add(new PaymentPamModel(biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));

            }
            PaymentPamAdapter paymentPamAdapter= new PaymentPamAdapter(getActivity(), android.R.layout.simple_spinner_item, paymentPamModels);
            cbo_voucher_name.setAdapter(paymentPamAdapter);

            cbo_voucher_name.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    PaymentPamModel selectedVoucher = paymentPamModels.get(position);
                    String billerType = selectedVoucher.getBillerInquiry();
                    if (billerType.equalsIgnoreCase("Y")) {
                        tbl_lblamount.setVisibility(View.GONE);
                        tbl_amount.setVisibility(View.GONE);
                        amt_line.setVisibility(View.GONE);
                    } else {
                        tbl_lblamount.setVisibility(View.VISIBLE);
                        tbl_amount.setVisibility(View.VISIBLE);
                        amt_line.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    private void initpaymentPhone(){
        if(getCountPhone==0){
            paymentPhoneModels.clear();
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getSherlockActivity()));
            client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
            RequestParams params = new RequestParams();

            try {

                String userid = AppHelper.getUserId(getActivity());
                String bankcode = AplConstants.Bankcode;
                String pcd = AplConstants.pcd;
                String URL = AplConstants.InquiryBiller;
                String appName = AplConstants.appName;
                String serviceName = URL.split("/")[4];
                String apiKey = AppHelper.getAccessKey(getActivity());
                UUID uniqueKey = UUID.randomUUID();
                String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;
                Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
                SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
                sha256_HMAC.init(secret_key);
                byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
                String signatureKey = new String(encodeUrlSafe(hmacData));
                params.put("userid", userid);
                params.put("bankcode", bankcode);
                params.put("passcode", pcd);
                params.put("categorycode", menu_biller_code);
                params.put("rc_uuid", uniqueKey.toString());
                params.put("rc_dtime", dateTime);
                params.put("signature", signatureKey);

            } catch (Exception e) {
                System.out.println("Error");
            }
            client.post(AplConstants.InquiryBiller, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject object) {


                    try {

                        if (pDialog != null) {
                            pDialog.dismiss();
                        }
                        JSONObject body_data = object.getJSONObject("body_data");
                        String error_code = body_data.getString("error_code");
                        String error_message = body_data.getString("error_message");

                        if (error_code.equalsIgnoreCase("0000")) {
                            JSONArray data = body_data.getJSONArray("biller_data");
                            int len = data.length();
                            biller_id_arr = new String[len];
                            biller_code_arr = new String[len];
                            product_code_arr = new String[len];
                            biller_name_arr = new String[len];
                            biller_inquiry_arr = new String[len];
                            product_name_arr = new String[len];
                            product_amount_arr = new String[len];

                            for (int i = 0; i < data.length(); i++) {
                                biller_id_arr[i] = data.getJSONObject(i).getString("billerId");
                                biller_code_arr[i] = data.getJSONObject(i).getString("billerCode");
                                biller_name_arr[i] = data.getJSONObject(i).getString("billerName");
                                biller_inquiry_arr[i] = data.getJSONObject(i).getString("billerInquiry");
                                product_code_arr[i] = data.getJSONObject(i).getString("productCode");
                                product_name_arr[i] = data.getJSONObject(i).getString("productName");
                                product_amount_arr[i] = data.getJSONObject(i).getString("productAmount");

                                paymentPhoneModels.add(new PaymentPhoneModel(biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));

                                PaymentPhoneModel paymentPhoneModel = new PaymentPhoneModel();
                                paymentPhoneModel.bilerId = data.getJSONObject(i).getString("billerId");
                                paymentPhoneModel.billerCode = data.getJSONObject(i).getString("billerCode");
                                paymentPhoneModel.billerName = data.getJSONObject(i).getString("billerName");
                                paymentPhoneModel.billerInquiry = data.getJSONObject(i).getString("billerInquiry");
                                paymentPhoneModel.productCode = data.getJSONObject(i).getString("productCode");
                                paymentPhoneModel.productName = data.getJSONObject(i).getString("productName");
                                paymentPhoneModel.productAmount = data.getJSONObject(i).getString("productAmount");
                                paymentPhoneModel.save();
                            }
                            PaymentPhoneAdapter paymentPhoneAdapter = new PaymentPhoneAdapter(getActivity(), android.R.layout.simple_spinner_item, paymentPhoneModels);

                            cbo_voucher_name.setAdapter(paymentPhoneAdapter);

                            cbo_voucher_name.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    PaymentPhoneModel selectedVoucher = paymentPhoneModels.get(position);
                                    String billerType = selectedVoucher.getBillerInquiry();
                                    if (billerType.equalsIgnoreCase("Y")) {
                                        tbl_lblamount.setVisibility(View.GONE);
                                        tbl_amount.setVisibility(View.GONE);
                                        amt_line.setVisibility(View.GONE);
                                    } else {
                                        tbl_lblamount.setVisibility(View.VISIBLE);
                                        tbl_amount.setVisibility(View.VISIBLE);
                                        amt_line.setVisibility(View.VISIBLE);
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });

                        } else if (error_code.equalsIgnoreCase("0404")) {

                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                            alert.setMessage(error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Session session;
                                    session = new Session(getActivity());
                                    session.signOut();
                                }
                            });
                            alert.show();

                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                            alert.setMessage(getResources().getString(R.string.lbl_payment_title_alert) + error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Fragment newFragment = null;
                                    newFragment = new FragmentPaymentGeneral();
                                    switchFragment(newFragment);
                                }
                            });
                            alert.show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    }
                }


            });
        }else{
            paymentPhoneModels.clear();
            List<PaymentPhoneModel> itemList = new Select().all().from(PaymentPhoneModel.class).execute();

            int len = itemList.size();
            biller_id_arr = new String[len];
            biller_code_arr = new String[len];
            product_code_arr = new String[len];
            biller_name_arr = new String[len];
            biller_inquiry_arr = new String[len];
            product_name_arr = new String[len];
            product_amount_arr = new String[len];
            for (int i=0 ; i<itemList.size();i++) {

                biller_id_arr[i] = itemList.get(i).getBilerId();
                biller_code_arr[i] = itemList.get(i).getBillerCode();
                biller_name_arr[i] = itemList.get(i).getBillerName();
                biller_inquiry_arr[i] = itemList.get(i).getBillerInquiry();
                product_code_arr[i] = itemList.get(i).getProductCode();
                product_name_arr[i] = itemList.get(i).getProductName();
                product_amount_arr[i] = itemList.get(i).getProductAmount();

                paymentPhoneModels.add(new PaymentPhoneModel(biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));

            }
            PaymentPhoneAdapter paymentPhoneAdapter= new PaymentPhoneAdapter(getActivity(), android.R.layout.simple_spinner_item, paymentPhoneModels);
            cbo_voucher_name.setAdapter(paymentPhoneAdapter);

            cbo_voucher_name.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    PaymentPhoneModel selectedVoucher = paymentPhoneModels.get(position);
                    String billerType = selectedVoucher.getBillerInquiry();
                    if (billerType.equalsIgnoreCase("Y")) {
                        tbl_lblamount.setVisibility(View.GONE);
                        tbl_amount.setVisibility(View.GONE);
                        amt_line.setVisibility(View.GONE);
                    } else {
                        tbl_lblamount.setVisibility(View.VISIBLE);
                        tbl_amount.setVisibility(View.VISIBLE);
                        amt_line.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }


    private void initpaymentTicket(){
        if(getCountTicket==0){
            paymentTicketModels.clear();
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getSherlockActivity()));
            client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
            RequestParams params = new RequestParams();

            try {

                String userid = AppHelper.getUserId(getActivity());
                String bankcode = AplConstants.Bankcode;
                String pcd = AplConstants.pcd;
                String URL = AplConstants.InquiryBiller;
                String appName = AplConstants.appName;
                String serviceName = URL.split("/")[4];
                String apiKey = AppHelper.getAccessKey(getActivity());
                UUID uniqueKey = UUID.randomUUID();
                String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;
                Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
                SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
                sha256_HMAC.init(secret_key);
                byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
                String signatureKey = new String(encodeUrlSafe(hmacData));
                params.put("userid", userid);
                params.put("bankcode", bankcode);
                params.put("passcode", pcd);
                params.put("categorycode", menu_biller_code);
                params.put("rc_uuid", uniqueKey.toString());
                params.put("rc_dtime", dateTime);
                params.put("signature", signatureKey);

            } catch (Exception e) {
                System.out.println("Error");
            }

            client.post(AplConstants.InquiryBiller, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject object) {


                    try {

                        if (pDialog != null) {
                            pDialog.dismiss();
                        }
                        JSONObject body_data = object.getJSONObject("body_data");
                        String error_code = body_data.getString("error_code");
                        String error_message = body_data.getString("error_message");

                        if (error_code.equalsIgnoreCase("0000")) {
                            JSONArray data = body_data.getJSONArray("biller_data");
                            int len = data.length();
                            biller_id_arr = new String[len];
                            biller_code_arr = new String[len];
                            product_code_arr = new String[len];
                            biller_name_arr = new String[len];
                            biller_inquiry_arr = new String[len];
                            product_name_arr = new String[len];
                            product_amount_arr = new String[len];

                            for (int i = 0; i < data.length(); i++) {
                                biller_id_arr[i] = data.getJSONObject(i).getString("billerId");
                                biller_code_arr[i] = data.getJSONObject(i).getString("billerCode");
                                biller_name_arr[i] = data.getJSONObject(i).getString("billerName");
                                biller_inquiry_arr[i] = data.getJSONObject(i).getString("billerInquiry");
                                product_code_arr[i] = data.getJSONObject(i).getString("productCode");
                                product_name_arr[i] = data.getJSONObject(i).getString("productName");
                                product_amount_arr[i] = data.getJSONObject(i).getString("productAmount");

                                paymentTicketModels.add(new PaymentTicketModel(biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));

                                PaymentTicketModel paymentTicketModel = new PaymentTicketModel();
                                paymentTicketModel.bilerId = data.getJSONObject(i).getString("billerId");
                                paymentTicketModel.billerCode = data.getJSONObject(i).getString("billerCode");
                                paymentTicketModel.billerName = data.getJSONObject(i).getString("billerName");
                                paymentTicketModel.billerInquiry = data.getJSONObject(i).getString("billerInquiry");
                                paymentTicketModel.productCode = data.getJSONObject(i).getString("productCode");
                                paymentTicketModel.productName = data.getJSONObject(i).getString("productName");
                                paymentTicketModel.productAmount = data.getJSONObject(i).getString("productAmount");
                                paymentTicketModel.save();
                            }
                            PaymentTicketAdapter paymentTicketAdapter = new PaymentTicketAdapter(getActivity(), android.R.layout.simple_spinner_item, paymentTicketModels);

                            cbo_voucher_name.setAdapter(paymentTicketAdapter);

                            cbo_voucher_name.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    PaymentTicketModel selectedVoucher = paymentTicketModels.get(position);
                                    String billerType = selectedVoucher.getBillerInquiry();
                                    if (billerType.equalsIgnoreCase("Y")) {
                                        tbl_lblamount.setVisibility(View.GONE);
                                        tbl_amount.setVisibility(View.GONE);
                                        amt_line.setVisibility(View.GONE);
                                    } else {
                                        tbl_lblamount.setVisibility(View.VISIBLE);
                                        tbl_amount.setVisibility(View.VISIBLE);
                                        amt_line.setVisibility(View.VISIBLE);
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });

                        } else if (error_code.equalsIgnoreCase("0404")) {

                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                            alert.setMessage(error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Session session;
                                    session = new Session(getActivity());
                                    session.signOut();
                                }
                            });
                            alert.show();

                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                            alert.setMessage(getResources().getString(R.string.lbl_payment_title_alert) + error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Fragment newFragment = null;
                                    newFragment = new FragmentPaymentGeneral();
                                    switchFragment(newFragment);
                                }
                            });
                            alert.show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    }
                }


            });
        }else{
            paymentTicketModels.clear();
            List<PaymentTicketModel> itemList = new Select().all().from(PaymentTicketModel.class).execute();

            int len = itemList.size();
            biller_id_arr = new String[len];
            biller_code_arr = new String[len];
            product_code_arr = new String[len];
            biller_name_arr = new String[len];
            biller_inquiry_arr = new String[len];
            product_name_arr = new String[len];
            product_amount_arr = new String[len];
            for (int i=0 ; i<itemList.size();i++) {

                biller_id_arr[i] = itemList.get(i).getBilerId();
                biller_code_arr[i] = itemList.get(i).getBillerCode();
                biller_name_arr[i] = itemList.get(i).getBillerName();
                biller_inquiry_arr[i] = itemList.get(i).getBillerInquiry();
                product_code_arr[i] = itemList.get(i).getProductCode();
                product_name_arr[i] = itemList.get(i).getProductName();
                product_amount_arr[i] = itemList.get(i).getProductAmount();

                paymentTicketModels.add(new PaymentTicketModel(biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));

            }
            PaymentTicketAdapter paymentTicketAdapter= new PaymentTicketAdapter(getActivity(), android.R.layout.simple_spinner_item, paymentTicketModels);
            cbo_voucher_name.setAdapter(paymentTicketAdapter);

            cbo_voucher_name.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    PaymentTicketModel selectedVoucher = paymentTicketModels.get(position);
                    String billerType = selectedVoucher.getBillerInquiry();
                    if (billerType.equalsIgnoreCase("Y")) {
                        tbl_lblamount.setVisibility(View.GONE);
                        tbl_amount.setVisibility(View.GONE);
                        amt_line.setVisibility(View.GONE);
                    } else {
                        tbl_lblamount.setVisibility(View.VISIBLE);
                        tbl_amount.setVisibility(View.VISIBLE);
                        amt_line.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    private void initpaymentTax(){
        if(getCountTax==0){
            paymentTaxModels.clear();
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getSherlockActivity()));
            client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
            RequestParams params = new RequestParams();

            try {

                String userid = AppHelper.getUserId(getActivity());
                String bankcode = AplConstants.Bankcode;
                String pcd = AplConstants.pcd;
                String URL = AplConstants.InquiryBiller;
                String appName = AplConstants.appName;
                String serviceName = URL.split("/")[4];
                String apiKey = AppHelper.getAccessKey(getActivity());
                UUID uniqueKey = UUID.randomUUID();
                String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;
                Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
                SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
                sha256_HMAC.init(secret_key);
                byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
                String signatureKey = new String(encodeUrlSafe(hmacData));
                params.put("userid", userid);
                params.put("bankcode", bankcode);
                params.put("passcode", pcd);
                params.put("categorycode", menu_biller_code);
                params.put("rc_uuid", uniqueKey.toString());
                params.put("rc_dtime", dateTime);
                params.put("signature", signatureKey);

            } catch (Exception e) {
                System.out.println("Error");
            }
            client.post(AplConstants.InquiryBiller,params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject object) {




                    try {

                        if (pDialog != null) {
                            pDialog.dismiss();
                        }
                        JSONObject body_data = object.getJSONObject("body_data");
                        String error_code = body_data.getString("error_code");
                        String error_message = body_data.getString("error_message");

                        if (error_code.equalsIgnoreCase("0000")) {
                            JSONArray data = body_data.getJSONArray("biller_data");
                            int len = data.length();
                            biller_id_arr = new String[len];
                            biller_code_arr = new String[len];
                            product_code_arr = new String[len];
                            biller_name_arr = new String[len];
                            biller_inquiry_arr = new String[len];
                            product_name_arr = new String[len];
                            product_amount_arr = new String[len];

                            for (int i = 0; i < data.length(); i++) {
                                biller_id_arr[i] = data.getJSONObject(i).getString("billerId");
                                biller_code_arr[i] = data.getJSONObject(i).getString("billerCode");
                                biller_name_arr[i] = data.getJSONObject(i).getString("billerName");
                                biller_inquiry_arr[i] = data.getJSONObject(i).getString("billerInquiry");
                                product_code_arr[i] = data.getJSONObject(i).getString("productCode");
                                product_name_arr[i] = data.getJSONObject(i).getString("productName");
                                product_amount_arr[i] = data.getJSONObject(i).getString("productAmount");

                                paymentTaxModels.add(new PaymentTaxModel(biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));

                                PaymentTaxModel paymentTaxModel = new PaymentTaxModel();
                                paymentTaxModel.bilerId = data.getJSONObject(i).getString("billerId");
                                paymentTaxModel.billerCode = data.getJSONObject(i).getString("billerCode");
                                paymentTaxModel.billerName = data.getJSONObject(i).getString("billerName");
                                paymentTaxModel.billerInquiry = data.getJSONObject(i).getString("billerInquiry");
                                paymentTaxModel.productCode = data.getJSONObject(i).getString("productCode");
                                paymentTaxModel.productName = data.getJSONObject(i).getString("productName");
                                paymentTaxModel.productAmount = data.getJSONObject(i).getString("productAmount");
                                paymentTaxModel.save();
                            }
                            PaymentTaxAdapter paymentTaxAdapter= new PaymentTaxAdapter(getActivity(), android.R.layout.simple_spinner_item, paymentTaxModels);

                            cbo_voucher_name.setAdapter(paymentTaxAdapter);

                            cbo_voucher_name.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    PaymentTaxModel selectedVoucher = paymentTaxModels.get(position);
                                    String billerType = selectedVoucher.getBillerInquiry();
                                    if (billerType.equalsIgnoreCase("Y")) {
                                        tbl_lblamount.setVisibility(View.GONE);
                                        tbl_amount.setVisibility(View.GONE);
                                        amt_line.setVisibility(View.GONE);
                                    } else {
                                        tbl_lblamount.setVisibility(View.VISIBLE);
                                        tbl_amount.setVisibility(View.VISIBLE);
                                        amt_line.setVisibility(View.VISIBLE);
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });

                        } else if (error_code.equalsIgnoreCase("0404")) {

                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                            alert.setMessage(error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Session session;
                                    session = new Session(getActivity());
                                    session.signOut();
                                }
                            });
                            alert.show();

                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                            alert.setMessage(getResources().getString(R.string.lbl_payment_title_alert) + error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Fragment newFragment = null;
                                    newFragment = new FragmentPaymentGeneral();
                                    switchFragment(newFragment);
                                }
                            });
                            alert.show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    }
                }


            });
        }else{
            paymentTaxModels.clear();
            List<PaymentTaxModel> itemList = new Select().all().from(PaymentTaxModel.class).execute();

            int len = itemList.size();
            biller_id_arr = new String[len];
            biller_code_arr = new String[len];
            product_code_arr = new String[len];
            biller_name_arr = new String[len];
            biller_inquiry_arr = new String[len];
            product_name_arr = new String[len];
            product_amount_arr = new String[len];
            for (int i=0 ; i<itemList.size();i++) {

                biller_id_arr[i] = itemList.get(i).getBilerId();
                biller_code_arr[i] = itemList.get(i).getBillerCode();
                biller_name_arr[i] = itemList.get(i).getBillerName();
                biller_inquiry_arr[i] = itemList.get(i).getBillerInquiry();
                product_code_arr[i] = itemList.get(i).getProductCode();
                product_name_arr[i] = itemList.get(i).getProductName();
                product_amount_arr[i] = itemList.get(i).getProductAmount();

                paymentTaxModels.add(new PaymentTaxModel(biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));

            }
            PaymentTaxAdapter paymentTaxAdapter= new PaymentTaxAdapter(getActivity(), android.R.layout.simple_spinner_item, paymentTaxModels);
            cbo_voucher_name.setAdapter(paymentTaxAdapter);

            cbo_voucher_name.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    PaymentTaxModel selectedVoucher = paymentTaxModels.get(position);
                    String billerType = selectedVoucher.getBillerInquiry();
                    if (billerType.equalsIgnoreCase("Y")) {
                        tbl_lblamount.setVisibility(View.GONE);
                        tbl_amount.setVisibility(View.GONE);
                        amt_line.setVisibility(View.GONE);
                    } else {
                        tbl_lblamount.setVisibility(View.VISIBLE);
                        tbl_amount.setVisibility(View.VISIBLE);
                        amt_line.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    public static byte[] encodeUrlSafe(byte[] data) {
        byte[] encode = Base64.encodeBase64(data);
        for (int i = 0; i < encode.length; i++) {
            if (encode[i] == '+') {
                encode[i] = '-';
            } else if (encode[i] == '=') {
                encode[i] = '_';
            } else if (encode[i] == '/') {
                encode[i] = '~';
            }
        }
        return encode;
    }


    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);

        getSherlockActivity().getActionBar().removeAllTabs();
        getSherlockActivity().getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
    }
}

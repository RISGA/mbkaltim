package sgo.mobile.bankkaltim.app.fragments;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.bankkaltim.AppHelper;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.activities.MainActivity;
import sgo.mobile.bankkaltim.app.adapter.ReportAdapterList;
import sgo.mobile.bankkaltim.conf.AplConstants;
import sgo.mobile.bankkaltim.frameworks.net.Networks;
import sgo.mobile.bankkaltim.frameworks.net.loopj.android.http.CustomSSLSocketFactory;
import sgo.mobile.bankkaltim.frameworks.session.Session;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

/**
 * Created by thinkpad on 1/19/2015.
 */
public class FragmentReport extends Fragment {

    public static ArrayList<Integer> imagePaymentType = new ArrayList<Integer>();
    public static ArrayList<String> id =  new ArrayList<String>();
    public static ArrayList<String> name =  new ArrayList<String>();
    public static ArrayList<String> type =  new ArrayList<String>();

    ListView mListView;
    ProgressBar prgLoading;
    LinearLayout llDate;
    Button btnDate;
    EditText txtStart, txtEnd;
    TextView txtAlert,txtHeader,lbl_error,lblPaymentReport;
    ReportAdapterList reportAdapterList;
    private ProgressDialog pDialog;

    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");//dd/MM/yyyy
    SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");//dd/MM/yyyy
    Date now = new Date();
    String startDate = sdfDate.format(now);
    String endDate = sdfDate.format(now);
    String rdate= dt.format(now);

    boolean networks;

    String account_name,account_no;
    int mYear,mMonth,mDay,mActionDatePicker=0;
    DatePickerDialog dpd;
    Date mDateStart, mDateEnd;
    String error_msg;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_report, container, false);

        imagePaymentType.add(R.drawable.ic_electricity);
        imagePaymentType.add(R.drawable.ic_water);
        imagePaymentType.add(R.drawable.ic_phone);
        imagePaymentType.add(R.drawable.ic_internet_payment);
        imagePaymentType.add(R.drawable.ic_tv_cable);
        imagePaymentType.add(R.drawable.ic_tax);
        imagePaymentType.add(R.drawable.ic_credit_card);
        imagePaymentType.add(R.drawable.ic_installment);
        imagePaymentType.add(R.drawable.ic_insurance);
        imagePaymentType.add(R.drawable.ic_education);
        imagePaymentType.add(R.drawable.ic_ticket);
        imagePaymentType.add(R.drawable.ic_other);
        imagePaymentType.add(R.drawable.ic_electricity);
        imagePaymentType.add(R.drawable.ic_phone);
        imagePaymentType.add(R.drawable.ic_other);
        prgLoading = (ProgressBar) view.findViewById(R.id.prgLoading);
        mListView = (ListView) view.findViewById(R.id.list_payment_type);
        llDate = (LinearLayout) view.findViewById(R.id.llDate);

        btnDate = (Button) view.findViewById(R.id.BtnDate);
        txtStart = (EditText) view.findViewById(R.id.txtStart);
        txtEnd = (EditText) view.findViewById(R.id.txtEnd);

        txtStart.setText(startDate);
        txtEnd.setText(endDate);
        parseDate();

        txtHeader = (TextView) view.findViewById(R.id.label_header);
        txtHeader.setVisibility(View.GONE);

        lbl_error = (TextView) view.findViewById(R.id.label_error);
        lbl_error.setVisibility(View.GONE);

        txtAlert = (TextView) view.findViewById(R.id.txtAlert);
        txtAlert.setVisibility(View.GONE);

        reportAdapterList = new ReportAdapterList(getActivity());
        mListView.setAdapter(reportAdapterList);

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        dpd = new DatePickerDialog(getActivity(),new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear, int dayOfMonth) {
                if(mActionDatePicker == 1){
                    txtStart.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                    mDateStart = new java.sql.Date(year-1900,monthOfYear,dayOfMonth);
                }
                else {
                    txtEnd.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                    mDateEnd = new java.sql.Date(year-1900,monthOfYear,dayOfMonth);
                }
            }
        }, mYear, mMonth, mDay);

        //set maksimal datepicker dialog memunculkan tgl dan waktu saat ini saja
        dpd.getDatePicker().setMaxDate(c.getTimeInMillis());

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {
                Fragment newFragment = null;
                newFragment = new FragmentReportInquiry();
                Bundle args = new Bundle();
                args.putString("paymentId", id.get(position));
                args.putString("paymentName", name.get(position));
                args.putString("startDate", startDate);
                args.putString("endDate", endDate);
                newFragment.setArguments(args);
                switchFragment(newFragment);
            }
        });

        txtStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActionDatePicker = 1;
                dpd.show();
            }
        });

        txtEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActionDatePicker = 2;
                dpd.show();
            }
        });

        Bundle bundle = this.getArguments();
        if(bundle != null) {
            startDate = bundle.getString("startDate");
            endDate = bundle.getString("endDate");
            txtStart.setText(startDate);
            txtEnd.setText(endDate);
            parseDate();
        }
        try {
            networks = new Networks().isConnectingToInternet(getActivity());
            if (networks) {
                parseJSONData();
            } else {
                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                alert.setTitle(getResources().getString(R.string.lbl_transaction_report_title));
                alert.setMessage(getResources().getString(R.string.lbl_alert_connection));
                alert.setPositiveButton("OK", null);
                alert.show();

            }
        }catch (Exception e){
            e.printStackTrace();
        }

        btnDate.setOnClickListener(submitFilterListener);

        return view;
    }

    public void parseDate(){
        try {
            java.util.Date utilStartDate = sdfDate.parse(txtStart.getText().toString());
            mDateStart = new java.sql.Date(utilStartDate.getTime());
            java.util.Date utilEndDate = sdfDate.parse(txtEnd.getText().toString());
            mDateEnd = new java.sql.Date(utilEndDate.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    //listener untuk submit dari filter tanggalnya
    Button.OnClickListener submitFilterListener = new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
        if(inputValidation()){
            mListView.setVisibility(View.GONE);
            txtAlert.setVisibility(View.GONE);
            prgLoading.setVisibility(View.VISIBLE);
            clearData();
            startDate = txtStart.getText().toString();
            endDate = txtEnd.getText().toString();
            try {
                networks = new Networks().isConnectingToInternet(getActivity());
                if (networks) {
                    parseJSONData();
                    prgLoading.setVisibility(8);
                    //Toast.makeText(getActivity(), "Sorry, this process will available soon !", Toast.LENGTH_LONG).show();

                } else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setTitle(getResources().getString(R.string.lbl_transaction_report_title));
                    alert.setMessage(getResources().getString(R.string.lbl_alert_connection));
                    alert.setPositiveButton("OK", null);
                    alert.show();

                }
            }catch (Exception e){
                e.printStackTrace();
            }

        }
        else Toast.makeText(getActivity(), error_msg, Toast.LENGTH_SHORT).show();
        }
    };

    public boolean inputValidation(){
        if(txtStart.getText().toString().equals("")){
            error_msg = getResources().getString(R.string.lbl_transaction_report_start_date);
            return false;
        }
        else if(txtEnd.getText().toString().equals("")){
            error_msg = getResources().getString(R.string.lbl_transaction_report_start_date);
            return false;
        }
        else if(mDateEnd.compareTo(mDateStart)<0) {
            error_msg = getResources().getString(R.string.lbl_transaction_report_unmatch_date);
            return false;
        }
        return true;
    }

    public void parseJSONData(){
        clearData();
        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getActivity()));
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
        RequestParams params = new RequestParams();

        try {
            String userid = AppHelper.getUserId(getActivity());
            String bankcode = AplConstants.Bankcode;
            String pcd = AplConstants.pcd;
            String URL = AplConstants.BillerPaymentType ;
            String appName = AplConstants.appName;
            String serviceName = URL.split("/")[4];
            String apiKey = AppHelper.getAccessKey(getActivity());
            UUID uniqueKey = UUID.randomUUID();

            String message = uniqueKey + rdate + appName + serviceName + bankcode + userid;

            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] hmacData = sha256_HMAC.doFinal(message.getBytes("UTF-8"));
            String signatureKey = new String(encodeUrlSafe(hmacData));
            params.put("userid", userid);
            params.put("bankcode", bankcode);
            params.put("passcode", pcd);
            params.put("startdate", startDate);
            params.put("enddate", endDate);
            params.put("rc_uuid", uniqueKey.toString());
            params.put("rc_dtime",rdate);
            params.put("signature",signatureKey);

        }catch (Exception e){
            System.out.println("Error");
        }
        client.post(AplConstants.BillerPaymentType, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject json) {



                try {
                    JSONObject data = json.getJSONObject("body_data"); // this is the "items: [ ] part
                    String error_code = data.getString("error_code");
                    String error_message = data.getString("error_message");


                    if(error_code.equalsIgnoreCase("0000")) {
                        JSONArray payment_type_data = data.getJSONArray("payment_type_data");
                        if (payment_type_data.length()== 0) {
                            if (pDialog != null) {
                                pDialog.dismiss();
                            }
                            txtAlert.setVisibility(View.VISIBLE);
                            txtAlert.setText(getResources().getString(R.string.lbl_transaction_report_null));
                        }else if(payment_type_data.equals(null)){
                            if (pDialog != null) {
                                pDialog.dismiss();
                            }
                            txtAlert.setVisibility(View.VISIBLE);
                            txtAlert.setText(getResources().getString(R.string.lbl_transaction_report_null));
                        }
                        else {
                            for (int i = 0; i < payment_type_data.length(); i++) {
                                JSONObject object = payment_type_data.getJSONObject(i);

                                String idTemp = object.getString("id");
                                boolean flag = false;
                                for (int index = 0; index < id.size(); index++) {
                                    if (id.get(index).toString().equals(idTemp)) {
                                        flag = true;
                                        break;
                                    }
                                }
                                if (flag == false) {
                                    id.add(object.getString("id"));
                                    name.add(object.getString("name"));
                                    type.add(object.getString("type"));
                                }
                            }
                        }
                    }else if(error_code.equalsIgnoreCase("0404")){
                        prgLoading.setVisibility(View.GONE);
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                        alert.setMessage(error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Session session;
                                session = new Session(getActivity());
                                session.signOut();
                            }
                        });
                        alert.show();

                    }else{
                        if (pDialog != null) {
                            pDialog.dismiss();
                        }
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_transaction_report_title));
                        alert.setMessage(getResources().getString(R.string.lbl_transaction_report_null)+" "+ error_message);
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    }

                    // when finish parsing, hide progressbar
                    prgLoading.setVisibility(View.GONE);

                    // if data available show data on list
                    // otherwise, show alert text
                    if (name.size() > 0 ){
                        mListView.setVisibility(View.VISIBLE);
                        mListView.setAdapter(reportAdapterList);
                    } else {
                        txtAlert.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();

                }
            }
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (pDialog != null) {
                    pDialog.dismiss();
                }
                if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                } else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                }
            }

        });
    }

    // clear arraylist variables before used
    void clearData(){
        id.clear();
        name.clear();
        type.clear();
    }

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
        // TODO Auto-generated method stub
        super.handleMessage(msg);
        }
    };

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }

    public static byte[] encodeUrlSafe(byte[] data) {
        byte[] encode = Base64.encodeBase64(data);
        for (int i = 0; i < encode.length; i++) {
            if (encode[i] == '+') {
                encode[i] = '-';
            } else if (encode[i] == '=') {
                encode[i] = '_';
            } else if (encode[i] == '/') {
                encode[i] = '~';
            }
        }
        return encode;
    }
}

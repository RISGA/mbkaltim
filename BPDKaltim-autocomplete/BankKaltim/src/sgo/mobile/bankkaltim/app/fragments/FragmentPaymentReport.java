package sgo.mobile.bankkaltim.app.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TextView;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.activities.MainActivity;
import sgo.mobile.bankkaltim.conf.AplConstants;
import sgo.mobile.bankkaltim.frameworks.math.FormatCurrency;

/**
 * Created by User on 11/10/2014.
 */
public class FragmentPaymentReport extends SherlockFragment {
    private ProgressDialog pDialog;
    String source_account_no,date,trx_id,order_id,source_account_name,biller_code,biller_name,member_no,amount,description,menu_biller_code,status;
    Button btnDone;
    TextView lbl_header,lbl_title_order_id;

    TableLayout tbl_electric,tbl_ticket_garuda,tbl_tv;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payment_general_report, container, false);
//        tbl_electric =(TableLayout)view.findViewById(R.id.tbl_electric);
//        tbl_ticket_garuda =(TableLayout)view.findViewById(R.id.tbl_garuda);
//        tbl_tv = (TableLayout)view.findViewById(R.id.tbl_tv);
        lbl_header = (TextView)view.findViewById(R.id.label_header);
        lbl_title_order_id = (TextView)view.findViewById(R.id.lbl_title_order_id);
        btnDone =(Button)view.findViewById(R.id.btnDone);
        Bundle bundle = this.getArguments();
        menu_biller_code = bundle.getString("menu_biller_code");
        source_account_no = bundle.getString("source_account_no");
        source_account_name = bundle.getString("source_account_name");
        biller_name = bundle.getString("biller_name");
        biller_code = bundle.getString("biller_code");
        order_id = bundle.getString("order_id");
        date = bundle.getString("date");
        amount = bundle.getString("amount");
        status = bundle.getString("status");
        trx_id= bundle.getString("trx_id");


        if(menu_biller_code.equals(AplConstants.billCableTV)) {
            lbl_header.setText(getResources().getString(R.string.lbl_payment_tv_title));
            lbl_title_order_id.setText(getResources().getString(R.string.lbl_payment_orderid));

        }if(menu_biller_code.equals(AplConstants.billElectricity)) {
            lbl_header.setText(getResources().getString(R.string.lbl_payment_electricity_title));
            lbl_title_order_id.setText(getResources().getString(R.string.lbl_payment_orderid));


        }if(menu_biller_code.equals(AplConstants.billITicket)) {
            lbl_header.setText(getResources().getString(R.string.lbl_payment_ticket_title));
            lbl_title_order_id.setText(getResources().getString(R.string.lbl_payment_ticket_orderid));


        }if(menu_biller_code.equals(AplConstants.billPhone)) {
            lbl_header.setText(getResources().getString(R.string.lbl_payment_phone_title));
            lbl_title_order_id.setText(getResources().getString(R.string.lbl_payment_phone_orderid));


        }if(menu_biller_code.equals(AplConstants.billInstallment)) {
            lbl_header.setText(getResources().getString(R.string.lbl_payment_installment_title));
            lbl_title_order_id.setText(getResources().getString(R.string.lbl_payment_orderid));


        }if(menu_biller_code.equals(AplConstants.billInternet)) {
            lbl_header.setText(getResources().getString(R.string.lbl_payment_Internet_title));
            lbl_title_order_id.setText(getResources().getString(R.string.lbl_payment_orderid));


        }if(menu_biller_code.equals(AplConstants.billIInsurance)) {
            lbl_header.setText(getResources().getString(R.string.lbl_payment_insurance_title));
            lbl_title_order_id.setText(getResources().getString(R.string.lbl_payment_insurance_orderid));


        }if(menu_biller_code.equals(AplConstants.billwater)) {
            lbl_header.setText(getResources().getString(R.string.lbl_payment_pam_title));
            lbl_title_order_id.setText(getResources().getString(R.string.lbl_payment_orderid));


        }

        TextView lbl_date = (TextView) view.findViewById(R.id.lbl_date);
        lbl_date.setText(date);

        TextView source_account = (TextView) view.findViewById(R.id.lbl_source_account);
        source_account.setText(source_account_no+ "("+source_account_name+")");

        TextView payment_type = (TextView) view.findViewById(R.id.lbl_payment_type);
        payment_type.setText(biller_name);

        TextView lbl_order_id = (TextView) view.findViewById(R.id.lbl_order_id);
        lbl_order_id.setText(order_id);

        TextView lbl_amount = (TextView) view.findViewById(R.id.lbl_amount);
        lbl_amount.setText(FormatCurrency.CurrencyIDR(amount));

        TextView lbl_status = (TextView) view.findViewById(R.id.lbl_status);
        lbl_status.setText(status);

        TextView lbl_trx_id = (TextView) view.findViewById(R.id.lbl_trx_id);
        lbl_trx_id.setText(trx_id);






        /*if(menu_biller_code.equals(AplConstants.billElectricity)) {
            lbl_header.setText("Electricity Payment Report");
            tbl_electric.setVisibility(View.VISIBLE);
            String customer_name = bundle.getString("customer_name");
            String max_kwh = bundle.getString("max_kwh");
            String up_phone_no = bundle.getString("up_phone_no");
            String orderId = bundle.getString("order_id");
            String dateTime = bundle.getString("date");
            String amount = bundle.getString("amount");

            String status = bundle.getString("error_message");


             TextView lbl_date = (TextView) view.findViewById(R.id.lbl_date);
             lbl_date.setText(dateTime);


             TextView lbl_source_account = (TextView) view.findViewById(R.id.lbl_source_account);
            lbl_source_account.setText(source_account_no +"("+source_account_name+")");

             TextView lbl_provider = (TextView) view.findViewById(R.id.lbl_provider);
            lbl_provider.setText(biller_name);


             TextView lbl_customer_id= (TextView) view.findViewById(R.id.lbl_customer_id);
            lbl_customer_id.setText(orderId);


             TextView lbl_customer_name = (TextView) view.findViewById(R.id.lbl_customer_name);
             lbl_customer_name.setText(customer_name);


             TextView lbl_up_phone_no = (TextView) view.findViewById(R.id.lbl_up_phone_no);
            lbl_up_phone_no.setText(up_phone_no);


             TextView lbl_max_kwh = (TextView) view.findViewById(R.id.lbl_max_kwh);
            lbl_max_kwh.setText(max_kwh);



             TextView lbl_total = (TextView) view.findViewById(R.id.lbl_total);
            lbl_total.setText(FormatCurrency.CurrencyIDR(amount));

             TextView lbl_status = (TextView) view.findViewById(R.id.lbl_status);
             lbl_status.setText(status);



        }*else if(menu_biller_code.equals(AplConstants.billITicket) && biller_code.equalsIgnoreCase("GA_TICKET")) {
            lbl_header.setText(getResources().getString(R.string.lbl_payment_report_title));
            tbl_ticket_garuda.setVisibility(View.VISIBLE);

                String order_id = bundle.getString("order_id");
                String date = bundle.getString("date");
                String amount = bundle.getString("amount");
                String error_message = bundle.getString("status");

                TextView lbl_date_tiket_ga = (TextView) view.findViewById(R.id.lbl_date_ticket_ga);
                lbl_date_tiket_ga.setText(date);

                TextView lbl_source_account_tiket_ga = (TextView) view.findViewById(R.id.lbl_source_account_ticket_ga);
                lbl_source_account_tiket_ga.setText(source_account_no + "(" + source_account_name + ")");

                TextView lbl_biller_name_tiket_ga = (TextView) view.findViewById(R.id.lbl_payment_type);
                lbl_biller_name_tiket_ga.setText(biller_name);

                TextView lbl_payment_code_tiket_ga = (TextView) view.findViewById(R.id.lbl_payment_code_ticket_ga);
                lbl_payment_code_tiket_ga.setText(order_id);


                TextView lbl_total_ticket_ga = (TextView) view.findViewById(R.id.lbl_total_ticket_ga);
                lbl_total_ticket_ga.setText(FormatCurrency.CurrencyIDR(amount));

                TextView lbl_status_ticket_ga = (TextView) view.findViewById(R.id.lbl_status_ticket_ga);
                lbl_status_ticket_ga.setText(error_message);



        }*/




        btnDone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                //submitPurchase();
                Fragment newFragment = null;
                newFragment = new FragmentPaymentGeneral();
                Bundle args = new Bundle();
                args.putString("menu_biller_code", menu_biller_code);
                newFragment.setArguments(args);
                switchFragment(newFragment);
            }
        });

        return  view;


    }

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
        }

    };

    private void switchFragment(android.support.v4.app.Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);

        getSherlockActivity().getActionBar().removeAllTabs();
        getSherlockActivity().getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
    }
}


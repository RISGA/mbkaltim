package sgo.mobile.bankkaltim.app.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.activities.MainActivity;

public class FragmentTransfer extends Fragment{
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_transfer, container, false);
        TextView txtSaldo =(TextView) view.findViewById(R.id.txtSaldo);
        txtSaldo.setText("Rp.0");

        return view;
    }

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }

}

package sgo.mobile.bankkaltim.app.fragments;

import android.annotation.TargetApi;
import android.app.ListFragment;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleAdapter;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.activities.MainActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by SGO-Mobile-DEV on 4/3/2015.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class ListProviderVoucherPhone extends ListFragment {

    // Array of strings storing country names


    // Array of integers points to images stored in /res/drawable/
    int[] providerIcon = new int[]{
            R.drawable.ic_telkomsel,
            R.drawable.ic_indosat,
            R.drawable.ic_smart,
            R.drawable.ic_xl,
            R.drawable.ic_esia

    };

    // Array of strings to store currencies
    String[] providerName = new String[]{
            "Telkomsel",
            "Indosat",
            "Smart",
            "XL",
            "Esia"
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        // Each row in the list stores country name, currency and flag
        List<HashMap<String,String>> aList = new ArrayList<HashMap<String,String>>();

        for(int i=0;i<10;i++){
            HashMap<String, String> hm = new HashMap<String,String>();
            hm.put("pName","Provider Name :" + providerName[i]);
            hm.put("pIcon", Integer.toString(providerIcon[i]) );
            aList.add(hm);
        }

        // Keys used in Hashmap
        String[] from = { "pIcon","pName"};

        // Ids of views in listview_layout
        int[] to = { R.id.providerName,R.id.providerIcon};

        // Instantiating an adapter to store each items
        // R.layout.listview_layout defines the layout of each item
        SimpleAdapter adapter = new SimpleAdapter(getActivity().getBaseContext(), aList, R.layout.listview_provider, from, to);

        setListAdapter(adapter);

        return super.onCreateView(inflater, container, savedInstanceState);
    }



    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }
}

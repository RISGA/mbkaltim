package sgo.mobile.bankkaltim.app.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.activities.MainActivity;

/**
 * Created by SGO-Mobile-DEV on 3/18/2015.
 */
public class FragmentChangePasswordReport extends SherlockFragment {
    TextView lbl_header, lbl_success;
    ImageView ic_success;
    Button btnDone;
    MainActivity mainActivity;
    static Activity thisActivity = null;
    private boolean isExit;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.fragment_change_password_report, container, false);

        lbl_header = (TextView)view.findViewById(R.id.label_header);
        lbl_success = (TextView)view.findViewById(R.id.lbl_success);
        lbl_success.setText(getResources().getString(R.string.lbl_success));
        // ic_success =(ImageView)findViewById(R.id.ic_success);
        btnDone = (Button)view.findViewById(R.id.btnDone);

        btnDone.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View view) {
                Fragment newFragment = null;
                newFragment = new FragmentSetting();
                switchFragment(newFragment);
            }
        });
        return  view;
    }

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
        getSherlockActivity().getActionBar().removeAllTabs();
        getSherlockActivity().getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
    }

}

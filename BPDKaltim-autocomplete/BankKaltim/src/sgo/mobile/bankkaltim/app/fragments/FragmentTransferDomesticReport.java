package sgo.mobile.bankkaltim.app.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.activities.MainActivity;

import java.util.Random;

public class FragmentTransferDomesticReport extends SherlockFragment {
    private ProgressDialog pDialog;
    private ViewPager viewPager;
    private FragmentMainTransfer fragmentMainTransfer;

    private static final char[] CHARSET_AZ_09 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();

    String ref, source_account_no, source_account_name, dest_account_no, error_message, dest_account_name, dest_bank_code, dest_bank_name, dest_email, amount, memo, paymentdate;
    Button btnDone,btnCancel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_transfer_domestic_report, container, false);

        Bundle bundle         = this.getArguments();
        source_account_no     = bundle.getString("source_account_no");
        source_account_name   = bundle.getString("source_account_name");
        dest_account_no       = bundle.getString("dest_account_no");
        dest_account_name     = bundle.getString("dest_account_name");
        dest_bank_code        = bundle.getString("dest_bank_code");
        dest_bank_name        = bundle.getString("dest_bank_name");
        amount                = bundle.getString("totalAmount");
        memo                  = bundle.getString("memo");
        dest_email            = bundle.getString("email");
        paymentdate           = bundle.getString("paymentdate");
        error_message         = bundle.getString("error_message");
        ref                   = bundle.getString("ref");

        TextView lbl_source_account = (TextView) view.findViewById(R.id.lbl_source_account);
        lbl_source_account.setText(source_account_name );
        TextView lbl_benef_account = (TextView) view.findViewById(R.id.lbl_benef_account);
        lbl_benef_account.setText(dest_account_name);
        TextView lbl_benef_no = (TextView) view.findViewById(R.id.lbl_benef_no);
        lbl_benef_no.setText(dest_account_no);
        TextView lbl_benef_email = (TextView) view.findViewById(R.id.lbl_benef_email);
        lbl_benef_email.setText(dest_email);
        TextView lbl_amount        = (TextView) view.findViewById(R.id.lbl_amount);
        lbl_amount.setText(amount);
        TextView lbl_message       = (TextView) view.findViewById(R.id.lbl_message);
        lbl_message.setText(memo);
        TextView lbl_date           = (TextView) view.findViewById(R.id.lbl_date);
        lbl_date.setText(paymentdate);
        TextView lbl_status           = (TextView) view.findViewById(R.id.lbl_status);
        lbl_status.setText(error_message);
        TextView lbl_trx_id           = (TextView) view.findViewById(R.id.lbl_trx_id);
        lbl_trx_id.setText(ref);

        btnDone               = (Button) view.findViewById(R.id.btnDone);
        btnDone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Fragment newFragment = null;
                newFragment = new FragmentMainTransferDomestic();
                switchFragment(newFragment);

            }
        });

        btnCancel             = (Button) view.findViewById(R.id.btnCancel);
        return view;
    }




    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);


        getSherlockActivity().getActionBar().removeAllTabs();
        getSherlockActivity().getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
       // viewPager.setCurrentItem(1,false);
        //getActivity().getActionBar();
    }




    public static String randomTrxId(char[] characterSet, int length) {
        Random random = new java.security.SecureRandom();
        char[] result = new char[length];
        for (int i = 0; i < result.length; i++) {
            // picks a random index out of character set > random character
            int randomCharIndex = random.nextInt(characterSet.length);
            result[i] = characterSet[randomCharIndex];
        }
        return new String(result);
    }

   // public

    public void cancelTransaction(View view)
    {
        AlertDialog.Builder alertbox = new AlertDialog.Builder(getActivity());
        alertbox.setTitle(getResources().getString(R.string.lbl_exit));
        alertbox.setMessage(getResources().getString(R.string.lbl_exit_message));
        alertbox.setPositiveButton(getResources().getString(R.string.lbl_exit_yes), new
                DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        Fragment newFragment = null;
                        newFragment = new FragmentBenefTo();
                        switchFragment(newFragment);
                    }
                });
        alertbox.setNegativeButton(getResources().getString(R.string.lbl_exit_no), new
                DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {}
                });
        alertbox.show();
    }
}
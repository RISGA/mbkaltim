package sgo.mobile.bankkaltim.app.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.actionbarsherlock.app.SherlockFragment;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.bankkaltim.AppHelper;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.activities.MainActivity;
import sgo.mobile.bankkaltim.app.adapter.BenefListAdapter;
import sgo.mobile.bankkaltim.conf.AplConstants;
import sgo.mobile.bankkaltim.frameworks.net.Networks;
import sgo.mobile.bankkaltim.frameworks.net.loopj.android.http.CustomSSLSocketFactory;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

/**
 * Created by SGO-Mobile-DEV on 3/6/2015.
 */
public class FragmentDeleteBeneficiary extends SherlockFragment {

    private ProgressDialog pDialog;
    FragmentManager fm;
    ListView listMenu;
    ProgressBar prgLoading;
    TextView txtAlert,txtHeader,lbl_error;
    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");//dd/MM/yyyy
    Date now = new Date();
    String dateTime = sdfDate.format(now);
    boolean networks;
    BenefListAdapter benefListAdapter;
    public String dataDestAccount="";
    public static ArrayList<String> dest_bank_code_arr= new ArrayList<String>();
    public static ArrayList<String> dest_bank_name_arr = new ArrayList<String>();
    public static ArrayList<String> dest_account_no_arr = new ArrayList<String>();
    public static ArrayList<String> dest_account_name_arr = new ArrayList<String>();
    public static ArrayList<String> dest_ccy_id_arr = new ArrayList<String>();
    public static ArrayList<String> dest_email_arr = new ArrayList<String>();
    public static ArrayList<String> benefid = new ArrayList<String>();
    public static ArrayList<String> account_type_arr = new ArrayList<String>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view  = inflater.inflate(R.layout.fragment_benef_list, container, false);
        prgLoading = (ProgressBar) view.findViewById(R.id.prgLoading);
        listMenu   = (ListView) view.findViewById(R.id.listBenef);

        txtHeader = (TextView) view.findViewById(R.id.label_header);
        txtHeader.setVisibility(View.GONE);
        lbl_error = (TextView) view.findViewById(R.id.label_error);
        lbl_error.setVisibility(View.GONE);

        benefListAdapter = new BenefListAdapter(getActivity());

        try {
            networks = new Networks().isConnectingToInternet(getActivity());
            if (networks) {
                getBenef();
            } else {
                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                alert.setTitle("Notice");
                alert.setMessage("Please check your internet connection");
                alert.setPositiveButton("OK", null);
                alert.show();

            }
        }catch (Exception e){
            e.printStackTrace();
        }

        listMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {
                Fragment newFragment = null;
                newFragment = new FragmentBenefDetail();
                Bundle args = new Bundle();
                args.putString("bankCode", dest_bank_code_arr.get(position));
                args.putString("bankName", dest_bank_name_arr.get(position));
                args.putString("accountNo", dest_account_no_arr.get(position));
                args.putString("accountName", dest_account_name_arr.get(position));
                args.putString("ccyId", dest_ccy_id_arr.get(position));
                args.putString("email", dest_email_arr.get(position));
                args.putString("benefId", benefid.get(position));
                args.putString("accountType", account_type_arr.get(position));
                newFragment.setArguments(args);
                switchFragment(newFragment);
            }
        });

        return view;
    }


    public void getBenef(){
        clearData();

        dataDestAccount = AppHelper.getDestAccount(getSherlockActivity());
        if (dataDestAccount.equals("")) {
            AsyncHttpClient clients = new AsyncHttpClient();
            clients.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getSherlockActivity()));
            clients.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
            RequestParams params = new RequestParams();

            try {
                String userid = AppHelper.getUserId(getActivity());
                String bankcode = AplConstants.Bankcode;
                String pcd = AplConstants.pcd;
                String type = AplConstants.TypeBeneficiary;
                String URL = AplConstants.DestinationAccountListAPI;
                String appName = AplConstants.appName;
                String serviceName = URL.split("/")[4];
                String apiKey = AppHelper.getAccessKey(getActivity());
                UUID uniqueKey = UUID.randomUUID();
                String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;


                Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
                SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
                sha256_HMAC.init(secret_key);
                byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
                String signatureKey = new String(encodeUrlSafe(hmacData));
                params.put("userid", userid);
                params.put("bankcode", bankcode);
                params.put("passcode", pcd);
                params.put("destbankcode", "");
                // params.put("type", "");
                params.put("rc_uuid", uniqueKey.toString());
                params.put("rc_dtime", dateTime);
                params.put("signature", signatureKey);

            } catch (Exception e) {
                System.out.println("Error");
            }

            clients.post(AplConstants.DestinationAccountListAPI, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject object) {


                    try {
                        if (pDialog != null) {
                            pDialog.dismiss();
                        }
                        JSONObject body_data = object.getJSONObject("body_data");
                        String error_code = body_data.getString("error_code");
                        String error_message = body_data.getString("error_message");


                        if (error_code.equalsIgnoreCase("0000")) {
                            JSONArray data = body_data.getJSONArray("balance_account_data"); // this is the "items: [ ] part
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject objects = data.getJSONObject(i);
                                dest_bank_code_arr.add(objects.getString("bank_code"));
                                dest_bank_name_arr.add(objects.getString("bank_name"));
                                dest_account_no_arr.add(objects.getString("account_no"));
                                dest_account_name_arr.add(objects.getString("account_name"));
                                dest_ccy_id_arr.add(objects.getString("ccy_id"));
                                dest_email_arr.add(objects.getString("email"));
                                benefid.add(objects.getString("beneficiaryid"));
                                account_type_arr.add(objects.getString("account_type"));

                                SecurePreferences prefs = new SecurePreferences(getSherlockActivity());
                                SecurePreferences.Editor mEditor = prefs.edit();
                                mEditor.putString("destAccountData", data.toString());
                                mEditor.apply();
                            }

                            prgLoading.setVisibility(8);

                            // if data available show data on list
                            // otherwise, show alert text
                            if (dest_bank_code_arr.size() > 0) {
                                prgLoading.setVisibility(View.GONE);
                                txtHeader.setVisibility(View.VISIBLE);
                                listMenu.setVisibility(0);
                                listMenu.setAdapter(benefListAdapter);
                                //  lbl_error.setVisibility(0);
                            } else {
                                //  lbl_error.setVisibility(0);
                                txtHeader.setVisibility(View.GONE);
                                lbl_error.setVisibility(0);
                            }

                        } else if (error_code.equalsIgnoreCase("0404")) {

                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle("Session Expired");
                            alert.setMessage("Message : " + error_message);
                            alert.setPositiveButton("OK", null);
                            alert.show();


                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle("Delete Beneficiary");
                            alert.setMessage("Delete Beneficiary : " + error_message);
                            alert.setPositiveButton("OK", null);
                            alert.show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    }
                }
            });
        }else{
            clearData();
            dataDestAccount = AppHelper.getDestAccount(getSherlockActivity());

            try {
                JSONArray data = new JSONArray(dataDestAccount);
                for (int i = 0; i < data.length(); i++) {
                    JSONObject objects = data.getJSONObject(i);
                    dest_bank_code_arr.add(objects.getString("bank_code"));
                    dest_bank_name_arr.add(objects.getString("bank_name"));
                    dest_account_no_arr.add(objects.getString("account_no"));
                    dest_account_name_arr.add(objects.getString("account_name"));
                    dest_ccy_id_arr.add(objects.getString("ccy_id"));
                    dest_email_arr.add(objects.getString("email"));
                    benefid.add(objects.getString("beneficiaryid"));
                    account_type_arr.add(objects.getString("account_type"));

                    SecurePreferences prefs = new SecurePreferences(getSherlockActivity());
                    SecurePreferences.Editor mEditor = prefs.edit();
                    mEditor.putString("destAccountData", data.toString());
                    mEditor.apply();
                }

                prgLoading.setVisibility(8);

                // if data available show data on list
                // otherwise, show alert text
                if (dest_bank_code_arr.size() > 0) {
                    prgLoading.setVisibility(View.GONE);
                    txtHeader.setVisibility(View.VISIBLE);
                    listMenu.setVisibility(0);
                    listMenu.setAdapter(benefListAdapter);
                    //  lbl_error.setVisibility(0);
                } else {
                    //  lbl_error.setVisibility(0);
                    txtHeader.setVisibility(View.GONE);
                    lbl_error.setVisibility(0);
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static byte[] encodeUrlSafe(byte[] data) {
        byte[] encode = Base64.encodeBase64(data);
        for (int i = 0; i < encode.length; i++) {
            if (encode[i] == '+') {
                encode[i] = '-';
            } else if (encode[i] == '=') {
                encode[i] = '_';
            } else if (encode[i] == '/') {
                encode[i] = '~';
            }
        }
        return encode;
    }
    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }

    void clearData(){
        dest_bank_code_arr.clear();
        dest_bank_name_arr.clear();
        dest_account_no_arr.clear();
        dest_account_name_arr.clear();
        dest_ccy_id_arr.clear();
        dest_email_arr.clear();
        benefid.clear();
        account_type_arr.clear();
    }

}

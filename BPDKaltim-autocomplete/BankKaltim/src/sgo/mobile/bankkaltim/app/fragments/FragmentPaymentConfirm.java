package sgo.mobile.bankkaltim.app.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.telephony.SmsMessage;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.bankkaltim.AppHelper;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.activities.MainActivity;
import sgo.mobile.bankkaltim.app.ui.dialog.DefinedDialog;
import sgo.mobile.bankkaltim.conf.AplConstants;
import sgo.mobile.bankkaltim.frameworks.math.FormatCurrency;
import sgo.mobile.bankkaltim.frameworks.net.Networks;
import sgo.mobile.bankkaltim.frameworks.net.loopj.android.http.CustomSSLSocketFactory;
import sgo.mobile.bankkaltim.frameworks.session.Session;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by User on 10/10/2014.
 */
public class FragmentPaymentConfirm extends SherlockFragment {
    String source_account, billerInquiry,productName,productCode,customer_name,max_kwh,up_phone_no, source_account_no,source_account_name,biller_id,biller_code,biller_name,member_no,amount,description,menu_biller_code,menu_biller_name,pCode,p_name,biller_inquiry,amt;
    Button btnDone, btnGetOtp;
    EditText txtOtp;
    TextView lbl_member_no,lbl_header,lbl_title;
    private ProgressDialog pDialog;
    String billOther = AplConstants.billOthers;
    String billCreditCard = AplConstants.billCreditCard;
    String billElectricity = AplConstants.billElectricity;
    String billPhone = AplConstants.billPhone;
    String billTax = AplConstants.billTax;
    String billInternet = AplConstants.billInternet;
    String billWater = AplConstants.billwater;
    String billEducation= AplConstants.billIEducation;
    String billInsurance= AplConstants.billIInsurance;
    String billInstalment= AplConstants.billInstallment;
    String billTv= AplConstants.billCableTV;
    String billTicket= AplConstants.billITicket;
    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");//dd/MM/yyyy
    Date now = new Date();
    String dateTime = sdfDate.format(now);
    boolean networks;
    TableRow tbl_otp;
    TableLayout tbl_tv,tbl_installment,tbl_electric,tbl_tiket_ga,tbl_insurance,tbl_internet,tbl_pam,tbl_phone;
    View amt_line;
    private static LinkedHashMap<String,String> dataUiList;
    private static Map json;
    // Response Tiket Garuda
    String airline_code,airline_code2,total_flight,passenger_name, pnr_code;

    public String orderId,billerCode;
    View line_otp;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payment_confirm, container, false);
        Bundle bundle = this.getArguments();
        menu_biller_code = bundle.getString("menu_biller_code");
        menu_biller_name = bundle.getString("menu_biller_name");
        biller_code = bundle.getString("biller_code");
        line_otp = view.findViewById(R.id.line_otp);
        line_otp.setVisibility(View.GONE);
        lbl_header = (TextView)view.findViewById(R.id.label_header);
        tbl_electric = (TableLayout)view.findViewById(R.id.tbl_electric);
        tbl_tiket_ga = (TableLayout)view.findViewById(R.id.tbl_garuda);
        tbl_tv = (TableLayout)view.findViewById(R.id.tbl_tv);
        tbl_installment = (TableLayout)view.findViewById(R.id.tbl_installment);
        tbl_insurance = (TableLayout)view.findViewById(R.id.tbl_insurance);
        tbl_internet= (TableLayout)view.findViewById(R.id.tbl_internet);
        tbl_pam= (TableLayout)view.findViewById(R.id.tbl_pam);
        tbl_phone= (TableLayout)view.findViewById(R.id.tbl_phone);
        tbl_otp = (TableRow)view.findViewById(R.id.rowOtp);
        tbl_otp.setVisibility(View.GONE);
        txtOtp = (EditText)view.findViewById(R.id.txtOtp);
        btnDone = (Button)view.findViewById(R.id.btnDone);
        btnDone.setVisibility(View.GONE);
        btnGetOtp=(Button)view.findViewById(R.id.btnGetOtp);

        if(menu_biller_code.equals(billElectricity)){
            lbl_header.setText("Electricity Payment");
            tbl_electric.setVisibility(View.VISIBLE);
            source_account_name = bundle.getString("source_account_name");
            source_account_no = bundle.getString("source_account_no");
            biller_name = bundle.getString("billerName");
            orderId = bundle.getString("orderId");
            customer_name = bundle.getString("name");
            up_phone_no = bundle.getString("up_phone_no");
            max_kwh = bundle.getString("max_kwh");
            amount = bundle.getString("amount");
            pCode = bundle.getString("pCode");

            TextView lbl_source_account = (TextView)view.findViewById(R.id.lbl_source_account);
            lbl_source_account.setText(source_account_no + "(" +source_account_name+ ")");

            TextView lbl_biller_name = (TextView)view.findViewById(R.id.lbl_biller_name);
            lbl_biller_name.setText(biller_name);
            TextView lbl_orderid_key= (TextView)view.findViewById(R.id.lbl_orderid_key_electric);
            lbl_orderid_key.setText(getResources().getString(R.string.lbl_payment_orderid));
            TextView lbl_order_id = (TextView)view.findViewById(R.id.lbl_order_id);
            lbl_order_id.setText(orderId);

            TextView lbl_customer_name = (TextView)view.findViewById(R.id.lbl_customer_name);
            lbl_customer_name.setText(customer_name);

            TextView lbl_up_phone_no = (TextView)view.findViewById(R.id.lbl_up_phone_no);
            lbl_up_phone_no.setText(up_phone_no);

            TextView lbl_max_kwh = (TextView)view.findViewById(R.id.lbl_max_kwh);
            lbl_max_kwh.setText(max_kwh);

            TextView lbl_amount = (TextView)view.findViewById(R.id.lbl_amount);
            lbl_amount.setText(FormatCurrency.CurrencyIDR(amount));


        }else if(menu_biller_code.equals(billTicket)){
            lbl_header.setText(getResources().getString(R.string.lbl_payment_ticket_title));
            tbl_tiket_ga.setVisibility(View.VISIBLE);
            if (biller_code.equalsIgnoreCase("GA_TICKET")){

                source_account_name = bundle.getString("source_account_name");
                source_account_no = bundle.getString("source_account_no");
                biller_name = bundle.getString("billerName");
                orderId = bundle.getString("orderId");
                pCode = bundle.getString("pCode");
                airline_code = bundle.getString("airline_code");
                airline_code2 = bundle.getString("airline_code2");
                total_flight = bundle.getString("total_flight");
                passenger_name = bundle.getString("passenger_name");
                pnr_code = bundle.getString("pnr_code");
                amount = bundle.getString("amount");
                TextView lbl_source_account_tiket_ga = (TextView)view.findViewById(R.id.lbl_source_account_ticket_ga);
                lbl_source_account_tiket_ga.setText(source_account_no + "("+source_account_name+")");

                TextView lbl_biller_name_tiket_ga = (TextView)view.findViewById(R.id.lbl_biller_name_ticket_ga);
                lbl_biller_name_tiket_ga.setText(biller_name);

                TextView lbl_payment_code_tiket_ga = (TextView)view.findViewById(R.id.lbl_payment_code_ticket_ga);
                lbl_payment_code_tiket_ga.setText(orderId);

                TextView lbl_airline_code = (TextView)view.findViewById(R.id.lbl_airline_code_ticket_ga);
                lbl_airline_code.setText(airline_code);

                TextView lbl_airline_code2 = (TextView)view.findViewById(R.id.lbl_airline_code2_ticket_ga);
                lbl_airline_code2.setText(airline_code2);

                TextView lbl_total_flight_tiket_ga = (TextView)view.findViewById(R.id.lbl_total_flight_ticket_ga);
                lbl_total_flight_tiket_ga.setText(total_flight);

                TextView lbl_passenger_tiket_ga = (TextView)view.findViewById(R.id.lbl_passenger_name_ticket_ga);
                lbl_passenger_tiket_ga.setText(passenger_name);

                TextView lbl_pnr_code_tiket_ga = (TextView)view.findViewById(R.id.lbl_pnr_code_ticket_ga);
                lbl_pnr_code_tiket_ga.setText(pnr_code);

                TextView lbl_amount_ticket_ga = (TextView)view.findViewById(R.id.lbl_amount_ticket_ga);
                lbl_amount_ticket_ga.setText(FormatCurrency.CurrencyIDR(amount));

                TextView lbl_total_ticket_ga = (TextView)view.findViewById(R.id.lbl_total_ticket_ga);
                lbl_total_ticket_ga.setText(FormatCurrency.CurrencyIDR(amount));
            }
        }
        if(menu_biller_code.equals(billTv)) {
            source_account_name = bundle.getString("source_account_name");
            source_account_no = bundle.getString("source_account_no");
            biller_id = bundle.getString("biller_id");
            amount = bundle.getString("amount");
            orderId = bundle.getString("order_id");
            biller_name = bundle.getString("biller_name");
            pCode = bundle.getString("pCode");
            productName = bundle.getString("product_name");
            billerInquiry = bundle.getString("biller_inquiry");

            tbl_tv.setVisibility(View.VISIBLE);
            lbl_header.setText(getResources().getString(R.string.lbl_payment_tv_title));
            TextView lbl_tv_source_account = (TextView)view.findViewById(R.id.lbl_tv_source_account);
            lbl_tv_source_account.setText(source_account_no +"("+source_account_name+")");

            TextView lbl_tv_provider = (TextView)view.findViewById(R.id.lbl_tv_provider);
            lbl_tv_provider.setText(biller_name);
            TextView lbl_orderid_key= (TextView)view.findViewById(R.id.lbl_orderid_key_tv);
            lbl_orderid_key.setText(getResources().getString(R.string.lbl_payment_orderid));
            TextView lbl_tv_order_id = (TextView)view.findViewById(R.id.lbl_tv_customer_id);
            lbl_tv_order_id.setText(orderId);
            TextView lbl_tv_amount = (TextView)view.findViewById(R.id.lbl_tv_amount);
            lbl_tv_amount.setText(FormatCurrency.CurrencyIDR(amount));

        }

        if(menu_biller_code.equals(billInstalment)) {

            lbl_header.setText(getResources().getString(R.string.lbl_payment_installment_title));
            tbl_installment.setVisibility(View.VISIBLE);
            source_account_name = bundle.getString("source_account_name");
            source_account_no = bundle.getString("source_account_no");
            biller_id = bundle.getString("biller_id");
            amount = bundle.getString("amount");
            orderId = bundle.getString("order_id");
            biller_name = bundle.getString("biller_name");
            pCode = bundle.getString("pCode");
            productName = bundle.getString("product_name");
            billerInquiry = bundle.getString("biller_inquiry");
            TextView lbl_installment_source_account = (TextView)view.findViewById(R.id.lbl_installment_source_account);
            lbl_installment_source_account.setText(source_account_no +"("+source_account_name+")");

            TextView lbl_installment_provider = (TextView)view.findViewById(R.id.lbl_installment_provider);
            lbl_installment_provider.setText(biller_name);
            TextView lbl_orderid_key= (TextView)view.findViewById(R.id.lbl_orderid_key_installment);
            lbl_orderid_key.setText(getResources().getString(R.string.lbl_payment_orderid));
            TextView lbl_installment_order_id = (TextView)view.findViewById(R.id.lbl_installment_customer_id);
            lbl_installment_order_id.setText(orderId);
            TextView lbl_installment_amount = (TextView)view.findViewById(R.id.lbl_installment_amount);
            lbl_installment_amount.setText(FormatCurrency.CurrencyIDR(amount));

        }if(menu_biller_code.equals(billInsurance)) {

            lbl_header.setText(getResources().getString(R.string.lbl_payment_insurance_title));
            tbl_insurance.setVisibility(View.VISIBLE);
            source_account_name = bundle.getString("source_account_name");
            source_account_no = bundle.getString("source_account_no");
            biller_id = bundle.getString("biller_id");
            amount = bundle.getString("amount");
            orderId = bundle.getString("order_id");
            biller_name = bundle.getString("biller_name");
            pCode = bundle.getString("pCode");
            productName = bundle.getString("product_name");
            billerInquiry = bundle.getString("biller_inquiry");
            TextView lbl_insurance_source_account = (TextView)view.findViewById(R.id.lbl_insurance_source_account);
            lbl_insurance_source_account.setText(source_account_no +"("+source_account_name+")");

            TextView lbl_insurance_provider = (TextView)view.findViewById(R.id.lbl_insurance_provider);
            lbl_insurance_provider.setText(biller_name);
            TextView lbl_orderid_key= (TextView)view.findViewById(R.id.lbl_orderid_key_insurance);
            lbl_orderid_key.setText(getResources().getString(R.string.lbl_payment_insurance_orderid));
            TextView lbl_insurance_order_id = (TextView)view.findViewById(R.id.lbl_insurance_customer_id);
            lbl_insurance_order_id.setText(orderId);
            TextView lbl_insurance_amount = (TextView)view.findViewById(R.id.lbl_insurance_amount);
            lbl_insurance_amount.setText(FormatCurrency.CurrencyIDR(amount));

        }

        if(menu_biller_code.equals(billInternet)) {

            lbl_header.setText(getResources().getString(R.string.lbl_payment_Internet_title));
            tbl_internet.setVisibility(View.VISIBLE);
            source_account_name = bundle.getString("source_account_name");
            source_account_no = bundle.getString("source_account_no");
            biller_id = bundle.getString("biller_id");
            amount = bundle.getString("amount");
            orderId = bundle.getString("order_id");
            biller_name = bundle.getString("biller_name");
            pCode = bundle.getString("pCode");
            productName = bundle.getString("product_name");
            billerInquiry = bundle.getString("biller_inquiry");
            TextView lbl_internet_source_account = (TextView)view.findViewById(R.id.lbl_internet_source_account);
            lbl_internet_source_account.setText(source_account_no +"("+source_account_name+")");

            TextView lbl_internet_provider = (TextView)view.findViewById(R.id.lbl_internet_provider);
            lbl_internet_provider.setText(biller_name);
            TextView lbl_orderid_key= (TextView)view.findViewById(R.id.lbl_orderid_key_internet);
            lbl_orderid_key.setText(getResources().getString(R.string.lbl_payment_orderid));
            TextView lbl_internet_order_id = (TextView)view.findViewById(R.id.lbl_internet_customer_id);
            lbl_internet_order_id.setText(orderId);
            TextView lbl_internet_amount = (TextView)view.findViewById(R.id.lbl_internet_amount);
            lbl_internet_amount.setText(FormatCurrency.CurrencyIDR(amount));

        }

        if(menu_biller_code.equals(billWater)) {

            lbl_header.setText(getResources().getString(R.string.lbl_payment_pam_title));
            tbl_pam.setVisibility(View.VISIBLE);
            source_account_name = bundle.getString("source_account_name");
            source_account_no = bundle.getString("source_account_no");
            biller_id = bundle.getString("biller_id");
            amount = bundle.getString("amount");
            orderId = bundle.getString("order_id");
            biller_name = bundle.getString("biller_name");
            pCode = bundle.getString("pCode");
            productName = bundle.getString("product_name");
            billerInquiry = bundle.getString("biller_inquiry");
            TextView lbl_pam_source_account = (TextView)view.findViewById(R.id.lbl_pam_source_account);
            lbl_pam_source_account.setText(source_account_no +"("+source_account_name+")");

            TextView lbl_pam_provider = (TextView)view.findViewById(R.id.lbl_pam_provider);
            lbl_pam_provider.setText(biller_name);
            TextView lbl_orderid_key= (TextView)view.findViewById(R.id.lbl_orderid_key_pam);
            lbl_orderid_key.setText(getResources().getString(R.string.lbl_payment_orderid));
            TextView lbl_pam_order_id = (TextView)view.findViewById(R.id.lbl_pam_customer_id);
            lbl_pam_order_id.setText(orderId);
            TextView lbl_pam_amount = (TextView)view.findViewById(R.id.lbl_pam_amount);
            lbl_pam_amount.setText(FormatCurrency.CurrencyIDR(amount));

        }

        if(menu_biller_code.equals(billPhone)) {

            lbl_header.setText(getResources().getString(R.string.lbl_payment_phone_title));
            tbl_phone.setVisibility(View.VISIBLE);
            source_account_name = bundle.getString("source_account_name");
            source_account_no = bundle.getString("source_account_no");
            biller_id = bundle.getString("biller_id");
            amount = bundle.getString("amount");
            orderId = bundle.getString("order_id");
            biller_name = bundle.getString("biller_name");
            pCode = bundle.getString("pCode");
            productName = bundle.getString("product_name");
            billerInquiry = bundle.getString("biller_inquiry");
            TextView lbl_phone_source_account = (TextView)view.findViewById(R.id.lbl_phone_source_account);
            lbl_phone_source_account.setText(source_account_no +"("+source_account_name+")");

            TextView lbl_phone_provider = (TextView)view.findViewById(R.id.lbl_phone_provider);
            lbl_phone_provider.setText(biller_name);
            TextView lbl_orderid_key= (TextView)view.findViewById(R.id.lbl_orderid_key_phone);
            lbl_orderid_key.setText(getResources().getString(R.string.lbl_payment_phone_orderid));
            TextView lbl_phone_order_id = (TextView)view.findViewById(R.id.lbl_phone_customer_id);
            lbl_phone_order_id.setText(orderId);
            TextView lbl_phone_amount = (TextView)view.findViewById(R.id.lbl_phone_amount);
            lbl_phone_amount.setText(FormatCurrency.CurrencyIDR(amount));

        }



        btnDone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                final String txtToken = txtOtp.getText().toString();
                if (txtToken.equalsIgnoreCase("")) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setTitle(getResources().getString(R.string.lbl_alert_otp_title));
                    alert.setMessage(getResources().getString(R.string.lbl_otp_validation_message));
                    alert.setPositiveButton("OK", null);
                    alert.show();
                } else {
                    try {
                        networks = new Networks().isConnectingToInternet(getActivity());
                        if (networks) {
                            submitPayment();
                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                            alert.setMessage(getResources().getString(R.string.lbl_alert_connection));
                            alert.setPositiveButton("OK", null);
                            alert.show();

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        });


        btnGetOtp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {

                try {
                    networks = new Networks().isConnectingToInternet(getActivity());
                    if (networks) {
                        requestToken();
                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
                        alert.setMessage(getResources().getString(R.string.lbl_alert_connection));
                        alert.setPositiveButton("OK", null);
                        alert.show();

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });

        return view;
    }



    public void requestToken() {
        pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.lbl_loading_process));
        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getSherlockActivity()));
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
        RequestParams params = new RequestParams();

        try {
            String userid = AppHelper.getUserId(getActivity());
            String bankcode = AplConstants.Bankcode;
            String pcd = AplConstants.pcd;
            String URL = AplConstants.RequestTokenAPI;
            String appName = AplConstants.appName;
            String serviceName = URL.split("/")[4];
            String apiKey = AppHelper.getAccessKey(getActivity());
            UUID uniqueKey = UUID.randomUUID();
            String signature = uniqueKey+dateTime+ appName + serviceName+ bankcode + userid;


            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
            String signatureKey = new String(encodeUrlSafe(hmacData));
            params.put("userid", userid);
            params.put("bankcode", bankcode);
            params.put("passcode", pcd);
            params.put("rc_uuid", uniqueKey.toString());
            params.put("rc_dtime", dateTime);
            params.put("signature", signatureKey);

        }catch (Exception e){
            System.out.println("Error");
        }
        client.post(AplConstants.RequestTokenAPI, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject object) {

                try {
                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    JSONObject body_data = object.getJSONObject("body_data");
                    // parse json data and store into arraylist variables
                    String error_code = body_data.getString("error_code");
                    String error_message = body_data.getString("error_message");
                    if (error_code.equals("0000")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_alert_otp_title));
                        alert.setMessage(getResources().getString(R.string.lbl_alert_otp_message));
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                btnGetOtp.setVisibility(View.GONE);
                                tbl_otp.setVisibility(View.VISIBLE);
                                btnDone.setVisibility(View.VISIBLE);
                                line_otp.setVisibility(View.VISIBLE);

                            }
                        });
                        alert.show();


                    } else if (error_code.equalsIgnoreCase("0404")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                        alert.setMessage(error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Session session;
                                session = new Session(getActivity());
                                session.signOut();
                            }
                        });
                        alert.show();

                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_alert_otp_title));
                        alert.setMessage(error_message);
                        alert.setPositiveButton("OK", null);
                        alert.show();

                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (pDialog != null) {
                    pDialog.dismiss();
                }
                if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                } else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                }
            }

        });
    }



    private static String parseNumber(String s){
        String numberFragment = s.substring(s.lastIndexOf(' '), s.length());//extract number
        return numberFragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        toggleMyBroadcastReceiver(true);
    }

    @Override
    public void onPause() {
        super.onPause();
        toggleMyBroadcastReceiver(false);
    }

    private void toggleMyBroadcastReceiver(Boolean _on){
        if (getActivity() == null)
            return;

        MainActivity fca = (MainActivity) getActivity();
        fca.togglerBroadcastReceiver(_on,myReceiver);
    }

    public BroadcastReceiver myReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle mBundle = intent.getExtras();
            SmsMessage[] mSMS =null;
            String strMessage = "";

            if(mBundle != null){
                Object[] pdus = (Object[]) mBundle.get("pdus");
                mSMS = new SmsMessage[pdus.length];

                for (int i = 0; i < mSMS.length ; i++){
                    mSMS[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                    strMessage += mSMS[i].getMessageBody();

                    final Matcher matcher = Pattern.compile("mTOKEN : ").matcher(strMessage);
                    if(matcher.find()){
                        // System.out.println(sentence.substring(matcher.end()).trim());
                        String res =strMessage.substring(matcher.end()).trim();
                        int lenght = res.length();
                        final String otp = res.substring(0,6);
                        txtOtp.setText(otp);
                    }
                }


            }
        }
    };


    private void submitPayment() {
        pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.lbl_loading_process));
        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getSherlockActivity()));
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
        RequestParams params = new RequestParams();


        try {

            String userid = AppHelper.getUserId(getActivity());
            String bankcode = AplConstants.Bankcode;
            String pcd = AplConstants.pcd;
            String URL = AplConstants.BillerPaymentNotif;
            String appName = AplConstants.appName;
            String serviceName = URL.split("/")[4];
            String apiKey = AppHelper.getAccessKey(getActivity());
            UUID uniqueKey = UUID.randomUUID();
            String signature = uniqueKey+dateTime+ appName + serviceName+ bankcode + userid;


            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
            String signatureKey = new String(encodeUrlSafe(hmacData));
            params.put("userid", userid);
            params.put("bankcode", bankcode);
            params.put("passcode", pcd);
            params.put("productcode", pCode);
            params.put("orderid", orderId);
            params.put("amount", amount);
            params.put("responsecode", txtOtp.getText().toString());
            params.put("sourceacctno", source_account_no);
            params.put("rc_uuid", uniqueKey.toString());
            params.put("rc_dtime",dateTime);
            params.put("signature",signatureKey);

        }catch (Exception e){
            System.out.println("Error");
        }


        client.post(AplConstants.BillerPaymentNotif, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject object) {


                try {

                    if (pDialog != null) {
                        pDialog.dismiss();
                    }


        JSONObject body_data = object.getJSONObject("body_data");
        String error_code = body_data.getString("error_code");
        final String error_message = body_data.getString("error_message");

        if (error_code.equalsIgnoreCase("0000")) {
            JSONObject data = body_data.getJSONObject("data");
            final String psNumber = data.getString("psNumber");
            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
            alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
            alert.setMessage(getResources().getString(R.string.lbl_payment_title_alert) + " " + error_message);
            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    Fragment newFragment = null;
                    newFragment = new FragmentPaymentReport();
                    Bundle args = new Bundle();
                    args.putString("menu_biller_code", menu_biller_code);
                    args.putString("source_account_no", source_account_no);
                    args.putString("source_account_name", source_account_name);
                    args.putString("biller_name", biller_name);
                    args.putString("biller_code", biller_code);
                    args.putString("order_id", orderId);
                    args.putString("date", dateTime);
                    args.putString("amount", amount);
                    args.putString("status", error_message);
                    args.putString("trx_id", psNumber);
                    newFragment.setArguments(args);
                    switchFragment(newFragment);

                }
            });
            alert.show();

        } else if (error_code.equalsIgnoreCase("0404")) {

            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
            alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
            alert.setMessage(error_message);
            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Session session;
                    session = new Session(getActivity());
                    session.signOut();
                }
            });
            alert.show();

        } else {
            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
            alert.setTitle(getResources().getString(R.string.lbl_payment_title_alert));
            alert.setMessage(getResources().getString(R.string.lbl_payment_title_alert) + " " + error_message);
            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {


                    Fragment newFragment = null;
                    newFragment = new FragmentPaymentReport();
                    Bundle args = new Bundle();
                    args.putString("menu_biller_code", menu_biller_code);
                    args.putString("source_account_no", source_account_no);
                    args.putString("source_account_name", source_account_name);
                    args.putString("biller_name", biller_name);
                    args.putString("biller_code", biller_code);
                    args.putString("order_id", orderId);
                    args.putString("date", dateTime);
                    args.putString("amount", amount);
                    args.putString("status", error_message);
                    newFragment.setArguments(args);
                    switchFragment(newFragment);

                }
            });
            alert.show();

        }
    } catch (JSONException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }
}


            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (pDialog != null) {
                    pDialog.dismiss();
                }
                if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                } else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                }

                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                alert.setTitle(getResources().getString(R.string.lbl_tab_inhouse));
                alert.setMessage(getResources().getString(R.string.lbl_transfer_exception));
                alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Fragment newFragment = null;
                        newFragment = new FragmentMainTransfer();
                        switchFragment(newFragment);
                    }
                });
                alert.show();
            }



});
        }

        Handler mHandler = new Handler() {

@Override
public void handleMessage(Message msg) {
        // TODO Auto-generated method stub
        super.handleMessage(msg);
        }

        };

private void switchFragment(android.support.v4.app.Fragment fragment) {
        if (getActivity() == null)
        return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
        getSherlockActivity().getActionBar().removeAllTabs();
        getSherlockActivity().getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
    }

    public static byte[] encodeUrlSafe(byte[] data) {
        byte[] encode = Base64.encodeBase64(data);
        for (int i = 0; i < encode.length; i++) {
            if (encode[i] == '+') {
                encode[i] = '-';
            } else if (encode[i] == '=') {
                encode[i] = '_';
            } else if (encode[i] == '/') {
                encode[i] = '~';
            }
        }
        return encode;
    }
}

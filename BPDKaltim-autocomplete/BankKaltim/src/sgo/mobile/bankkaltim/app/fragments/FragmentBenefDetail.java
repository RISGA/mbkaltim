package sgo.mobile.bankkaltim.app.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.bankkaltim.AppHelper;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.activities.MainActivity;
import sgo.mobile.bankkaltim.app.models.DestinationAccountListModel;
import sgo.mobile.bankkaltim.app.ui.dialog.DefinedDialog;
import sgo.mobile.bankkaltim.conf.AplConstants;
import sgo.mobile.bankkaltim.frameworks.net.Networks;
import sgo.mobile.bankkaltim.frameworks.net.loopj.android.http.CustomSSLSocketFactory;
import sgo.mobile.bankkaltim.frameworks.session.Session;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * Created by SGO-Mobile-DEV on 3/6/2015.
 */
public class FragmentBenefDetail extends SherlockFragment{
    String bankCode, bankName, accountNo, accountName, ccyId, email, benefId, accountType;
    Button btnCancel, btnDelete;

    private ProgressDialog pDialog;
    FragmentManager fm;
    ListView listMenu;
    ProgressBar prgLoading;
    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");//dd/MM/yyyy
    Date now = new Date();
    String dateTime = sdfDate.format(now);
    boolean networks;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_benef_detail, container, false);
        Bundle bundle = this.getArguments();
        bankCode = bundle.getString("bankCode");
        bankName = bundle.getString("bankName");
        accountNo = bundle.getString("accountNo");
        accountName = bundle.getString("accountName");
        ccyId = bundle.getString("ccyId");
        email = bundle.getString("email");
        benefId = bundle.getString("benefId");
        accountType = bundle.getString("accountType");


        btnCancel = (Button) view.findViewById(R.id.btnCancel);
        btnDelete = (Button) view.findViewById(R.id.btnDelete);

        TextView label_benefId = (TextView) view.findViewById(R.id.label_benefId);
        label_benefId.setText(benefId);

        TextView label_bankName = (TextView) view.findViewById(R.id.label_bankName);
        label_bankName.setText(bankName);

        TextView label_accountNo = (TextView) view.findViewById(R.id.label_accountNo);
        label_accountNo.setText(accountNo);

        TextView label_accountName = (TextView) view.findViewById(R.id.label_accoutName);
        label_accountName.setText(accountName);

        TextView label_ccy = (TextView) view.findViewById(R.id.label_ccyId);
        label_ccy.setText(ccyId);

        TextView label_email = (TextView) view.findViewById(R.id.label_email);
        label_email.setText(email);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Fragment newFragment = null;
                newFragment = new FragmentDeleteBeneficiary();
                Bundle args = new Bundle();
                newFragment.setArguments(args);
                switchFragment(newFragment);
            }
        });
        btnDelete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                alert.setTitle(getResources().getString(R.string.lbl_delete_beneficiary));
                alert.setMessage(getResources().getString(R.string.lbl_delete_beneficiary_alert));
                alert.setNegativeButton(getResources().getString(R.string.lbl_exit_no),null);
                alert.setPositiveButton(getResources().getString(R.string.lbl_exit_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            networks = new Networks().isConnectingToInternet(getActivity());
                            if (networks) {
                                deleteBenef();
                            } else {
                                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                alert.setTitle(getResources().getString(R.string.lbl_delete_beneficiary_detail));
                                alert.setMessage(getResources().getString(R.string.lbl_alert_connection));
                                alert.setPositiveButton("OK", null);
                                alert.show();

                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
                alert.show();
            }
        });

        return view;


    }


    public void deleteBenef() {
        pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.lbl_delete_beneficiary_loading));

        AsyncHttpClient clients = new AsyncHttpClient();
        clients.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getSherlockActivity()));
        clients.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
        RequestParams params = new RequestParams();

        try {
            String userid = AppHelper.getUserId(getActivity());
            String bankcode = AplConstants.Bankcode;
            String pcd = AplConstants.pcd;
            String URL = AplConstants.DeleteBeneficiary;
            String appName = AplConstants.appName;
            String serviceName = URL.split("/")[4];
            String apiKey = AppHelper.getAccessKey(getActivity());
            UUID uniqueKey = UUID.randomUUID();
            String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;


            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
            String signatureKey = new String(encodeUrlSafe(hmacData));
            params.put("userid", userid);
            params.put("bankcode", bankcode);
            params.put("passcode", pcd);
            params.put("type", accountType);
            params.put("benef_id", benefId);
            params.put("rc_uuid", uniqueKey.toString());
            params.put("rc_dtime", dateTime);
            params.put("signature", signatureKey);

        } catch (Exception e) {
            System.out.println("Error");
        }
        clients.post(AplConstants.DeleteBeneficiary, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject object) {


                try {
                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    JSONObject body_data = object.getJSONObject("body_data");
                    String error_code = body_data.getString("error_code");
                    String error_message = body_data.getString("error_message");


                    if (error_code.equalsIgnoreCase("0000")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_delete_beneficiary));
                        alert.setMessage(error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Fragment newFragment = null;
                                newFragment = new FragmentDeleteBeneficiary();
                                switchFragment(newFragment);
                                DestinationAccountListModel.deleteAll();
                                SecurePreferences prefs = new SecurePreferences(getSherlockActivity());
                                prefs.edit().remove("destAccountData").commit();
                            }
                        });

                        alert.show();

                    } else if (error_code.equalsIgnoreCase("0404")) {

                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                        alert.setMessage(error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Session session;
                                session = new Session(getActivity());
                                session.signOut();
                            }
                        });
                        alert.show();

                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_delete_beneficiary));
                        alert.setMessage(error_message);
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (pDialog != null) {
                    pDialog.dismiss();
                }
                if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                } else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getSherlockActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                }
            }
        });
    }

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);

        getSherlockActivity().getActionBar().removeAllTabs();
        getSherlockActivity().getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
    }

    public static byte[] encodeUrlSafe(byte[] data) {
        byte[] encode = Base64.encodeBase64(data);
        for (int i = 0; i < encode.length; i++) {
            if (encode[i] == '+') {
                encode[i] = '-';
            } else if (encode[i] == '=') {
                encode[i] = '_';
            } else if (encode[i] == '/') {
                encode[i] = '~';
            }
        }
        return encode;
    }
}

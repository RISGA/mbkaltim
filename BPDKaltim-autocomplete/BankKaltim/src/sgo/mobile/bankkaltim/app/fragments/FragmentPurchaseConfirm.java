package sgo.mobile.bankkaltim.app.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.telephony.SmsMessage;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.bankkaltim.AppHelper;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.activities.MainActivity;
import sgo.mobile.bankkaltim.app.adapter.PaymentConfirmListAdapter;
import sgo.mobile.bankkaltim.app.ui.dialog.DefinedDialog;
import sgo.mobile.bankkaltim.conf.AplConstants;
import sgo.mobile.bankkaltim.frameworks.math.FormatCurrency;
import sgo.mobile.bankkaltim.frameworks.net.Networks;
import sgo.mobile.bankkaltim.frameworks.net.loopj.android.http.CustomSSLSocketFactory;
import sgo.mobile.bankkaltim.frameworks.session.Session;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by User on 09/10/2014.
 */
public class FragmentPurchaseConfirm extends SherlockFragment {
    String nominal,trx_id,name,biller_id,customer_id,meter_number,id_meter,order_id,productAmount,amount,source_account_no,source_account_name,biller_code,biller_name, product_code,catalog_code,catalog_nominal,menu_biller_code;
    Button btnDone, btnGetOtp;
    EditText txtOtp;
    TextView lbl_member_no,lbl_header,lbl_title;
    private ProgressDialog pDialog;
    JSONObject dataJSON =null;
    String purchaseElectricity= AplConstants.purchaseElectricityVoucher;
    String purchaseVoucherPhone = AplConstants.purchasePhoneVoucher;
    String purchaseOthers = AplConstants.purchaseOthers;
    ListView listData;
    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");//dd/MM/yyyy
    Date now = new Date();
    String dateTime = sdfDate.format(now);
    boolean networks;
    TableRow rowOtp;
    View view_token1,view_token2;
    View line_meter, line_cust_id;
    TableRow tbl_cust_id, tbl_meter;
    TextView lbl_cust_id, lbl_meter;
    public static ArrayList<String> name_arr = new ArrayList<String>();
    public static ArrayList<String> value_arr = new ArrayList<String>();
    PaymentConfirmListAdapter paymentConfirmListAdapter;
    String data;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_purchase_confirm, container, false);

        Bundle bundle = this.getArguments();
        menu_biller_code = bundle.getString("menu_biller_code");
        lbl_header = (TextView)view.findViewById(R.id.label_header);
        lbl_title = (TextView)view.findViewById(R.id.lbl_title);
       /* view_token1 =view.findViewById(R.id.view_token1);
        view_token2 =view.findViewById(R.id.view_token2);
        view_token1.setVisibility(View.GONE);
        view_token2.setVisibility(View.GONE);*/
        lbl_member_no = (TextView)view.findViewById(R.id.lbl_member_no);
        rowOtp = (TableRow)view.findViewById(R.id.rowOtp);
        //listData   = (ListView) view.findViewById(R.id.listData);
        //data=bundle.getString("data");

        if (menu_biller_code.equals(purchaseVoucherPhone)){
            lbl_member_no.setText(getResources().getString(R.string.lbl_purchase_hp_orderid));
            lbl_header.setText(getResources().getString(R.string.lbl_purchase_hp_title));

            source_account_no = bundle.getString("source_account_no");
            source_account_name = bundle.getString("source_account_name");
            product_code = bundle.getString("product_code");
            biller_code = bundle.getString("biller_code");
            biller_name = bundle.getString("biller_name");
            catalog_code = bundle.getString("catalog_code");
            catalog_nominal = bundle.getString("catalog_nominal");
            order_id = bundle.getString("phone_number");
            productAmount = bundle.getString("productAmount");


            TextView lbl_source_account = (TextView)view.findViewById(R.id.lbl_source_account);
            lbl_source_account.setText(source_account_no + "(" + source_account_name + ")");
            TextView lbl_voucher = (TextView)view.findViewById(R.id.lbl_voucher);
            lbl_voucher.setText(biller_name);
            TextView lbl_phone_number = (TextView)view.findViewById(R.id.lbl_phone_number);
            lbl_phone_number.setText(order_id);
            TextView lbl_nominal = (TextView)view.findViewById(R.id.lbl_nominal);
            lbl_nominal.setText(FormatCurrency.DenomIDR(productAmount));

            TableRow tbl_total = (TableRow)view.findViewById(R.id.tbl_total);
            tbl_total.setVisibility(View.VISIBLE);
            TextView lbl_total = (TextView)view.findViewById(R.id.lbl_total);
            lbl_total.setText(FormatCurrency.CurrencyIDR(catalog_nominal));



        }else if(menu_biller_code.equals(purchaseElectricity)){
            lbl_member_no.setText(getResources().getString(R.string.lbl_purchase_electricity_orderid));
            lbl_header.setText(getResources().getString(R.string.lbl_purchase_electricity_title));
            source_account_no = bundle.getString("source_account_no");
            source_account_name = bundle.getString("source_account_name");
            product_code = bundle.getString("product_code");
            biller_id = bundle.getString("biller_code");
            biller_code = bundle.getString("biller_code");
            biller_name = bundle.getString("biller_name");
            order_id = bundle.getString("order_id");
            name = bundle.getString("name");
            catalog_nominal = bundle.getString("amount");
            customer_id = bundle.getString("customer_id");
            meter_number = bundle.getString("meter_number");
            amount= bundle.getString("amount");


            TableRow tbl_meter = (TableRow)view.findViewById(R.id.tbl_meter);
            tbl_meter.setVisibility(View.VISIBLE);
            TextView lbl_source_account = (TextView)view.findViewById(R.id.lbl_source_account);
            lbl_source_account.setText(source_account_no + "(" + source_account_name + ")");
            TextView lbl_voucher = (TextView)view.findViewById(R.id.lbl_voucher);
            lbl_voucher.setText(biller_name);
            TextView lbl_phone_number = (TextView)view.findViewById(R.id.lbl_phone_number);
            lbl_phone_number.setText(order_id);
            TextView lbl_nominal = (TextView)view.findViewById(R.id.lbl_nominal);
            lbl_nominal.setText(FormatCurrency.CurrencyIDR(amount));
            TextView lbl_meter = (TextView)view.findViewById(R.id.lbl_meter);
            lbl_meter.setText(meter_number);

        }


        btnDone = (Button) view.findViewById(R.id.btnDone);
        btnDone.setVisibility(View.GONE);
        btnDone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                final String txtToken = txtOtp.getText().toString();
                if (txtToken.equalsIgnoreCase("")) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setTitle(getResources().getString(R.string.lbl_alert_otp_title));
                    alert.setMessage(getResources().getString(R.string.lbl_otp_validation_message));
                    alert.setPositiveButton("OK", null);
                    alert.show();
                } else {

                    try {
                        networks = new Networks().isConnectingToInternet(getActivity());
                        if (networks) {
                            submitPurchase();
                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_purchase_title));
                            alert.setMessage(getResources().getString(R.string.lbl_alert_connection));
                            alert.setPositiveButton("OK", null);
                            alert.show();

                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }


                }
            }
        });
        btnGetOtp = (Button) view.findViewById(R.id.btnGetOtp);
        txtOtp = (EditText) view.findViewById(R.id.txtOtp);
        txtOtp.setVisibility(View.GONE);



        btnGetOtp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {

                try {
                    networks = new Networks().isConnectingToInternet(getActivity());
                    if (networks) {
                        requestToken();
                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_purchase_title));
                        alert.setMessage(getResources().getString(R.string.lbl_alert_connection));
                        alert.setPositiveButton("OK", null);
                        alert.show();

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }



            }
        });
        return view;
    }
    void clearData(){
        //id.clear();
        name_arr.clear();
        value_arr.clear();
    }
    private static String parseNumber(String s){
        String numberFragment = s.substring(s.lastIndexOf(' '), s.length());//extract number
        return numberFragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        toggleMyBroadcastReceiver(true);
    }

    @Override
    public void onPause() {
        super.onPause();
        toggleMyBroadcastReceiver(false);
    }

    private void toggleMyBroadcastReceiver(Boolean _on){
        if (getActivity() == null)
            return;

        MainActivity fca = (MainActivity) getActivity();
        fca.togglerBroadcastReceiver(_on, myReceiver);
    }

    public BroadcastReceiver myReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle mBundle = intent.getExtras();
            SmsMessage[] mSMS =null;
            String strMessage = "";

            if(mBundle != null){
                Object[] pdus = (Object[]) mBundle.get("pdus");
                mSMS = new SmsMessage[pdus.length];

                for (int i = 0; i < mSMS.length ; i++){
                    mSMS[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                    strMessage += mSMS[i].getMessageBody();

                    final Matcher matcher = Pattern.compile("mTOKEN : ").matcher(strMessage);
                    if(matcher.find()){
                        // System.out.println(sentence.substring(matcher.end()).trim());
                        String res =strMessage.substring(matcher.end()).trim();
                        int lenght = res.length();
                        final String otp = res.substring(0,6);
                        txtOtp.setText(otp);
                    }
                }


            }
        }
    };

    public void requestToken() {
        pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.lbl_loading_process));
        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getActivity()));
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
        RequestParams params = new RequestParams();

        try {
            String userid = AppHelper.getUserId(getActivity());
            String bankcode = AplConstants.Bankcode;
            String pcd = AplConstants.pcd;
            String URL = AplConstants.RequestTokenAPI;
            String appName = AplConstants.appName;
            String serviceName = URL.split("/")[4];
            String apiKey = AppHelper.getAccessKey(getActivity());
            UUID uniqueKey = UUID.randomUUID();

            String message = uniqueKey+dateTime+ appName + serviceName+ bankcode + userid;

            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] hmacData = sha256_HMAC.doFinal(message.getBytes("UTF-8"));
            String signatureKey = new String(encodeUrlSafe(hmacData));
            params.put("userid", userid);
            params.put("bankcode", bankcode);
            params.put("passcode", pcd);
            params.put("rc_uuid", uniqueKey.toString());
            params.put("rc_dtime",dateTime);
            params.put("signature",signatureKey);

        }catch (Exception e){
            System.out.println("Error");
        }

        client.post(AplConstants.RequestTokenAPI, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject object) {


                try {
                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    JSONObject body_data = object.getJSONObject("body_data");
                    // parse json data and store into arraylist variables
                    String error_code = body_data.getString("error_code");
                    String error_message = body_data.getString("error_message");
                    if (error_code.equals("0000")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_alert_otp_title));
                        alert.setMessage(getResources().getString(R.string.lbl_alert_otp_message));
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                btnGetOtp.setVisibility(View.GONE);
                                txtOtp.setVisibility(View.VISIBLE);
                                btnDone.setVisibility(View.VISIBLE);
                                txtOtp.setCursorVisible(true);
                                rowOtp.setVisibility(View.VISIBLE);
                                // view_token1.setVisibility(View.VISIBLE);
                                // view_token2.setVisibility(View.VISIBLE);

                            }
                        });
                        alert.show();


                    }else if(error_code.equalsIgnoreCase("0404")){
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                        alert.setMessage(error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Session session;
                                session = new Session(getActivity());
                                session.signOut();
                            }
                        });
                        alert.show();

                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_alert_otp_title));
                        alert.setMessage(error_message);
                        alert.setPositiveButton("OK", null);
                        alert.show();

                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (pDialog != null) {
                    pDialog.dismiss();
                }
                if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                } else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                }
            }


        });
    }




    private void submitPurchase() {
        pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.lbl_loading_process));
        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getActivity()));
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
        RequestParams params = new RequestParams();

        try {
            String userid = AppHelper.getUserId(getActivity());
            String txtOTP = txtOtp.getText().toString().trim();
            String bankcode = AplConstants.Bankcode;
            String pcd = AplConstants.pcd;
            String URL = AplConstants.BillerPaymentNotif;
            String appName = AplConstants.appName;
            String serviceName = URL.split("/")[4];
            String apiKey = AppHelper.getAccessKey(getActivity());
            UUID uniqueKey = UUID.randomUUID();

            String message = uniqueKey+dateTime+ appName + serviceName+ bankcode + userid;

            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] hmacData = sha256_HMAC.doFinal(message.getBytes("UTF-8"));
            String signatureKey = new String(encodeUrlSafe(hmacData));
            params.put("userid", userid);
            params.put("bankcode", bankcode);
            params.put("passcode", pcd);
            params.put("productcode", product_code);
            params.put("orderid", order_id);
            params.put("amount", catalog_nominal);
            params.put("responsecode", txtOTP);
            params.put("sourceacctno", source_account_no);
            params.put("rc_uuid", uniqueKey.toString());
            params.put("rc_dtime",dateTime);
            params.put("signature",signatureKey);

        }catch (Exception e){
            System.out.println("Error");
        }
        client.post(AplConstants.BillerPaymentNotif, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject object) {


                try {
                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    JSONObject body_data = object.getJSONObject("body_data");
                    String error_code = body_data.getString("error_code");

                    JSONObject header_data = object.getJSONObject("header_data");


                    final String error_message = body_data.getString("error_message");
                    if (error_code.equals("0000")) {

                        final String date = header_data.getString("rs_datetime");
                        JSONObject data = body_data.getJSONObject("data");
                        trx_id = data.getString("psNumber");
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_purchase_alert_title));
                        alert.setMessage(getResources().getString(R.string.lbl_purchase_alert_title)+" "+error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Fragment newFragment = null;
                                newFragment = new FragmentPurchaseReport();
                                Bundle args = new Bundle();
                                args.putString("menu_biller_code", menu_biller_code);
                                args.putString("source_account_name", source_account_name);
                                args.putString("source_account_no", source_account_no);
                                args.putString("biller_code", biller_code);
                                args.putString("biller_name", biller_name);
                                args.putString("catalog_code", catalog_code);
                                args.putString("catalog_nominal", catalog_nominal);
                                args.putString("order_id", order_id);
                                args.putString("trx_id", trx_id);
                                args.putString("date", date);
                                args.putString("error_message",error_message);
                                newFragment.setArguments(args);
                                switchFragment(newFragment);
                            }
                        });
                        alert.show();

                    } else if(error_code.equalsIgnoreCase("0404")){
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                        alert.setMessage(error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Session session;
                                session = new Session(getActivity());
                                session.signOut();
                            }
                        });
                        alert.show();

                    } else if(error_code.equals("0003")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_purchase_alert_title));
                        alert.setMessage(getResources().getString(R.string.lbl_purchase_alert_title) + " " +error_message);
                        alert.setPositiveButton("OK", null);
                        alert.show();

                    }else if(error_code.equals("X001")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_purchase_alert_title));
                        alert.setMessage(getResources().getString(R.string.lbl_purchase_alert_title) + " " + error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Session session;
                                session = new Session(getActivity());
                                session.signOut();
                            }
                        });
                        alert.show();

                    }else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_purchase_alert_title));
                        alert.setMessage(getResources().getString(R.string.lbl_purchase_alert_title)+" "+error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Fragment newFragment = null;
                                newFragment = new FragmentPurchaseReport();
                                Bundle args = new Bundle();
                                args.putString("menu_biller_code", menu_biller_code);
                                args.putString("source_account_name", source_account_name);
                                args.putString("source_account_no", source_account_no);
                                args.putString("biller_code", biller_code);
                                args.putString("biller_name", biller_name);
                                args.putString("catalog_code", catalog_code);
                                args.putString("catalog_nominal", catalog_nominal);
                                args.putString("order_id", order_id);
                                args.putString("error_message",error_message);
                                args.putString("trx_id", trx_id);
                                newFragment.setArguments(args);
                                switchFragment(newFragment);
                            }
                        });
                        alert.show();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (pDialog != null) {
                    pDialog.dismiss();
                }
                if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                } else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                }

                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                alert.setTitle(getResources().getString(R.string.lbl_tab_inhouse));
                alert.setMessage(getResources().getString(R.string.lbl_transfer_exception));
                alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Fragment newFragment = null;
                        newFragment = new FragmentMainTransfer();
                        switchFragment(newFragment);
                    }
                });
                alert.show();
            }

        });
    }

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);

        getSherlockActivity().getActionBar().removeAllTabs();
        getSherlockActivity().getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
    }

    public static byte[] encodeUrlSafe(byte[] data) {
        byte[] encode = Base64.encodeBase64(data);
        for (int i = 0; i < encode.length; i++) {
            if (encode[i] == '+') {
                encode[i] = '-';
            } else if (encode[i] == '=') {
                encode[i] = '_';
            } else if (encode[i] == '/') {
                encode[i] = '~';
            }
        }
        return encode;
    }
}

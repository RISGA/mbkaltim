package sgo.mobile.bankkaltim.app.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.telephony.SmsMessage;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.bankkaltim.AppHelper;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.activities.MainActivity;
import sgo.mobile.bankkaltim.app.ui.dialog.DefinedDialog;
import sgo.mobile.bankkaltim.conf.AplConstants;
import sgo.mobile.bankkaltim.frameworks.math.FormatCurrency;
import sgo.mobile.bankkaltim.frameworks.net.Networks;
import sgo.mobile.bankkaltim.frameworks.net.loopj.android.http.CustomSSLSocketFactory;
import sgo.mobile.bankkaltim.frameworks.session.Session;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class FragmentTransferInHouseConfirmToken extends SherlockFragment {

    String source_account_no, source_account_name, benef_bank_code, benef_account_no, benef_account_name, amount, memo, paymentdate, benef_email;
    Button btnDone, btnGetOtp;
    EditText txtOtp;
    private ProgressDialog pDialog;
    private boolean isExit;
    SimpleCursorAdapter adapter;
    TextView lblMsg, lblNo;
    private String stringToken;
    boolean networks;
    View lineOtp;
    TableRow rowOtp;
    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");//dd/MM/yyyy
    Date now = new Date();
    String dateTime = sdfDate.format(now);
    public String signatureKey="";




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_transfer_inhouse_confirmtoken, container, false);
        final InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(
                Context.INPUT_METHOD_SERVICE);


        Bundle bundle = this.getArguments();
        source_account_name = bundle.getString("source_account_name");
        source_account_no = bundle.getString("source_account_no");
        benef_account_name = bundle.getString("benef_account_name");
        benef_account_no = bundle.getString("benef_account_no");
        benef_bank_code = bundle.getString("dest_bank_code");
        amount = bundle.getString("totalAmount");
        memo = bundle.getString("memo");
        paymentdate = bundle.getString("paymentdate");
        benef_email = bundle.getString("email");

        lineOtp = view.findViewById(R.id.lineOtp);
        rowOtp = (TableRow)view.findViewById(R.id.rowOtp);
        TextView lbl_source_account = (TextView) view.findViewById(R.id.lbl_source_account);
        lbl_source_account.setText(source_account_name);
        TextView lbl_benef_account = (TextView) view.findViewById(R.id.lbl_benef_account);
        lbl_benef_account.setText(benef_account_name);
        TextView lbl_benef_email = (TextView) view.findViewById(R.id.lbl_benef_email);
        lbl_benef_email.setText(benef_email);
        TextView lbl_amount = (TextView) view.findViewById(R.id.lbl_amount);
        lbl_amount.setText(FormatCurrency.CurrencyIDR(amount));
        TextView lbl_message = (TextView) view.findViewById(R.id.lbl_message);
        lbl_message.setText(memo);
        TextView lbl_date = (TextView) view.findViewById(R.id.lbl_date);
        lbl_date.setText(paymentdate);

        btnDone = (Button) view.findViewById(R.id.btnDone);
        btnDone.setVisibility(View.GONE);
        btnDone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                final String txtToken = txtOtp.getText().toString();
                if (txtToken.equalsIgnoreCase("")) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setTitle(getResources().getString(R.string.lbl_tab_inhouse));
                    alert.setMessage(getResources().getString(R.string.lbl_otp_validation_message));
                    alert.setPositiveButton("OK", null);
                    alert.show();
                    return;

                } else {
                    try {
                        networks = new Networks().isConnectingToInternet(getActivity());
                        if (networks) {
                           submitFundTransfer();
                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_tab_inhouse));
                            alert.setMessage(getResources().getString(R.string.lbl_alert_connection));
                            alert.setPositiveButton("OK", null);
                            alert.show();
                            return;
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }

            }
        });

        btnGetOtp = (Button) view.findViewById(R.id.btnGetOtp);
        txtOtp = (EditText) view.findViewById(R.id.txtOtp);
        txtOtp.setVisibility(View.GONE);

        btnGetOtp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                try {
                    networks = new Networks().isConnectingToInternet(getActivity());
                    if (networks) {
                        requestToken();
                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_tab_inhouse));
                        alert.setMessage(getResources().getString(R.string.lbl_alert_connection));
                        alert.setPositiveButton("OK", null);
                        alert.show();

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }


            }
        });


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        toggleMyBroadcastReceiver(true);
    }

    @Override
    public void onPause() {
        super.onPause();
        toggleMyBroadcastReceiver(false);
    }

    private void toggleMyBroadcastReceiver(Boolean _on){
        if (getActivity() == null)
            return;

        MainActivity fca = (MainActivity) getActivity();
        fca.togglerBroadcastReceiver(_on,myReceiver);
    }



    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
        getSherlockActivity().getActionBar().removeAllTabs();
        getSherlockActivity().getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
    }


    public void requestToken() {
        pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog,getResources().getString(R.string.lbl_loading_process));
        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getActivity()));
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
        RequestParams params = new RequestParams();

        try {
            String userid = AppHelper.getUserId(getActivity());
            String bankcode = AplConstants.Bankcode;
            String pcd = AplConstants.pcd;
            String URL = AplConstants.RequestTokenAPI;
            String appName = AplConstants.appName;
            String serviceName = URL.split("/")[4];
            String apiKey = AppHelper.getAccessKey(getActivity());
            UUID uniqueKey = UUID.randomUUID();
            String signature = uniqueKey+dateTime+ appName + serviceName+ bankcode + userid;


            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
            signatureKey = new String(encodeUrlSafe(hmacData));
            params.put("userid", userid);
            params.put("bankcode", bankcode);
            params.put("passcode", pcd);
            params.put("rc_uuid", uniqueKey.toString());
            params.put("rc_dtime", dateTime);
            params.put("signature", signatureKey);

        }catch (Exception e){
            System.out.println("Error");
        }


        client.post(AplConstants.RequestTokenAPI, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject object) {


                try {
                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    JSONObject body_data = object.getJSONObject("body_data");
                    // parse json data and store into arraylist variables
                    String error_code = body_data.getString("error_code");
                    String error_message = body_data.getString("error_message");
                    if (error_code.equals("0000")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_alert_otp_title));
                        alert.setMessage(getResources().getString(R.string.lbl_alert_otp_message));
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                btnGetOtp.setVisibility(View.GONE);
                                txtOtp.setVisibility(View.VISIBLE);
                                btnDone.setVisibility(View.VISIBLE);
                                lineOtp.setVisibility(View.VISIBLE);
                                rowOtp.setVisibility(View.VISIBLE);

                            }
                        });
                        alert.show();


                    } else if(error_code.equalsIgnoreCase("0404")){

                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                        alert.setMessage(error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Session session;
                                session = new Session(getActivity());
                                session.signOut();
                            }
                        });
                        alert.show();

                    }else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_alert_otp_title));
                        alert.setMessage(error_message);
                        alert.setPositiveButton("OK", null);
                        alert.show();

                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (pDialog != null) {
                    pDialog.dismiss();
                }
                if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                } else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                }
            }

        });
    }

   /* private void submitToken() {
        pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, "Authenticating...");

        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
        RequestParams params = new RequestParams();

        try {
            String userid = AppHelper.getUserId(getActivity());
            String bankcode = AplConstants.Bankcode;
            String pcd = AplConstants.pcd;
            String txtOTP = txtOtp.getText().toString().trim();
            String URL = AplConstants.ConfirmTokenAPI;
            String appName = AplConstants.appName;
            String serviceName = URL.split("/")[4];
            String apiKey = AppHelper.getAccessKey(getActivity());
            UUID uniqueKey = UUID.randomUUID();
            String signature = uniqueKey+dateTime+ appName + serviceName+ bankcode + userid;


            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
            String signatureKey = new String(encodeUrlSafe(hmacData));
            params.put("userid", userid);
            params.put("bankcode", bankcode);
            params.put("passcode", pcd);
            params.put("mtoken", txtOTP);
            params.put("rc_uuid", uniqueKey.toString());
            params.put("rc_dtime", dateTime);
            params.put("signature", signatureKey);

        }catch (Exception e){
            System.out.println("Error");
        }



        client.post(AplConstants.ConfirmTokenAPI, params, new AsyncHttpResponseHandler() {
            public void onSuccess(String content) {


                try {

                    if (pDialog != null) {
                        pDialog.dismiss();
                    }

                    JSONObject object = new JSONObject(content);
                    JSONObject body_data = object.getJSONObject("body_data");
                    // parse json data and store into arraylist variables
                    String error_code = body_data.getString("error_code");
                    String error_message = body_data.getString("error_message");

                    if (error_code.equals("0003")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle("Inhouse Transfer");
                        alert.setMessage("Message: " + error_message);
                        alert.setPositiveButton("OK", null);
                        alert.show();

                    }else if(error_code.equals("X001")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle("Inhouse Transfer");
                        alert.setMessage("Message: " + error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Session session;
                                session = new Session(getActivity());
                                session.signOut();
                            }
                        });
                        alert.show();

                    }else if(error_code.equalsIgnoreCase("0404")){
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle("Session Expired");
                        alert.setMessage("Message : " + error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Session session;
                                session = new Session(getActivity());
                                session.signOut();
                            }
                        });
                        alert.show();


                    }else if(error_code.equals("0000")) {
                     submitFundTransfer();
                    }else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle("Submit Token");
                        alert.setMessage("Submit Token : " + error_message);
                        alert.setPositiveButton("OK", null);
                        alert.show();

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            public void onFailure(Throwable error, String content) {
                if (pDialog != null) {
                    pDialog.dismiss();
                }
                Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
            }

        });
    }
*/


    public BroadcastReceiver myReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle mBundle = intent.getExtras();
            SmsMessage[] mSMS =null;
            String strMessage = "";

            if(mBundle != null){
                Object[] pdus = (Object[]) mBundle.get("pdus");
                mSMS = new SmsMessage[pdus.length];

                for (int i = 0; i < mSMS.length ; i++){
                    mSMS[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                    strMessage += mSMS[i].getMessageBody();

                    final Matcher matcher = Pattern.compile("mTOKEN : ").matcher(strMessage);
                    if(matcher.find()){
                        // System.out.println(sentence.substring(matcher.end()).trim());
                        String res =strMessage.substring(matcher.end()).trim();
                        int lenght = res.length();
                        final String otp = res.substring(0,6);
                        txtOtp.setText(otp);
                    }
                }


            }
        }
    };

   private static Map<String, String> parseSmsToken(String s){
       Map<String, String> ret = new HashMap<String, String>();
       StringTokenizer t = new StringTokenizer(s, "\n");
       while (t.hasMoreTokens()){
           String b = t.nextToken().trim();
           if (b.startsWith("mTOKEN : ")){
               String number = parseNumber(b);
               ret.put("token", number);
           }
       }

       return ret;
   }


    private static String parseNumber(String s){
        String numberFragment = s.substring(s.lastIndexOf(' '), s.length());//extract number
        return numberFragment;
    }

    private void submitFundTransfer()
    {
        pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.lbl_loading_process));

        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getActivity()));
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
        RequestParams params   = new RequestParams();

        try {
            String userid = AppHelper.getUserId(getActivity());
            String bankcode = AplConstants.Bankcode;
            String pcd = AplConstants.pcd;
            String executetransferProcess = AplConstants.executetransferProcess;
            String currencyType = AplConstants.CurrencyIDR;
            String trxId = "";
            String URL = AplConstants.FundTransferAPI;
            String appName = AplConstants.appName;
            String serviceName = URL.split("/")[4];
            String apiKey = AppHelper.getAccessKey(getActivity());
            UUID uniqueKey = UUID.randomUUID();
            String signature = uniqueKey+dateTime+ appName + serviceName+ bankcode + userid;

            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
            String signatureKey = new String(encodeUrlSafe(hmacData));
            params.put("userid", userid);
            params.put("bankcode", bankcode);
            params.put("passcode", pcd);
            params.put("sourceacctccy", currencyType);
            params.put("sourceacctno",source_account_no);
            params.put("destaccountno", benef_account_no);
            params.put("destaccountemail", benef_email);
            params.put("transactionamount",amount);
            params.put("transactionid",trxId);
            params.put("description",memo);
            params.put("responsecode", txtOtp.getText().toString());
            params.put("executetransfer", executetransferProcess);
            params.put("rc_uuid", uniqueKey.toString());
            params.put("rc_dtime", dateTime);
            params.put("signature", signatureKey);

        }catch (Exception e){
            System.out.println("Error");
        }
        client.post(AplConstants.FundTransferAPI, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject object) {




                try {

                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    JSONObject body_data = object.getJSONObject("body_data");


                    String error_code         = body_data.getString("error_code");
                    final String error_message      = body_data.getString("error_message");


                    if(error_code.equals("0000")){

                        JSONObject response_data = body_data.getJSONObject("response_data");
                        final String psNumber = response_data.getString("psNumber");
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_transfer_success_inhouse));
                        alert.setMessage(getResources().getString(R.string.lbl_transfer_success_inhouse)+" "+ error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Fragment newFragment = null;
                                newFragment = new FragmentTransferInHouseReport();
                                Bundle args = new Bundle();
                                args.putString("source_account_name", source_account_name);
                                args.putString("source_account_no", source_account_no);
                                args.putString("benef_acount_name", benef_account_name);
                                args.putString("benef_account_no", benef_account_no);
                                args.putString("totalAmount", amount);
                                args.putString("memo", memo);
                                args.putString("paymentdate", paymentdate);
                                args.putString("error_message", error_message);
                                args.putString("email", benef_email);
                                args.putString("ref", psNumber);
                                newFragment.setArguments(args);
                                switchFragment(newFragment);

                            }
                        });
                        alert.show();
                    }else if(error_code.equalsIgnoreCase("0404")){
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                        alert.setMessage(error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Session session;
                                session = new Session(getActivity());
                                session.signOut();
                            }
                        });
                        alert.show();

                    }else if(error_code.equals("0003")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_tab_inhouse));
                        alert.setMessage(getResources().getString(R.string.lbl_transfer_success_inhouse) + " " + error_message);
                        alert.setPositiveButton("OK", null);
                        alert.show();

                    }else if(error_code.equals("X001")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_tab_inhouse));
                        alert.setMessage(getResources().getString(R.string.lbl_transfer_success_inhouse) + " " + error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Session session;
                                session = new Session(getActivity());
                                session.signOut();
                            }
                        });
                        alert.show();

                    }else{
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_tab_inhouse));
                        alert.setMessage(getResources().getString(R.string.lbl_transfer_success_inhouse)+" "+error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Fragment newFragment = null;
                                newFragment = new FragmentMainTransfer();
                                switchFragment(newFragment);
                            }
                        });
                        alert.show();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (pDialog != null) {
                    pDialog.dismiss();
                }
                if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                } else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                }

                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                alert.setTitle(getResources().getString(R.string.lbl_tab_inhouse));
                alert.setMessage(getResources().getString(R.string.lbl_transfer_exception));
                alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Fragment newFragment = null;
                        newFragment = new FragmentMainTransfer();
                        switchFragment(newFragment);
                    }
                });
                alert.show();
            }

        });
    }

    public void exit() {
        if (!isExit) {
            isExit = true;
            Toast.makeText(getActivity(), getResources().getString(R.string.lbl_exit), Toast.LENGTH_SHORT).show();
            mHandler.sendEmptyMessageDelayed(0, 2000);
        } else {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            startActivity(intent);
            System.exit(0);
        }
    }
    public static byte[] encodeUrlSafe(byte[] data) {
        byte[] encode = Base64.encodeBase64(data);
        for (int i = 0; i < encode.length; i++) {
            if (encode[i] == '+') {
                encode[i] = '-';
            } else if (encode[i] == '=') {
                encode[i] = '_';
            } else if (encode[i] == '/') {
                encode[i] = '~';
            }
        }
        return encode;
    }


    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
            isExit = false;
        }

    };

}
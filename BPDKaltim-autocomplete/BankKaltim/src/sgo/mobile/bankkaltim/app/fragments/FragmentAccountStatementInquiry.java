package sgo.mobile.bankkaltim.app.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.bankkaltim.AppHelper;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.activities.MainActivity;
import sgo.mobile.bankkaltim.app.adapter.AccountStatementAdapter;
import sgo.mobile.bankkaltim.conf.AplConstants;
import sgo.mobile.bankkaltim.frameworks.net.Networks;
import sgo.mobile.bankkaltim.frameworks.net.loopj.android.http.CustomSSLSocketFactory;
import sgo.mobile.bankkaltim.frameworks.session.Session;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

/**
 * Created by User on 19/08/2014.
 */
public class FragmentAccountStatementInquiry extends Fragment {
    private ProgressDialog pDialog;
    FragmentManager fm;

    // declare view objects
    ListView listMenu;
    ProgressBar prgLoading;

   public TextView txtAlert;

    AccountStatementAdapter accountStatementAdapter;

    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");//dd/MM/yyyy
    SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");//dd/MM/yyyy
    Date now = new Date();
    String strDate = sdfDate.format(now);
    String rdate= dt.format(now);
    boolean networks;



    // create arraylist variables to store data from server
    /*
    public static ArrayList<String> id = new ArrayList<String>();
    public static ArrayList<String> date = new ArrayList<String>();
    public static ArrayList<String> description = new ArrayList<String>();
    public static ArrayList<String> debet = new ArrayList<String>();
    public static ArrayList<String> credit = new ArrayList<String>();
    public static ArrayList<String> source_account = new ArrayList<String>();
    public static ArrayList<String> source_name = new ArrayList<String>();
    public static ArrayList<String> ccy = new ArrayList<String>();
    */

    public static ArrayList<String> dateTime = new ArrayList<String>();
    public static ArrayList<String> description = new ArrayList<String>();
    public static ArrayList<String> amount = new ArrayList<String>();
    public static ArrayList<String> flag = new ArrayList<String>();
    public static ArrayList<String> ccy = new ArrayList<String>();
    public static ArrayList<String> balance = new ArrayList<String>();

    public static String opening_balance, closing_balance;

    String source_account,account_name,account_no;
    TextView txtHeader, txtTextCredit,txtTextDebit,txtNull,lbl_null;
    TableLayout tbl_info;
    ImageView img_icon_debit, img_icon_credit;
/*
    DateFormat df = new SimpleDateFormat(" dd MMM yyyy");
    public String date = df.format(Calendar.getInstance().getTime());*/



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.fragment_inquiry_trx, container, false);
        prgLoading = (ProgressBar) view.findViewById(R.id.prgLoading);
        listMenu   = (ListView) view.findViewById(R.id.listMenu);
        tbl_info = (TableLayout)view.findViewById(R.id.tbl_info);
        tbl_info.setVisibility(View.GONE);
        txtNull = (TextView)view.findViewById(R.id.txtNull);
        txtAlert = (TextView)view.findViewById(R.id.txtAlert);
        lbl_null = (TextView)view.findViewById(R.id.lbl_null);
       /* tbl_info = (TableLayout)view.findViewById(R.id.tbl_info);
        tbl_info.setVisibility(View.GONE);*/
        /*txtHeader = (TextView)view.findViewById(R.id.label_header);
        txtHeader.setVisibility(View.GONE);*/
      /*  txtTextCredit = (TextView)view.findViewById(R.id.txtTextCredit);
        txtTextCredit.setVisibility(View.GONE);
        txtTextDebit = (TextView)view.findViewById(R.id.txtTextDebit);
        txtTextDebit.setVisibility(View.GONE);

        img_icon_credit = (ImageView) view.findViewById(R.id.img_icon_credit);
        img_icon_credit.setVisibility(View.GONE);
        img_icon_debit = (ImageView) view.findViewById(R.id.img_icon_debit);
        img_icon_debit.setVisibility(View.GONE);*/

        Bundle bundle         = this.getArguments();
        source_account   = bundle.getString("source_account_name");
        account_name = bundle.getString("account_name");
        account_no = bundle.getString("account_no");
       /* FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_left);*/
        accountStatementAdapter = new AccountStatementAdapter(getActivity());
        String bcd = AplConstants.Bankcode;
        if(bcd.equalsIgnoreCase("134")){
            prgLoading.setVisibility(View.GONE);
            listMenu.setVisibility(View.GONE);
            txtAlert.setVisibility(View.GONE);
            txtNull.setVisibility(View.GONE);
            tbl_info.setVisibility(View.GONE);
            lbl_null.setVisibility(View.VISIBLE);
        }else{

            lbl_null.setVisibility(View.GONE);
        }
        try {
            networks = new Networks().isConnectingToInternet(getActivity());
            if (networks) {
                parseJSONData();
            } else {
                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());

                alert.setTitle(getResources().getString(R.string.lbl_account_statement_title));
                alert.setMessage(getResources().getString(R.string.lbl_alert_connection));
                alert.setPositiveButton("OK", null);
                alert.show();

            }
        }catch (Exception e){
            e.printStackTrace();
        }



        return view;
    }

    public void parseJSONData(){

        clearData();
        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getActivity()));
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);

        RequestParams params = new RequestParams();

        Bundle bundle            = this.getArguments();
        final String source_account_no = bundle.getString("source_account");
        final String account_name = bundle.getString("account_name");

      //  String userid = AppHelper.getUserId(getActivity());
       // String bankcode = AplConstants.Bankcode;
        //String passcode = AplConstants.Passcode;
       // String curency = AplConstants.CurrencyIDR;

        final String accNo = FragmentAccountStatement.acc_no.toString();


        try {
            String userid = AppHelper.getUserId(getActivity());
            String bankcode = AplConstants.Bankcode;
            String pcd = AplConstants.pcd;
            String curency = AplConstants.CurrencyIDR;
            String URL = AplConstants.AccountStatementAPI ;
            String appName = AplConstants.appName;
            String serviceName = URL.split("/")[4];
            String apiKey = AppHelper.getAccessKey(getActivity());
            UUID uniqueKey = UUID.randomUUID();

            String message = uniqueKey+rdate+ appName + serviceName+ bankcode + userid;

            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] hmacData = sha256_HMAC.doFinal(message.getBytes("UTF-8"));
            String signatureKey = new String(encodeUrlSafe(hmacData));
            params.put("userid", userid);
            params.put("bankcode", bankcode);
            params.put("passcode", pcd);
            params.put("accountno", account_no);
            params.put("accountccy", curency);
            params.put("datefrom", strDate);
            params.put("dateto", strDate);
            params.put("rc_uuid", uniqueKey.toString());
            params.put("rc_dtime",rdate);
            params.put("signature",signatureKey);

        }catch (Exception e){
            System.out.println("Error");
        }


        client.post(AplConstants.AccountStatementAPI,params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject json) {


                try {
                    JSONObject data = json.getJSONObject("body_data"); // this is the "items: [ ] part
                    String error_code = data.getString("error_code");
                    String error_message = data.getString("error_message");


                    if (error_code.equalsIgnoreCase("0000")) {
                        JSONArray history_data = data.getJSONArray("history");
                        opening_balance = data.getString("opening_balance");
                        closing_balance = data.getString("closing_balance");
                        if (history_data.equals("")) {
                            if (pDialog != null) {
                                pDialog.dismiss();
                            }
                            txtNull.setVisibility(View.VISIBLE);

                        } else {


                            for (int i = 0; i < history_data.length(); i++) {
                                JSONObject object = history_data.getJSONObject(i);

                                dateTime.add(object.getString("dateTime"));
                                description.add(object.getString("description"));
                                amount.add(object.getString("amount"));
                                flag.add(object.getString("flag"));
                                ccy.add(object.getString("ccy"));
                                balance.add(object.getString("balance"));

                            }

                            // when finish parsing, hide progressbar
                            prgLoading.setVisibility(8);

                            // if data available show data on list
                            // otherwise, show alert text

                            if (amount.size() > 0) {
                                tbl_info.setVisibility(View.VISIBLE);
                                listMenu.setVisibility(0);
                                listMenu.setAdapter(accountStatementAdapter);
                            } else {
                                //txtAlert.setVisibility(0);
                                txtNull.setVisibility(View.VISIBLE);
                            }
                        }

                    } else if (error_code.equalsIgnoreCase("0404")) {
                        prgLoading.setVisibility(View.GONE);
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                        alert.setMessage(error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Session session;
                                session = new Session(getActivity());
                                session.signOut();
                            }
                        });
                        alert.show();

                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_account_statement_title));
                        alert.setMessage(getResources().getString(R.string.lbl_alert_sessionexpire_title) + " " + error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Fragment newFragment = null;
                                newFragment = new FragmentAccountStatement();
                                switchFragment(newFragment);
                            }
                        });
                        alert.show();

                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();

                }
            }

            ;



            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (pDialog != null) {
                    pDialog.dismiss();
                }
//                if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
//                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
//                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
//                    alert.setPositiveButton("OK", null);
//                    alert.show();
//                } else {
//                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
//                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
//                    alert.setPositiveButton("OK", null);
//                    alert.show();
//                }
            }

        });
    }

    // clear arraylist variables before used
    void clearData(){
        dateTime.clear();
        description.clear();
        amount.clear();
        flag.clear();
        ccy.clear();
        balance.clear();

}

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
        }

    };

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }
    public static byte[] encodeUrlSafe(byte[] data) {
        byte[] encode = Base64.encodeBase64(data);
        for (int i = 0; i < encode.length; i++) {
            if (encode[i] == '+') {
                encode[i] = '-';
            } else if (encode[i] == '=') {
                encode[i] = '_';
            } else if (encode[i] == '/') {
                encode[i] = '~';
            }
        }
        return encode;
    }

}

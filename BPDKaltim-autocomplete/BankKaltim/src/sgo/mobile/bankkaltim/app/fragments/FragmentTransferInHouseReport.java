package sgo.mobile.bankkaltim.app.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.activities.ActivityLogin;
import sgo.mobile.bankkaltim.app.activities.MainActivity;
import sgo.mobile.bankkaltim.frameworks.math.FormatCurrency;

import java.util.Random;

public class FragmentTransferInHouseReport extends SherlockFragment {
    private ProgressDialog pDialog;
    private ViewPager mPager;

    private static final char[] CHARSET_AZ_09 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();

    String ref, source_account_name,source_account_no,  benef_account_name, benef_account_no, amount, memo, paymentdate,error_message, benef_email;
    Button btnDone;
    private static Spinner cbo_source_account = null;
    private static Spinner cbo_destination_account = null;
    FragmentMainTransfer mainTransfer;
    ActivityLogin activityLogin;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_transfer_inhouse_report, container, false);


        final Bundle bundle         = this.getArguments();
        source_account_name   = bundle.getString("source_account_name");
        source_account_no     = bundle.getString("source_account_no");
        benef_account_name    = bundle.getString("benef_acount_name");
        benef_account_no      = bundle.getString("benef_account_no");
        amount                = bundle.getString("totalAmount");
        memo                  = bundle.getString("memo");
        benef_email           = bundle.getString("email");
        paymentdate           = bundle.getString("paymentdate");
        error_message         = bundle.getString("error_message");
        ref                   = bundle.getString("ref");

        TextView lbl_source_account = (TextView) view.findViewById(R.id.lbl_source_account);
        lbl_source_account.setText(source_account_name);
        TextView lbl_benef_account = (TextView) view.findViewById(R.id.lbl_benef_account);
        lbl_benef_account.setText(benef_account_name);
        TextView lbl_amount        = (TextView) view.findViewById(R.id.lbl_amount);
        lbl_amount.setText(FormatCurrency.CurrencyIDR(amount));
        TextView lbl_message       = (TextView) view.findViewById(R.id.lbl_message);
        lbl_message.setText(memo);
        TextView lbl_date           = (TextView) view.findViewById(R.id.lbl_date);
        lbl_date.setText(paymentdate);
        TextView lbl_status           = (TextView) view.findViewById(R.id.lbl_status);
        lbl_status.setText(error_message);
        TextView lbl_trx_id           = (TextView) view.findViewById(R.id.lbl_trx_id);
        lbl_trx_id.setText(ref);

        btnDone               = (Button) view.findViewById(R.id.btnDone);
        btnDone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Fragment newFragment = null;
                newFragment = new FragmentMainTransfer();
                switchFragment(newFragment);

            }
        });

        return view;
    }



    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);

        getSherlockActivity().getActionBar().removeAllTabs();
        getSherlockActivity().getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
    }


    public static String randomTrxId(char[] characterSet, int length) {
        Random random = new java.security.SecureRandom();
        char[] result = new char[length];
        for (int i = 0; i < result.length; i++) {
            // picks a random index out of character set > random character
            int randomCharIndex = random.nextInt(characterSet.length);
            result[i] = characterSet[randomCharIndex];
        }
        return new String(result);
    }


}
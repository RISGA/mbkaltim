package sgo.mobile.bankkaltim.app.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.activeandroid.query.Select;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.bankkaltim.AppHelper;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.activities.MainActivity;
import sgo.mobile.bankkaltim.app.adapter.PurchaseVoucherPhoneAdapter;
import sgo.mobile.bankkaltim.app.beans.BillerVoucherCatalogBean;
import sgo.mobile.bankkaltim.app.models.purchase.PurchaseVoucherPhoneModel;
import sgo.mobile.bankkaltim.conf.AplConstants;
import sgo.mobile.bankkaltim.frameworks.net.loopj.android.http.CustomSSLSocketFactory;
import sgo.mobile.bankkaltim.frameworks.session.Session;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by SGO-Mobile-DEV on 4/2/2015.
 */

public class FragmentPurchasePhoneVoucher extends SherlockFragment {
    private ProgressDialog pDialog;
    ListView listMenu;
    TextView lbl_header;
    ProgressBar prgLoading;
    TextView txtAlert;
    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");//dd/MM/yyyy
    Date now = new Date();
    String dateTime = sdfDate.format(now);
    boolean networks;
    public String menu_biller_code;

    public static ArrayList<String> billerId = new ArrayList<String>();
    public static ArrayList<String> billerCode = new ArrayList<String>();
    public static ArrayList<String> billerName= new ArrayList<String>();
    public static ArrayList<String> billerInquiry = new ArrayList<String>();
    public static ArrayList<String> productCode = new ArrayList<String>();
    public static ArrayList<String> productName = new ArrayList<String>();
    public static ArrayList<String> productAmount = new ArrayList<String>();

    public static ArrayList<String> pCode = new ArrayList<String>();
    public static ArrayList<String> bid = new ArrayList<String>();
    public static ArrayList<String> pIcon = new ArrayList<String>();
    public static ArrayList<String> stringList = new ArrayList<String>();
    int getCountBiller;
    public  String[] biller_code_arr, biller_name_arr,biller_inquiry_arr, biller_id_arr, product_code_arr, product_name_arr,product_amount_arr;
    public String[] billID;
    private ArrayList<BillerVoucherCatalogBean> BillerVoucherList = new ArrayList<BillerVoucherCatalogBean>();

    public String[] providerCode = new String[] {"1074","1075","1076","1077","1078","1079","1080","1081","1082","1083"};

    public int[] providerIcon = new int[] {R.drawable.ic_telkomsel,
            R.drawable.ic_indosat, R.drawable.ic_xl, R.drawable.ic_esia, R.drawable.ic_smart, R.drawable.ic_esia};

    PurchaseVoucherPhoneAdapter purchaseVoucherPhoneAdapter;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_purchase_phone, container, false);
        prgLoading = (ProgressBar) view.findViewById(R.id.prgLoading);
        getCountBiller = new Select().from(PurchaseVoucherPhoneModel.class).count();
        listMenu   = (ListView) view.findViewById(R.id.listMenu);
        listMenu.setVisibility(View.VISIBLE);
        lbl_header  = (TextView) view.findViewById(R.id.label_header);
        purchaseVoucherPhoneAdapter = new PurchaseVoucherPhoneAdapter(getActivity());
        Bundle bundle = this.getArguments();
        menu_biller_code = bundle.getString("menu_biller_code");

        //ListView listView = (ListView)view.findViewById(R.id.listMenu);
        // final ArrayList<pr> listItems = new ArrayList<String>();

        //List<String> list = Arrays.asList(providerCode);
       /* pCode.clear();
        pCode = Arrays.asList(providerCode);


            if (pCode.size() > 0) {
                prgLoading.setVisibility(View.GONE);
                lbl_header.setVisibility(0);
                listMenu.setVisibility(0);
                listMenu.setAdapter(purchaseVoucherPhoneAdapter);
            } else {
                txtAlert.setVisibility(0);
            }*/



        listMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {
                Fragment newFragment = null;
                newFragment = new FragmentPurchasePhoneGeneral();
                Bundle args = new Bundle();
                args.putString("menu_biller_code", menu_biller_code);
                args.putString("billerCode",bid.get(position));

                newFragment.setArguments(args);
                switchFragment(newFragment);
            }
        });

        parseJSONData();
        return view;
    }


    public void parseJSONData() {
        if(getCountBiller==0) {
            clearData();
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getSherlockActivity()));
            client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
            RequestParams params = new RequestParams();

            try {

                String userid = AppHelper.getUserId(getActivity());
                String bankcode = AplConstants.Bankcode;
                String pcd = AplConstants.pcd;
                String URL = AplConstants.InquiryBiller;
                String appName = AplConstants.appName;
                //String bCode = bid.get(p)
                String serviceName = URL.split("/")[4];
                String apiKey = AppHelper.getAccessKey(getActivity());
                UUID uniqueKey = UUID.randomUUID();
                String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;
                Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
                SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
                sha256_HMAC.init(secret_key);
                byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
                String signatureKey = new String(encodeUrlSafe(hmacData));
                params.put("userid", userid);
                params.put("bankcode", bankcode);
                params.put("passcode", pcd);
                params.put("categorycode", menu_biller_code);
                //params.put("billercode", bid.get(position));
                params.put("rc_uuid", uniqueKey.toString());
                params.put("rc_dtime", dateTime);
                params.put("signature", signatureKey);

            } catch (Exception e) {
                System.out.println("Error");
            }

            client.post(AplConstants.InquiryBiller, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject object) {


                    try {
                        if (pDialog != null) {
                            pDialog.dismiss();
                        }
                        JSONObject body_data = object.getJSONObject("body_data");
                        String error_code = body_data.getString("error_code");
                        String error_message = body_data.getString("error_message");

                        if (error_code.equalsIgnoreCase("0000")) {
                            JSONArray data = body_data.getJSONArray("biller_data");

                            for (int i = 0; i < data.length(); i++) {

                                JSONObject objects = data.getJSONObject(i);
                                billerId.add(objects.getString("billerId"));
                                billerCode.add(objects.getString("billerCode"));
                                billerName.add(objects.getString("billerName"));
                                billerInquiry.add(objects.getString("billerInquiry"));
                                productCode.add(objects.getString("productCode"));
                                productName.add(objects.getString("productName"));
                                productAmount.add(objects.getString("productAmount"));

                                PurchaseVoucherPhoneModel purchaseVoucherPhoneModel = new PurchaseVoucherPhoneModel();
                                purchaseVoucherPhoneModel.bilerId =data.getJSONObject(i).getString("billerId");
                                purchaseVoucherPhoneModel.billerCode = data.getJSONObject(i).getString("billerCode");
                                purchaseVoucherPhoneModel.billerName = data.getJSONObject(i).getString("billerName");
                                purchaseVoucherPhoneModel.billerInquiry = data.getJSONObject(i).getString("billerInquiry");
                                purchaseVoucherPhoneModel.productCode = data.getJSONObject(i).getString("productCode");
                                purchaseVoucherPhoneModel.productName = data.getJSONObject(i).getString("productName");
                                purchaseVoucherPhoneModel.productAmount =data.getJSONObject(i).getString("productAmount");

                                purchaseVoucherPhoneModel.save();

                            }
                            prgLoading.setVisibility(8);
                            //if(billerId.size()>0){
                            /*String [] bid = billerId.toArray(new String[billerId.size()]);
                            for (String input : bid) {

                                System.out.println("After removing duplicates   : " + Arrays.toString(removeDuplicates(input)));
                            }*/


                            pCode.addAll(billerCode);
                            HashSet<String> hashSet = new HashSet<String>(pCode);
                            List<String> arrayList2 = new ArrayList<String>(hashSet);
                            for (Object item : arrayList2)
                                bid.add(item.toString());
                            if (bid.size() > 0) {
                                prgLoading.setVisibility(View.GONE);
                                lbl_header.setVisibility(0);
                                listMenu.setVisibility(0);
                                listMenu.setAdapter(purchaseVoucherPhoneAdapter);
                            } else {
                                txtAlert.setVisibility(0);
                            }

                        } else if (error_code.equalsIgnoreCase("0404")) {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                            alert.setMessage(error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Session session;
                                    session = new Session(getActivity());
                                    session.signOut();
                                }
                            });
                            alert.show();

                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_purchase_alert_title));
                            alert.setMessage(getResources().getString(R.string.lbl_purchase_alert_title) + " " + error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Fragment newFragment = null;
                                    newFragment = new FragmentPurchaseGeneral();
                                    switchFragment(newFragment);
                                }
                            });
                            alert.show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    }
                }
            });
        }else{
            clearData();
            List<PurchaseVoucherPhoneModel> itemList = new Select().all().from(PurchaseVoucherPhoneModel.class).execute();

            for (int i=0 ; i<itemList.size();i++) {

                billerId.add(itemList.get(i).getBilerId());
                billerCode.add(itemList.get(i).getBillerCode());
                billerName.add(itemList.get(i).getBillerName());
                billerInquiry.add(itemList.get(i).getBillerInquiry());
                productCode.add(itemList.get(i).getProductCode());
                productName.add(itemList.get(i).getProductName());
                productAmount.add(itemList.get(i).getProductAmount());


            }
            pCode.addAll(billerCode);
            HashSet<String> hashSet = new HashSet<String>(pCode);
            List<String> arrayList2 = new ArrayList<String>(hashSet);
            for (Object item : arrayList2)
                bid.add(item.toString());

            if (billerCode.size() > 0) {
                prgLoading.setVisibility(View.GONE);
                lbl_header.setVisibility(0);
                listMenu.setVisibility(0);
                listMenu.setAdapter(purchaseVoucherPhoneAdapter);
            } else {
                txtAlert.setVisibility(0);
            }

        }

    }


    void clearData(){
        pCode.clear();
        bid.clear();
        billerId.clear();
        billerCode.clear();
        billerName.clear();
        billerInquiry.clear();
        productCode.clear();
        productName.clear();
        productAmount.clear();
    }

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);

        getSherlockActivity().getActionBar().removeAllTabs();
        getSherlockActivity().getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
    }
    public static byte[] encodeUrlSafe(byte[] data) {
        byte[] encode = Base64.encodeBase64(data);
        for (int i = 0; i < encode.length; i++) {
            if (encode[i] == '+') {
                encode[i] = '-';
            } else if (encode[i] == '=') {
                encode[i] = '_';
            } else if (encode[i] == '/') {
                encode[i] = '~';
            }
        }
        return encode;
    }
}

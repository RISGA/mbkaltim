package sgo.mobile.bankkaltim.app.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.activeandroid.query.Select;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.bankkaltim.AppHelper;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.activities.MainActivity;
import sgo.mobile.bankkaltim.app.adapter.Purchase.*;
import sgo.mobile.bankkaltim.app.adapter.SourceAccountAdapters;
import sgo.mobile.bankkaltim.app.beans.BillerVoucherNominalBean;
import sgo.mobile.bankkaltim.app.models.SourceAccountModel;
import sgo.mobile.bankkaltim.app.models.purchase.*;
import sgo.mobile.bankkaltim.app.ui.dialog.DefinedDialog;
import sgo.mobile.bankkaltim.conf.AplConstants;
import sgo.mobile.bankkaltim.frameworks.net.Networks;
import sgo.mobile.bankkaltim.frameworks.net.loopj.android.http.CustomSSLSocketFactory;
import sgo.mobile.bankkaltim.frameworks.session.Session;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by SGO-Mobile-DEV on 4/23/2015.
 */
public class FragmentPurchasePhoneGeneral extends SherlockFragment {
    private static Spinner cbo_source_account = null;
    public String[] account_no_arr, account_name_arr, ccy_id_arr;
    private ArrayList<SourceAccountModel> SourceAcccountList = new ArrayList<SourceAccountModel>();

    private static Spinner cbo_voucher_name = null;
    public String[] biller_code_arr, biller_name_arr, biller_inquiry_arr, biller_id_arr, product_code_arr, product_name_arr, product_amount_arr;
    private ArrayList<PurchaseCatalogPhoneModel> BillerVoucherList = new ArrayList<PurchaseCatalogPhoneModel>();
    private ArrayList<PurchaseVoucherKartuAsModel> BillerVoucherListKartuAs = new ArrayList<PurchaseVoucherKartuAsModel>();
    private ArrayList<PurchaseVoucherEsiaModel> BillerVoucherListEsia = new ArrayList<PurchaseVoucherEsiaModel>();
    private ArrayList<PurchaseVoucherBoltModel> BillerVoucherListBolt = new ArrayList<PurchaseVoucherBoltModel>();
    private ArrayList<PurchaseVoucherSimpatiModel> BillerVoucherListSimpati = new ArrayList<PurchaseVoucherSimpatiModel>();
    private ArrayList<PurchaseVoucherIm3SmartModel> BillerVoucherListIm3Smart = new ArrayList<PurchaseVoucherIm3SmartModel>();
    private ArrayList<PurchaseVoucherSmartfrenModel> BillerVoucherListSmartfren = new ArrayList<PurchaseVoucherSmartfrenModel>();
    private ArrayList<PurchaseVoucherMentariModel> BillerVoucherListMentari = new ArrayList<PurchaseVoucherMentariModel>();
    private ArrayList<PurchaseVoucherThreeModel> BillerVoucherListThree = new ArrayList<PurchaseVoucherThreeModel>();
    private ArrayList<PurchaseVoucherXlModel> BillerVoucherListXl = new ArrayList<PurchaseVoucherXlModel>();

    private static Spinner cbo_voucher_nominal = null;
    public String[] catalog_code_arr, catalog_nominal_arr;
    private ArrayList<BillerVoucherNominalBean> VoucherNominalList = new ArrayList<BillerVoucherNominalBean>();
    public String phoneNumber;
    private ProgressDialog pDialog;
    public String userid, bankcode, passcode, type, menu_biller_code,billerCCode;
    Button btnDone, btnBack;
    EditText inpPhoneNumber;
    TextView lbl_header, lbl_title, lbl_note;
    ImageView ic_img;
    String purchaseElectricity = AplConstants.purchaseElectricityVoucher;
    String purchaseVoucherPhone = AplConstants.purchasePhoneVoucher;
    String purchaseOthers = AplConstants.purchaseOthers;

    public String smartfrenId ="1080";
    public String telkomselId="1074";
    public String indosatId="1075";
    public String indosatInternetId="1075";
    public String indosatSmsId="1075";
    public String xlId="1075";
    int getCountSourceAccount, getCountCatalog;
    int getCountAs, getCountSimpati, getCountEsia, getCountMentari, getCountIm3Smart, getCountSmarften, getCountBolt, getCountThree, getCountXl;
    public String[] denomTelkomselRegular={"5000","10000","20000","25000","50000","10000"};
    public String dataSourceAccount="";
    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");//dd/MM/yyyy
    Date now = new Date();
    String dateTime = sdfDate.format(now);
    boolean networks;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_purchase_general_phone, container, false);
        Bundle bundle = this.getArguments();
        getCountSourceAccount =  new Select().from(SourceAccountModel.class).count();
        getCountXl =  new Select().from(PurchaseVoucherXlModel.class).count();
        getCountAs =  new Select().from(PurchaseVoucherKartuAsModel.class).count();
        getCountSmarften =  new Select().from(PurchaseVoucherSmartfrenModel.class).count();
        getCountSimpati=  new Select().from(PurchaseVoucherSimpatiModel.class).count();
        getCountMentari =  new Select().from(PurchaseVoucherMentariModel.class).count();
        getCountBolt =  new Select().from(PurchaseVoucherBoltModel.class).count();
        getCountEsia =  new Select().from(PurchaseVoucherEsiaModel.class).count();
        getCountIm3Smart =  new Select().from(PurchaseVoucherIm3SmartModel.class).count();
        getCountThree =  new Select().from(PurchaseVoucherThreeModel.class).count();
       // getCountNominal =  new Select().from(PurchaseNominalPhoneModel.class).count();
        menu_biller_code = bundle.getString("menu_biller_code");
        billerCCode = bundle.getString("billerCode");
       // Toast.makeText(getActivity(),billerCode, Toast.LENGTH_LONG).show();
        //Toast.makeText(getActivity(), billerCode, Toast.LENGTH_LONG).show();

        cbo_source_account = (Spinner) view.findViewById(R.id.cbo_source_account);
        cbo_voucher_name = (Spinner) view.findViewById(R.id.cbo_voucher);
        // cbo_voucher_nominal = (Spinner) view.findViewById(R.id.cbo_voucher_nominal);
        inpPhoneNumber = (EditText) view.findViewById(R.id.inpPhoneNumber);
        lbl_header = (TextView) view.findViewById(R.id.label_header);
        ic_img = (ImageView) view.findViewById(R.id.ic_img);
        lbl_title = (TextView) view.findViewById(R.id.lbl_title);
       // lbl_note = (TextView) view.findViewById(R.id.label_note);

        lbl_title.setText("ID Meter");
        lbl_title.setText("Handphone No");
        ic_img.setImageResource(R.drawable.ic_mobile);
        inpPhoneNumber.setHint("* Input Nandphone No");
       // lbl_note.setText("*min 10 digits, max 16 digits");


        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(inpPhoneNumber.getWindowToken(), 0);

        btnDone = (Button) view.findViewById(R.id.btnDone);
        btnDone.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                try {
                    final String txtMemberNo = inpPhoneNumber.getText().toString();
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    if (txtMemberNo.equals("")) {
                        alert.setTitle(getResources().getString(R.string.lbl_purchase_title));
                        alert.setMessage(getResources().getString(R.string.lbl_purchase_hp_orderid_alert));
                        alert.setPositiveButton("OK", null);
                        alert.show();
                        return;
                    }else if(txtMemberNo.length() <  10 && txtMemberNo.length() > 16){

                        alert.setTitle(getResources().getString(R.string.lbl_purchase_title));
                        alert.setMessage(getResources().getString(R.string.lbl_purchase_hp_orderid_alert_incorect));
                        alert.setPositiveButton("OK", null);
                        alert.show();
                        return;

                    }else{
                        try {
                            networks = new Networks().isConnectingToInternet(getActivity());
                            if (networks) {
                               // Toast.makeText(getActivity(), "Sorry, this process will available soon !", Toast.LENGTH_LONG).show();
                                 getPurchase();
                            } else {
                                AlertDialog.Builder alerts = new AlertDialog.Builder(getActivity());
                                alerts.setTitle(getResources().getString(R.string.lbl_purchase_title));
                                alerts.setMessage(getResources().getString(R.string.lbl_alert_connection));
                                alerts.setPositiveButton("OK", null);
                                alerts.show();

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });
        btnBack = (Button) view.findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Fragment newFragment = null;
                newFragment = new FragmentPurchasePhoneVoucher();
                Bundle args = new Bundle();
                args.putString("menu_biller_code", menu_biller_code);
                newFragment.setArguments(args);
                switchFragment(newFragment);

            }
        });


        //
        // Toast.makeText(getActivity(),menu_biller_code, Toast.LENGTH_LONG).show();
        checkMenuCode();
        initSourceAccount();
      //  initNominal();
        return view;

    }


    public void getPurchase() {
        SourceAccountModel sourceAccountBean = (SourceAccountModel) cbo_source_account.getSelectedItem();
        String source_account_no = sourceAccountBean.account_no;
        String source_account_name = sourceAccountBean.account_name;

//        PurchaseCatalogPhoneModel billerVoucherCatalogBean = (PurchaseCatalogPhoneModel) cbo_voucher_name.getSelectedItem();
//        String vourcherCode = billerVoucherCatalogBean.billerCode;
//        String voucherName = billerVoucherCatalogBean.billerName;
//        String productCode = billerVoucherCatalogBean.productCode;
//        String productAmount = billerVoucherCatalogBean.productAmount;
//        String inquiry = billerVoucherCatalogBean.billerInquiry;
        String vourcherCode=null;
        String voucherName=null;
        String productCode=null;
        String productAmount=null;
        String inquiry = null;

        if(billerCCode.equalsIgnoreCase("TSIM_PRE")){
           PurchaseVoucherSimpatiModel purchaseVoucherSimpatiModel = (PurchaseVoucherSimpatiModel)cbo_voucher_name.getSelectedItem();
            vourcherCode = purchaseVoucherSimpatiModel.billerCode;
            voucherName = purchaseVoucherSimpatiModel.billerName;
            productCode = purchaseVoucherSimpatiModel.productCode;
            productAmount = purchaseVoucherSimpatiModel.productAmount;
            inquiry = purchaseVoucherSimpatiModel.billerInquiry;
        }else if(billerCCode.equalsIgnoreCase("XLR_PRE")){
            PurchaseVoucherXlModel purchaseVoucherXlModel = (PurchaseVoucherXlModel)cbo_voucher_name.getSelectedItem();
            vourcherCode = purchaseVoucherXlModel.billerCode;
            voucherName = purchaseVoucherXlModel.billerName;
            productCode = purchaseVoucherXlModel.productCode;
            productAmount = purchaseVoucherXlModel.productAmount;
            inquiry = purchaseVoucherXlModel.billerInquiry;
        }else if(billerCCode.equalsIgnoreCase("SMR_PRE")){
            PurchaseVoucherSmartfrenModel purchaseVoucherSmartfrenModel = (PurchaseVoucherSmartfrenModel)cbo_voucher_name.getSelectedItem();
            vourcherCode = purchaseVoucherSmartfrenModel.billerCode;
            voucherName = purchaseVoucherSmartfrenModel.billerName;
            productCode = purchaseVoucherSmartfrenModel.productCode;
            productAmount = purchaseVoucherSmartfrenModel.productAmount;
            inquiry = purchaseVoucherSmartfrenModel.billerInquiry;
        }else if(billerCCode.equalsIgnoreCase("ESA_PRE")){
            PurchaseVoucherEsiaModel purchaseVoucherEsiaModel = (PurchaseVoucherEsiaModel)cbo_voucher_name.getSelectedItem();
            vourcherCode = purchaseVoucherEsiaModel.billerCode;
            voucherName = purchaseVoucherEsiaModel.billerName;
            productCode = purchaseVoucherEsiaModel.productCode;
            productAmount = purchaseVoucherEsiaModel.productAmount;
            inquiry = purchaseVoucherEsiaModel.billerInquiry;
        }else if(billerCCode.equalsIgnoreCase("TRE_PRE")){
            PurchaseVoucherThreeModel purchaseVoucherThreeModel = (PurchaseVoucherThreeModel)cbo_voucher_name.getSelectedItem();
            vourcherCode = purchaseVoucherThreeModel.billerCode;
            voucherName = purchaseVoucherThreeModel.billerName;
            productCode = purchaseVoucherThreeModel.productCode;
            productAmount = purchaseVoucherThreeModel.productAmount;
            inquiry = purchaseVoucherThreeModel.billerInquiry;
        }else if(billerCCode.equalsIgnoreCase("IM3_PHONE")){
            PurchaseVoucherIm3SmartModel purchaseVoucherIm3SmartModel = (PurchaseVoucherIm3SmartModel)cbo_voucher_name.getSelectedItem();
            vourcherCode = purchaseVoucherIm3SmartModel.billerCode;
            voucherName = purchaseVoucherIm3SmartModel.billerName;
            productCode = purchaseVoucherIm3SmartModel.productCode;
            productAmount = purchaseVoucherIm3SmartModel.productAmount;
            inquiry = purchaseVoucherIm3SmartModel.billerInquiry;
        }else if(billerCCode.equalsIgnoreCase("MNT_PHONE")){
            PurchaseVoucherMentariModel purchaseVoucherMentariModel = (PurchaseVoucherMentariModel)cbo_voucher_name.getSelectedItem();
            vourcherCode = purchaseVoucherMentariModel.billerCode;
            voucherName = purchaseVoucherMentariModel.billerName;
            productCode = purchaseVoucherMentariModel.productCode;
            productAmount = purchaseVoucherMentariModel.productAmount;
            inquiry = purchaseVoucherMentariModel.billerInquiry;
        }else if(billerCCode.equalsIgnoreCase("TSAS_PRE")){
            PurchaseVoucherKartuAsModel purchaseVoucherKartuAsModel = (PurchaseVoucherKartuAsModel)cbo_voucher_name.getSelectedItem();
            vourcherCode = purchaseVoucherKartuAsModel.billerCode;
            voucherName = purchaseVoucherKartuAsModel.billerName;
            productCode = purchaseVoucherKartuAsModel.productCode;
            productAmount = purchaseVoucherKartuAsModel.productAmount;
            inquiry = purchaseVoucherKartuAsModel.billerInquiry;
        }else if(billerCCode.equalsIgnoreCase("BOLT_PRE")){
            PurchaseVoucherBoltModel purchaseVoucherBoltModel  = (PurchaseVoucherBoltModel)cbo_voucher_name.getSelectedItem();
            vourcherCode = purchaseVoucherBoltModel.billerCode;
            voucherName = purchaseVoucherBoltModel.billerName;
            productCode = purchaseVoucherBoltModel.productCode;
            productAmount = purchaseVoucherBoltModel.productAmount;
            inquiry = purchaseVoucherBoltModel.billerInquiry;
        }


        phoneNumber = inpPhoneNumber.getText().toString();

        if(inquiry.equalsIgnoreCase("Y")){
            checkingBiller();
        }else{
            Fragment newFragment = null;
            newFragment = new FragmentPurchaseConfirm();
            Bundle args = new Bundle();
            args.putString("menu_biller_code", menu_biller_code);
            args.putString("source_account_no", source_account_no);
            args.putString("source_account_name", source_account_name);
            args.putString("product_code", productCode);
            args.putString("biller_code", vourcherCode);
            args.putString("biller_name", voucherName);
            args.putString("catalog_code", productCode);
            args.putString("catalog_nominal", productAmount);
            args.putString("phone_number", phoneNumber);

            newFragment.setArguments(args);
            switchFragment(newFragment);
        }




    }

    public void checkingBiller() {

        SourceAccountModel sourceAccountBean = (SourceAccountModel) cbo_source_account.getSelectedItem();
        final String source_account_no = sourceAccountBean.account_no;
        final String source_account_name = sourceAccountBean.account_name;

//        PurchaseCatalogPhoneModel billerVoucherCatalogBean = (PurchaseCatalogPhoneModel) cbo_voucher_name.getSelectedItem();
//        final String billerCode = billerVoucherCatalogBean.billerCode;
//        final String productCode = billerVoucherCatalogBean.productCode;
//        final String productAmount = billerVoucherCatalogBean.productAmount;

        String billerCode=null;
        String productCode=null;
        String productAmount=null;

        if(billerCCode.equalsIgnoreCase("TSIM_PRE")){
            PurchaseVoucherSimpatiModel purchaseVoucherSimpatiModel = (PurchaseVoucherSimpatiModel)cbo_voucher_name.getSelectedItem();
            billerCode = purchaseVoucherSimpatiModel.billerCode;
            productCode = purchaseVoucherSimpatiModel.productCode;
            productAmount = purchaseVoucherSimpatiModel.productAmount;
        }else if(billerCCode.equalsIgnoreCase("XLR_PRE")){
            PurchaseVoucherXlModel purchaseVoucherXlModel = (PurchaseVoucherXlModel)cbo_voucher_name.getSelectedItem();
            billerCode = purchaseVoucherXlModel.billerCode;
            productCode = purchaseVoucherXlModel.productCode;
            productAmount = purchaseVoucherXlModel.productAmount;
        }else if(billerCCode.equalsIgnoreCase("SMR_PRE")){
            PurchaseVoucherSmartfrenModel purchaseVoucherSmartfrenModel = (PurchaseVoucherSmartfrenModel)cbo_voucher_name.getSelectedItem();
            billerCode = purchaseVoucherSmartfrenModel.billerCode;
            productCode = purchaseVoucherSmartfrenModel.productCode;
            productAmount = purchaseVoucherSmartfrenModel.productAmount;
        }else if(billerCCode.equalsIgnoreCase("ESA_PRE")){
            PurchaseVoucherEsiaModel purchaseVoucherEsiaModel = (PurchaseVoucherEsiaModel)cbo_voucher_name.getSelectedItem();
            billerCode = purchaseVoucherEsiaModel.billerCode;
            productCode = purchaseVoucherEsiaModel.productCode;
            productAmount = purchaseVoucherEsiaModel.productAmount;
        }else if(billerCCode.equalsIgnoreCase("TRE_PRE")){
            PurchaseVoucherThreeModel purchaseVoucherThreeModel = (PurchaseVoucherThreeModel)cbo_voucher_name.getSelectedItem();
            billerCode = purchaseVoucherThreeModel.billerCode;
            productCode = purchaseVoucherThreeModel.productCode;
            productAmount = purchaseVoucherThreeModel.productAmount;
        }else if(billerCCode.equalsIgnoreCase("IM3_PHONE")){
            PurchaseVoucherIm3SmartModel purchaseVoucherIm3SmartModel = (PurchaseVoucherIm3SmartModel)cbo_voucher_name.getSelectedItem();
            billerCode = purchaseVoucherIm3SmartModel.billerCode;
            productCode = purchaseVoucherIm3SmartModel.productCode;
            productAmount = purchaseVoucherIm3SmartModel.productAmount;
        }else if(billerCCode.equalsIgnoreCase("MNT_PHONE")){
            PurchaseVoucherMentariModel purchaseVoucherMentariModel = (PurchaseVoucherMentariModel)cbo_voucher_name.getSelectedItem();
            billerCode = purchaseVoucherMentariModel.billerCode;
            productCode = purchaseVoucherMentariModel.productCode;
            productAmount = purchaseVoucherMentariModel.productAmount;
        }else if(billerCCode.equalsIgnoreCase("TSAS_PRE")){
            PurchaseVoucherKartuAsModel purchaseVoucherKartuAsModel = (PurchaseVoucherKartuAsModel)cbo_voucher_name.getSelectedItem();
            billerCode = purchaseVoucherKartuAsModel.billerCode;
            productCode = purchaseVoucherKartuAsModel.productCode;
            productAmount = purchaseVoucherKartuAsModel.productAmount;
        }else if(billerCCode.equalsIgnoreCase("BOLT_PRE")){
            PurchaseVoucherBoltModel purchaseVoucherBoltModel  = (PurchaseVoucherBoltModel)cbo_voucher_name.getSelectedItem();
            billerCode = purchaseVoucherBoltModel.billerCode;
            productCode = purchaseVoucherBoltModel.productCode;
            productAmount = purchaseVoucherBoltModel.productAmount;
        }
        pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.lbl_loading_process));

        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getActivity()));
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
        final RequestParams params = new RequestParams();
        phoneNumber = inpPhoneNumber.getText().toString();
        try {
            String userid = AppHelper.getUserId(getActivity());
            String bankcode = AplConstants.Bankcode;
            String pcd = AplConstants.pcd;
            String URL = AplConstants.InquiryBillerPayment;
            String appName = AplConstants.appName;
            String serviceName = URL.split("/")[4];
            String apiKey = AppHelper.getAccessKey(getActivity());
            UUID uniqueKey = UUID.randomUUID();
            String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;
            //Toast.makeText(getActivity(), userid + " " + apiKey, Toast.LENGTH_LONG).show();

            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
            String signatureKey = new String(encodeUrlSafe(hmacData));
            params.put("userid", userid);
            params.put("bankcode", bankcode);
            params.put("passcode", pcd);
            params.put("productcode",productCode);
            params.put("orderid", phoneNumber);
            params.put("rc_uuid", uniqueKey.toString());
            params.put("rc_dtime", dateTime);
            params.put("signature", signatureKey);

        } catch (Exception e) {
            System.out.println("Error");
        }
        final String finalProductCode = productCode;
        final String finalProductAmount = productAmount;
        client.post(AplConstants.InquiryBillerPayment, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject object) {


                try {

                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    JSONObject body_data = object.getJSONObject("body_data");
                    String error_code = body_data.getString("error_code");
                    String error_message = body_data.getString("error_message");
                    if(error_code.equalsIgnoreCase("0000")){
                        JSONObject biller_data = body_data.getJSONObject("biller_data");

                        String orderId = biller_data.getString("orderId");
                        String amount = biller_data.getString("amount");
                        String billerId = biller_data.getString("billerId");
                        String billerCode = biller_data.getString("billerCode");
                        final String billerName = biller_data.getString("billerName");
                        String description = biller_data.getString("description");
                        String denomination = biller_data.getString("denomination");
                        final String price = biller_data.getString("price");


                                Fragment newFragment = null;
                                newFragment = new FragmentPurchaseConfirm();
                                Bundle args = new Bundle();
                                args.putString("menu_biller_code", menu_biller_code);
                                args.putString("source_account_no", source_account_no);
                                args.putString("source_account_name", source_account_name);
                                args.putString("product_code", finalProductCode);
                                args.putString("biller_code", billerCode);
                                args.putString("biller_name", billerName);
                                args.putString("catalog_code", finalProductCode);
                                args.putString("catalog_nominal", price);
                                args.putString("phone_number", phoneNumber);
                                args.putString("productAmount", finalProductAmount);
                                newFragment.setArguments(args);
                                switchFragment(newFragment);


                    } else if(error_code.equalsIgnoreCase("0404")){
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                        alert.setMessage(error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Session session;
                                session = new Session(getActivity());
                                session.signOut();
                            }
                        });
                        alert.show();

                    }else if(error_code.equalsIgnoreCase("14")){
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_purchase_title));
                        alert.setMessage("Please Input Biller Number Correctly !!");
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Fragment newFragment = null;
                                newFragment = new FragmentPaymentGeneral();
                                Bundle args = new Bundle();
                                args.putString("menu_biller_code", menu_biller_code);
                                newFragment.setArguments(args);
                                switchFragment(newFragment);
                            }
                        });
                        alert.show();
                    }else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_purchase_alert_title));
                        alert.setMessage(getResources().getString(R.string.lbl_purchase_alert_title)+" "+ error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Fragment newFragment = null;
                                newFragment = new FragmentPurchasePhoneVoucher();
                                Bundle args = new Bundle();
                                args.putString("menu_biller_code", menu_biller_code);
                                newFragment.setArguments(args);
                                switchFragment(newFragment);
                            }
                        });
                        alert.show();

                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (pDialog != null) {
                    pDialog.dismiss();
                }
                if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                } else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                }
            }
        });
    }


    private void initSourceAccount() {
        SourceAcccountList.clear();
        dataSourceAccount = AppHelper.getSourceAccount(getSherlockActivity());
        if(dataSourceAccount.equals("")){
            SourceAcccountList.clear();
            pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.lbl_loading_data));

            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getActivity()));
            client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
            RequestParams params = new RequestParams();

            try {
                String userid = AppHelper.getUserId(getActivity());
                String bankcode = AplConstants.Bankcode;
                String pcd = AplConstants.pcd;
                String URL = AplConstants.SourceAccountListAPI;
                String appName = AplConstants.appName;
                String serviceName = URL.split("/")[4];
                String apiKey = AppHelper.getAccessKey(getActivity());
                UUID uniqueKey = UUID.randomUUID();
                String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;
                //Toast.makeText(getActivity(), userid + " " + apiKey, Toast.LENGTH_LONG).show();

                Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
                SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
                sha256_HMAC.init(secret_key);
                byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
                String signatureKey = new String(encodeUrlSafe(hmacData));
                params.put("userid", userid);
                params.put("bankcode", bankcode);
                params.put("passcode", pcd);
                params.put("rc_uuid", uniqueKey.toString());
                params.put("rc_dtime", dateTime);
                params.put("signature", signatureKey);

            } catch (Exception e) {
                System.out.println("Error");
            }
            client.post(AplConstants.SourceAccountListAPI, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject object) {



                    try {

                        if (pDialog != null) {
                            pDialog.dismiss();
                        }
                        JSONObject body_data = object.getJSONObject("body_data");
                        // parse json data and store into arraylist variables
                        String error_code = body_data.getString("error_code");
                        String error_message = body_data.getString("error_message");
                        JSONArray data = body_data.getJSONArray("source_account_data"); // this is the "items: [ ] part

                        if (error_code.equalsIgnoreCase("0000")) {

                            int len = data.length();
                            account_no_arr = new String[len];
                            account_name_arr = new String[len];
                            ccy_id_arr = new String[len];
                            for (int i = 0; i < data.length(); i++) {
                                account_no_arr[i] = data.getJSONObject(i).getString("account_no");
                                account_name_arr[i] = data.getJSONObject(i).getString("account_name");
                                ccy_id_arr[i] = data.getJSONObject(i).getString("ccy_id");
                                SourceAcccountList.add(new SourceAccountModel(account_no_arr[i], account_name_arr[i], ccy_id_arr[i]));

                                SecurePreferences prefs = new SecurePreferences(getSherlockActivity());
                                SecurePreferences.Editor mEditor = prefs.edit();
                                mEditor.putString("sourceAccountData", data.toString());
                                mEditor.apply();

                            }
                            SourceAccountAdapters cAdapter = new SourceAccountAdapters(getActivity(), android.R.layout.simple_spinner_item, SourceAcccountList);
                            cbo_source_account.setAdapter(cAdapter);

                        } else if (error_code.equalsIgnoreCase("0404")) {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                            alert.setMessage(error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Session session;
                                    session = new Session(getActivity());
                                    session.signOut();
                                }
                            });
                            alert.show();

                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_purchase_title));
                            alert.setMessage(getResources().getString(R.string.lbl_purchase_title) + " " + error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Fragment newFragment = null;
                                    newFragment = new FragmentPurchaseGeneral();
                                    switchFragment(newFragment);
                                }
                            });
                            alert.show();
                        }


                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    }
                }


            });
        }else{
            SourceAcccountList.clear();
            try {
                JSONArray data = new JSONArray(dataSourceAccount);
                int len = data.length();
                account_no_arr = new String[len];
                account_name_arr = new String[len];
                ccy_id_arr = new String[len];
                for (int i = 0; i < data.length(); i++) {
                    account_no_arr[i] = data.getJSONObject(i).getString("account_no");
                    account_name_arr[i] = data.getJSONObject(i).getString("account_name");
                    ccy_id_arr[i] = data.getJSONObject(i).getString("ccy_id");
                    SourceAcccountList.add(new SourceAccountModel(account_no_arr[i], account_name_arr[i], ccy_id_arr[i]));
                }
                SourceAccountAdapters cAdapter = new SourceAccountAdapters(getActivity(), android.R.layout.simple_spinner_item, SourceAcccountList);
                cbo_source_account.setAdapter(cAdapter);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }




    }



    private void initNominalKartuAs(){
        if(getCountAs==0){
            BillerVoucherListKartuAs.clear();
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getActivity()));
            client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
            RequestParams params = new RequestParams();

            try {

                String userid = AppHelper.getUserId(getActivity());
                String bankcode = AplConstants.Bankcode;
                String pcd = AplConstants.pcd;
                String URL = AplConstants.InquiryBiller;
                String appName = AplConstants.appName;
                String serviceName = URL.split("/")[4];
                String apiKey = AppHelper.getAccessKey(getActivity());
                UUID uniqueKey = UUID.randomUUID();
                String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;
                Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
                SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
                sha256_HMAC.init(secret_key);
                byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
                String signatureKey = new String(encodeUrlSafe(hmacData));
                params.put("userid", userid);
                params.put("bankcode", bankcode);
                params.put("passcode", pcd);
                params.put("categorycode", menu_biller_code);
                params.put("billercode", billerCCode);
                params.put("rc_uuid", uniqueKey.toString());
                params.put("rc_dtime", dateTime);
                params.put("signature", signatureKey);

            } catch (Exception e) {
                System.out.println("Error");
            }

            client.post(AplConstants.InquiryBiller, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject object) {



                    try {

                        if (pDialog != null) {
                            pDialog.dismiss();
                        }
                        JSONObject body_data = object.getJSONObject("body_data");
                        String error_code = body_data.getString("error_code");
                        String error_message = body_data.getString("error_message");

                        if (error_code.equalsIgnoreCase("0000")) {
                            JSONArray data = body_data.getJSONArray("biller_data");
                            int len = data.length();
                            biller_id_arr = new String[len];
                            biller_code_arr = new String[len];
                            biller_name_arr = new String[len];
                            biller_inquiry_arr = new String[len];
                            product_code_arr = new String[len];
                            product_name_arr = new String[len];
                            product_amount_arr = new String[len];
                            for (int i = 0; i < data.length(); i++) {

                                biller_id_arr[i] = data.getJSONObject(i).getString("billerCode");
                                biller_code_arr[i] = data.getJSONObject(i).getString("billerCode");
                                biller_name_arr[i] = data.getJSONObject(i).getString("billerName");
                                biller_inquiry_arr[i] = data.getJSONObject(i).getString("billerInquiry");
                                product_code_arr[i] = data.getJSONObject(i).getString("productCode");
                                product_name_arr[i] = data.getJSONObject(i).getString("productName");
                                product_amount_arr[i] = data.getJSONObject(i).getString("productAmount");

                                BillerVoucherListKartuAs.add(new PurchaseVoucherKartuAsModel(biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));

                                PurchaseVoucherKartuAsModel purchaseVoucherKartuAsModel = new PurchaseVoucherKartuAsModel();
                                purchaseVoucherKartuAsModel.bilerId = data.getJSONObject(i).getString("billerId");
                                purchaseVoucherKartuAsModel.billerCode = data.getJSONObject(i).getString("billerCode");
                                purchaseVoucherKartuAsModel.billerName = data.getJSONObject(i).getString("billerName");
                                purchaseVoucherKartuAsModel.billerInquiry = data.getJSONObject(i).getString("billerInquiry");
                                purchaseVoucherKartuAsModel.productCode = data.getJSONObject(i).getString("productCode");
                                purchaseVoucherKartuAsModel.productName = data.getJSONObject(i).getString("productName");
                                purchaseVoucherKartuAsModel.productAmount = data.getJSONObject(i).getString("productAmount");

                                purchaseVoucherKartuAsModel.save();
                            }
                            BillerVoucherPhoneKartuAs billerVoucherPhoneKartuAs = new BillerVoucherPhoneKartuAs(getActivity(), android.R.layout.simple_spinner_item, BillerVoucherListKartuAs);
                            cbo_voucher_name.setAdapter(billerVoucherPhoneKartuAs);


                        } else if (error_code.equalsIgnoreCase("0404")) {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                            alert.setMessage(error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Session session;
                                    session = new Session(getActivity());
                                    session.signOut();
                                }
                            });
                            alert.show();

                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_purchase_title));
                            alert.setMessage(getResources().getString(R.string.lbl_purchase_title) + " " + error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Fragment newFragment = null;
                                    newFragment = new FragmentPurchaseGeneral();
                                    switchFragment(newFragment);
                                }
                            });
                            alert.show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            });
        }else{
            List<PurchaseVoucherKartuAsModel> itemList = new Select().all().from(PurchaseVoucherKartuAsModel.class).where("billerCode = ?", billerCCode).execute();
            int len = itemList.size();
            biller_id_arr = new String[len];
            biller_code_arr = new String[len];
            biller_name_arr = new String[len];
            biller_inquiry_arr = new String[len];
            product_code_arr = new String[len];
            product_name_arr = new String[len];
            product_amount_arr = new String[len];
            for (int i=0 ; i<itemList.size();i++) {
                biller_id_arr[i]=itemList.get(i).getBilerId();
                biller_code_arr[i]=itemList.get(i).getBillerCode();
                biller_name_arr[i]=itemList.get(i).getBillerName();
                biller_inquiry_arr[i]=itemList.get(i).getBillerInquiry();
                product_code_arr[i]=itemList.get(i).getProductCode();
                product_name_arr[i]=itemList.get(i).getProductName();
                product_amount_arr[i]=itemList.get(i).getProductAmount();
                BillerVoucherListKartuAs.add(new PurchaseVoucherKartuAsModel(biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));

            }
            BillerVoucherPhoneKartuAs billerVoucherPhoneKartuAs = new BillerVoucherPhoneKartuAs(getActivity(), android.R.layout.simple_spinner_item, BillerVoucherListKartuAs);
            cbo_voucher_name.setAdapter(billerVoucherPhoneKartuAs);
        }
    }

    private void initNominalEsia(){
        if(getCountEsia==0){
            BillerVoucherListEsia.clear();
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getActivity()));
            client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
            RequestParams params = new RequestParams();

            try {

                String userid = AppHelper.getUserId(getActivity());
                String bankcode = AplConstants.Bankcode;
                String pcd = AplConstants.pcd;
                String URL = AplConstants.InquiryBiller;
                String appName = AplConstants.appName;
                String serviceName = URL.split("/")[4];
                String apiKey = AppHelper.getAccessKey(getActivity());
                UUID uniqueKey = UUID.randomUUID();
                String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;
                Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
                SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
                sha256_HMAC.init(secret_key);
                byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
                String signatureKey = new String(encodeUrlSafe(hmacData));
                params.put("userid", userid);
                params.put("bankcode", bankcode);
                params.put("passcode", pcd);
                params.put("categorycode", menu_biller_code);
                params.put("billercode", billerCCode);
                params.put("rc_uuid", uniqueKey.toString());
                params.put("rc_dtime", dateTime);
                params.put("signature", signatureKey);

            } catch (Exception e) {
                System.out.println("Error");
            }

            client.post(AplConstants.InquiryBiller, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject object) {



                    try {

                        if (pDialog != null) {
                            pDialog.dismiss();
                        }
                        JSONObject body_data = object.getJSONObject("body_data");
                        String error_code = body_data.getString("error_code");
                        String error_message = body_data.getString("error_message");

                        if (error_code.equalsIgnoreCase("0000")) {
                            JSONArray data = body_data.getJSONArray("biller_data");
                            int len = data.length();
                            biller_id_arr = new String[len];
                            biller_code_arr = new String[len];
                            biller_name_arr = new String[len];
                            biller_inquiry_arr = new String[len];
                            product_code_arr = new String[len];
                            product_name_arr = new String[len];
                            product_amount_arr = new String[len];
                            for (int i = 0; i < data.length(); i++) {

                                biller_id_arr[i] = data.getJSONObject(i).getString("billerCode");
                                biller_code_arr[i] = data.getJSONObject(i).getString("billerCode");
                                biller_name_arr[i] = data.getJSONObject(i).getString("billerName");
                                biller_inquiry_arr[i] = data.getJSONObject(i).getString("billerInquiry");
                                product_code_arr[i] = data.getJSONObject(i).getString("productCode");
                                product_name_arr[i] = data.getJSONObject(i).getString("productName");
                                product_amount_arr[i] = data.getJSONObject(i).getString("productAmount");

                                BillerVoucherListEsia.add(new PurchaseVoucherEsiaModel(biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));

                                PurchaseVoucherEsiaModel purchaseVoucherEsiaModel = new PurchaseVoucherEsiaModel();
                               purchaseVoucherEsiaModel.bilerId = data.getJSONObject(i).getString("billerId");
                               purchaseVoucherEsiaModel.billerCode = data.getJSONObject(i).getString("billerCode");
                               purchaseVoucherEsiaModel.billerName = data.getJSONObject(i).getString("billerName");
                               purchaseVoucherEsiaModel.billerInquiry = data.getJSONObject(i).getString("billerInquiry");
                               purchaseVoucherEsiaModel.productCode = data.getJSONObject(i).getString("productCode");
                               purchaseVoucherEsiaModel.productName = data.getJSONObject(i).getString("productName");
                               purchaseVoucherEsiaModel.productAmount = data.getJSONObject(i).getString("productAmount");

                               purchaseVoucherEsiaModel.save();
                            }
                            BillerVoucherPhoneEsia billerVoucherPhoneEsia= new BillerVoucherPhoneEsia(getActivity(), android.R.layout.simple_spinner_item, BillerVoucherListEsia);
                            cbo_voucher_name.setAdapter(billerVoucherPhoneEsia);


                        } else if (error_code.equalsIgnoreCase("0404")) {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                            alert.setMessage(error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Session session;
                                    session = new Session(getActivity());
                                    session.signOut();
                                }
                            });
                            alert.show();

                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_purchase_title));
                            alert.setMessage(getResources().getString(R.string.lbl_purchase_title) + " " + error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Fragment newFragment = null;
                                    newFragment = new FragmentPurchaseGeneral();
                                    switchFragment(newFragment);
                                }
                            });
                            alert.show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            });
        }else{
            List<PurchaseVoucherEsiaModel> itemList = new Select().all().from(PurchaseVoucherEsiaModel.class).where("billerCode = ?", billerCCode).execute();
            int len = itemList.size();
            biller_id_arr = new String[len];
            biller_code_arr = new String[len];
            biller_name_arr = new String[len];
            biller_inquiry_arr = new String[len];
            product_code_arr = new String[len];
            product_name_arr = new String[len];
            product_amount_arr = new String[len];
            for (int i=0 ; i<itemList.size();i++) {
                biller_id_arr[i]=itemList.get(i).getBilerId();
                biller_code_arr[i]=itemList.get(i).getBillerCode();
                biller_name_arr[i]=itemList.get(i).getBillerName();
                biller_inquiry_arr[i]=itemList.get(i).getBillerInquiry();
                product_code_arr[i]=itemList.get(i).getProductCode();
                product_name_arr[i]=itemList.get(i).getProductName();
                product_amount_arr[i]=itemList.get(i).getProductAmount();
                BillerVoucherListEsia.add(new PurchaseVoucherEsiaModel(biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));

            }
            BillerVoucherPhoneEsia billerVoucherPhoneEsia = new BillerVoucherPhoneEsia(getActivity(), android.R.layout.simple_spinner_item, BillerVoucherListEsia);
            cbo_voucher_name.setAdapter(billerVoucherPhoneEsia);
        }
    }

    private void initNominalMentari(){
        if(getCountMentari==0){
            BillerVoucherListMentari.clear();
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getActivity()));
            client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
            RequestParams params = new RequestParams();

            try {

                String userid = AppHelper.getUserId(getActivity());
                String bankcode = AplConstants.Bankcode;
                String pcd = AplConstants.pcd;
                String URL = AplConstants.InquiryBiller;
                String appName = AplConstants.appName;
                String serviceName = URL.split("/")[4];
                String apiKey = AppHelper.getAccessKey(getActivity());
                UUID uniqueKey = UUID.randomUUID();
                String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;
                Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
                SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
                sha256_HMAC.init(secret_key);
                byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
                String signatureKey = new String(encodeUrlSafe(hmacData));
                params.put("userid", userid);
                params.put("bankcode", bankcode);
                params.put("passcode", pcd);
                params.put("categorycode", menu_biller_code);
                params.put("billercode", billerCCode);
                params.put("rc_uuid", uniqueKey.toString());
                params.put("rc_dtime", dateTime);
                params.put("signature", signatureKey);

            } catch (Exception e) {
                System.out.println("Error");
            }

            client.post(AplConstants.InquiryBiller, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject object) {



                    try {

                        if (pDialog != null) {
                            pDialog.dismiss();
                        }
                        JSONObject body_data = object.getJSONObject("body_data");
                        String error_code = body_data.getString("error_code");
                        String error_message = body_data.getString("error_message");

                        if (error_code.equalsIgnoreCase("0000")) {
                            JSONArray data = body_data.getJSONArray("biller_data");
                            int len = data.length();
                            biller_id_arr = new String[len];
                            biller_code_arr = new String[len];
                            biller_name_arr = new String[len];
                            biller_inquiry_arr = new String[len];
                            product_code_arr = new String[len];
                            product_name_arr = new String[len];
                            product_amount_arr = new String[len];
                            for (int i = 0; i < data.length(); i++) {

                                biller_id_arr[i] = data.getJSONObject(i).getString("billerCode");
                                biller_code_arr[i] = data.getJSONObject(i).getString("billerCode");
                                biller_name_arr[i] = data.getJSONObject(i).getString("billerName");
                                biller_inquiry_arr[i] = data.getJSONObject(i).getString("billerInquiry");
                                product_code_arr[i] = data.getJSONObject(i).getString("productCode");
                                product_name_arr[i] = data.getJSONObject(i).getString("productName");
                                product_amount_arr[i] = data.getJSONObject(i).getString("productAmount");

                                BillerVoucherListMentari.add(new PurchaseVoucherMentariModel(biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));

                                PurchaseVoucherMentariModel purchaseVoucherMentariModel = new PurchaseVoucherMentariModel();
                                purchaseVoucherMentariModel.bilerId = data.getJSONObject(i).getString("billerId");
                                purchaseVoucherMentariModel.billerCode = data.getJSONObject(i).getString("billerCode");
                                purchaseVoucherMentariModel.billerName = data.getJSONObject(i).getString("billerName");
                                purchaseVoucherMentariModel.billerInquiry = data.getJSONObject(i).getString("billerInquiry");
                                purchaseVoucherMentariModel.productCode = data.getJSONObject(i).getString("productCode");
                                purchaseVoucherMentariModel.productName = data.getJSONObject(i).getString("productName");
                                purchaseVoucherMentariModel.productAmount = data.getJSONObject(i).getString("productAmount");

                                purchaseVoucherMentariModel.save();
                            }
                            BillerVoucherPhoneMentari billerVoucherPhoneMentari = new BillerVoucherPhoneMentari(getActivity(), android.R.layout.simple_spinner_item, BillerVoucherListMentari);
                            cbo_voucher_name.setAdapter(billerVoucherPhoneMentari);


                        } else if (error_code.equalsIgnoreCase("0404")) {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                            alert.setMessage(error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Session session;
                                    session = new Session(getActivity());
                                    session.signOut();
                                }
                            });
                            alert.show();

                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_purchase_title));
                            alert.setMessage(getResources().getString(R.string.lbl_purchase_title) + " " + error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Fragment newFragment = null;
                                    newFragment = new FragmentPurchaseGeneral();
                                    switchFragment(newFragment);
                                }
                            });
                            alert.show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            });
        }else{
            List<PurchaseVoucherMentariModel> itemList = new Select().all().from(PurchaseVoucherMentariModel.class).where("billerCode = ?", billerCCode).execute();
            int len = itemList.size();
            biller_id_arr = new String[len];
            biller_code_arr = new String[len];
            biller_name_arr = new String[len];
            biller_inquiry_arr = new String[len];
            product_code_arr = new String[len];
            product_name_arr = new String[len];
            product_amount_arr = new String[len];
            for (int i=0 ; i<itemList.size();i++) {
                biller_id_arr[i]=itemList.get(i).getBilerId();
                biller_code_arr[i]=itemList.get(i).getBillerCode();
                biller_name_arr[i]=itemList.get(i).getBillerName();
                biller_inquiry_arr[i]=itemList.get(i).getBillerInquiry();
                product_code_arr[i]=itemList.get(i).getProductCode();
                product_name_arr[i]=itemList.get(i).getProductName();
                product_amount_arr[i]=itemList.get(i).getProductAmount();
                BillerVoucherListMentari.add(new PurchaseVoucherMentariModel(biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));

            }
            BillerVoucherPhoneMentari billerVoucherPhoneMentari = new BillerVoucherPhoneMentari(getActivity(), android.R.layout.simple_spinner_item, BillerVoucherListMentari);
            cbo_voucher_name.setAdapter(billerVoucherPhoneMentari);
        }
    }

    private void initNominalSimpati(){
        if(getCountSimpati==0){
            BillerVoucherListSimpati.clear();
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getActivity()));
            client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
            RequestParams params = new RequestParams();

            try {

                String userid = AppHelper.getUserId(getActivity());
                String bankcode = AplConstants.Bankcode;
                String pcd = AplConstants.pcd;
                String URL = AplConstants.InquiryBiller;
                String appName = AplConstants.appName;
                String serviceName = URL.split("/")[4];
                String apiKey = AppHelper.getAccessKey(getActivity());
                UUID uniqueKey = UUID.randomUUID();
                String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;
                Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
                SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
                sha256_HMAC.init(secret_key);
                byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
                String signatureKey = new String(encodeUrlSafe(hmacData));
                params.put("userid", userid);
                params.put("bankcode", bankcode);
                params.put("passcode", pcd);
                params.put("categorycode", menu_biller_code);
                params.put("billercode", billerCCode);
                params.put("rc_uuid", uniqueKey.toString());
                params.put("rc_dtime", dateTime);
                params.put("signature", signatureKey);

            } catch (Exception e) {
                System.out.println("Error");
            }

            client.post(AplConstants.InquiryBiller, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject object) {



                    try {

                        if (pDialog != null) {
                            pDialog.dismiss();
                        }
                        JSONObject body_data = object.getJSONObject("body_data");
                        String error_code = body_data.getString("error_code");
                        String error_message = body_data.getString("error_message");

                        if (error_code.equalsIgnoreCase("0000")) {
                            JSONArray data = body_data.getJSONArray("biller_data");
                            int len = data.length();
                            biller_id_arr = new String[len];
                            biller_code_arr = new String[len];
                            biller_name_arr = new String[len];
                            biller_inquiry_arr = new String[len];
                            product_code_arr = new String[len];
                            product_name_arr = new String[len];
                            product_amount_arr = new String[len];
                            for (int i = 0; i < data.length(); i++) {

                                biller_id_arr[i] = data.getJSONObject(i).getString("billerCode");
                                biller_code_arr[i] = data.getJSONObject(i).getString("billerCode");
                                biller_name_arr[i] = data.getJSONObject(i).getString("billerName");
                                biller_inquiry_arr[i] = data.getJSONObject(i).getString("billerInquiry");
                                product_code_arr[i] = data.getJSONObject(i).getString("productCode");
                                product_name_arr[i] = data.getJSONObject(i).getString("productName");
                                product_amount_arr[i] = data.getJSONObject(i).getString("productAmount");

                                BillerVoucherListSimpati.add(new PurchaseVoucherSimpatiModel(biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));

                                PurchaseVoucherSimpatiModel purchaseVoucherSimpatiModel = new PurchaseVoucherSimpatiModel();
                               purchaseVoucherSimpatiModel.bilerId = data.getJSONObject(i).getString("billerId");
                               purchaseVoucherSimpatiModel.billerCode = data.getJSONObject(i).getString("billerCode");
                               purchaseVoucherSimpatiModel.billerName = data.getJSONObject(i).getString("billerName");
                               purchaseVoucherSimpatiModel.billerInquiry = data.getJSONObject(i).getString("billerInquiry");
                               purchaseVoucherSimpatiModel.productCode = data.getJSONObject(i).getString("productCode");
                               purchaseVoucherSimpatiModel.productName = data.getJSONObject(i).getString("productName");
                               purchaseVoucherSimpatiModel.productAmount = data.getJSONObject(i).getString("productAmount");

                               purchaseVoucherSimpatiModel.save();
                            }
                            BillerVoucherPhoneSimpati billerVoucherPhoneSimpati = new BillerVoucherPhoneSimpati(getActivity(), android.R.layout.simple_spinner_item, BillerVoucherListSimpati);
                            cbo_voucher_name.setAdapter(billerVoucherPhoneSimpati);


                        } else if (error_code.equalsIgnoreCase("0404")) {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                            alert.setMessage(error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Session session;
                                    session = new Session(getActivity());
                                    session.signOut();
                                }
                            });
                            alert.show();

                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_purchase_title));
                            alert.setMessage(getResources().getString(R.string.lbl_purchase_title) + " " + error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Fragment newFragment = null;
                                    newFragment = new FragmentPurchaseGeneral();
                                    switchFragment(newFragment);
                                }
                            });
                            alert.show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            });
        }else{
            List<PurchaseVoucherSimpatiModel> itemList = new Select().all().from(PurchaseVoucherSimpatiModel.class).where("billerCode = ?", billerCCode).execute();
            int len = itemList.size();
            biller_id_arr = new String[len];
            biller_code_arr = new String[len];
            biller_name_arr = new String[len];
            biller_inquiry_arr = new String[len];
            product_code_arr = new String[len];
            product_name_arr = new String[len];
            product_amount_arr = new String[len];
            for (int i=0 ; i<itemList.size();i++) {
                biller_id_arr[i]=itemList.get(i).getBilerId();
                biller_code_arr[i]=itemList.get(i).getBillerCode();
                biller_name_arr[i]=itemList.get(i).getBillerName();
                biller_inquiry_arr[i]=itemList.get(i).getBillerInquiry();
                product_code_arr[i]=itemList.get(i).getProductCode();
                product_name_arr[i]=itemList.get(i).getProductName();
                product_amount_arr[i]=itemList.get(i).getProductAmount();
                BillerVoucherListSimpati.add(new PurchaseVoucherSimpatiModel(biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));

            }
            BillerVoucherPhoneSimpati billerVoucherPhoneSimpati = new BillerVoucherPhoneSimpati(getActivity(), android.R.layout.simple_spinner_item, BillerVoucherListSimpati);
            cbo_voucher_name.setAdapter(billerVoucherPhoneSimpati);
        }
    }

    private void initNominalSmartfren(){
        if(getCountSmarften==0){
            BillerVoucherListSmartfren.clear();
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getActivity()));
            client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
            RequestParams params = new RequestParams();

            try {

                String userid = AppHelper.getUserId(getActivity());
                String bankcode = AplConstants.Bankcode;
                String pcd = AplConstants.pcd;
                String URL = AplConstants.InquiryBiller;
                String appName = AplConstants.appName;
                String serviceName = URL.split("/")[4];
                String apiKey = AppHelper.getAccessKey(getActivity());
                UUID uniqueKey = UUID.randomUUID();
                String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;
                Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
                SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
                sha256_HMAC.init(secret_key);
                byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
                String signatureKey = new String(encodeUrlSafe(hmacData));
                params.put("userid", userid);
                params.put("bankcode", bankcode);
                params.put("passcode", pcd);
                params.put("categorycode", menu_biller_code);
                params.put("billercode", billerCCode);
                params.put("rc_uuid", uniqueKey.toString());
                params.put("rc_dtime", dateTime);
                params.put("signature", signatureKey);

            } catch (Exception e) {
                System.out.println("Error");
            }

            client.post(AplConstants.InquiryBiller, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject object) {



                    try {

                        if (pDialog != null) {
                            pDialog.dismiss();
                        }
                        JSONObject body_data = object.getJSONObject("body_data");
                        String error_code = body_data.getString("error_code");
                        String error_message = body_data.getString("error_message");

                        if (error_code.equalsIgnoreCase("0000")) {
                            JSONArray data = body_data.getJSONArray("biller_data");
                            int len = data.length();
                            biller_id_arr = new String[len];
                            biller_code_arr = new String[len];
                            biller_name_arr = new String[len];
                            biller_inquiry_arr = new String[len];
                            product_code_arr = new String[len];
                            product_name_arr = new String[len];
                            product_amount_arr = new String[len];
                            for (int i = 0; i < data.length(); i++) {

                                biller_id_arr[i] = data.getJSONObject(i).getString("billerCode");
                                biller_code_arr[i] = data.getJSONObject(i).getString("billerCode");
                                biller_name_arr[i] = data.getJSONObject(i).getString("billerName");
                                biller_inquiry_arr[i] = data.getJSONObject(i).getString("billerInquiry");
                                product_code_arr[i] = data.getJSONObject(i).getString("productCode");
                                product_name_arr[i] = data.getJSONObject(i).getString("productName");
                                product_amount_arr[i] = data.getJSONObject(i).getString("productAmount");

                                BillerVoucherListSmartfren.add(new PurchaseVoucherSmartfrenModel(biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));

                                PurchaseVoucherSmartfrenModel purchaseVoucherSmartfrenModel = new PurchaseVoucherSmartfrenModel();
                                purchaseVoucherSmartfrenModel.bilerId = data.getJSONObject(i).getString("billerId");
                                purchaseVoucherSmartfrenModel.billerCode = data.getJSONObject(i).getString("billerCode");
                                purchaseVoucherSmartfrenModel.billerName = data.getJSONObject(i).getString("billerName");
                                purchaseVoucherSmartfrenModel.billerInquiry = data.getJSONObject(i).getString("billerInquiry");
                                purchaseVoucherSmartfrenModel.productCode = data.getJSONObject(i).getString("productCode");
                                purchaseVoucherSmartfrenModel.productName = data.getJSONObject(i).getString("productName");
                                purchaseVoucherSmartfrenModel.productAmount = data.getJSONObject(i).getString("productAmount");

                                purchaseVoucherSmartfrenModel.save();
                            }
                            BillerVoucherPhoneSmartfren billerVoucherPhoneSmartfren = new BillerVoucherPhoneSmartfren(getActivity(), android.R.layout.simple_spinner_item, BillerVoucherListSmartfren);
                            cbo_voucher_name.setAdapter(billerVoucherPhoneSmartfren);


                        } else if (error_code.equalsIgnoreCase("0404")) {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                            alert.setMessage(error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Session session;
                                    session = new Session(getActivity());
                                    session.signOut();
                                }
                            });
                            alert.show();

                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_purchase_title));
                            alert.setMessage(getResources().getString(R.string.lbl_purchase_title) + " " + error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Fragment newFragment = null;
                                    newFragment = new FragmentPurchaseGeneral();
                                    switchFragment(newFragment);
                                }
                            });
                            alert.show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            });
        }else{
            List<PurchaseVoucherSmartfrenModel> itemList = new Select().all().from(PurchaseVoucherSmartfrenModel.class).where("billerCode = ?", billerCCode).execute();
            int len = itemList.size();
            biller_id_arr = new String[len];
            biller_code_arr = new String[len];
            biller_name_arr = new String[len];
            biller_inquiry_arr = new String[len];
            product_code_arr = new String[len];
            product_name_arr = new String[len];
            product_amount_arr = new String[len];
            for (int i=0 ; i<itemList.size();i++) {
                biller_id_arr[i]=itemList.get(i).getBilerId();
                biller_code_arr[i]=itemList.get(i).getBillerCode();
                biller_name_arr[i]=itemList.get(i).getBillerName();
                biller_inquiry_arr[i]=itemList.get(i).getBillerInquiry();
                product_code_arr[i]=itemList.get(i).getProductCode();
                product_name_arr[i]=itemList.get(i).getProductName();
                product_amount_arr[i]=itemList.get(i).getProductAmount();
                BillerVoucherListSmartfren.add(new PurchaseVoucherSmartfrenModel(biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));

            }
            BillerVoucherPhoneSmartfren billerVoucherPhoneSmartfren = new BillerVoucherPhoneSmartfren(getActivity(), android.R.layout.simple_spinner_item, BillerVoucherListSmartfren);
            cbo_voucher_name.setAdapter(billerVoucherPhoneSmartfren);
        }
    }

    private void initNominalIm3Smart(){
        if(getCountIm3Smart==0){
            BillerVoucherListIm3Smart.clear();
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getActivity()));
            client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
            RequestParams params = new RequestParams();

            try {

                String userid = AppHelper.getUserId(getActivity());
                String bankcode = AplConstants.Bankcode;
                String pcd = AplConstants.pcd;
                String URL = AplConstants.InquiryBiller;
                String appName = AplConstants.appName;
                String serviceName = URL.split("/")[4];
                String apiKey = AppHelper.getAccessKey(getActivity());
                UUID uniqueKey = UUID.randomUUID();
                String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;
                Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
                SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
                sha256_HMAC.init(secret_key);
                byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
                String signatureKey = new String(encodeUrlSafe(hmacData));
                params.put("userid", userid);
                params.put("bankcode", bankcode);
                params.put("passcode", pcd);
                params.put("categorycode", menu_biller_code);
                params.put("billercode", billerCCode);
                params.put("rc_uuid", uniqueKey.toString());
                params.put("rc_dtime", dateTime);
                params.put("signature", signatureKey);

            } catch (Exception e) {
                System.out.println("Error");
            }

            client.post(AplConstants.InquiryBiller, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject object) {



                    try {

                        if (pDialog != null) {
                            pDialog.dismiss();
                        }
                        JSONObject body_data = object.getJSONObject("body_data");
                        String error_code = body_data.getString("error_code");
                        String error_message = body_data.getString("error_message");

                        if (error_code.equalsIgnoreCase("0000")) {
                            JSONArray data = body_data.getJSONArray("biller_data");
                            int len = data.length();
                            biller_id_arr = new String[len];
                            biller_code_arr = new String[len];
                            biller_name_arr = new String[len];
                            biller_inquiry_arr = new String[len];
                            product_code_arr = new String[len];
                            product_name_arr = new String[len];
                            product_amount_arr = new String[len];
                            for (int i = 0; i < data.length(); i++) {

                                biller_id_arr[i] = data.getJSONObject(i).getString("billerCode");
                                biller_code_arr[i] = data.getJSONObject(i).getString("billerCode");
                                biller_name_arr[i] = data.getJSONObject(i).getString("billerName");
                                biller_inquiry_arr[i] = data.getJSONObject(i).getString("billerInquiry");
                                product_code_arr[i] = data.getJSONObject(i).getString("productCode");
                                product_name_arr[i] = data.getJSONObject(i).getString("productName");
                                product_amount_arr[i] = data.getJSONObject(i).getString("productAmount");

                                BillerVoucherListIm3Smart.add(new PurchaseVoucherIm3SmartModel(biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));

                                PurchaseVoucherIm3SmartModel purchaseVoucherIm3SmartModel = new PurchaseVoucherIm3SmartModel();
                                purchaseVoucherIm3SmartModel.bilerId = data.getJSONObject(i).getString("billerId");
                                purchaseVoucherIm3SmartModel.billerCode = data.getJSONObject(i).getString("billerCode");
                                purchaseVoucherIm3SmartModel.billerName = data.getJSONObject(i).getString("billerName");
                                purchaseVoucherIm3SmartModel.billerInquiry = data.getJSONObject(i).getString("billerInquiry");
                                purchaseVoucherIm3SmartModel.productCode = data.getJSONObject(i).getString("productCode");
                                purchaseVoucherIm3SmartModel.productName = data.getJSONObject(i).getString("productName");
                                purchaseVoucherIm3SmartModel.productAmount = data.getJSONObject(i).getString("productAmount");

                                purchaseVoucherIm3SmartModel.save();
                            }
                            BillerVoucherPhoneIm3Smart billerVoucherPhoneIm3Smart = new BillerVoucherPhoneIm3Smart(getActivity(), android.R.layout.simple_spinner_item, BillerVoucherListIm3Smart);
                            cbo_voucher_name.setAdapter(billerVoucherPhoneIm3Smart);


                        } else if (error_code.equalsIgnoreCase("0404")) {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                            alert.setMessage(error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Session session;
                                    session = new Session(getActivity());
                                    session.signOut();
                                }
                            });
                            alert.show();

                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_purchase_title));
                            alert.setMessage(getResources().getString(R.string.lbl_purchase_title) + " " + error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Fragment newFragment = null;
                                    newFragment = new FragmentPurchaseGeneral();
                                    switchFragment(newFragment);
                                }
                            });
                            alert.show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            });
        }else{
            List<PurchaseVoucherIm3SmartModel> itemList = new Select().all().from(PurchaseVoucherIm3SmartModel.class).where("billerCode = ?", billerCCode).execute();
            int len = itemList.size();
            biller_id_arr = new String[len];
            biller_code_arr = new String[len];
            biller_name_arr = new String[len];
            biller_inquiry_arr = new String[len];
            product_code_arr = new String[len];
            product_name_arr = new String[len];
            product_amount_arr = new String[len];
            for (int i=0 ; i<itemList.size();i++) {
                biller_id_arr[i]=itemList.get(i).getBilerId();
                biller_code_arr[i]=itemList.get(i).getBillerCode();
                biller_name_arr[i]=itemList.get(i).getBillerName();
                biller_inquiry_arr[i]=itemList.get(i).getBillerInquiry();
                product_code_arr[i]=itemList.get(i).getProductCode();
                product_name_arr[i]=itemList.get(i).getProductName();
                product_amount_arr[i]=itemList.get(i).getProductAmount();
                BillerVoucherListIm3Smart.add(new PurchaseVoucherIm3SmartModel(biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));

            }
            BillerVoucherPhoneIm3Smart billerVoucherPhoneIm3Smart = new BillerVoucherPhoneIm3Smart(getActivity(), android.R.layout.simple_spinner_item, BillerVoucherListIm3Smart);
            cbo_voucher_name.setAdapter(billerVoucherPhoneIm3Smart);
        }
    }

    private void initNominalBolt(){
        if(getCountBolt==0){
            BillerVoucherListBolt.clear();
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getActivity()));
            client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
            RequestParams params = new RequestParams();

            try {

                String userid = AppHelper.getUserId(getActivity());
                String bankcode = AplConstants.Bankcode;
                String pcd = AplConstants.pcd;
                String URL = AplConstants.InquiryBiller;
                String appName = AplConstants.appName;
                String serviceName = URL.split("/")[4];
                String apiKey = AppHelper.getAccessKey(getActivity());
                UUID uniqueKey = UUID.randomUUID();
                String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;
                Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
                SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
                sha256_HMAC.init(secret_key);
                byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
                String signatureKey = new String(encodeUrlSafe(hmacData));
                params.put("userid", userid);
                params.put("bankcode", bankcode);
                params.put("passcode", pcd);
                params.put("categorycode", menu_biller_code);
                params.put("billercode", billerCCode);
                params.put("rc_uuid", uniqueKey.toString());
                params.put("rc_dtime", dateTime);
                params.put("signature", signatureKey);

            } catch (Exception e) {
                System.out.println("Error");
            }

            client.post(AplConstants.InquiryBiller, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject object) {



                    try {

                        if (pDialog != null) {
                            pDialog.dismiss();
                        }
                        JSONObject body_data = object.getJSONObject("body_data");
                        String error_code = body_data.getString("error_code");
                        String error_message = body_data.getString("error_message");

                        if (error_code.equalsIgnoreCase("0000")) {
                            JSONArray data = body_data.getJSONArray("biller_data");
                            int len = data.length();
                            biller_id_arr = new String[len];
                            biller_code_arr = new String[len];
                            biller_name_arr = new String[len];
                            biller_inquiry_arr = new String[len];
                            product_code_arr = new String[len];
                            product_name_arr = new String[len];
                            product_amount_arr = new String[len];
                            for (int i = 0; i < data.length(); i++) {

                                biller_id_arr[i] = data.getJSONObject(i).getString("billerCode");
                                biller_code_arr[i] = data.getJSONObject(i).getString("billerCode");
                                biller_name_arr[i] = data.getJSONObject(i).getString("billerName");
                                biller_inquiry_arr[i] = data.getJSONObject(i).getString("billerInquiry");
                                product_code_arr[i] = data.getJSONObject(i).getString("productCode");
                                product_name_arr[i] = data.getJSONObject(i).getString("productName");
                                product_amount_arr[i] = data.getJSONObject(i).getString("productAmount");

                                BillerVoucherListBolt.add(new PurchaseVoucherBoltModel(biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));

                                PurchaseVoucherBoltModel purchaseVoucherBoltModel = new PurchaseVoucherBoltModel();
                                purchaseVoucherBoltModel.bilerId = data.getJSONObject(i).getString("billerId");
                                purchaseVoucherBoltModel.billerCode = data.getJSONObject(i).getString("billerCode");
                                purchaseVoucherBoltModel.billerName = data.getJSONObject(i).getString("billerName");
                                purchaseVoucherBoltModel.billerInquiry = data.getJSONObject(i).getString("billerInquiry");
                                purchaseVoucherBoltModel.productCode = data.getJSONObject(i).getString("productCode");
                                purchaseVoucherBoltModel.productName = data.getJSONObject(i).getString("productName");
                                purchaseVoucherBoltModel.productAmount = data.getJSONObject(i).getString("productAmount");

                                purchaseVoucherBoltModel.save();
                            }
                            BillerVoucherPhoneBolt billerVoucherPhoneBolt = new BillerVoucherPhoneBolt(getActivity(), android.R.layout.simple_spinner_item, BillerVoucherListBolt);
                            cbo_voucher_name.setAdapter(billerVoucherPhoneBolt);


                        } else if (error_code.equalsIgnoreCase("0404")) {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                            alert.setMessage(error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Session session;
                                    session = new Session(getActivity());
                                    session.signOut();
                                }
                            });
                            alert.show();

                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_purchase_title));
                            alert.setMessage(getResources().getString(R.string.lbl_purchase_title) + " " + error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Fragment newFragment = null;
                                    newFragment = new FragmentPurchaseGeneral();
                                    switchFragment(newFragment);
                                }
                            });
                            alert.show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            });
        }else{
            List<PurchaseVoucherBoltModel> itemList = new Select().all().from(PurchaseVoucherBoltModel.class).where("billerCode = ?", billerCCode).execute();
            int len = itemList.size();
            biller_id_arr = new String[len];
            biller_code_arr = new String[len];
            biller_name_arr = new String[len];
            biller_inquiry_arr = new String[len];
            product_code_arr = new String[len];
            product_name_arr = new String[len];
            product_amount_arr = new String[len];
            for (int i=0 ; i<itemList.size();i++) {
                biller_id_arr[i]=itemList.get(i).getBilerId();
                biller_code_arr[i]=itemList.get(i).getBillerCode();
                biller_name_arr[i]=itemList.get(i).getBillerName();
                biller_inquiry_arr[i]=itemList.get(i).getBillerInquiry();
                product_code_arr[i]=itemList.get(i).getProductCode();
                product_name_arr[i]=itemList.get(i).getProductName();
                product_amount_arr[i]=itemList.get(i).getProductAmount();
                BillerVoucherListBolt.add(new PurchaseVoucherBoltModel(biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));

            }
            BillerVoucherPhoneBolt billerVoucherPhoneBolt = new BillerVoucherPhoneBolt(getActivity(), android.R.layout.simple_spinner_item, BillerVoucherListBolt);
            cbo_voucher_name.setAdapter(billerVoucherPhoneBolt);
        }
    }

    private void initNominaThree(){
        if(getCountThree==0){
            BillerVoucherListThree.clear();
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getActivity()));
            client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
            RequestParams params = new RequestParams();

            try {

                String userid = AppHelper.getUserId(getActivity());
                String bankcode = AplConstants.Bankcode;
                String pcd = AplConstants.pcd;
                String URL = AplConstants.InquiryBiller;
                String appName = AplConstants.appName;
                String serviceName = URL.split("/")[4];
                String apiKey = AppHelper.getAccessKey(getActivity());
                UUID uniqueKey = UUID.randomUUID();
                String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;
                Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
                SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
                sha256_HMAC.init(secret_key);
                byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
                String signatureKey = new String(encodeUrlSafe(hmacData));
                params.put("userid", userid);
                params.put("bankcode", bankcode);
                params.put("passcode", pcd);
                params.put("categorycode", menu_biller_code);
                params.put("billercode", billerCCode);
                params.put("rc_uuid", uniqueKey.toString());
                params.put("rc_dtime", dateTime);
                params.put("signature", signatureKey);

            } catch (Exception e) {
                System.out.println("Error");
            }

            client.post(AplConstants.InquiryBiller, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject object) {



                    try {

                        if (pDialog != null) {
                            pDialog.dismiss();
                        }
                        JSONObject body_data = object.getJSONObject("body_data");
                        String error_code = body_data.getString("error_code");
                        String error_message = body_data.getString("error_message");

                        if (error_code.equalsIgnoreCase("0000")) {
                            JSONArray data = body_data.getJSONArray("biller_data");
                            int len = data.length();
                            biller_id_arr = new String[len];
                            biller_code_arr = new String[len];
                            biller_name_arr = new String[len];
                            biller_inquiry_arr = new String[len];
                            product_code_arr = new String[len];
                            product_name_arr = new String[len];
                            product_amount_arr = new String[len];
                            for (int i = 0; i < data.length(); i++) {

                                biller_id_arr[i] = data.getJSONObject(i).getString("billerCode");
                                biller_code_arr[i] = data.getJSONObject(i).getString("billerCode");
                                biller_name_arr[i] = data.getJSONObject(i).getString("billerName");
                                biller_inquiry_arr[i] = data.getJSONObject(i).getString("billerInquiry");
                                product_code_arr[i] = data.getJSONObject(i).getString("productCode");
                                product_name_arr[i] = data.getJSONObject(i).getString("productName");
                                product_amount_arr[i] = data.getJSONObject(i).getString("productAmount");

                                BillerVoucherListThree.add(new PurchaseVoucherThreeModel(biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));

                                PurchaseVoucherThreeModel purchaseVoucherThreeModel = new PurchaseVoucherThreeModel();
                                purchaseVoucherThreeModel.bilerId = data.getJSONObject(i).getString("billerId");
                                purchaseVoucherThreeModel.billerCode = data.getJSONObject(i).getString("billerCode");
                                purchaseVoucherThreeModel.billerName = data.getJSONObject(i).getString("billerName");
                                purchaseVoucherThreeModel.billerInquiry = data.getJSONObject(i).getString("billerInquiry");
                                purchaseVoucherThreeModel.productCode = data.getJSONObject(i).getString("productCode");
                                purchaseVoucherThreeModel.productName = data.getJSONObject(i).getString("productName");
                                purchaseVoucherThreeModel.productAmount = data.getJSONObject(i).getString("productAmount");

                                purchaseVoucherThreeModel.save();
                            }
                            BillerVoucherPhoneThree billerVoucherPhoneThree = new BillerVoucherPhoneThree(getActivity(), android.R.layout.simple_spinner_item, BillerVoucherListThree);
                            cbo_voucher_name.setAdapter(billerVoucherPhoneThree);


                        } else if (error_code.equalsIgnoreCase("0404")) {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                            alert.setMessage(error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Session session;
                                    session = new Session(getActivity());
                                    session.signOut();
                                }
                            });
                            alert.show();

                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_purchase_title));
                            alert.setMessage(getResources().getString(R.string.lbl_purchase_title) + " " + error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Fragment newFragment = null;
                                    newFragment = new FragmentPurchaseGeneral();
                                    switchFragment(newFragment);
                                }
                            });
                            alert.show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            });
        }else{
            List<PurchaseVoucherThreeModel> itemList = new Select().all().from(PurchaseVoucherThreeModel.class).where("billerCode = ?", billerCCode).execute();
            int len = itemList.size();
            biller_id_arr = new String[len];
            biller_code_arr = new String[len];
            biller_name_arr = new String[len];
            biller_inquiry_arr = new String[len];
            product_code_arr = new String[len];
            product_name_arr = new String[len];
            product_amount_arr = new String[len];
            for (int i=0 ; i<itemList.size();i++) {
                biller_id_arr[i]=itemList.get(i).getBilerId();
                biller_code_arr[i]=itemList.get(i).getBillerCode();
                biller_name_arr[i]=itemList.get(i).getBillerName();
                biller_inquiry_arr[i]=itemList.get(i).getBillerInquiry();
                product_code_arr[i]=itemList.get(i).getProductCode();
                product_name_arr[i]=itemList.get(i).getProductName();
                product_amount_arr[i]=itemList.get(i).getProductAmount();
                BillerVoucherListThree.add(new PurchaseVoucherThreeModel(biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));

            }
            BillerVoucherPhoneThree billerVoucherPhoneThree = new BillerVoucherPhoneThree(getActivity(), android.R.layout.simple_spinner_item, BillerVoucherListThree);
            cbo_voucher_name.setAdapter(billerVoucherPhoneThree);
        }
    }

    private void initNominalXl(){
        if(getCountXl==0){
            BillerVoucherListXl.clear();
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getActivity()));
            client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
            RequestParams params = new RequestParams();

            try {

                String userid = AppHelper.getUserId(getActivity());
                String bankcode = AplConstants.Bankcode;
                String pcd = AplConstants.pcd;
                String URL = AplConstants.InquiryBiller;
                String appName = AplConstants.appName;
                String serviceName = URL.split("/")[4];
                String apiKey = AppHelper.getAccessKey(getActivity());
                UUID uniqueKey = UUID.randomUUID();
                String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;
                Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
                SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
                sha256_HMAC.init(secret_key);
                byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
                String signatureKey = new String(encodeUrlSafe(hmacData));
                params.put("userid", userid);
                params.put("bankcode", bankcode);
                params.put("passcode", pcd);
                params.put("categorycode", menu_biller_code);
                params.put("billercode", billerCCode);
                params.put("rc_uuid", uniqueKey.toString());
                params.put("rc_dtime", dateTime);
                params.put("signature", signatureKey);

            } catch (Exception e) {
                System.out.println("Error");
            }

            client.post(AplConstants.InquiryBiller, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject object) {



                    try {

                        if (pDialog != null) {
                            pDialog.dismiss();
                        }
                        JSONObject body_data = object.getJSONObject("body_data");
                        String error_code = body_data.getString("error_code");
                        String error_message = body_data.getString("error_message");

                        if (error_code.equalsIgnoreCase("0000")) {
                            JSONArray data = body_data.getJSONArray("biller_data");
                            int len = data.length();
                            biller_id_arr = new String[len];
                            biller_code_arr = new String[len];
                            biller_name_arr = new String[len];
                            biller_inquiry_arr = new String[len];
                            product_code_arr = new String[len];
                            product_name_arr = new String[len];
                            product_amount_arr = new String[len];
                            for (int i = 0; i < data.length(); i++) {

                                biller_id_arr[i] = data.getJSONObject(i).getString("billerCode");
                                biller_code_arr[i] = data.getJSONObject(i).getString("billerCode");
                                biller_name_arr[i] = data.getJSONObject(i).getString("billerName");
                                biller_inquiry_arr[i] = data.getJSONObject(i).getString("billerInquiry");
                                product_code_arr[i] = data.getJSONObject(i).getString("productCode");
                                product_name_arr[i] = data.getJSONObject(i).getString("productName");
                                product_amount_arr[i] = data.getJSONObject(i).getString("productAmount");

                                BillerVoucherListXl.add(new PurchaseVoucherXlModel(biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));

                                PurchaseVoucherXlModel purchaseVoucherXlModel = new PurchaseVoucherXlModel();
                                purchaseVoucherXlModel.bilerId = data.getJSONObject(i).getString("billerId");
                                purchaseVoucherXlModel.billerCode = data.getJSONObject(i).getString("billerCode");
                                purchaseVoucherXlModel.billerName = data.getJSONObject(i).getString("billerName");
                                purchaseVoucherXlModel.billerInquiry = data.getJSONObject(i).getString("billerInquiry");
                                purchaseVoucherXlModel.productCode = data.getJSONObject(i).getString("productCode");
                                purchaseVoucherXlModel.productName = data.getJSONObject(i).getString("productName");
                                purchaseVoucherXlModel.productAmount = data.getJSONObject(i).getString("productAmount");

                                purchaseVoucherXlModel.save();
                            }
                            BillerVoucherPhoneXl billerVoucherPhoneXl = new BillerVoucherPhoneXl(getActivity(), android.R.layout.simple_spinner_item, BillerVoucherListXl);
                            cbo_voucher_name.setAdapter(billerVoucherPhoneXl);


                        } else if (error_code.equalsIgnoreCase("0404")) {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                            alert.setMessage(error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Session session;
                                    session = new Session(getActivity());
                                    session.signOut();
                                }
                            });
                            alert.show();

                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_purchase_title));
                            alert.setMessage(getResources().getString(R.string.lbl_purchase_title) + " " + error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Fragment newFragment = null;
                                    newFragment = new FragmentPurchaseGeneral();
                                    switchFragment(newFragment);
                                }
                            });
                            alert.show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            });
        }else{
            List<PurchaseVoucherXlModel> itemList = new Select().all().from(PurchaseVoucherXlModel.class).where("billerCode = ?", billerCCode).execute();
            int len = itemList.size();
            biller_id_arr = new String[len];
            biller_code_arr = new String[len];
            biller_name_arr = new String[len];
            biller_inquiry_arr = new String[len];
            product_code_arr = new String[len];
            product_name_arr = new String[len];
            product_amount_arr = new String[len];
            for (int i=0 ; i<itemList.size();i++) {
                biller_id_arr[i]=itemList.get(i).getBilerId();
                biller_code_arr[i]=itemList.get(i).getBillerCode();
                biller_name_arr[i]=itemList.get(i).getBillerName();
                biller_inquiry_arr[i]=itemList.get(i).getBillerInquiry();
                product_code_arr[i]=itemList.get(i).getProductCode();
                product_name_arr[i]=itemList.get(i).getProductName();
                product_amount_arr[i]=itemList.get(i).getProductAmount();
                BillerVoucherListXl.add(new PurchaseVoucherXlModel(biller_id_arr[i], biller_code_arr[i], biller_name_arr[i], biller_inquiry_arr[i], product_code_arr[i], product_name_arr[i], product_amount_arr[i]));

            }
            BillerVoucherPhoneXl billerVoucherPhoneXl= new BillerVoucherPhoneXl(getActivity(), android.R.layout.simple_spinner_item, BillerVoucherListXl);
            cbo_voucher_name.setAdapter(billerVoucherPhoneXl);
        }
    }

    private void checkMenuCode() {

        try {

/*
            if(billerCode.equalsIgnoreCase("1074")){
                lbl_header.setText("Telkomsel");
            }else if(billerCode.equalsIgnoreCase("1075")){
                lbl_header.setText("Indosat Reguler");
            }else if(billerCode.equalsIgnoreCase("1076")){
                lbl_header.setText("Indosat GPRS");
            }else if(billerCode.equalsIgnoreCase("1077")){
                lbl_header.setText("Indosat SMS");
            }else if(billerCode.equalsIgnoreCase("1078")){
                lbl_header.setText("XL");
            }else if(billerCode.equalsIgnoreCase("1079")){
                lbl_header.setText("Axis");
            }else if(billerCode.equalsIgnoreCase("1080")){
                lbl_header.setText("Smartfren");
            }else if(billerCode.equalsIgnoreCase("1081")){
                lbl_header.setText("Flexi");
            }else if(billerCode.equalsIgnoreCase("1082")){
                lbl_header.setText("Esia");
            }else if(billerCode.equalsIgnoreCase("1083")){
                lbl_header.setText("Three");
            }else if(billerCode.equalsIgnoreCase("1084")){
                lbl_header.setText("Three Internet");
            }else if(billerCode.equalsIgnoreCase("1149")){
                lbl_header.setText("IM3 Smart");
            }else if(billerCode.equalsIgnoreCase("1150")){
                lbl_header.setText("StarOne");
            }else if(billerCode.equalsIgnoreCase("1148")){
                lbl_header.setText("Mentari");
            }else if(billerCode.equalsIgnoreCase("1085")){
                lbl_header.setText("Kartu As");
            }
*/
            if(billerCCode.equalsIgnoreCase("TSIM_PRE")){
                lbl_header.setText("Telkomsel");
                initNominalSimpati();
            }else if(billerCCode.equalsIgnoreCase("IDSR_PRE")){
                lbl_header.setText("Indosat Reguler");
            }else if(billerCCode.equalsIgnoreCase("IDSG_PRE")){
                lbl_header.setText("Indosat GPRS");
            }else if(billerCCode.equalsIgnoreCase("IDSS_PRE")){
                lbl_header.setText("Indosat SMS");
            }else if(billerCCode.equalsIgnoreCase("XLR_PRE")){
                lbl_header.setText("XL");
                initNominalXl();
            }else if(billerCCode.equalsIgnoreCase("AXS_PRE")){
                lbl_header.setText("Axis");
            }else if(billerCCode.equalsIgnoreCase("SMR_PRE")){
                lbl_header.setText("Smartfren");
                initNominalSmartfren();
            }else if(billerCCode.equalsIgnoreCase("FLE_PRE")){
                lbl_header.setText("Flexi");
            }else if(billerCCode.equalsIgnoreCase("ESA_PRE")){
                lbl_header.setText("Esia");
                initNominalEsia();
            }else if(billerCCode.equalsIgnoreCase("TRE_PRE")){
                lbl_header.setText("Three");
                initNominaThree();
            }else if(billerCCode.equalsIgnoreCase("THREE_PHON")){
                lbl_header.setText("Three Internet");
            }else if(billerCCode.equalsIgnoreCase("TRI_PRE")){
                lbl_header.setText("IM3 Smart");

            }else if(billerCCode.equalsIgnoreCase("IM3_PHONE")){
                lbl_header.setText("IM3 Smart");
                initNominalIm3Smart();
            }else if(billerCCode.equalsIgnoreCase("STR_PHONE")){
                lbl_header.setText("StarOne");
            }else if(billerCCode.equalsIgnoreCase("MNT_PHONE")){
                lbl_header.setText("Mentari");
                initNominalMentari();
            }else if(billerCCode.equalsIgnoreCase("TSAS_PRE")){
                lbl_header.setText("Kartu As");
                initNominalKartuAs();
            }else if(billerCCode.equalsIgnoreCase("BOLT_PRE")){
                lbl_header.setText("BOLT");
                initNominalBolt();
            }


           /* if (menu_biller_code.equals(purchaseVoucherPhone)) {
                lbl_header.setText("Handphone Voucher Purchase");
                lbl_title.setText("Handphone No");
                ic_img.setImageResource(R.drawable.ic_mobile);
                inpPhoneNumber.setHint("* Input Nandphone No");
                lbl_note.setText("*min 10 digits, max 16 digits");
            }
            if (menu_biller_code.equals(purchaseOthers)) {
                lbl_header.setText("Purchase Others");
                lbl_title.setText("Account No");
                ic_img.setImageResource(R.drawable.ic_voucher);
                inpPhoneNumber.setHint("* Input Account No");
            }*/


        } catch (Exception e) {
            e.printStackTrace();
        }

    }




    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);

        getSherlockActivity().getActionBar().removeAllTabs();
        getSherlockActivity().getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
    }

    public static byte[] encodeUrlSafe(byte[] data) {
        byte[] encode = Base64.encodeBase64(data);
        for (int i = 0; i < encode.length; i++) {
            if (encode[i] == '+') {
                encode[i] = '-';
            } else if (encode[i] == '=') {
                encode[i] = '_';
            } else if (encode[i] == '/') {
                encode[i] = '~';
            }
        }
        return encode;
    }
}

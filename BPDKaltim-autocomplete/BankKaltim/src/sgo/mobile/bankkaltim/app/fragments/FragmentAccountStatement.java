package sgo.mobile.bankkaltim.app.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.bankkaltim.AppHelper;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.activities.MainActivity;
import sgo.mobile.bankkaltim.app.adapter.AccountStatementAdapterList;
import sgo.mobile.bankkaltim.conf.AplConstants;
import sgo.mobile.bankkaltim.frameworks.net.Networks;
import sgo.mobile.bankkaltim.frameworks.net.loopj.android.http.CustomSSLSocketFactory;
import sgo.mobile.bankkaltim.frameworks.session.Session;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

/**
 * Created by User on 19/08/2014.
 */
public class FragmentAccountStatement extends Fragment {
    private ProgressDialog pDialog;
    FragmentManager fm;

    // declare view objects
    ListView listMenu;
    ProgressBar prgLoading;
    TextView txtAlert,txtHeader,lbl_error;
    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");//dd/MM/yyyy
    Date now = new Date();
    String dateTime = sdfDate.format(now);
    boolean networks;

    AccountStatementAdapterList accountStatementAdapterList;

    // create arraylist variables to store data from server

    public static ArrayList<String> acc_no = new ArrayList<String>();
    public static ArrayList<String> acc_name = new ArrayList<String>();
    public static ArrayList<String> ccy_id = new ArrayList<String>();
    public static ArrayList<String> acct_bal = new ArrayList<String>();
    public static ArrayList<String> debet = new ArrayList<String>();
    public static ArrayList<String> credit = new ArrayList<String>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.fragment_account_list, container, false);
        prgLoading = (ProgressBar) view.findViewById(R.id.prgLoading);
        listMenu   = (ListView) view.findViewById(R.id.listMenu);
        txtHeader = (TextView) view.findViewById(R.id.label_header);
        txtHeader.setVisibility(View.GONE);

        lbl_error = (TextView) view.findViewById(R.id.label_error);
        lbl_error.setVisibility(View.GONE);
        accountStatementAdapterList = new AccountStatementAdapterList(getActivity());
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_left);


        try {
            networks = new Networks().isConnectingToInternet(getActivity());
            if (networks) {
                parseJSONData();
            } else {
                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                alert.setTitle(getResources().getString(R.string.lbl_account_statement_title));
                alert.setMessage(getResources().getString(R.string.lbl_alert_connection));
                alert.setPositiveButton("OK", null);
                alert.show();
            }
        }catch (Exception e){
            e.printStackTrace();
        }


        // event listener to handle list when clicked
        listMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {
                Fragment newFragment = null;
                newFragment = new FragmentAccountStatementInquiry();
                Bundle args = new Bundle();
                args.putString("account_no", acc_no.get(position));
                newFragment.setArguments(args);
                switchFragment(newFragment);
            }
        });


        return view;
    }

    public void parseJSONData(){
        clearData();
        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getActivity()));
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);

        RequestParams params = new RequestParams();

        try {
            String userid = AppHelper.getUserId(getActivity());
            String bankcode = AplConstants.Bankcode;
            String pcd = AplConstants.pcd;
            String URL = AplConstants.AccountListAPI ;
            String appName = AplConstants.appName;
            String serviceName = URL.split("/")[4];
            String apiKey = AppHelper.getAccessKey(getActivity());
            UUID uniqueKey = UUID.randomUUID();

            String message = uniqueKey+dateTime + appName + serviceName+ bankcode + userid;

            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] hmacData = sha256_HMAC.doFinal(message.getBytes("UTF-8"));
            String signatureKey = new String(encodeUrlSafe(hmacData));

            params.put("userid", userid);
            params.put("bankcode", bankcode);
            params.put("passcode", pcd);
            params.put("rc_uuid", uniqueKey.toString());
            params.put("rc_dtime",dateTime );
            params.put("signature",signatureKey);

        }catch (Exception e){
            System.out.println("Error");
        }

        client.get(AplConstants.AccountListAPI,params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject json) {




                try {
                    JSONObject body_data = json.getJSONObject("body_data");
                    String error_code = body_data.getString("error_code");
                    String error_message = body_data.getString("error_message");


                    if(error_code.equalsIgnoreCase("0000")) {
                        txtHeader.setVisibility(View.VISIBLE);
                        JSONArray data = body_data.getJSONArray("balance_account_data"); // this is the "items: [ ] part
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject object = data.getJSONObject(i);

                            // id.add(object.getString("id"));
                            acc_no.add(object.getString("account_no"));
                            acc_name.add(object.getString("account_name"));
                            ccy_id.add(object.getString("ccy_id"));
                            acct_bal.add(object.getString("acct_balance"));
                        }

                        // when finish parsing, hide progressbar
                        prgLoading.setVisibility(8);

                        // if data available show data on list
                        // otherwise, show alert text
                        if (acc_no.size() > 0) {
                            listMenu.setVisibility(0);
                            listMenu.setAdapter(accountStatementAdapterList);
                        } else {
                            txtHeader.setVisibility(View.GONE);
                            lbl_error.setVisibility(0);
                        }
                    } else if(error_code.equalsIgnoreCase("0404")){
                        prgLoading.setVisibility(View.GONE);
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                        alert.setMessage(error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Session session;
                                session = new Session(getActivity());
                                session.signOut();
                            }
                        });
                        alert.show();

                    }


                    else{
                        prgLoading.setVisibility(8);
                        lbl_error.setVisibility(0);
                        //lbl_error.setText(error_message);
                        }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();

                }
            };


            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (pDialog != null) {
                    pDialog.dismiss();
                }
                if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                } else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                }
            }

        });
    }

    // clear arraylist variables before used
    void clearData(){
        //id.clear();
        acc_no.clear();
        acc_name.clear();
        ccy_id.clear();
        acct_bal.clear();
    }

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
        }
    };

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }

    public static byte[] encodeUrlSafe(byte[] data) {
        byte[] encode = Base64.encodeBase64(data);
        for (int i = 0; i < encode.length; i++) {
            if (encode[i] == '+') {
                encode[i] = '-';
            } else if (encode[i] == '=') {
                encode[i] = '_';
            } else if (encode[i] == '/') {
                encode[i] = '~';
            }
        }
        return encode;
    }



}



package sgo.mobile.bankkaltim.app.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.activeandroid.query.Select;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.bankkaltim.AppHelper;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.activities.MainActivity;
import sgo.mobile.bankkaltim.app.adapter.BankCodeAdapter;
import sgo.mobile.bankkaltim.app.adapter.DestinationAccountAdapters;
import sgo.mobile.bankkaltim.app.adapter.SourceAccountAdapters;
import sgo.mobile.bankkaltim.app.beans.SourceAccountBean;
import sgo.mobile.bankkaltim.app.models.BankListModel;
import sgo.mobile.bankkaltim.app.models.DestinationAccountListModel;
import sgo.mobile.bankkaltim.app.models.SourceAccountModel;
import sgo.mobile.bankkaltim.app.ui.dialog.DefinedDialog;
import sgo.mobile.bankkaltim.conf.AplConstants;
import sgo.mobile.bankkaltim.frameworks.net.Networks;
import sgo.mobile.bankkaltim.frameworks.net.loopj.android.http.CustomSSLSocketFactory;
import sgo.mobile.bankkaltim.frameworks.security.Encrypt;
import sgo.mobile.bankkaltim.frameworks.session.Session;
import sgo.mobile.bankkaltim.frameworks.text.NumberTextWacther;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class FragmentTransferDomestic extends SherlockFragment {

    private ProgressDialog pDialog;
    public String dataBankCode, dataBankName, dataAccountNo, dataAccountName, dataCcyId, dataEmail;
    private static Spinner cbo_source_account = null;
    //private static Spinner cbo_destination_accounts = null;
    private static Spinner spListBankCode = null;
    public String selectedBank;
    Encrypt encrypt = new Encrypt();
    public String[] bankCode_arr, bankName_arr;
    private ArrayList<BankListModel> BankCodeList = new ArrayList<BankListModel>();
    public String dataSourceAccount="";
    public String destination_account_name, destination_bank_code;
    List<SourceAccountModel> accountModels;
    public String[] dest_bank_code_arr, dest_bank_name_arr, dest_account_no_arr, dest_account_name_arr, dest_ccy_id_arr, dest_email_arr;
    private ArrayList<DestinationAccountListModel> DestinationAccountList = new ArrayList<DestinationAccountListModel>();
    private static final char[] CHARSET_AZ_09 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();
    public String[] bankcode, bankname;
    boolean setVISIBLE;
    Button btnDone;
    TextView lbl_Date, lblBankName;

    EditText inpAmount, inpMemo, inpDestAccount;
    public String mydate = java.text.DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());
    private static AutoCompleteTextView autoCompleteBenefAccount;
    DateFormat df = new SimpleDateFormat(" dd MMMM yyyy");
    DateFormat rq_dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    String date = df.format(Calendar.getInstance().getTime());
    String dateTime = rq_dateFormat.format(Calendar.getInstance().getTime());
    boolean networks;
    String amount;
    public String posisi;
    int getCountSourceAccount, getCountDestAccount, getCountBankList, getCountBeneficiaryInquiry;

    public String[] account_no_arr, account_name_arr, ccy_id_arr;
    private ArrayList<SourceAccountModel> SourceAcccountList = new ArrayList<SourceAccountModel>();
    public String dataDestAccount;
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        setUserVisibleHint(setVISIBLE);
        View view = inflater.inflate(R.layout.fragment_transfer_domestic, container, false);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                Context.INPUT_METHOD_SERVICE);
        getCountSourceAccount = new Select().from(SourceAccountModel.class).count();
        getCountDestAccount = new Select().from(DestinationAccountListModel.class).count();
        getCountBankList = new Select().from(BankListModel.class).count();
        cbo_source_account = (Spinner) view.findViewById(R.id.cbo_source_account);
        autoCompleteBenefAccount = (AutoCompleteTextView) view.findViewById(R.id.autoCompleteBenefAccount);
        spListBankCode = (Spinner) view.findViewById(R.id.spListBankCode);
        dataDestAccount = AppHelper.getDestAccount(getSherlockActivity());
        /* View Properties */
        TextView lbl_null = (TextView) view.findViewById(R.id.lbl_null);
        TableRow tbl_row_sourceaccount_title = (TableRow) view.findViewById(R.id.tbl_row_sourceaccount_title);
        TableRow tbl_row_sourceaccount = (TableRow) view.findViewById(R.id.tbl_row_sourceaccount);
        TableRow tbl_row_bank_title = (TableRow) view.findViewById(R.id.tbl_row_bank_title);
        TableRow tbl_row_bank = (TableRow) view.findViewById(R.id.tbl_row_bank);
        TableRow tbl_row_beneficiary_title = (TableRow) view.findViewById(R.id.tbl_row_beneficary_title);
        TableRow tbl_row_beneficiary = (TableRow) view.findViewById(R.id.tbl_row_beneficary);
        TableRow tbl_row_amount_title = (TableRow) view.findViewById(R.id.tbl_row_amount_title);
        TableRow tbl_row_amount = (TableRow) view.findViewById(R.id.tbl_row_amount);
        TableRow tbl_row_message_title = (TableRow) view.findViewById(R.id.tbl_row_message_title);
        TableRow tbl_row_message = (TableRow) view.findViewById(R.id.tbl_row_message);
        TableRow tbl_row_date_title = (TableRow) view.findViewById(R.id.tbl_row_date_title);
        TableRow tbl_row_date = (TableRow) view.findViewById(R.id.tbl_row_date);
        View line = view.findViewById(R.id.line);
        View line1 = view.findViewById(R.id.line1);
        View line2 = view.findViewById(R.id.line2);
        View line3 = view.findViewById(R.id.line3);
        View line4 = view.findViewById(R.id.line4);


        btnDone = (Button) view.findViewById(R.id.btnDone);
        inpAmount = (EditText) view.findViewById(R.id.inpAmount);
        inpAmount.addTextChangedListener(new NumberTextWacther(inpAmount));
        inpMemo = (EditText) view.findViewById(R.id.inpMemo);
        imm.hideSoftInputFromWindow(inpAmount.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(inpMemo.getWindowToken(), 0);
        lbl_Date = (TextView) view.findViewById(R.id.lblDate);
        lbl_Date.setText(date);
        lbl_Date.setClickable(true);
        lbl_Date.setFocusable(false);

       /* String bcd = AplConstants.Bankcode;
        if(bcd.equalsIgnoreCase("134")){
            line.setVisibility(View.GONE);
            line1.setVisibility(View.GONE);
            line2.setVisibility(View.GONE);
            line3.setVisibility(View.GONE);
            line4.setVisibility(View.GONE);

            tbl_row_sourceaccount_title.setVisibility(View.GONE);
            tbl_row_sourceaccount.setVisibility(View.GONE);
            tbl_row_bank_title.setVisibility(View.GONE);
            tbl_row_bank.setVisibility(View.GONE);
            tbl_row_beneficiary_title.setVisibility(View.GONE);
            tbl_row_beneficiary.setVisibility(View.GONE);
            tbl_row_amount_title.setVisibility(View.GONE);
            tbl_row_amount.setVisibility(View.GONE);
            tbl_row_message_title.setVisibility(View.GONE);
            tbl_row_message.setVisibility(View.GONE);
            tbl_row_date_title.setVisibility(View.GONE);
            tbl_row_date.setVisibility(View.GONE);
            lbl_null.setVisibility(View.VISIBLE);
            btnDone.setVisibility(View.GONE);
        }*/

        lbl_Date.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                alert.setTitle(getResources().getString(R.string.lbl_tab_domestic));
                alert.setMessage(getResources().getString(R.string.lbl_transfer_date_alert));
                alert.setPositiveButton("OK", null);
                alert.show();
            }
        });

        inpAmount.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                imm.showSoftInputFromInputMethod(inpAmount.getWindowToken(), 0);
            }
        });


        inpMemo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                imm.showSoftInputFromInputMethod(inpMemo.getWindowToken(), 0);
            }
        });

        try {
            networks = new Networks().isConnectingToInternet(getActivity());
            if (networks) {
                initBankList();
            } else {
                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                alert.setTitle(getResources().getString(R.string.lbl_tab_domestic));
                alert.setMessage(getResources().getString(R.string.lbl_alert_connection));
                alert.setPositiveButton("OK", null);
                alert.show();
                return view;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        autoCompleteBenefAccount.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                autoCompleteBenefAccount.showDropDown();

            }
        });

        autoCompleteBenefAccount.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DestinationAccountListModel destinationAccountListModel = (DestinationAccountListModel) parent.getItemAtPosition(position);

                dataBankCode = destinationAccountListModel.dest_bank_code;
                dataBankName = destinationAccountListModel.dest_bank_name;
                dataAccountNo = destinationAccountListModel.dest_account_no;
                dataAccountName = destinationAccountListModel.dest_account_name;
                dataCcyId = destinationAccountListModel.dest_ccy_id;
                dataEmail = destinationAccountListModel.dest_email;

                posisi = parent.getItemAtPosition(position).toString();

            }
        });

//        autoCompleteBenefAccount.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                BankListModel bankListModel = (BankListModel)parent.getItemAtPosition(position);
//
//                selectedBank = bankListModel.bankCode;
//            }
//        });


        btnDone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {

                if (autoCompleteBenefAccount.getText().toString().trim().equalsIgnoreCase("")) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setTitle(getResources().getString(R.string.lbl_tab_domestic));
                    alert.setMessage(getResources().getString(R.string.lbl_transfer_no_benef));
                    alert.setPositiveButton("OK", null);
                    alert.show();
                    return;
                }

                if(!autoCompleteBenefAccount.getText().toString().equals(posisi)){
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setTitle(getResources().getString(R.string.lbl_tab_inhouse));
                    alert.setMessage(getResources().getString(R.string.lbl_transfer_no_select_benef));
                    alert.setPositiveButton("OK", null);
                    alert.show();
                    return;
                }

                final String txtAmount = inpAmount.getText().toString();
                if(autoCompleteBenefAccount.getText().toString().trim().length()==0){
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setTitle(getResources().getString(R.string.lbl_tab_domestic));
                    alert.setMessage(getResources().getString(R.string.lbl_transfer_no_benef));
                    alert.setPositiveButton("OK", null);
                    alert.show();
                    return;
                }
                if (txtAmount.equalsIgnoreCase("")) {

                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setTitle(getResources().getString(R.string.lbl_tab_domestic));
                    alert.setMessage(getResources().getString(R.string.lbl_transfer_alert_amount));
                    alert.setPositiveButton("OK", null);
                    alert.show();
                    return;
                }

                final String txtMessage = inpMemo.getText().toString();
                if (txtMessage.equalsIgnoreCase("")) {

                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setTitle(getResources().getString(R.string.lbl_tab_domestic));
                    alert.setMessage(getResources().getString(R.string.lbl_transfer_alert_message));
                    alert.setPositiveButton("OK", null);
                    alert.show();
                    return;

                } else {
                    validate();
                }

            }
        });


        return view;
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisible()) {
            if (isVisibleToUser) {
                initSourceAccount();

            } else {
            }
        }
    }

    public void validate() {
        ////Validation Here
        pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.lbl_loading_process));
        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getSherlockActivity()));
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
        RequestParams params = new RequestParams();

        try {
            SourceAccountModel sourceAccValues = (SourceAccountModel) cbo_source_account.getSelectedItem();
            String source_account_name = sourceAccValues.account_name;
            String source_account_no = sourceAccValues.account_no;

            amount = inpAmount.getText().toString().replaceAll("[\\-\\+\\.\\^:,]", "");

            String message = inpMemo.getText().toString();

            String userid = AppHelper.getUserId(getActivity());
            String bankcode = AplConstants.Bankcode;
            String pcd = AplConstants.pcd;
            String currencyType = AplConstants.CurrencyIDR;
            String trxId = randomTrxId(CHARSET_AZ_09, 10);
            String executetransferValidate = AplConstants.executetransferValidate;
            String URL = AplConstants.FundDomesticTransferAPI;
            String appName = AplConstants.appName;
            String serviceName = URL.split("/")[4];
            String apiKey = AppHelper.getAccessKey(getActivity());
            UUID uniqueKey = UUID.randomUUID();
            String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;

            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
            String signatureKey = new String(encodeUrlSafe(hmacData));
            params.put("userid", userid);
            params.put("bankcode", bankcode);
            params.put("passcode", pcd);
            params.put("sourceacctccy", currencyType);
            params.put("sourceacctno", source_account_no);
            params.put("destaccountno", dataAccountNo);
            params.put("destaccountemail", dataEmail);
            params.put("destaccountname", dataAccountName);
            params.put("destbankcode", dataBankCode);
            params.put("transactionid", trxId);
            params.put("transactionamount", amount);
            params.put("description", message);
            params.put("executetransfer", executetransferValidate);
            params.put("rc_uuid", uniqueKey.toString());
            params.put("rc_dtime", dateTime);
            params.put("signature", signatureKey);

        } catch (Exception e) {
            System.out.println("Error");
        }

        client.post(AplConstants.FundDomesticTransferAPI, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject object) {


                try {

                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    JSONObject body_data = object.getJSONObject("body_data");

                    String error_code = body_data.getString("error_code");
                    String error_message = body_data.getString("error_message");

                    if (error_code.equalsIgnoreCase("0000")) {

                        // submitData();
                        SourceAccountModel sourceAccValues = (SourceAccountModel) cbo_source_account.getSelectedItem();
                        String source_account_name = sourceAccValues.account_name;
                        String source_account_no = sourceAccValues.account_no;

                        String memo = inpMemo.getText().toString();
                        String paymentdate = lbl_Date.getText().toString();
                        Fragment newFragment = null;
                        newFragment = new FragmentTransferDomesticConfirmToken();
                        Bundle args = new Bundle();
                        args.putString("source_account_name", source_account_name);
                        args.putString("source_account_no", source_account_no);
                        args.putString("dest_bank_name", dataBankName);
                        args.putString("dest_account_no", dataAccountNo);
                        args.putString("dest_bank_code", dataBankCode);
                        args.putString("dest_account_name", dataAccountName);
                        args.putString("totalAmount", amount);
                        args.putString("memo", memo);
                        args.putString("email", dataEmail);
                        args.putString("paymentdate", paymentdate);
                        newFragment.setArguments(args);
                        switchFragment(newFragment);

                    } else if (error_code.equalsIgnoreCase("0404")) {

                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                        alert.setMessage(error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Session session;
                                session = new Session(getActivity());
                                session.signOut();
                            }
                        });
                        alert.show();

                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_tab_domestic));
                        alert.setMessage(getResources().getString(R.string.lbl_tab_domestic) + error_message);
                        alert.setPositiveButton("OK", null);
                        alert.show();
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            public void onFailure(Throwable error, String content) {
                if (pDialog != null) {
                    pDialog.dismiss();
                }

                if(error.getMessage().equalsIgnoreCase("no peer certificate"))
                {
                    pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.SSLhandler_dialog_message) + error.toString());
                }
                else
                {
                    pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.inethandler_dialog_message) + error.toString());
                }
                Toast.makeText(getActivity(), getResources().getString(R.string.lbl_alert_unexpected_connection), Toast.LENGTH_LONG).show();
            }

        });
        /// End of Transfer Validation
    }


    private void submitData() {

        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getSherlockActivity()));
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
        RequestParams params = new RequestParams();

        try {

            String userid = AppHelper.getUserId(getActivity());
            String bankcode = AplConstants.Bankcode;
            String pcd = AplConstants.pcd;
            String ccy = AplConstants.CurrencyIDR;

            SourceAccountBean sourceAccValues = (SourceAccountBean) cbo_source_account.getSelectedItem();
            String source_account_name = sourceAccValues.account_name;
            String source_account_no = sourceAccValues.account_no;
            DestinationAccountListModel domesticAccountBean = (DestinationAccountListModel) autoCompleteBenefAccount.getAdapter();
            String dest_bank_name = domesticAccountBean.dest_bank_name;
            String dest_bank_code = domesticAccountBean.dest_bank_code;
            String dest_acct_name = domesticAccountBean.dest_account_name;
            String dest_acct_no = domesticAccountBean.dest_account_no;
            String dest_email = domesticAccountBean.dest_email;

            String inputAmount = inpAmount.getText().toString();
            String URL = AplConstants.FundTransferInquiryAPI;
            String appName = AplConstants.appName;
            String serviceName = URL.split("/")[4];
            String apiKey = AppHelper.getAccessKey(getActivity());
            UUID uniqueKey = UUID.randomUUID();
            String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;


            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
            String signatureKey = new String(encodeUrlSafe(hmacData));
            params.put("userid", userid);
            params.put("bankcode", bankcode);
            params.put("passcode", pcd);
            params.put("sourceacctccy", ccy);
            params.put("sourceacctno", source_account_no);
            params.put("destaccountno", dest_acct_no);
            params.put("destbankcode", dest_bank_code);
            params.put("transactionamount", inputAmount);
            params.put("rc_uuid", uniqueKey.toString());
            params.put("rc_dtime", dateTime);
            params.put("signature", signatureKey);

        } catch (Exception e) {
            System.out.println("Error");
        }
        client.post(AplConstants.FundTransferInquiryAPI, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject object) {


                try {

                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    JSONObject body_data = object.getJSONObject("body_data");
                    String error_code = body_data.getString("error_code");
                    String error_message = body_data.getString("error_message");
                    destination_account_name = body_data.getString("destination_account_name");
                    destination_bank_code = body_data.getString("destination_bank_code");

                    if (error_code.equalsIgnoreCase("0000")) {
                        // Toast.makeText(getActivity(), "Success", Toast.LENGTH_LONG).show();
                        SourceAccountBean sourceAccValues = (SourceAccountBean) cbo_source_account.getSelectedItem();
                        String source_account_name = sourceAccValues.account_name;
                        String source_account_no = sourceAccValues.account_no;
                        DestinationAccountListModel domesticAccountBean = (DestinationAccountListModel) autoCompleteBenefAccount.getAdapter();
                        String dest_bank_name = domesticAccountBean.dest_bank_name;
                        String dest_bank_code = domesticAccountBean.dest_bank_code;
                        String dest_acct_name = domesticAccountBean.dest_account_name;
                        String dest_acct_no = domesticAccountBean.dest_account_no;
                        String dest_email = domesticAccountBean.dest_email;
                        String amount = inpAmount.getText().toString();
                        String memo = inpMemo.getText().toString();
                        String paymentdate = lbl_Date.getText().toString();

                        Fragment newFragment = null;
                        newFragment = new FragmentTransferDomesticConfirmToken();
                        Bundle args = new Bundle();
                        args.putString("source_account_name", source_account_name);
                        args.putString("source_account_no", source_account_no);
                        args.putString("dest_bank_name", dest_bank_name);
                        args.putString("dest_account_no", dest_acct_no);
                        args.putString("dest_bank_code", dest_bank_code);
                        args.putString("dest_account_name", dest_acct_name);
                        args.putString("totalAmount", amount);
                        args.putString("memo", memo);
                        args.putString("email", dest_email);
                        args.putString("paymentdate", paymentdate);
                        newFragment.setArguments(args);
                        switchFragment(newFragment);


                    } else if (error_code.equalsIgnoreCase("0404")) {

                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                        alert.setMessage(error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Session session;
                                session = new Session(getActivity());
                                session.signOut();
                            }
                        });
                        alert.show();

                    } else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_tab_domestic));
                        alert.setMessage(getResources().getString(R.string.lbl_transfer_success_inhouse) + " " + error_message);
                        alert.setPositiveButton("OK", null);
                        alert.show();

                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            public void onFailure(Throwable error, String content) {
                if (pDialog != null) {
                    pDialog.dismiss();
                }

                if(error.getMessage().equalsIgnoreCase("no peer certificate"))
                {
                    pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.SSLhandler_dialog_message) + error.toString());
                }
                else
                {
                    pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.inethandler_dialog_message) + error.toString());
                }
                Toast.makeText(getActivity(), getResources().getString(R.string.lbl_alert_unexpected_connection), Toast.LENGTH_LONG).show();
            }
        });
    }


    private void initSourceAccount() {
        SourceAcccountList.clear();
        dataSourceAccount = AppHelper.getSourceAccount(getSherlockActivity());
        try {
            JSONArray data = new JSONArray(dataSourceAccount);
            int len = data.length();
            account_no_arr = new String[len];
            account_name_arr = new String[len];
            ccy_id_arr = new String[len];
            for (int i = 0; i < data.length(); i++) {
                account_no_arr[i] = data.getJSONObject(i).getString("account_no");
                account_name_arr[i] = data.getJSONObject(i).getString("account_name");
                ccy_id_arr[i] = data.getJSONObject(i).getString("ccy_id");
                SourceAcccountList.add(new SourceAccountModel(account_no_arr[i], account_name_arr[i], ccy_id_arr[i]));
            }
            SourceAccountAdapters cAdapter = new SourceAccountAdapters(getActivity(), android.R.layout.simple_spinner_item, SourceAcccountList);
            cbo_source_account.setAdapter(cAdapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onStart() {
        initSourceAccount();
        super.onStart();
    }

    @Override
    public void onResume() {
        initSourceAccount();
        super.onResume();
    }

    private void initBankList() {

        if (getCountBankList == 0) {
            pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.lbl_loading_data));

            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getSherlockActivity()));
            client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
            RequestParams params = new RequestParams();
            try {
                String userid = AppHelper.getUserId(getActivity());
                String bankcode = AplConstants.Bankcode;
                String pcd = AplConstants.pcd;
                String URL = AplConstants.BankCodeAPI;
                String appName = AplConstants.appName;
                String serviceName = URL.split("/")[4];
                String apiKey = AppHelper.getAccessKey(getActivity());
                UUID uniqueKey = UUID.randomUUID();
                String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;
                Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
                SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
                sha256_HMAC.init(secret_key);
                byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
                String signatureKey = new String(encodeUrlSafe(hmacData));
                params.put("userid", userid);
                params.put("bankcode", bankcode);
                params.put("passcode", pcd);
                params.put("rc_uuid", uniqueKey.toString());
                params.put("rc_dtime", dateTime);
                params.put("signature", signatureKey);
            } catch (Exception e) {
                System.out.println("Error");
            }
            client.post(AplConstants.BankCodeAPI, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject object) {

                    try {
                        if (pDialog != null) {
                            pDialog.dismiss();
                        }
                        JSONObject body_data = object.getJSONObject("body_data");
                        String error_code = body_data.getString("error_code");
                        String error_message = body_data.getString("error_message");
                        if (error_code.equalsIgnoreCase("0000")) {
                            JSONArray data = body_data.getJSONArray("bank_data");
                            int len = data.length();
                            String bc = AplConstants.Bankcode;
                            bankCode_arr = new String[len];
                            bankName_arr = new String[len];
                            for (int i = 0; i < data.length(); i++) {
                                bankCode_arr[i] = data.getJSONObject(i).getString("bankCode");
                                bankName_arr[i] = data.getJSONObject(i).getString("bankName");
                                BankListModel bankListModel = new BankListModel();
                                bankListModel.bankCode = data.getJSONObject(i).getString("bankCode");
                                bankListModel.bankName = data.getJSONObject(i).getString("bankName");
                                bankListModel.save();
                                if (bankCode_arr[i].equals(bc)) {
                                    BankCodeList.remove(bankCode_arr[i].equals(bc));

                                } else {
                                    BankCodeList.add(new BankListModel(bankCode_arr[i], bankName_arr[i]));
                                }


                            }

                            BankCodeAdapter bankCodeAdapter = new BankCodeAdapter(getActivity(), android.R.layout.simple_spinner_item, BankCodeList);
                            spListBankCode.setAdapter(bankCodeAdapter);
                            spListBankCode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    BankListModel selectedBank = BankCodeList.get(position);
                                    DestinationAccountList.clear();
                                    AsyncHttpClient clients = new AsyncHttpClient();
                                    clients.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getSherlockActivity()));
                                    clients.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
                                    RequestParams params = new RequestParams();

                                    try {
                                        String userid = AppHelper.getUserId(getActivity());
                                        String bankcode = AplConstants.Bankcode;
                                        String pcd = AplConstants.pcd;
                                        String type = AplConstants.TypeBeneficiary;
                                        String URL = AplConstants.DestinationAccountListAPI;
                                        String appName = AplConstants.appName;
                                        String serviceName = URL.split("/")[4];
                                        String apiKey = AppHelper.getAccessKey(getActivity());
                                        UUID uniqueKey = UUID.randomUUID();
                                        String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;

                                        Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
                                        SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
                                        sha256_HMAC.init(secret_key);
                                        byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
                                        String signatureKey = new String(encodeUrlSafe(hmacData));
                                        params.put("userid", userid);
                                        params.put("bankcode", bankcode);
                                        params.put("passcode", pcd);
                                        params.put("destbankcode", selectedBank.getBankCode());
                                        params.put("type", type);
                                        params.put("rc_uuid", uniqueKey.toString());
                                        params.put("rc_dtime", dateTime);
                                        params.put("signature", signatureKey);

                                    } catch (Exception e) {
                                        System.out.println("Error");
                                    }
                                    clients.post(AplConstants.DestinationAccountListAPI, params, new JsonHttpResponseHandler() {
                                        @Override
                                        public void onSuccess(int statusCode, Header[] headers, JSONObject object) {


                                            try {
                                                if (pDialog != null) {
                                                    pDialog.dismiss();
                                                }
                                                JSONObject body_data = object.getJSONObject("body_data");
                                                String error_code = body_data.getString("error_code");
                                                String error_message = body_data.getString("error_message");


                                                if (error_code.equalsIgnoreCase("0000")) {
                                                    //--------------------------------------------------------------------------------------------
                                                    String a = null;
                                                    JSONArray data = body_data.getJSONArray("balance_account_data"); // this is the "items: [ ] part
                                                    if (data.isNull(0)) {
                                                        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                                                                R.array.no_benef_bank, android.R.layout.simple_spinner_item);
                                                        autoCompleteBenefAccount.setAdapter(adapter);
                                                        autoCompleteBenefAccount.setClickable(false);

                                                    } else {
                                                        int len = data.length();
                                                        dest_bank_code_arr = new String[len];
                                                        dest_bank_name_arr = new String[len];
                                                        dest_account_no_arr = new String[len];
                                                        dest_account_name_arr = new String[len];
                                                        dest_ccy_id_arr = new String[len];
                                                        dest_email_arr = new String[len];
                                                        for (int i = 0; i < data.length(); i++) {
                                                            dest_bank_code_arr[i] = data.getJSONObject(i).getString("bank_code");
                                                            dest_bank_name_arr[i] = data.getJSONObject(i).getString("bank_name");
                                                            dest_account_no_arr[i] = data.getJSONObject(i).getString("account_no");
                                                            dest_account_name_arr[i] = data.getJSONObject(i).getString("account_name");
                                                            dest_ccy_id_arr[i] = data.getJSONObject(i).getString("ccy_id");
                                                            dest_email_arr[i] = data.getJSONObject(i).getString("email");
                                                            DestinationAccountList.add(new DestinationAccountListModel(dest_bank_code_arr[i], dest_bank_name_arr[i], dest_account_no_arr[i], dest_account_name_arr[i], dest_ccy_id_arr[i], dest_email_arr[i]));
                                                        }


                                                        DestinationAccountAdapters cAdapter = new DestinationAccountAdapters(getActivity(), android.R.layout.simple_spinner_item, DestinationAccountList);
                                                        autoCompleteBenefAccount.setAdapter(cAdapter);

                                                    }

                                                } else if (error_code.equalsIgnoreCase("0404")) {

                                                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                                    alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                                                    alert.setMessage(error_message);
                                                    alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            Session session;
                                                            session = new Session(getActivity());
                                                            session.signOut();
                                                        }
                                                    });
                                                    alert.show();

                                                } else {
                                                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                                    alert.setTitle(getResources().getString(R.string.lbl_tab_domestic));
                                                    alert.setMessage(getResources().getString(R.string.lbl_tab_domestic) + error_message);
                                                    alert.setPositiveButton("OK", null);
                                                    alert.show();
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });


                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });


                        } else if (error_code.equalsIgnoreCase("0404")) {

                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                            alert.setMessage(error_message);
                            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Session session;
                                    session = new Session(getActivity());
                                    session.signOut();
                                }
                            });
                            alert.show();

                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_tab_domestic));
                            alert.setMessage(getResources().getString(R.string.lbl_tab_domestic) + error_message);
                            alert.setPositiveButton("OK", null);
                            alert.show();

                        }

                        // End of Get Bank

                    } catch (
                            JSONException e
                            )

                    {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

                public void onFailure(Throwable error, String content) {
                    if (pDialog != null) {
                        pDialog.dismiss();
                    }

                    if(error.getMessage().equalsIgnoreCase("no peer certificate"))
                    {
                        pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.SSLhandler_dialog_message) + error.toString());
                    }
                    else
                    {
                        pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.inethandler_dialog_message) + error.toString());
                    }
                    Toast.makeText(getActivity(), getResources().getString(R.string.lbl_alert_unexpected_connection), Toast.LENGTH_LONG).show();
                }

            });
        } else {

            String bc = AplConstants.Bankcode;
            List<BankListModel> itemList = new Select().all().from(BankListModel.class).execute();


            int len = itemList.size();
            bankCode_arr = new String[len];
            bankName_arr = new String[len];
            for (int i = 0; i < itemList.size(); i++) {
                bankCode_arr[i] = itemList.get(i).getBankCode();
                bankName_arr[i] = itemList.get(i).getBankName();
                if (bankCode_arr[i].equals(bc)) {
                    BankCodeList.remove(bankCode_arr[i].equals(bc));

                } else {
                    BankCodeList.add(new BankListModel(bankCode_arr[i], bankName_arr[i]));
                }

            }
            BankCodeAdapter bankCodeAdapter = new BankCodeAdapter(getActivity(), android.R.layout.simple_spinner_item, BankCodeList);
            spListBankCode.setAdapter(bankCodeAdapter);

        }
    }

    private void initDestAccount() {

//      //  BankCodeBean selectedBank = BankCodeList.get(position);
//        DestinationAccountList.clear();
//        AsyncHttpClient clients = new AsyncHttpClient();
//        clients.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
//        clients.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
//        RequestParams params = new RequestParams();
//
//        try {
//            String userid = AppHelper.getUserId(getActivity());
//            String bankcode = AplConstants.Bankcode;
//            String pcd = AplConstants.pcd;
//            String type = AplConstants.TypeBeneficiary;
//            String URL = AplConstants.DestinationAccountListAPI;
//            String appName = AplConstants.appName;
//            String serviceName = URL.split("/")[4];
//            String apiKey = AppHelper.getAccessKey(getActivity());
//            UUID uniqueKey = UUID.randomUUID();
//            String signature = uniqueKey + dateTime + appName + serviceName + bankcode + userid;
//
//            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
//            SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
//            sha256_HMAC.init(secret_key);
//            byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
//            String signatureKey = new String(encodeUrlSafe(hmacData));
//            params.put("userid", userid);
//            params.put("bankcode", bankcode);
//            params.put("passcode", pcd);
//           // params.put("destbankcode",selectedBank.getBank_code());
//            params.put("type", type);
//            params.put("rc_uuid", uniqueKey.toString());
//            params.put("rc_dtime", dateTime);
//            params.put("signature", signatureKey);
//
//        } catch (Exception e) {
//            System.out.println("Error");
//        }
//

//        clients.post(AplConstants.DestinationAccountListAPI, params, new AsyncHttpResponseHandler() {
//            public void onSuccess(String content) {

//                try {
//                    if (pDialog != null) {
//                        pDialog.dismiss();
//                    }
//
//                    JSONObject object = new JSONObject(content);
//                    JSONObject body_data = object.getJSONObject("body_data");
//                    String error_code = body_data.getString("error_code");
//                    String error_message = body_data.getString("error_message");
//
//
//                    if (error_code.equalsIgnoreCase("0000")) {
//                        //--------------------------------------------------------------------------------------------
//                        String a = null;
//                        JSONArray data = body_data.getJSONArray("balance_account_data"); // this is the "items: [ ] part
//                        if (data.isNull(0)) {
//                            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
//                                    R.array.no_benef_bank, android.R.layout.simple_spinner_item);
//                            cbo_destination_accounts.setAdapter(adapter);
//                            cbo_destination_accounts.setClickable(false);
//                        } else {
//
//                            int len = data.length();
//                            dest_bank_code_arr = new String[len];
//                            dest_bank_name_arr = new String[len];
//                            dest_account_no_arr = new String[len];
//                            dest_account_name_arr = new String[len];
//                            dest_ccy_id_arr = new String[len];
//                            dest_email_arr = new String[len];
//                            for (int i = 0; i < data.length(); i++) {
//                                dest_bank_code_arr[i] = data.getJSONObject(i).getString("bank_code");
//                                dest_bank_name_arr[i] = data.getJSONObject(i).getString("bank_name");
//                                dest_account_no_arr[i] = data.getJSONObject(i).getString("account_no");
//                                dest_account_name_arr[i] = data.getJSONObject(i).getString("account_name");
//                                dest_ccy_id_arr[i] = data.getJSONObject(i).getString("ccy_id");
//                                dest_email_arr[i] = data.getJSONObject(i).getString("email");
//                                DestinationAccountList.add(new DomesticAccountBean(dest_bank_code_arr[i], dest_bank_name_arr[i], dest_account_no_arr[i], dest_account_name_arr[i], dest_ccy_id_arr[i], dest_email_arr[i]));
//                            }
//
//
//                            DestinationAccountAdapter cAdapter = new DestinationAccountAdapter(getActivity(), android.R.layout.simple_spinner_item, DestinationAccountList);
//                            cbo_destination_accounts.setAdapter(cAdapter);
//                        }
//
//
//                    }else if (error_code.equalsIgnoreCase("0404")) {
//
//                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
//                        alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
//                        alert.setMessage(error_message);
//                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                Session session;
//                                session = new Session(getActivity());
//                                session.signOut();
//                            }
//                        });
//                        alert.show();
//
//                    } else {
//                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
//                        alert.setTitle(getResources().getString(R.string.lbl_tab_domestic));
//                        alert.setMessage(getResources().getString(R.string.lbl_tab_domestic) + error_message);
//                        alert.setPositiveButton("OK",null);
//                        alert.show();
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            public void onFailure(Throwable error, String content) {
//                if (pDialog != null) {
//                    pDialog.dismiss();
//                }
//                Toast.makeText(getActivity(),getResources().getString(R.string.lbl_alert_unexpected_connection), Toast.LENGTH_LONG).show();
//            }
//        });

        DestinationAccountList.clear();

        List<DestinationAccountListModel> itemList = new Select().all().from(DestinationAccountListModel.class).where("dest_bank_code = ?", selectedBank).execute();

        int len = itemList.size();
        dest_bank_code_arr = new String[len];
        dest_bank_name_arr = new String[len];
        dest_account_no_arr = new String[len];
        dest_account_name_arr = new String[len];
        dest_ccy_id_arr = new String[len];
        dest_email_arr = new String[len];
        for (int i = 0; i < itemList.size(); i++) {
            dest_bank_code_arr[i] = itemList.get(i).getDest_bank_code();
            dest_bank_name_arr[i] = itemList.get(i).getDest_bank_name();
            dest_account_no_arr[i] = itemList.get(i).getDest_account_no();
            dest_account_name_arr[i] = itemList.get(i).getDest_account_name();
            dest_ccy_id_arr[i] = itemList.get(i).getDest_ccy_id();
            dest_email_arr[i] = itemList.get(i).getDest_email();
            DestinationAccountList.add(new DestinationAccountListModel(dest_bank_code_arr[i], dest_bank_name_arr[i], dest_account_no_arr[i], dest_account_name_arr[i], dest_ccy_id_arr[i], dest_email_arr[i]));

        }
        DestinationAccountAdapters cAdapter = new DestinationAccountAdapters(getActivity(), android.R.layout.simple_spinner_item, DestinationAccountList);
        autoCompleteBenefAccount.setAdapter(cAdapter);


    }


    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);

        getSherlockActivity().getActionBar().removeAllTabs();
        getSherlockActivity().getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
    }


    public static String randomTrxId(char[] characterSet, int length) {
        Random random = new java.security.SecureRandom();
        char[] result = new char[length];
        for (int i = 0; i < result.length; i++) {
            // picks a random index out of character set > random character
            int randomCharIndex = random.nextInt(characterSet.length);
            result[i] = characterSet[randomCharIndex];
        }
        return new String(result);
    }

    public static byte[] encodeUrlSafe(byte[] data) {
        byte[] encode = Base64.encodeBase64(data);
        for (int i = 0; i < encode.length; i++) {
            if (encode[i] == '+') {
                encode[i] = '-';
            } else if (encode[i] == '=') {
                encode[i] = '_';
            } else if (encode[i] == '/') {
                encode[i] = '~';
            }
        }
        return encode;
    }

}
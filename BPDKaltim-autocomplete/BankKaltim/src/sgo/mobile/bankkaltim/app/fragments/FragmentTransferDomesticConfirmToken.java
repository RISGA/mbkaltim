package sgo.mobile.bankkaltim.app.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.telephony.SmsMessage;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.bankkaltim.AppHelper;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.activities.MainActivity;
import sgo.mobile.bankkaltim.app.ui.dialog.DefinedDialog;
import sgo.mobile.bankkaltim.conf.AplConstants;
import sgo.mobile.bankkaltim.frameworks.math.FormatCurrency;
import sgo.mobile.bankkaltim.frameworks.net.Networks;
import sgo.mobile.bankkaltim.frameworks.net.loopj.android.http.CustomSSLSocketFactory;
import sgo.mobile.bankkaltim.frameworks.session.Session;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FragmentTransferDomesticConfirmToken extends SherlockFragment {

    private ProgressDialog pDialog;
    String source_account_no, source_account_name, source_bank_name ,source_bank_code, dest_account_name, dest_account_no, dest_bank_code, dest_bank_name,dest_email, amount, memo, paymentdate;
    Button btnDone, btnGetOtp, btnSubmitToken;
    EditText txtOtp;
    boolean networks;
    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");//dd/MM/yyyy
    Date now = new Date();
    String dateTime = sdfDate.format(now);

    View lineOtp;
    TableRow rowOtp;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_transfer_domestic_confirmtoken, container, false);

        Bundle bundle         = this.getArguments();
        source_account_name   = bundle.getString("source_account_name");
        source_account_no     = bundle.getString("source_account_no");
        source_bank_code      = bundle.getString("source_bank_code");
        source_bank_name      = bundle.getString("source_bank_name");
        dest_account_no       = bundle.getString("dest_account_no");
        dest_account_name     = bundle.getString("dest_account_name");
        dest_bank_code        = bundle.getString("dest_bank_code");
        dest_bank_name        = bundle.getString("dest_bank_name");
        amount                = bundle.getString("totalAmount");
        memo                  = bundle.getString("memo");
        dest_email            = bundle.getString("email");
        paymentdate           = bundle.getString("paymentdate");

        lineOtp = view.findViewById(R.id.lineOtp);
        rowOtp = (TableRow)view.findViewById(R.id.rowOtp);
        TextView lbl_source_account = (TextView) view.findViewById(R.id.lbl_source_account);
        lbl_source_account.setText(source_account_name);
        TextView lbl_bank_name = (TextView) view.findViewById(R.id.lbl_destBank_name);
        lbl_bank_name.setText(dest_bank_name);
        TextView lbl_destAcct_name = (TextView) view.findViewById(R.id.lbl_destAcct_name);
        lbl_destAcct_name.setText(dest_account_name);
        TextView lbl_destAcct_no = (TextView) view.findViewById(R.id.lbl_destAcct_no);
        lbl_destAcct_no.setText(dest_account_no);
        TextView lbl_dest_email = (TextView) view.findViewById(R.id.lbl_dest_email);
        lbl_dest_email.setText(dest_email);
        TextView lbl_amount        = (TextView) view.findViewById(R.id.lbl_amount);
        lbl_amount.setText(FormatCurrency.CurrencyIDR(amount));
        TextView lbl_message       = (TextView) view.findViewById(R.id.lbl_message);
        lbl_message.setText(memo);
        TextView lbl_date       = (TextView) view.findViewById(R.id.lbl_date);
        lbl_date.setText(paymentdate);

        btnDone = (Button) view.findViewById(R.id.btnDone);
        btnDone.setVisibility(View.GONE);
        btnDone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                final String txtToken = txtOtp.getText().toString();
                if (txtToken.equalsIgnoreCase("")) {
                   /* Toast.makeText(getActivity(), "Please insert token key code !!", Toast.LENGTH_LONG).show();
                    return;*/

                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setTitle(getResources().getString(R.string.lbl_tab_domestic));
                    alert.setMessage(getResources().getString(R.string.lbl_otp_validation_message));
                    alert.setPositiveButton("OK", null);
                    alert.show();
                    return;
                } else {

                    try {
                        networks = new Networks().isConnectingToInternet(getActivity());
                        if (networks) {
                            submitDomesticTransfer();
                        } else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_tab_domestic));
                            alert.setMessage(getResources().getString(R.string.lbl_alert_connection));
                            alert.setPositiveButton("OK", null);
                            alert.show();
                            return;

                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }

            }
        });

        btnGetOtp = (Button) view.findViewById(R.id.btnGetOtp);
        txtOtp = (EditText) view.findViewById(R.id.txtOtp);
        txtOtp.setVisibility(View.GONE);

        btnGetOtp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                requestToken();

            }
        });

        return view;
    }




    private void requestToken()
    {
        pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.lbl_loading_process));

        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getActivity()));
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
        RequestParams params = new RequestParams();

        try {
            String userid = AppHelper.getUserId(getActivity());
            String bankcode = AplConstants.Bankcode;
            String pcd = AplConstants.pcd;
            String URL = AplConstants.RequestTokenAPI;
            String appName = AplConstants.appName;
            String serviceName = URL.split("/")[4];
            String apiKey = AppHelper.getAccessKey(getActivity());
            UUID uniqueKey = UUID.randomUUID();
            String signature = uniqueKey+dateTime+ appName + serviceName+ bankcode + userid;

            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
            String signatureKey = new String(encodeUrlSafe(hmacData));
            params.put("userid", userid);
            params.put("bankcode", bankcode);
            params.put("passcode", pcd);
            params.put("rc_uuid", uniqueKey.toString());
            params.put("rc_dtime", dateTime);
            params.put("signature", signatureKey);

        }catch (Exception e){
            System.out.println("Error");
        }

        client.post(AplConstants.RequestTokenAPI, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject object) {


                try {

                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    JSONObject body_data = object.getJSONObject("body_data");
                    // parse json data and store into arraylist variables
                    String error_code         = body_data.getString("error_code");
                    String error_message         = body_data.getString("error_message");
                    if(error_code.equalsIgnoreCase("0000")){
                            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                            alert.setTitle(getResources().getString(R.string.lbl_alert_otp_title));
                            alert.setMessage(getResources().getString(R.string.lbl_alert_otp_message));
                            alert.setPositiveButton("OK",new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    btnGetOtp.setVisibility(View.GONE);
                                    btnDone.setVisibility(View.VISIBLE);
                                    txtOtp.setVisibility(View.VISIBLE);
                                    lineOtp.setVisibility(View.VISIBLE);
                                    rowOtp.setVisibility(View.VISIBLE);
                                }
                            });
                        alert.show();
                    }else if(error_code.equalsIgnoreCase("0404")){

                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                        alert.setMessage(error_message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Session session;
                                session = new Session(getActivity());
                                session.signOut();
                            }
                        });
                        alert.show();

                    }else{
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_alert_otp_title));
                        alert.setMessage(error_message);
                        alert.setPositiveButton("OK", null);
                        alert.show();
                        return;
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (pDialog != null) {
                    pDialog.dismiss();
                }
                if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                } else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                }
            }

        });
    }



    private static Map<String, String> parseSmsToken(String s){
        Map<String, String> ret = new HashMap<String, String>();
        StringTokenizer t = new StringTokenizer(s, "\n");
        while (t.hasMoreTokens()){
            String b = t.nextToken().trim();
            if (b.startsWith("mTOKEN : ")){
                String number = parseNumber(b);
                ret.put("token", number);
            }
        }

        return ret;
    }


    private static String parseNumber(String s){
        String numberFragment = s.substring(s.lastIndexOf(' '), s.length());//extract number
        return numberFragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        toggleMyBroadcastReceiver(true);
    }

    @Override
    public void onPause() {
        super.onPause();
        toggleMyBroadcastReceiver(false);
    }

    private void toggleMyBroadcastReceiver(Boolean _on){
        if (getActivity() == null)
            return;

        MainActivity fca = (MainActivity) getActivity();
        fca.togglerBroadcastReceiver(_on,myReceiver);
    }

    public BroadcastReceiver myReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle mBundle = intent.getExtras();
            SmsMessage[] mSMS =null;
            String strMessage = "";

            if(mBundle != null){
                Object[] pdus = (Object[]) mBundle.get("pdus");
                mSMS = new SmsMessage[pdus.length];

                for (int i = 0; i < mSMS.length ; i++){
                    mSMS[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                    strMessage += mSMS[i].getMessageBody();

                    final Matcher matcher = Pattern.compile("mTOKEN : ").matcher(strMessage);
                    if(matcher.find()){
                        // System.out.println(sentence.substring(matcher.end()).trim());
                        String res =strMessage.substring(matcher.end()).trim();
                        int lenght = res.length();
                        final String otp = res.substring(0,6);
                        txtOtp.setText(otp);
                    }
                }


            }
        }
    };

    private void submitDomesticTransfer()
    {
        pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, getResources().getString(R.string.lbl_loading_process));
        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(CustomSSLSocketFactory.getSSLSocketFactory(getActivity()));
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
        RequestParams params   = new RequestParams();
        try {

            String userid = AppHelper.getUserId(getActivity());
            String bankcode = AplConstants.Bankcode;
            String pcd = AplConstants.pcd;
            String currencyType = AplConstants.CurrencyIDR;
            String trxId = "";
            String executetransferProcess = AplConstants.executetransferProcess;
            String URL = AplConstants.FundDomesticTransferAPI;
            String appName = AplConstants.appName;
            String serviceName = URL.split("/")[4];
            String apiKey = AppHelper.getAccessKey(getActivity());
            UUID uniqueKey = UUID.randomUUID();
            String signature = uniqueKey+dateTime+ appName + serviceName+ bankcode + userid;

            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(apiKey.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] hmacData = sha256_HMAC.doFinal(signature.getBytes("UTF-8"));
            String signatureKey = new String(encodeUrlSafe(hmacData));

            params.put("userid", userid);
            params.put("bankcode", bankcode);
            params.put("passcode", pcd);
            params.put("sourceacctccy", currencyType);
            params.put("sourceacctno",source_account_no);
            params.put("destaccountno", dest_account_no);
            params.put("destaccountemail", dest_email);
            params.put("destaccountname",dest_account_name);
            params.put("destbankcode",dest_bank_code);
            params.put("transactionid",trxId);
            params.put("transactionamount",amount);
            params.put("responsecode", txtOtp.getText().toString());
            params.put("description",memo);
            params.put("executetransfer", executetransferProcess);
            params.put("rc_uuid", uniqueKey.toString());
            params.put("rc_dtime", dateTime);
            params.put("signature", signatureKey);

        }catch (Exception e){
            System.out.println("Error");
        }
        client.post(AplConstants.FundDomesticTransferAPI, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject object) {


                try {

                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    JSONObject body_data = object.getJSONObject("body_data");


                    String error_code         = body_data.getString("error_code");
                   final String message      = body_data.getString("error_message");
                    if(error_code.equalsIgnoreCase("0000")){
                        JSONObject response_data = body_data.getJSONObject("response_data");
                        final String psNumber = response_data.getString("psNumber");
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_tab_domestic));
                        alert.setMessage(getResources().getString(R.string.lbl_transfer_success_domestic) + " " + message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Fragment newFragment = null;
                                newFragment = new FragmentTransferDomesticReport();
                                Bundle args = new Bundle();
                                String message = "Success";
                                // String psNumber = "377DA7ASD66238ASD";
                                args.putString("source_account_name", source_account_name);
                                args.putString("source_account_no", source_account_no);
                                args.putString("source_bank_name", source_bank_name);
                                args.putString("source_bank_code", source_bank_code);
                                args.putString("dest_account_no", dest_account_no);
                                args.putString("dest_bank_code", dest_bank_code);
                                args.putString("dest_account_name", dest_account_name);
                                args.putString("totalAmount", amount);
                                args.putString("memo", memo);
                                args.putString("email", dest_email);
                                args.putString("paymentdate", paymentdate);
                                args.putString("error_message", message);
                                args.putString("ref", psNumber);
                                newFragment.setArguments(args);
                                switchFragment(newFragment);

                            }
                        });
                        alert.show();

                    } else if(error_code.equalsIgnoreCase("0404")){

                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_alert_sessionexpire_title));
                        alert.setMessage(message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Session session;
                                session = new Session(getActivity());
                                session.signOut();
                            }
                        });
                        alert.show();

                    }else if(error_code.equals("0003")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_tab_inhouse));
                        alert.setMessage(getResources().getString(R.string.lbl_transfer_success_inhouse) + " " + message);
                        alert.setPositiveButton("OK", null);
                        alert.show();

                    }else if(error_code.equals("X001")) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_tab_inhouse));
                        alert.setMessage(getResources().getString(R.string.lbl_transfer_success_inhouse) + " " + message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Session session;
                                session = new Session(getActivity());
                                session.signOut();
                            }
                        });
                        alert.show();

                    }else{
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                        alert.setTitle(getResources().getString(R.string.lbl_tab_domestic));
                        alert.setMessage(getResources().getString(R.string.lbl_transfer_success_domestic)+" "+ message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Fragment newFragment = null;
                                newFragment = new FragmentMainTransferDomestic();
                                switchFragment(newFragment);
                            }
                        });
                        alert.show();
                        return;
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                if (pDialog != null) {
                    pDialog.dismiss();
                }
                if (throwable.getMessage().equalsIgnoreCase("no peer certificate")) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                } else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setMessage(getResources().getString(R.string.SSLhandler_dialog_message) + throwable.toString());
                    alert.setPositiveButton("OK", null);
                    alert.show();
                }

                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                alert.setTitle(getResources().getString(R.string.lbl_tab_inhouse));
                alert.setMessage(getResources().getString(R.string.lbl_transfer_exception));
                alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Fragment newFragment = null;
                        newFragment = new FragmentMainTransfer();
                        switchFragment(newFragment);
                    }
                });
                alert.show();
            }

        });
    }

    public static byte[] encodeUrlSafe(byte[] data) {
        byte[] encode = Base64.encodeBase64(data);
        for (int i = 0; i < encode.length; i++) {
            if (encode[i] == '+') {
                encode[i] = '-';
            } else if (encode[i] == '=') {
                encode[i] = '_';
            } else if (encode[i] == '/') {
                encode[i] = '~';
            }
        }
        return encode;
    }


    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);

        getSherlockActivity().getActionBar().removeAllTabs();
        getSherlockActivity().getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
    }
}
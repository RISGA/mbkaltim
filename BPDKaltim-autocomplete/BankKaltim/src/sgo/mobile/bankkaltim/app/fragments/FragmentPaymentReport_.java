package sgo.mobile.bankkaltim.app.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.bankkaltim.R;
import sgo.mobile.bankkaltim.app.activities.MainActivity;
import sgo.mobile.bankkaltim.app.adapter.PaymentReportAdapter;
import sgo.mobile.bankkaltim.conf.AplConstants;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by SGO-Mobile-DEV on 8/19/2015.
 */
public class FragmentPaymentReport_ extends SherlockFragment {
    String menu_biller_code,source_account_no,source_account_name,amount,order_id,biller_code,biller_name,catalog_code;
    ListView listData;
    TextView label_header;
    JSONObject dataJSON =null;
    String data;
    EditText txtOTP;
    Button btnDone;
    public static ArrayList<String> name_arr = new ArrayList<String>();
    public static ArrayList<String> value_arr = new ArrayList<String>();
    PaymentReportAdapter paymentReportAdapter;
    boolean networks;
    private ProgressDialog pDialog;
    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");//dd/MM/yyyy
    Date now = new Date();
    String dateTime = sdfDate.format(now);
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payment_report_, container, false);


        listData   = (ListView) view.findViewById(R.id.listData);

        btnDone =(Button)view.findViewById(R.id.btnDone);
        btnDone.setVisibility(View.GONE);
        label_header = (TextView)view.findViewById(R.id.label_header);

        Bundle bundle = this.getArguments();
        paymentReportAdapter = new PaymentReportAdapter(getActivity());
       /* menu_biller_code = bundle.getString("menu_biller_code");
        source_account_name = bundle.getString("source_account_name");
        source_account_no = bundle.getString("source_account_no");
        order_id=bundle.getString("order_id");
        amount=bundle.getString("amount");
        biller_code=bundle.getString("biller_code");
        biller_name=bundle.getString("biller_name");
        catalog_code=bundle.getString("catalog_code");*/
        data=bundle.getString("data");



        if (menu_biller_code.equals(AplConstants.billTax)){
            label_header.setText(getResources().getString(R.string.lbl_payment_tax_title));
        }else if (menu_biller_code.equals(AplConstants.billITicket)){
            label_header.setText(getResources().getString(R.string.lbl_payment_ticket_title));
        }else if (menu_biller_code.equals(AplConstants.billCableTV)){
            label_header.setText(getResources().getString(R.string.lbl_payment_tv_title));
        }


        clearData();
        try {
            dataJSON = new JSONObject(data);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        for(int i = 0; i<dataJSON.names().length(); i++){

            try {


                name_arr.add(dataJSON.names().getString(i));
                value_arr.add(String.valueOf(dataJSON.get(dataJSON.names().getString(i))));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if(name_arr.size() >0){
            listData.setVisibility(0);
            // tbl_otp.setVisibility(View.VISIBLE);
            listData.setAdapter(paymentReportAdapter);
        }else{

        }


        btnDone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {



            }
        });

        return view;
    }


    void clearData(){
        //id.clear();
        name_arr.clear();
        value_arr.clear();
    }






    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
        }

    };

    private void switchFragment(android.support.v4.app.Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);

        getSherlockActivity().getActionBar().removeAllTabs();
        getSherlockActivity().getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
    }

    public static byte[] encodeUrlSafe(byte[] data) {
        byte[] encode = Base64.encodeBase64(data);
        for (int i = 0; i < encode.length; i++) {
            if (encode[i] == '+') {
                encode[i] = '-';
            } else if (encode[i] == '=') {
                encode[i] = '_';
            } else if (encode[i] == '/') {
                encode[i] = '~';
            }
        }
        return encode;
    }



}
package sgo.mobile.bankkaltim.app.beans;

public class BankDomesticBean {
    public String id = "";
    public String code = "";
    public String name = "";
    public String clearing = "";
    public String city = "";

    public BankDomesticBean(String id, String code, String name, String clearing, String city) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.clearing = clearing;
        this.city = city;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClearing() {
        return clearing;
    }

    public void setClearing(String clearing) {
        this.clearing = clearing;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}

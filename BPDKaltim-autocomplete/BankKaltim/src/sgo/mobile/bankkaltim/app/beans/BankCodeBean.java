package sgo.mobile.bankkaltim.app.beans;

/**
 * Created by User on 22/08/2014.
 */
public class BankCodeBean {

    public String bankCode = "";
    public String bankName = "";

    public BankCodeBean(String bankCode,String bankName) {
        this.bankCode = bankCode;
        this.bankName = bankName;

    }
    public String getBank_code() {
        return bankCode;
    }

    public void setBank_code(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBank_name() {
        return bankName;
    }

    public void setBank_name(String bankName) {
        this.bankName = bankName;
    }


    @Override
    public String toString() {
        return  bankName;
    }


}

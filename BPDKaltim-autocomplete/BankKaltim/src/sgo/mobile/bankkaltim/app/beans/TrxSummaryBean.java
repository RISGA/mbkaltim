package sgo.mobile.bankkaltim.app.beans;

public class TrxSummaryBean {
    public String id = "";
    public String total = "";
    public String source_account="";
    public String source_account_name="";
    public String ccy="";

    public TrxSummaryBean(String id, String total, String source_account, String source_account_name, String ccy) {
        this.id = id;
        this.total = total;
        this.source_account = source_account;
        this.source_account_name = source_account_name;
        this.ccy = ccy;
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getSource_account() {
        return source_account;
    }

    public void setSource_account(String source_account) {
        this.source_account = source_account;
    }

    public String getSource_account_name() {
        return source_account_name;
    }

    public void setSource_account_name(String source_account_name) {
        this.source_account_name = source_account_name;
    }
}

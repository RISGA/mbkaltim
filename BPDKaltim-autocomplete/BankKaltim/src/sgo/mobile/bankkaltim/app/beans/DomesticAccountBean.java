package sgo.mobile.bankkaltim.app.beans;

/**
 * Created by User on 13/08/2014.
 */
public class DomesticAccountBean {
    public String bank_code = "";
    public String bank_name = "";
    public String account_no = "";
    public String account_name = "";
    public String ccy_id = "";
    public String email = "";


    public DomesticAccountBean(String bank_code, String bank_name, String account_no, String account_name, String ccy_id, String email) {
        this.bank_code = bank_code;
        this.bank_name = bank_name;
        this.account_no = account_no;
        this.account_name = account_name;
        this.ccy_id = ccy_id;
        this.email = email;
    }

    public String getBank_code() {
        return bank_code;
    }

    public void setBank_code(String bank_code) {
        this.bank_code = bank_code;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getAccount_no() {
        return account_no;
    }

    public void setAccount_no(String account_no) {
        this.account_no = account_no;
    }

    public String getAccount_name() {
        return account_name;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }

    public String getCcy_id() {
        return ccy_id;
    }

    public void setCcy_id(String ccy_id) {
        this.ccy_id = ccy_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return  account_no + " " + "(" +  account_name + ")";
    }
}


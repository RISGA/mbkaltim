package sgo.mobile.bankkaltim.app.beans;

public class BenefBean {
    public String benef_no = "";
    public String benef_name = "";
    public String bank_code = "";
    public String bank_name = "";
    public String clearing = "";
    public String city = "";

    public BenefBean(String benef_no, String benef_name, String bank_code, String bank_name, String clearing, String city) {
        this.benef_no = benef_no;
        this.benef_name = benef_name;
        this.bank_code = bank_code;
        this.bank_name = bank_name;
        this.clearing = clearing;
        this.city = city;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getBenef_no() {
        return benef_no;
    }

    public void setBenef_no(String bener_no) {
        this.benef_no = bener_no;
    }

    public String getBenef_name() {
        return benef_name;
    }

    public void setBenef_name(String benef_name) {
        this.benef_name = benef_name;
    }

    public String getBank_code() {
        return bank_code;
    }

    public void setBank_code(String bank_code) {
        this.bank_code = bank_code;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getClearing() {
        return clearing;
    }

    public void setClearing(String clearing) {
        this.clearing = clearing;
    }

    @Override
    public String toString() {
        return "BenefBean{" +
                "benef_name='" + benef_name + '\'' +
                '}';
    }

}
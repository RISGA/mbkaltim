package sgo.mobile.bankkaltim.app.beans;

public class TrxInquiryBean {
    public String id = "";
    public String date = "";
    public String description = "";
    public String debet = "";
    public String credit = "";
    public String source_account="";
    public String source_name="";
    public String ccy="";

    public TrxInquiryBean(String id, String date, String description, String debet, String credit, String source_account, String source_name, String ccy) {
        this.id = id;
        this.date = date;
        this.description = description;
        this.debet = debet;
        this.credit = credit;
        this.source_account = source_account;
        this.source_name = source_name;
        this.ccy = ccy;
    }

    public String getSource_name() {
        return source_name;
    }

    public void setSource_name(String source_name) {
        this.source_name = source_name;
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public String getSource_account() {
        return source_account;
    }

    public void setSource_account(String source_account) {
        this.source_account = source_account;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDebet() {
        return debet;
    }

    public void setDebet(String debet) {
        this.debet = debet;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }
}

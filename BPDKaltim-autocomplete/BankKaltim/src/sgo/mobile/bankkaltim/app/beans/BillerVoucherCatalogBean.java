package sgo.mobile.bankkaltim.app.beans;

import sgo.mobile.bankkaltim.frameworks.math.FormatCurrency;

/**
 * Created by User on 09/10/2014.
 */
public class BillerVoucherCatalogBean {

    public String billerId ="";
    public String billerCode ="";
    public String billerName ="";
    public String billerInquiry ="";
    public String productCode ="";
    public String productName ="";
    public String productAmount ="";




    public BillerVoucherCatalogBean(String billerId, String billerCode, String billerName, String billerInquiry, String productCode, String productName, String productAmount) {
        this.billerId = billerId;
        this.billerCode = billerCode;
        this.billerName = billerName;
        this.billerInquiry = billerInquiry;
        this.productCode = productCode;
        this.productName = productName;
        this.productAmount = productAmount;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getBillerId() {
        return billerId;
    }

    public void setBillerId(String billerId) {
        this.billerId = billerId;
    }

    public String getBillerCode() {
        return billerCode;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public void setBillerCode(String billerCode) {
        this.billerCode = billerCode;
    }

    public String getBillerName() {
        return billerName;
    }

    public void setBillerName(String billerName) {
        this.billerName = billerName;
    }

    public String getProductAmount() {
        return productAmount;
    }

    public void setProductAmount(String productAmount) {
        this.productAmount = productAmount;
    }

    public String getBillerInquiry() {
        return billerInquiry;
    }

    public void setBillerInquiry(String billerInquiry) {
        this.billerInquiry = billerInquiry;
    }

    @Override
    public String toString() {
                if(billerId.equalsIgnoreCase("1004")){
                    return FormatCurrency.CurrencyIDR(productAmount);
                }else{
                    return getProductName();
                }



    }


}

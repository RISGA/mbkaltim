package sgo.mobile.bankkaltim.app.beans;

public class AccountBean {
    public String source_acc_no = "";
    public String source_acc_name = "";

    public AccountBean(String source_acc_no, String source_acc_name) {
        this.source_acc_no = source_acc_no;
        this.source_acc_name = source_acc_name;
    }

    public String getSource_acc_no() {
        return source_acc_no;
    }

    public void setSource_acc_no(String source_acc_no) {
        this.source_acc_no = source_acc_no;
    }

    public String getSource_acc_name() {
        return source_acc_name;
    }

    public void setSource_acc_name(String source_acc_name) {
        this.source_acc_name = source_acc_name;
    }

    @Override
    public String toString() {
        return "AccountBean{" +
                "source_acc_name='" + source_acc_name + '\'' +
                '}';
    }
}

package sgo.mobile.bankkaltim.app.beans;

public class BeneficiaryBean {
    public String id = "";
    public String name = "";

    public BeneficiaryBean( String _id, String _name )
    {
        id = _id;
        name = _name;
    }

    public String toString()
    {
        return( name  );
    }


}
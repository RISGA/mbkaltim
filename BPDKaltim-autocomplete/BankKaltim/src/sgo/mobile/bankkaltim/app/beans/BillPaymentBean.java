package sgo.mobile.bankkaltim.app.beans;

/**
 * Created by SGO-Mobile-DEV on 8/12/2015.
 */
public class BillPaymentBean {

    public String billerId ="";
    public String billerCode ="";
    public String billerName ="";
    public String productCode ="";
    public String productName ="";

    public BillPaymentBean(String billerId, String billerCode, String billerName, String productCode, String productName) {
        this.billerId = billerId;
        this.billerCode = billerCode;
        this.billerName = billerName;
        this.productCode = productCode;
        this.productName = productName;
    }

    public String getBillerId() {
        return billerId;
    }

    public void setBillerId(String billerId) {
        this.billerId = billerId;
    }

    public String getBillerCode() {
        return billerCode;
    }

    public void setBillerCode(String billerCode) {
        this.billerCode = billerCode;
    }

    public String getBillerName() {
        return billerName;
    }

    public void setBillerName(String billerName) {
        this.billerName = billerName;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    @Override
    public String toString() {
        return  billerName ;
    }
}

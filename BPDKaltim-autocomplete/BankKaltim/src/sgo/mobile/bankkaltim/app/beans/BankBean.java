package sgo.mobile.bankkaltim.app.beans;

public class BankBean {
    public String id = "";
    public String code = "";
    public String name = "";
    public String clearing = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClearing() {
        return clearing;
    }

    public void setClearing(String clearing) {
        this.clearing = clearing;
    }

    public BankBean(String id, String code, String name, String clearing) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.clearing = clearing;
    }

    public BankBean( String _id, String _name )
    {
        id = _id;
        name = _name;
    }

    public String toString()
    {
        return( name  );
    }
}
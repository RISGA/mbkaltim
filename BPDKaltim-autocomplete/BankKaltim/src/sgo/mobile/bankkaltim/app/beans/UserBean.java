package sgo.mobile.bankkaltim.app.beans;

public class UserBean {
    public int id = 0;
    public String name = "";

    public UserBean( int _id, String _name )
    {
        id = _id;
        name = _name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString()
    {
        return( name  );
    }
}
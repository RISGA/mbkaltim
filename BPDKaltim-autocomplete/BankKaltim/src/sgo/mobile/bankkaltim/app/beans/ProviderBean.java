package sgo.mobile.bankkaltim.app.beans;

import android.graphics.Bitmap;

/**
 * Created by SGO-Mobile-DEV on 4/6/2015.
 */
public class ProviderBean {
    private Bitmap providerIcon;


    public ProviderBean(Bitmap providerIcon) {
        this.providerIcon = providerIcon;
    }

    public Bitmap getProviderIcon() {
        return providerIcon;
    }

    public void setProviderIcon(Bitmap providerIcon) {
        this.providerIcon = providerIcon;
    }
}

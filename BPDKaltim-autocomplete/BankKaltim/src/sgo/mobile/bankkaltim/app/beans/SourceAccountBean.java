package sgo.mobile.bankkaltim.app.beans;

public class SourceAccountBean {
    public String account_no = "";
    public String account_name = "";
    public String ccy_id = "";

    public SourceAccountBean(String account_no, String account_name, String ccy_id) {
        this.account_no = account_no;
        this.account_name = account_name;
        this.ccy_id = ccy_id;
    }

    public String getAccount_no() {
        return account_no;
    }

    public void setAccount_no(String account_no) {
        this.account_no = account_no;
    }

    public String getAccount_name() {
        return account_name;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }

    public String getCcy_id() {
        return ccy_id;
    }

    public void setCcy_id(String ccy_id) {
        this.ccy_id = ccy_id;
    }

    @Override
    public String toString() {
        return account_no + " " + "(" +  account_name+ ")";
    }




}

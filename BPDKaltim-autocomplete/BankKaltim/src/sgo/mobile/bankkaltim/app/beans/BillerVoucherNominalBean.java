package sgo.mobile.bankkaltim.app.beans;

import sgo.mobile.bankkaltim.frameworks.math.FormatCurrency;

/**
 * Created by User on 09/10/2014.
 */
public class BillerVoucherNominalBean {
    public String catalogCode ="";
    public String catalogNominal ="";

    public BillerVoucherNominalBean(String catalogCode, String catalogNominal) {
        this.catalogCode = catalogCode;
        this.catalogNominal = catalogNominal;
    }

    public String getCatalogCode() {
        return catalogCode;
    }

    public void setCatalogCode(String catalogCode) {
        this.catalogCode = catalogCode;
    }

    public String getCatalogNominal() {
        return catalogNominal;
    }

    public void setCatalogNominal(String catalogNominal) {
        this.catalogNominal = catalogNominal;
    }

    @Override
    public String toString() {
        return FormatCurrency.getRupiahFormat(catalogNominal);
    }
}

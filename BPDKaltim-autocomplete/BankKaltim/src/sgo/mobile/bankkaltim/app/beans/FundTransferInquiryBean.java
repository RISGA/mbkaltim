package sgo.mobile.bankkaltim.app.beans;

/**
 * Created by User on 27/08/2014.
 */
public class FundTransferInquiryBean {
   public String destAcctName="";
    public String destBankCode="";

    public String getDestAcctName() {
        return destAcctName;
    }

    public FundTransferInquiryBean(String destAcctName, String destBankCode) {
        this.destAcctName = destAcctName;
        this.destBankCode = destBankCode;
    }

    public void setDestAcctName(String destAcctName) {
        this.destAcctName = destAcctName;
    }

    public String getDestBankCode() {
        return destBankCode;
    }

    public void setDestBankCode(String destBankCode) {
        this.destBankCode = destBankCode;
    }


    @Override
    public String toString() {
        return "FundTransferInquiryBean{" +
                "destAcctName='" + destAcctName + '\'' +
                ", destBankCode='" + destBankCode + '\'' +
                '}';
    }
}

package sgo.mobile.bankkaltim.app.beans;

/**
 * Created by SGO-Mobile-DEV on 4/1/2015.
 */
public class FragmentVoucherPhoneBean {
    public String billerId ="";
    public String billerCode = "";
    public String billerName = "";
    public String billerInquiry = "";
    public String productCode = "";
    public String productName = "";
    public String productAmount= "";

    public String getBillerId() {
        return billerId;
    }

    public void setBillerId(String billerId) {
        this.billerId = billerId;
    }

    public String getBillerCode() {
        return billerCode;
    }

    public void setBillerCode(String billerCode) {
        this.billerCode = billerCode;
    }

    public String getBillerName() {
        return billerName;
    }

    public void setBillerName(String billerName) {
        this.billerName = billerName;
    }

    public String getBillerInquiry() {
        return billerInquiry;
    }

    public void setBillerInquiry(String billerInquiry) {
        this.billerInquiry = billerInquiry;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductAmount() {
        return productAmount;
    }

    public void setProductAmount(String productAmount) {
        this.productAmount = productAmount;
    }
}

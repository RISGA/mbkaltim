package sgo.mobile.bankkaltim.app.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.util.SQLiteUtils;

/**
 * Created by MOBILEDEV on 5/3/2016.
 */
@Table(name="MenuSettingModel")
public class MenuSettingModel extends Model {

    @Column(name="menu_setting_id", index = true)
    public String menu_setting_id;

    @Column(name="menu_setting_code", index = true)
    public String menu_setting_code;

    @Column(name="menu_setting_name", index = true)
    public String menu_setting_name;

    @Column(name="menu_setting_description", index = true)
    public String menu_setting_description;

    public MenuSettingModel(){
        super();
    }

    public MenuSettingModel(String menu_setting_id, String menu_setting_code, String menu_setting_name, String menu_setting_description) {
        super();
        this.menu_setting_id = menu_setting_id;
        this.menu_setting_code = menu_setting_code;
        this.menu_setting_name = menu_setting_name;
        this.menu_setting_description = menu_setting_description;
    }

    public String getMenu_setting_id() {
        return menu_setting_id;
    }

    public void setMenu_setting_id(String menu_setting_id) {
        this.menu_setting_id = menu_setting_id;
    }

    public String getMenu_setting_code() {
        return menu_setting_code;
    }

    public void setMenu_setting_code(String menu_setting_code) {
        this.menu_setting_code = menu_setting_code;
    }

    public String getMenu_setting_name() {
        return menu_setting_name;
    }

    public void setMenu_setting_name(String menu_setting_name) {
        this.menu_setting_name = menu_setting_name;
    }

    public String getMenu_setting_description() {
        return menu_setting_description;
    }

    public void setMenu_setting_description(String menu_setting_description) {
        this.menu_setting_description = menu_setting_description;
    }

    public static void deleteAll(){
        new Delete().from(MenuSettingModel.class).execute();
        SQLiteUtils.execSql("DELETE FROM SQLITE_SEQUENCE WHERE name ='MenuSettingModel';");
    }

    @Override
    public String toString() {
        return menu_setting_name;
    }
}

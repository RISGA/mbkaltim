package sgo.mobile.bankkaltim.app.models.purchase;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.util.SQLiteUtils;

/**
 * Created by MOBILEDEV on 3/31/2016.
 */
@Table(name="PurchaseVoucherPhoneModel")
public class PurchaseVoucherPhoneModel extends Model {


    @Column(name = "biilerId", index = true)
    public String bilerId;

    @Column(name="billerCode", index = true)
    public String billerCode;

    @Column(name="billerName", index = true)
    public String billerName;

    @Column(name="billerInquiry", index = true)
    public String billerInquiry;

    @Column(name="productCode", index = true)
    public String productCode;

    @Column(name="productName", index = true)
    public String productName;

    @Column(name="productAmount", index = true)
    public String productAmount;

    public PurchaseVoucherPhoneModel(){
        super();
    }


    public PurchaseVoucherPhoneModel(String bilerId, String billerCode, String billerName, String billerInquiry, String productCode, String productName, String productAmount) {
        super();
        this.bilerId = bilerId;
        this.billerCode = billerCode;
        this.billerName = billerName;
        this.billerInquiry = billerInquiry;
        this.productCode = productCode;
        this.productName = productName;
        this.productAmount = productAmount;
    }

    public String getBilerId() {
        return bilerId;
    }

    public void setBilerId(String bilerId) {
        this.bilerId = bilerId;
    }

    public String getBillerCode() {
        return billerCode;
    }

    public void setBillerCode(String billerCode) {
        this.billerCode = billerCode;
    }

    public String getBillerName() {
        return billerName;
    }

    public void setBillerName(String billerName) {
        this.billerName = billerName;
    }

    public String getBillerInquiry() {
        return billerInquiry;
    }

    public void setBillerInquiry(String billerInquiry) {
        this.billerInquiry = billerInquiry;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductAmount() {
        return productAmount;
    }

    public void setProductAmount(String productAmount) {
        this.productAmount = productAmount;
    }

    public static void deleteAll(){
        new Delete().from(PurchaseVoucherPhoneModel.class).execute();
        SQLiteUtils.execSql("DELETE FROM SQLITE_SEQUENCE WHERE name ='PurchaseVoucherPhoneModel';");
    }

    @Override
    public String toString() {
        return productAmount;
    }
}
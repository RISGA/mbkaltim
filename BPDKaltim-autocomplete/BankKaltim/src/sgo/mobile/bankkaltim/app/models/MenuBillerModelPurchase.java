package sgo.mobile.bankkaltim.app.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.util.SQLiteUtils;

/**
 * Created by MOBILEDEV on 3/30/2016.
 */
@Table(name = "MenuBillerModelPurchase")
public class MenuBillerModelPurchase extends Model {

    @Column(name="billerId", index = true)
    public String billerId;

    @Column(name="billerName", index = true)
    public String billerName;


    public MenuBillerModelPurchase(){
        super();
    }

    public MenuBillerModelPurchase(String billerId, String billerName) {
        super();
        this.billerId = billerId;
        this.billerName = billerName;
    }

    public String getBillerId() {
        return billerId;
    }

    public void setBillerId(String billerId) {
        this.billerId = billerId;
    }

    public String getBillerName() {
        return billerName;
    }

    public void setBillerName(String billerName) {
        this.billerName = billerName;
    }

    @Override
    public String toString() {
        return billerName;
    }

    public static void deleteAll(){
        new Delete().from(MenuBillerModelPurchase.class).execute();
        SQLiteUtils.execSql("DELETE FROM SQLITE_SEQUENCE WHERE name ='MenuBillerModelPurchase';");
    }

}


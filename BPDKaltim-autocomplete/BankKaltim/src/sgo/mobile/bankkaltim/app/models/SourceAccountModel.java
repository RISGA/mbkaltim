package sgo.mobile.bankkaltim.app.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.activeandroid.util.SQLiteUtils;

import java.util.List;

/**
 * Created by MOBILEDEV on 3/29/2016.
 */
@Table(name = "SourceAccountModel")
public class SourceAccountModel extends Model {

    @Column(name = "account_no", index = true, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    public String account_no;

    @Column(name="account_name", index = true)
    public String account_name;

    @Column(name = "ccy_id", index = true)
    public String ccy_id;


    public SourceAccountModel(){
        super();
    }

    public SourceAccountModel(String account_no, String account_name, String ccy_id) {
        super();
        this.account_no = account_no;
        this.account_name = account_name;
        this.ccy_id = ccy_id;
    }

    public String getAccount_no() {
        return account_no;
    }

    public void setAccount_no(String account_no) {
        this.account_no = account_no;
    }

    public String getAccount_name() {
        return account_name;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }

    public String getCcy_id() {
        return ccy_id;
    }

    public void setCcy_id(String ccy_id) {
        this.ccy_id = ccy_id;
    }

    public static List<SourceAccountModel> getAll() {
        // This is how you execute a query
        return new Select()
                .all()
                .from(SourceAccountModel.class).orderBy("account_no")
                .execute();
    }

    public static void deleteAll(){
        new Delete().from(SourceAccountModel.class).execute();
        SQLiteUtils.execSql("DELETE FROM SQLITE_SEQUENCE WHERE name ='SourceAccountModel';");
    }

    @Override
    public String toString() {
        return account_no + " " + "(" + account_name + ")";
    }
}

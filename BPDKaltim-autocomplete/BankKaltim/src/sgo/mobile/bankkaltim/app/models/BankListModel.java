package sgo.mobile.bankkaltim.app.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.util.SQLiteUtils;

/**
 * Created by MOBILEDEV on 3/29/2016.
 */
@Table(name="BankListModel")
public class BankListModel extends Model {

    @Column(name="bankCode", index = true)
    public  String bankCode;

    @Column(name="bankName", index = true)
    public String bankName;

    public BankListModel(){
        super();
    }

    public BankListModel(String bankCode, String bankName) {
        super();
        this.bankCode = bankCode;
        this.bankName = bankName;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public static void deleteAll(){
        new Delete().from(BankListModel.class).execute();
        SQLiteUtils.execSql("DELETE FROM SQLITE_SEQUENCE WHERE name ='BankListModel';");
    }


    @Override
    public String toString() {
        return bankName;
    }
}


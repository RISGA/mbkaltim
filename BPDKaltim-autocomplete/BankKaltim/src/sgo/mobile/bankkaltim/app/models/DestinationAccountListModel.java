package sgo.mobile.bankkaltim.app.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.activeandroid.util.SQLiteUtils;

import java.util.List;

/**
 * Created by MOBILEDEV on 3/29/2016.
 */
@Table(name = "DestinationAccountListModel")
public class DestinationAccountListModel extends Model {

    @Column(name = "dest_bank_code",index = true)
    public String dest_bank_code;

    @Column(name = "dest_bank_name",index = true)
    public String dest_bank_name;

    @Column(name = "dest_account_no",index = true)
    public String dest_account_no;

    @Column(name = "dest_account_name",index = true)
    public String dest_account_name;

    @Column(name = "dest_ccy_id",index = true)
    public String dest_ccy_id;

    @Column(name = "dest_email",index = true)
    public String dest_email;

    public DestinationAccountListModel(){
        super();
    }

    public DestinationAccountListModel(String dest_bank_code, String dest_bank_name, String dest_account_no, String dest_account_name, String dest_ccy_id, String dest_email) {
        super();
        this.dest_bank_code = dest_bank_code;
        this.dest_bank_name = dest_bank_name;
        this.dest_account_no = dest_account_no;
        this.dest_account_name = dest_account_name;
        this.dest_ccy_id = dest_ccy_id;
        this.dest_email = dest_email;
    }

    public String getDest_bank_code() {
        return dest_bank_code;
    }

    public void setDest_bank_code(String dest_bank_code) {
        this.dest_bank_code = dest_bank_code;
    }

    public String getDest_bank_name() {
        return dest_bank_name;
    }

    public void setDest_bank_name(String dest_bank_name) {
        this.dest_bank_name = dest_bank_name;
    }

    public String getDest_account_no() {
        return dest_account_no;
    }

    public void setDest_account_no(String dest_account_no) {
        this.dest_account_no = dest_account_no;
    }

    public String getDest_account_name() {
        return dest_account_name;
    }

    public void setDest_account_name(String dest_account_name) {
        this.dest_account_name = dest_account_name;
    }

    public String getDest_ccy_id() {
        return dest_ccy_id;
    }

    public void setDest_ccy_id(String dest_ccy_id) {
        this.dest_ccy_id = dest_ccy_id;
    }

    public String getDest_email() {
        return dest_email;
    }

    public void setDest_email(String dest_email) {
        this.dest_email = dest_email;
    }

    public static List<DestinationAccountListModel> getAll() {
        // This is how you execute a query
        return new Select()
                .all()
                .from(DestinationAccountListModel.class).orderBy("dest_account_no")
                .execute();
    }

    public static void deleteAll(){
        new Delete().from(DestinationAccountListModel.class).execute();
        SQLiteUtils.execSql("DELETE FROM SQLITE_SEQUENCE WHERE name ='DestinationAccountListModel';");
    }


    @Override
    public String toString() {
        return dest_account_no + " " + "(" +  dest_account_name + ")";
    }
}

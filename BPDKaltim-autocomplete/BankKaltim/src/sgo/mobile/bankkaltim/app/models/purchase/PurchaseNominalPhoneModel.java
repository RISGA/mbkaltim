package sgo.mobile.bankkaltim.app.models.purchase;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.util.SQLiteUtils;

/**
 * Created by MOBILEDEV on 3/31/2016.
 */

@Table(name="PurchaseNominalPhoneModel")
public class PurchaseNominalPhoneModel extends Model {

    @Column(name = "catalogCode", index = true)
    public String catalogCode;

    @Column(name = "catalogNominal", index = true)
    public String catalogNominal;

    public PurchaseNominalPhoneModel(){
        super();
    }

    public PurchaseNominalPhoneModel(String catalogCode, String catalogNominal) {
        super();
        this.catalogCode = catalogCode;
        this.catalogNominal = catalogNominal;
    }

    public String getCatalogCode() {
        return catalogCode;
    }

    public void setCatalogCode(String catalogCode) {
        this.catalogCode = catalogCode;
    }

    public String getCatalogNominal() {
        return catalogNominal;
    }

    public void setCatalogNominal(String catalogNominal) {
        this.catalogNominal = catalogNominal;
    }

    public static void deleteAll(){
        new Delete().from(PurchaseNominalPhoneModel.class).execute();
        SQLiteUtils.execSql("DELETE FROM SQLITE_SEQUENCE WHERE name ='PurchaseNominalPhoneModel';");
    }

    @Override
    public String toString() {
        return catalogNominal;
    }
}
